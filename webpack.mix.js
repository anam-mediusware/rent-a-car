const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (process.env.NODE_ENV === 'production') {
    //
} else {
    mix.sourceMaps().version();
    if (process.env.MIX_BROWSERSYNC_PROXY) {
        mix.browserSync({
            proxy: {
                target: process.env.MIX_BROWSERSYNC_PROXY,
                ws: true
            },
            port: 8080
        });
    }
}

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .js([
        'resources/views/web/home/js/index.js',
    ], 'public/js/pages/home-page.js');

// User End JS
const mixUser = mix;
mixUser.js('resources/js/user/app.js', 'public/web-assets/js')
    .js('resources/js/user/custom.js', 'public/web-assets/js')
    .sass('resources/sass/user/app.scss', 'public/web-assets/css')
    .js([
        'resources/views/user/booking/js/index.js',
    ], 'public/js/pages/user/booking.index.js');

// Admin End JS
const mixAdmin = mix;
mixAdmin.js('resources/js/admin/app.js', 'public/admin-assets/js');

/*mix.autoload({
    'jquery': ['$', 'window.jQuery', 'jQuery'],
});
mixUser.extract([
    'vue',
    'jquery',
    'bootstrap',
    'bxslider/dist/jquery.bxslider',
]);*/
