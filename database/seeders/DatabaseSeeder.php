<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $time_start = microtime(true);
        // User::factory(10)->create();
        $this->call(UsersTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(DrivingCities::class);
        $this->call(VehicleTypes::class);
        $this->call(CarModelSeeder::class);
        $this->call(DriverPayoutMethods::class);
        $this->call(ServiceCategoriesSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(AirportSeeder::class);
        $this->call(UpazilaBoundariesTableSeeder::class);

        // Used Factory
        $this->call(MerchantsTableSeeder::class);
        $this->call(DriversTableSeeder::class);
        $this->call(ServiceSettingsSeeder::class);
        $this->call(BiddingTripsTableSeeder::class);
        $this->call(BidsTableSeeder::class);
        $this->call(BookingsTableSeeder::class);
        $this->call(DriverRatingsTableSeeder::class);

        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "\nTotal Execution Time: {$time} Seconds\n";
    }
}
