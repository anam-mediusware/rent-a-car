<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
	            'category_id' => 1,
	            'service_name' => 'Body Rent',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'category_id' => 1,
	            'service_name' => 'Half Day',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
        	],
            [
	            'category_id' => 1,
	            'service_name' => 'Full Day',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
        	],
            [
	            'category_id' => 2,
	            'service_name' => 'Body Rent',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'category_id' => 2,
	            'service_name' => 'One Way',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'category_id' => 2,
	            'service_name' => 'Round Trip',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'category_id' => 2,
	            'service_name' => 'Cheap Rate Trips',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'category_id' => 3,
	            'service_name' => 'City to Airport',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'category_id' => 3,
	            'service_name' => 'Airport to City',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
			],
            [
	            'category_id' => 4,
	            'service_name' => 'Half Day',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'category_id' => 4,
	            'service_name' => 'Full Day',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
			],
            [
	            'category_id' => 5,
	            'service_name' => 'Corporate Car',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'category_id' => 6,
	            'service_name' => 'Monthly Car',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ]
        ]);
    }
}
