<?php

namespace Database\Seeders;

use App\Models\Airport;
use Illuminate\Database\Seeder;

class AirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = now();
        $data = [
            [
                'city_key' => 'dhaka',
                'city_title' => 'Dhaka',
                'airport' => 'Hazrat Shahjalal International Airport',
                'geo' => json_encode(['lat' => 23.842778, 'lon' => 90.400556]),
                'Dist_ID' => 26,
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'city_key' => 'chattogram',
                'city_title' => 'Chattogram',
                'airport' => 'Shah Amanat International Airport',
                'geo' => json_encode(['lat' => 22.249722, 'lon' => 91.813333]),
                'Dist_ID' => 15,
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'city_key' => 'sylhet',
                'city_title' => 'Sylhet',
                'airport' => 'Osmani International Airport',
                'geo' => json_encode(['lat' => 24.963333, 'lon' => 91.866944]),
                'Dist_ID' => 91,
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'city_key' => 'coxs bazar',
                'city_title' => 'Cox\'s Bazar',
                'airport' => 'Cox\'s Bazar Airport',
                'geo' => json_encode(['lat' => 21.451944, 'lon' => 91.963889]),
                'Dist_ID' => 22,
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'city_key' => 'rajshahi',
                'city_title' => 'Rajshahi',
                'airport' => 'Shah Makhdum Airport',
                'geo' => json_encode(['lat' => 24.436944, 'lon' => 88.616389]),
                'Dist_ID' => 81,
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'city_key' => 'jashore',
                'city_title' => 'Jashore',
                'airport' => 'Jashore Airport',
                'geo' => json_encode(['lat' => 23.183611, 'lon' => 89.160833]),
                'Dist_ID' => 41,
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'city_key' => 'saidpur',
                'city_title' => 'Saidpur',
                'airport' => 'Saidpur Airport',
                'geo' => json_encode(['lat' => 25.7632, 'lon' => 88.9095]),
                'Dist_ID' => 73,
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'city_key' => 'barishal',
                'city_title' => 'Barishal',
                'airport' => 'Barishal Airport',
                'geo' => json_encode(['lat' => 22.801111, 'lon' => 90.301111]),
                'Dist_ID' => 6,
                'created_at' => $time,
                'updated_at' => $time,
            ],
        ];

        Airport::insert($data);
    }
}
