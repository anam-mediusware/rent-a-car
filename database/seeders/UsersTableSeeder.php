<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ferdous Anam',
            'mobile' => '01738238012',
            'email' => 'ferdous.anam@gmail.com',
            'email_verified_at' => \Carbon\Carbon::now()->format('Y-m-d'),
            'password' => \Illuminate\Support\Facades\Hash::make('123456'),
            'status' => 1,
            'remember_token' => Str::random(60),
        ]);
        User::create([
            'name' => 'Anam',
            'mobile' => '01777888999',
            'email' => 'anam@gmail.com',
            'email_verified_at' => \Carbon\Carbon::now()->format('Y-m-d'),
            'password' => \Illuminate\Support\Facades\Hash::make('123456'),
            'status' => 1,
            'remember_token' => Str::random(60),
        ]);
    }
}
