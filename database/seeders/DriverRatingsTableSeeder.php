<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DriverRatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\DriverRating::factory(100)->bookingSequence()->create();
    }
}
