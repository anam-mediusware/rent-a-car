<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DriverPayoutMethods extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('drivers_payout_methods')->insert([
            [
	            'method_name_english' => 'bkash',
                'method_name_bangla' => 'বিকাস',
                'fields_english' => 'bkash Number',
	            'fields_bangla' => 'বিকাস সংখ্যা',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'method_name_english' => 'rocket',
                'method_name_bangla' => 'রকেট',
                'fields_english' => 'Rocket Number',
	            'fields_bangla' => 'রকেট সংখ্যা',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'method_name_english' => 'bank',
                'method_name_bangla' => 'ব্যাংক',
                'fields_english' => 'Account Name, Account Number, Bank Name, Branch',
	            'fields_bangla' => 'হিসাব নাম, হিসাব সংখ্যা, ব্যাংকের নাম, শাখা',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
        ]);
    }
}
