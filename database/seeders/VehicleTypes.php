<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleTypes extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_types')->insert([
            [
	            'vehicle_type_english' => '4 Seater',
	            'vehicle_type_bangla' => '৪ সিটের',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
                'vehicle_type_english' => '7 Seater',
                'vehicle_type_bangla' => '৭ সিটের',
                'status' => '1',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'vehicle_type_english' => '10 Seater',
                'vehicle_type_bangla' => '১০ সিটের',
                'status' => '1',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
	            'vehicle_type_english' => '14 Seater',
	            'vehicle_type_bangla' => '১৪ সিটের',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'vehicle_type_english' => '21++ Seater',
	            'vehicle_type_bangla' => '২১++ সিটের',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
        ]);
    }
}
