<?php

namespace Database\Seeders;

use App\Models\CarModel;
use Illuminate\Database\Seeder;

class CarModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = now();
        $data = [
            [
                'vehicle_type_id' => 1,
                'car_brand' => 'Toyota',
                'car_model' => 'G Corolla',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'vehicle_type_id' => 1,
                'car_brand' => 'Toyota',
                'car_model' => 'X Corolla',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'vehicle_type_id' => 2,
                'car_brand' => 'Toyota',
                'car_model' => 'Noah X',
                'created_at' => $time,
                'updated_at' => $time,
            ],
            [
                'vehicle_type_id' => 2,
                'car_brand' => 'Toyota',
                'car_model' => 'Noah G',
                'created_at' => $time,
                'updated_at' => $time,
            ],
        ];
        CarModel::insert($data);
    }
}
