<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceCategoriesSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_categories')->insert([
            [
                'serial_no' => 1,
                'category_name' => 'Inside City',
	            'image' => 'inside-city.png',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
                'serial_no' => 2,
	            'category_name' => 'Outside City',
	            'image' => 'outside-city.png',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
        	],
            [
                'serial_no' => 3,
	            'category_name' => 'Airport Transfer',
	            'image' => 'airport-transfer.jpg',
	            'status' => '0',
	            'created_at' => now(),
	            'updated_at' => now(),
			],
            [
                'serial_no' => 5,
	            'category_name' => 'Wedding',
	            'image' => 'wedding.jpg',
	            'status' => '0',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
                'serial_no' => 7,
	            'category_name' => 'Corporate',
	            'image' => 'corporate.jpg',
	            'status' => '0',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
                'serial_no' => 6,
	            'category_name' => 'Monthly Package',
	            'image' => 'monthly.jpg',
	            'status' => '0',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
                'serial_no' => 4,
	            'category_name' => 'Tour Package',
	            'image' => 'tour-package.jpg',
	            'status' => '0',
	            'created_at' => now(),
	            'updated_at' => now(),
            ]
        ]);
    }
}
