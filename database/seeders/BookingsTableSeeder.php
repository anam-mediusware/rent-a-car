<?php

namespace Database\Seeders;

use App\Models\AdditionalPricing;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class BookingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bids = \App\Models\Bid::all();
        $bidSeq = [];
        foreach ($bids as $bid) {
            $bidSeq[]['bid_id'] = $bid->id;
        }
        \App\Models\Booking::factory(100)
            ->state(new Sequence(...$bidSeq))
            ->biddingTrip()
            ->create();

        \App\Models\Booking::factory(100)
            ->fixedTrip()
            ->create()->each(function ($booking) {
                $booking->additionalPricings()->createMany(
                    AdditionalPricing::select(['price_name', 'price', 'price_type'])
                        ->where('service_setting_id', $booking->service_setting_id)
                        ->get()->toArray()
                );
            });
    }
}
