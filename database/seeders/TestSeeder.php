<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::factory()
            ->count(10)
            ->state(new Sequence(
                ['role' => 'X'],
                ['role' => 'Y'],
                ['role' => 'Z'],
            ))->make();

        dump($users->toArray());
    }
}
