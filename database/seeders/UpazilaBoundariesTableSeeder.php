<?php

namespace Database\Seeders;

use App\Models\UpazilaBoundary;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UpazilaBoundariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UpazilaBoundary::query()->truncate();
        DB::connection('bd_geo')->table('upazila_boundaries_shape')->orderBy('id')->chunk(50, function ($data) {
            $upazila_data = [];
            foreach ($data as $upazila) {
                $upazila_data[] = [
                    'geometry_name' => $upazila->geometry_name,
                    'Dist_ID' => $upazila->Dist_ID,
                    'Dist_name' => $upazila->Dist_name,
                    'Upz_ID' => $upazila->Upz_ID,
                    'Upaz_name' => $upazila->Upaz_name,
                    'Div_ID' => $upazila->Div_ID,
                    'Divi_name' => $upazila->Divi_name,
                    'Upz_UID' => $upazila->Upz_UID,
                    'Area' => $upazila->Area,
                    'shape' => $upazila->shape,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ];
            }
            UpazilaBoundary::insert($upazila_data);
        });
    }
}
