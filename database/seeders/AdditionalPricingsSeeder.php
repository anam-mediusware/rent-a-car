<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdditionalPricingsSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\AdditionalPricing::factory(50)->create([
            'service_pricing_id' => 1
        ]);
    }
}
