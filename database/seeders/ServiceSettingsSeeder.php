<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ServiceSettingsSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\ServiceSetting::factory(200)->startingPoint()->create()->each(function ($serviceSetting) {
            $serviceSetting->additionalPricings()->saveMany(\App\Models\AdditionalPricing::factory(random_int(1, 2))->make());
        });
    }
}
