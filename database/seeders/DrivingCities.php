<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DrivingCities extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('driving_cities')->insert([
            [
	            'city_name_english' => 'Dhaka',
	            'city_name_bangla' => 'ঢাকা',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'city_name_english' => 'Chittagong',
	            'city_name_bangla' => 'চট্টগ্রাম',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
        	],
            [
	            'city_name_english' => 'Gazipur',
	            'city_name_bangla' => 'গাজীপুর',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
        	],
            [
	            'city_name_english' => 'Narayanganj',
	            'city_name_bangla' => 'নারায়ণগঞ্জ',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'city_name_english' => 'Khulna',
	            'city_name_bangla' => 'খুলনা',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'city_name_english' => 'Sylhet',
	            'city_name_bangla' => 'সিলেট',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'city_name_english' => 'Rajshahi',
	            'city_name_bangla' => 'রাজশাহী',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'city_name_english' => 'Mymensingh',
	            'city_name_bangla' => 'ময়মনসিংহ',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'city_name_english' => 'Rangpur',
	            'city_name_bangla' => 'রংপুর',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'city_name_english' => 'Comilla',
	            'city_name_bangla' => 'কুমিল্লা',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ],
            [
	            'city_name_english' => 'Barisal',
	            'city_name_bangla' => 'বরিশাল',
	            'status' => '1',
	            'created_at' => now(),
	            'updated_at' => now(),
            ]
        ]);
    }
}
