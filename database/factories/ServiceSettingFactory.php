<?php

namespace Database\Factories;

use App\Http\Controllers\Admin\ServiceSettingController;
use App\Models\ServiceSetting;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceSettingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServiceSetting::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $service = \App\Models\Service::inRandomOrder()->whereIn('category_id', [1, 2])->first();
        $destination_point = $service->category_id == 2 ? \App\Models\UpazilaBoundary::inRandomOrder()->first()->id : null;
        return [
            'service_id' => $service->id,
            'vehicle_type_id' => \App\Models\VehicleType::inRandomOrder()->first()->id,
            'pricing_type' => 'fixed',
            'price' => $this->faker->numberBetween($min = 300, $max = 10000),
            'starting_point' => \App\Models\UpazilaBoundary::where('id', '<>', $destination_point)->inRandomOrder()->first()->id,
            'destination_point' => $destination_point,
            'status' => '1',
        ];
    }

    /**
     * Indicate that the user is suspended.
     *
     * @param null|string $location
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function startingPoint($location = null)
    {
        if ($location) {
            return $this->state([
                'starting_point' => $location,
            ]);
        }
        return $this;
    }
}
