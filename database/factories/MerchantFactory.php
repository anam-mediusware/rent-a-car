<?php

namespace Database\Factories;

use App\Models\Merchant;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MerchantFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Merchant::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'contact' => '01' . $this->faker->randomElement([3, 4, 6, 7, 8, 9]) . $this->faker->numerify('########'),
            'email' => $this->faker->unique()->safeEmail,
            'second_contact_name' => $this->faker->name,
            'second_contact_mobile_number' => '01' . $this->faker->randomElement([3, 4, 6, 7, 8, 9]) . $this->faker->numerify('########'),
            'password' => \Illuminate\Support\Facades\Hash::make('123456'),
            'district' => 'Dhaka',
            'company_name' => $this->faker->company,
        ];
    }
}
