<?php

namespace Database\Factories;

use App\Http\Controllers\Admin\ServiceSettingController;
use App\Models\BiddingTrip;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BiddingTripFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BiddingTrip::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $pick_up_date_time = $this->faker->dateTimeBetween($startDate = '+ 1 days', $endDate = '+ 30 days', $timezone = null);
        $drop_off_date_time = Carbon::parse($pick_up_date_time)->addHours(random_int(5, (5 * 24)));
        $pick_up_location = \App\Models\UpazilaBoundary::inRandomOrder()->first()->id;
        $drop_off_location = \App\Models\UpazilaBoundary::where('id', '<>', $pick_up_location)->inRandomOrder()->first()->id;

        return [
            'customer_type' => 2,
            'customer_id' => \App\Models\User::inRandomOrder()->first()->id,
            'vehicle_type_id' => \App\Models\VehicleType::inRandomOrder()->first()->id,
            'service_category_id' => \App\Models\ServiceCategory::inRandomOrder()->first()->id,
            'service_type_id' => \App\Models\Service::inRandomOrder()->first()->id,
            'pick_up_city' => $pick_up_location,
            'drop_off_city' => $drop_off_location,
            'pick_up_date_time' => $pick_up_date_time,
            'drop_off_date_time' => $drop_off_date_time,
            'price' => 0,
            'status' => 1,
        ];
    }
}
