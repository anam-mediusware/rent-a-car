<?php

namespace Database\Factories;

use App\Models\Driver;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DriverFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Driver::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $payment_method_id = $this->faker->randomElement([1, 2]);
        if ($payment_method_id == 1) {
            $payment_details = json_encode([[
                'value' => '01' . $this->faker->randomElement([3, 4, 6, 7, 8, 9]) . $this->faker->numerify('########'),
                'field' => 'bkash Number'
            ]]);
        } else {
            $payment_details = json_encode([[
                'value' => '01' . $this->faker->randomElement([3, 4, 6, 7, 8, 9]) . $this->faker->numerify('########'),
                'field' => 'Rocket Number'
            ]]);
        }
        return [
            'merchants_id' => \App\Models\Merchant::inRandomOrder()->first()->id,
            'full_name' => $this->faker->name,
            'mobile_number' => '01' . $this->faker->randomElement([3, 4, 6, 7, 8, 9]) . $this->faker->numerify('########'),
            'emergency_contact_number' => '01' . $this->faker->randomElement([3, 4, 6, 7, 8, 9]) . $this->faker->numerify('########'),
            'email_address' => $this->faker->unique()->safeEmail,
            // 'PIN' => 1234,
            'gender' => $this->faker->randomElement(['M', 'F']),
            'date_of_birth' => $this->faker->dateTimeBetween($startDate = '-60 years', $endDate = '-25 years', $timezone = null),
            'city_id' => \App\Models\DrivingCitie::inRandomOrder()->first()->id,
            'payment_method_id' => $payment_method_id,
            'payment_details' => $payment_details,
            'password' => \Illuminate\Support\Facades\Hash::make('123456'),
        ];
    }
}
