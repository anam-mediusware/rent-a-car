<?php

namespace Database\Factories;

use App\Http\Controllers\Admin\ServiceSettingController;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BookingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Booking::class;

    protected $pricing_type = 'fixed';

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        if ($this->pricing_type == 'fixed') {
            $service_setting = \App\Models\ServiceSetting::with('additionalPricings')->inRandomOrder()->first();
            $service_setting_id = $service_setting->id;
            $bid_id = null;
            $price = $service_setting->price;
            foreach ($service_setting->additionalPricings as $additionalPricing) {
                $price += $additionalPricing->price;
            }
        } else {
            $service_setting_id = null;
            $bid_id = \App\Models\Bid::inRandomOrder()->first()->id;
            $price = (int)$this->faker->numerify('####');
        }

        $pick_up_upazila = \App\Models\UpazilaBoundary::inRandomOrder()->first();
        $pick_up_city = $pick_up_upazila->id;
        $pick_up_location = $pick_up_upazila->Upaz_name . ', ' . $pick_up_upazila->Dist_name . ', ' . $pick_up_upazila->Divi_name;

        $drop_off_upazila = \App\Models\UpazilaBoundary::where('id', '<>', $pick_up_location)->inRandomOrder()->first();
        $drop_off_city = $drop_off_upazila->id;
        $drop_off_location = $drop_off_upazila->Upaz_name . ', ' . $drop_off_upazila->Dist_name . ', ' . $drop_off_upazila->Divi_name;

        $pick_up_date_time = $this->faker->dateTimeBetween($startDate = '+ 1 days', $endDate = '+ 30 days', $timezone = null);
        $drop_off_date_time = Carbon::parse($pick_up_date_time)->addHours(random_int(5, (5 * 24)));

        $charged_amount = $price * .30;

        return [
            'customer_type' => 2,
            'customer_id' => \App\Models\User::inRandomOrder()->first()->id,
            'service_setting_id' => $service_setting_id,
            'bid_id' => $bid_id,
            'vehicle_type_id' => \App\Models\VehicleType::inRandomOrder()->first()->id,
            'driver_id' => \App\Models\Driver::inRandomOrder()->first()->id,
            'service_category_id' => \App\Models\ServiceCategory::inRandomOrder()->first()->id,
            'service_type_id' => \App\Models\Service::inRandomOrder()->first()->id,
            'pricing_type' => $this->pricing_type,
            'pick_up_city' => $pick_up_city,
            'pick_up_location' => $pick_up_location,
            'drop_off_city' => $drop_off_city,
            'drop_off_location' => $drop_off_location,
            'pick_up_date_time' => $pick_up_date_time,
            'drop_off_date_time' => $drop_off_date_time,
            'price' => $price,
            'charged_amount' => $charged_amount,
            'transaction_id' => uniqid(),
            'status' => $this->faker->randomElement(['Pending', 'Processing']),
            'booking_status' => $this->faker->randomElement(['Pending', 'Confirmed', 'Cancelled by Customer', 'Cancelled by Driver', 'Trip Started', 'Completed']),
        ];
    }

    public function fixedTrip()
    {
        $this->pricing_type = 'fixed';
        return $this;
    }

    public function biddingTrip()
    {
        $this->pricing_type = 'bid';
        return $this;
    }
}
