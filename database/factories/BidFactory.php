<?php

namespace Database\Factories;

use App\Models\Bid;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BidFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bid::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'bidding_trip_id' => \App\Models\BiddingTrip::inRandomOrder()->first()->id,
            'driver_id' => \App\Models\Driver::inRandomOrder()->first()->id,
            'price' => (int)$this->faker->numerify('####'),
            'note' => $this->faker->realText(200),
        ];
    }
}
