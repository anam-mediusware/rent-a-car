<?php

namespace Database\Factories;

use App\Models\AdditionalPricing;
use Illuminate\Database\Eloquent\Factories\Factory;

class AdditionalPricingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AdditionalPricing::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
//            'service_pricing_id' => \App\Models\Service::inRandomOrder()->where('category_id', 1)->first()->id,
            'service_setting_id' => \App\Models\ServiceSetting::inRandomOrder()->first()->id,
            'price_name' => $this->faker->randomElement(['Driver Lunch', 'Car Cleaning']),
            'price' => $this->faker->numberBetween($min = 100, $max = 500),
        ];
    }
}
