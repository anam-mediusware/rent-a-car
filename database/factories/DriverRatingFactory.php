<?php

namespace Database\Factories;

use App\Models\DriverRating;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Str;

class DriverRatingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DriverRating::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'booking_id' => \App\Models\Booking::inRandomOrder()->first()->id,
            'driver_id' => \App\Models\Driver::inRandomOrder()->first()->id,
            'overall_rating' => $this->faker->numberBetween(1, 5),
            'car_condition' => $this->faker->numberBetween(1, 5),
            'driver_behavior' => $this->faker->numberBetween(1, 5),
            'driver_punctuality' => $this->faker->numberBetween(1, 5),
            'comment' => $this->faker->realText(200),
        ];
    }

    public function bookingSequence()
    {
        $bookings = \App\Models\Booking::all();
        $bookingSeq = [];
        $driverSeq = [];
        foreach ($bookings as $booking) {
            $bookingSeq[]['booking_id'] = $booking->id;
            $driverSeq[]['driver_id'] = $booking->driver_id;
        }
        return $this->state(new Sequence(...$bookingSeq))->state(new Sequence(...$driverSeq));
    }
}
