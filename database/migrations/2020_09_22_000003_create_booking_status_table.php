<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_status', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('booking_id')->unsigned();
            $table->enum('booking_status',['pending for payment','pending','confirmed','published','accepted by customer','accepted by driver','cancelled by customer','cancelled by driver'])->default('pending for payment')->comment('pending for payment, pending, confirmed, published,accepted by customer, accepted by driver, cancelled by customer, cancelled by driver');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_status');
    }
}
