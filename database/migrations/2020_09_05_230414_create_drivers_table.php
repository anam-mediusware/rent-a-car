<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('merchants_id')->nullable();
			$table->string('full_name',50);
			$table->string('mobile_number');
			$table->string('emergency_contact_number')->nullable();
			$table->string('email_address');
			// $table->integer('PIN');
			$table->enum('gender', ['M', 'F'])->comment('M = Male, F = Female');
			$table->date('date_of_birth');
			$table->integer('city_id');
			$table->integer('payment_method_id');
			$table->string('payment_details')->nullable();
			$table->string('image')->nullable();
            $table->string('password');
			$table->enum('status', ['0', '1'])->default('1')->comment('0 = Inactive, 1 = Active');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
