<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_settings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('service_id');
            $table->bigInteger('vehicle_type_id');
            $table->enum('pricing_type',['fixed', 'bid']);
            $table->tinyInteger('service_quality_type')->default(1)->comment('1=>Economy, 2=>Premium');
            $table->decimal('price',8,2)->nullable();
            $table->decimal('decoration_cost',8,2)->nullable();
            $table->unsignedBigInteger('starting_point')->nullable();
            $table->unsignedBigInteger('destination_point')->nullable();
            $table->unsignedBigInteger('airport_id')->nullable();
            $table->enum('status',['0', '1'])->default('1')->comment('0 = inactive, 1 = active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_settings');
    }
}
