<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpazilaBoundariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upazila_boundaries', function (Blueprint $table) {
            $table->id();
            $table->string('geometry_name', 100)->nullable();
            $table->integer('Dist_ID');
            $table->string('Dist_name', 100)->nullable();
            $table->integer('Upz_ID');
            $table->string('Upaz_name', 100)->nullable();
            $table->integer('Div_ID');
            $table->string('Divi_name', 100)->nullable();
            $table->string('Upz_UID');
            $table->string('Area', 50)->nullable();
            $table->geometry('shape');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upazila_boundaries');
    }
}
