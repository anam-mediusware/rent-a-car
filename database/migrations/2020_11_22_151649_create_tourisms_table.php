<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourismsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tourisms', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('tourism_type')->comment('1=> one day, 2 => others');
            $table->string('tourism_name');
            $table->string('tourism_slug');
            $table->string('tourism_thumb')->nullable();
            $table->string('tourism_image')->nullable();
            $table->text('tourism_details')->nullable();
            $table->unsignedSmallInteger('sorting')->nullable();
            $table->string('route_title')->nullable()->comment('For SEO');
            $table->string('route_keyword')->nullable()->comment('For SEO');
            $table->text('route_description')->nullable()->comment('For SEO');
            $table->unsignedTinyInteger('status')->comment('1=> Active, 2=>Deactivate');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tourisms');
    }
}
