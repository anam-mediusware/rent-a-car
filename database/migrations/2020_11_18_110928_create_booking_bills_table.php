<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_bills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_id');
            $table->decimal('bill_amount')->nullable();
            $table->decimal('customer_advance')->nullable();
            $table->decimal('cash_on_delivery')->nullable();
            $table->decimal('driver_rate')->nullable();
            $table->decimal('admin_commission')->nullable();
            $table->text('bill_remark')->nullable();
            $table->decimal('driver_receivable_amount')->nullable();
            $table->decimal('driver_receive_amount')->nullable();
            $table->decimal('driver_receive_due')->nullable()->comment('Receivable-Receive');
            $table->string('driver_receive_by', 20)->nullable();
            $table->text('driver_receive_remark')->nullable();
            $table->decimal('driver_payable_amount')->nullable();
            $table->decimal('driver_paid_amount')->nullable();
            $table->decimal('driver_paid_due')->nullable()->comment('Payable-Paid');
            $table->string('driver_paid_by', 20)->nullable();
            $table->text('driver_paid_remark')->nullable();
            $table->decimal('user_receivable_amount')->nullable();
            $table->decimal('user_receive_amount')->nullable();
            $table->decimal('user_receive_due')->nullable()->comment('Receivable-Receive');
            $table->string('user_receive_by', 20)->nullable();
            $table->text('user_receive_remark')->nullable();
            $table->unsignedBigInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_bills');
    }
}
