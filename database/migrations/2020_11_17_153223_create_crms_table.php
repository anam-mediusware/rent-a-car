<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crms', function (Blueprint $table) {
            $table->id();
            $table->string('customer_name')->nullable();
            $table->string('customer_number', 15);
            $table->string('customer_email')->nullable();
            $table->text('customer_address')->nullable();
            $table->bigInteger('ticket_number');
            $table->text('customer_query')->nullable();
            $table->dateTime('travel_datetime')->nullable();
            $table->string('vehicle_type', 100)->nullable();
            $table->text('feedback')->nullable();
            $table->unsignedTinyInteger('status')->comment('1=> Solved, 2 => Booking, 3=> Need to followup');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crms');
    }
}
