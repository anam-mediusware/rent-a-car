<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->id();
            $table->string('name',100)->nullable();
            $table->string('contact',50)->nullable();
            $table->string('email',100)->nullable();
            $table->string('second_contact_name',100)->nullable();
            $table->string('second_contact_mobile_number',50)->nullable();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('remember_token')->nullable();
            $table->text('address')->nullable();
            $table->string('district',100)->nullable();
            $table->string('photo_id')->nullable();
            $table->string('company_name',100)->nullable();
            $table->string('thana',100)->nullable();
            $table->string('phone_number',100)->nullable();
            $table->string('email_cc',100)->nullable();
            $table->string('account_name',100)->nullable();
            $table->string('account_number',50)->nullable();
            $table->string('bank_name',150)->nullable();
            $table->string('branch_name',50)->nullable();
            $table->string('routing_number',50)->nullable();
            $table->string('bkash_number',50)->nullable();
            $table->string('rocket_number',50)->nullable();
            $table->enum('status',[ '1', '2' ])->default('1')->comment('1 = Active,2 = Deactivate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
