<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('drivers_id')->default(0);
			$table->unsignedBigInteger('merchants_id')->nullable();
            $table->string('metro');
            $table->string('alphabetical_serial');
            $table->string('serial_number');
            $table->string('vehicle_type');
            $table->unsignedBigInteger('car_model_id')->nullable();
            $table->string('car_brand');
            $table->string('model');
			$table->year('year');
			$table->string('owner_name')->nullable();
			$table->text('owner_address')->nullable();
            $table->string('owner_mobile_number')->nullable();
            $table->string('owner_email')->nullable();
			$table->enum('status', ['0', '1'])->default('1')->comment('0 = Inactive, 1 = Active');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
