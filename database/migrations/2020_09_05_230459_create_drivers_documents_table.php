<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers_documents', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('drivers_id');
			$table->enum('document_type', ['NID_front', 'NID_back', 'driving_license_front', 'driving_license_back'])->comment('NID_front = NID Front Side, NID_back = NID Back Side, driving_license_front = Driveing License Front Side, driving_license_back = Driving License Back side');
			$table->string('file_name');
			$table->date('expiry_date')->nullable();
			$table->enum('verification_status', ['0','1'])->default(0)->comment('0 = Not Verify, 1 = Verify');
			$table->enum('status', ['0', '1'])->default('1')->comment('0 = Inactive, 1 = Active');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers_documents');
    }
}
