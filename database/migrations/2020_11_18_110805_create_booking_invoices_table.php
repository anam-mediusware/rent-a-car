<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_id');
            $table->string('invoice_number', 20);
            $table->date('invoice_date')->nullable();
            $table->decimal('rental_amount')->nullable();
            $table->text('rental_remark')->nullable();
            $table->decimal('extra_uses_cost')->nullable();
            $table->text('extra_uses_remark')->nullable();
            $table->decimal('driver_food_cost')->nullable();
            $table->text('driver_food_remark')->nullable();
            $table->decimal('driver_overtime_cost')->nullable();
            $table->text('driver_overtime_remark')->nullable();
            $table->decimal('fuel_cost')->nullable();
            $table->text('fuel_remark')->nullable();
            $table->decimal('toll_cost')->nullable();
            $table->text('toll_remark')->nullable();
            $table->decimal('other_cost')->nullable();
            $table->text('other_remark')->nullable();
            $table->decimal('vat_cost')->nullable();
            $table->text('vat_remark')->nullable();
            $table->decimal('invoice_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_invoices');
    }
}
