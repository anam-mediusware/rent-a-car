<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('vehicle_type_id')->unsigned();
            $table->string('from_location');
            $table->string('to_location');
            $table->enum('day_trip_status',['yes','no'])->comment('yes = Day Trip, no = Night Stay');
            $table->bigInteger('day_count')->default(0)->unsigned();
            $table->bigInteger('night_count')->default(0)->unsigned();
            $table->date('trip_start_date')->nullable();
            $table->date('trip_end_date')->nullable();
            $table->date('package_start_date');
            $table->date('package_end_date');
            $table->text('package_description')->nullable();
            $table->string('package_title')->nullable();
            $table->string('package_seo_title')->nullable();
            $table->string('package_seo_keywords')->nullable();
            $table->longText('package_seo_description')->nullable();
            $table->string('banner_image')->nullable();
            $table->enum('status',['0','1'])->default('1')->comment('0 = Inactive, 1 = Active');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement("ALTER TABLE packages AUTO_INCREMENT = 14;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
