<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars_documents', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('cars_id');
			$table->enum('document_type', ['registration_front', 'registration_back', 'fitness_paper', 'tax_token', 'insurance_paper'])->comment('registration_front = Registration Front Paper, registration_back = registration Back Side, fitness_paper = Fitness Paper, tax_token = Tax token, insurance_paper = Insurance Paper');
            $table->string('file_name');
			$table->date('expiry_date');
			$table->enum('verification_status', ['0','1'])->default(0)->comment('0 = Not Verify, 1 = Verify');
			$table->enum('status', ['0', '1'])->default('1')->comment('0 = Inactive, 1 = Active');
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars_documents');
    }
}
