<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiddingTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bidding_trips', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_type')->unsigned()->default('2');
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('vehicle_type_id')->unsigned();
            $table->bigInteger('service_category_id')->unsigned();
            $table->bigInteger('service_type_id')->unsigned();
            $table->tinyInteger('service_quality_type')->default(1)->comment('1=>Economy, 2=>Premium');
            $table->unsignedBigInteger('car_model_id')->nullable();
            $table->unsignedBigInteger('airport_id')->nullable();
            $table->unsignedBigInteger('pick_up_city');
            $table->string('pick_up_location')->nullable();
            $table->string('pick_up_point')->nullable()->comment('JSON Encoded Lat, Lon');
            $table->unsignedBigInteger('drop_off_city')->nullable();
            $table->string('drop_off_location')->nullable();
            $table->string('drop_off_point')->nullable()->comment('JSON Encoded Lat, Lon');
            $table->timestamp('pick_up_date_time')->nullable();
            $table->timestamp('drop_off_date_time')->nullable();
            $table->decimal('price',10,2)->default(0);
            $table->tinyInteger('status')->default(1)->comment('0 => Inactive, 1 => Active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bidding_trips');
    }
}
