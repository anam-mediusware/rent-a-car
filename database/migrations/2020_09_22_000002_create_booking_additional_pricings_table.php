<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingAdditionalPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_additional_pricings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('booking_id');
            $table->string('price_name');
            $table->decimal('price',8,2);
            $table->enum('price_type',['atactual', 'percentage'])->default('atactual');
            $table->enum('status',['0', '1'])->default('1')->comment('0 = inactive, 1 = active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_additional_pricings');
    }
}
