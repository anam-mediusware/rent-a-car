<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->integer('book_number')->unsigned()->nullable();
            $table->bigInteger('customer_type')->unsigned()->default('2');
            $table->bigInteger('customer_id')->unsigned();
            $table->bigInteger('service_setting_id')->nullable()->unsigned();
            $table->bigInteger('bid_id')->nullable()->unsigned();
            $table->bigInteger('driver_id')->nullable()->unsigned();
            $table->bigInteger('vehicle_type_id')->unsigned();
            $table->bigInteger('service_category_id')->unsigned();
            $table->bigInteger('service_type_id')->unsigned();
            $table->tinyInteger('service_quality_type')->default(1)->comment('1=>Economy, 2=>Premium');
            $table->unsignedBigInteger('car_model_id')->nullable();
            $table->unsignedBigInteger('starting_point')->nullable();
            $table->unsignedBigInteger('airport_id')->nullable();
            $table->enum('pricing_type',['fixed', 'bid']);
            $table->unsignedBigInteger('pick_up_city');
            $table->string('pick_up_location')->nullable();
            $table->string('pick_up_point')->nullable()->comment('JSON Encoded Lat, Lon');
            $table->unsignedBigInteger('drop_off_city')->nullable();
            $table->string('drop_off_location')->nullable();
            $table->string('drop_off_point')->nullable()->comment('JSON Encoded Lat, Lon');
            $table->timestamp('pick_up_date_time')->nullable();
            $table->timestamp('drop_off_date_time')->nullable();
            $table->decimal('price',10,2);
            $table->decimal('decoration_cost',8,2)->nullable();
            $table->timestamp('trip_started')->nullable();
            $table->timestamp('completed')->nullable();
            $table->string('booking_status', 25)->default('Pending')->comment('Pending, Confirmed, Cancelled by Customer, Cancelled by Driver, Trip Started, Completed');
            $table->decimal('charged_amount',10,2);
            $table->string('transaction_id')->nullable();
            $table->string('status', 10)->default('Pending')->comment('SSL Commerz Payment Status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
