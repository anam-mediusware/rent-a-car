<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_contacts', function (Blueprint $table) {
            $table->id();
            $table->integer('partner_id');
            $table->unsignedTinyInteger('receiver_type')->comment('1 = admin, 2 = partner');
            $table->unsignedTinyInteger('sender_type')->comment('1 = admin, 2 = partner');
            $table->text('message');
            $table->string('attachment1')->nullable();
            $table->string('attachment2')->nullable();
            $table->string('attachment3')->nullable();
            $table->enum('status',['1','2'])->default('1')->comment('1 = Active, 2 = Deactivate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_contacts');
    }
}
