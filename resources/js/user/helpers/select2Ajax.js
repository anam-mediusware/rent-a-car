"use strict";

((exports, $garivara) => {
    $garivara.googleAutocompleteSelect2 = function (selector, place_ids = null) {
        const goggleAutocompleteSelect2 = $(selector);
        if (place_ids) {
            if (typeof place_ids == 'number' || typeof place_ids == 'string') {
                place_ids = [place_ids]
                this.hasPreSelectedUpazilas = true;
            } else if (typeof place_ids == 'object') {
                this.hasPreSelectedUpazilas = (place_ids.length > 0);
            }

            if (this.hasPreSelectedUpazilas) {
                // Fetch the preselected item, and add to the control
                axios.get('/api/google/place-details/', {
                    params: {
                        place_ids
                    }
                }).then(({data}) => {
                    let options = [];
                    data.forEach(item => {
                        // create the option and append to Select2
                        options.push(new Option(item.address, item.place_id, true, true));
                    })
                    goggleAutocompleteSelect2.append(options).trigger('change');

                    // manually trigger the `select2:select` event
                    goggleAutocompleteSelect2.trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    });
                });
            }
        }

        goggleAutocompleteSelect2.select2({
            minimumInputLength: 1,
            cache: true,
            placeholder: 'City, Airport, Station, etc.',
            tags: false,
            ajax: {
                url: '/api/google/autocomplete',
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                    };
                },
                processResults: function (data) {
                    let results = [];
                    data.data.forEach((item) => {
                        results.push({
                            id: item.structured_formatting.main_text,
                            address: item.structured_formatting.main_text,
                        });
                    });
                    return {
                        results: results,
                    };
                }
            },
            escapeMarkup: function (m) {
                return m;
            },
            templateResult: function (data) {
                if (data.text) return data.text;
                return data.address;
            },
            templateSelection: function (data) {
                if (data.address) {
                    return data.address;
                } else {
                    return data.text;
                }
            }
        }).trigger('change');
    }
})(exports, $garivara);
