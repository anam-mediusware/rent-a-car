export default {
    methods: {
        capitalize: (string) => {
            if (!string) return ''
            string = string.toString();
            return string.charAt(0).toUpperCase() + string.slice(1);
        },
        capitalizeWords: (string) => {
            return string.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
    }
}
