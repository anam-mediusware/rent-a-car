export default {
    methods: {
        toastError(error) {
            console.log(error.response)
            toastr.error(error.response.statusText)
        }
    }
}
