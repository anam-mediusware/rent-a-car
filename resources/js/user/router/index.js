import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from '../pages/Home';
import About from '../pages/About';

const routes = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            component: Home,
        },
        {
            path: '/about',
            component: About,
        },
        {
            path: '/services',
            component: require('../pages/Services').default,
            name: 'user.services.index',
        },
    ]
});

export default routes;
