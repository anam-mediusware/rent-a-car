window.$garivara = {};
import Vue from 'vue';
import router from './router/index';

require('./bootstrap');
require('./helpers/select2Ajax');

window.Vue = require('vue');

import Antd from 'ant-design-vue';
import DatePicker from 'vue2-datepicker';
import HelperMixin from './mixins/helper-mixin';
import FormatMixin from './mixins/format-mixin';


Vue.use(Antd);
Vue.use(DatePicker)
Vue.use(require('vue-moment'));

// Mixins
Vue.mixin(HelperMixin)
Vue.mixin(FormatMixin)

// import VueConfirmDialog from 'vue-confirm-dialog'
//
// Vue.use(VueConfirmDialog)
// Vue.component('vue-confirm-dialog', VueConfirmDialog.default)

// Vue.component('booking-component', require('./pages/Bookings').default);
// Vue.component('bidding-trip-component', require('./pages/BiddingTrips').default);
Vue.component('profile-component', require('./pages/Profile').default);
// Vue.component('payment-component', require('./pages/Payments').default);

Vue.config.debug = true;
Vue.config.devtools = true;

window.$ = jQuery;

if ($("#app").length > 0) {
    const app = new Vue({
        el: '#app',
        router,
        Antd,
        DatePicker
    });
}
