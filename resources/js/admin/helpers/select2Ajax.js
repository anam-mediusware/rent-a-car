"use strict";

((exports, $garivara) => {
    $garivara.upazilaSelect2 = function (selector, upazila_ids = null) {
        const upazilaSelect2 = $(selector);
        if (upazila_ids) {
            if (typeof upazila_ids == 'number' || typeof upazila_ids == 'string') {
                upazila_ids = [upazila_ids]
                this.hasPreSelectedUpazilas = true;
            } else if (typeof upazila_ids == 'object') {
                this.hasPreSelectedUpazilas = (upazila_ids.length > 0);
            }

            if (this.hasPreSelectedUpazilas) {
                // Fetch the preselected item, and add to the control
                axios.get('/api/get-upazilas-by-upazila-boundary-ids/', {
                    params: {
                        upazila_ids
                    }
                }).then(({data}) => {
                    let options = [];
                    data.forEach(item => {
                        // create the option and append to Select2
                        options.push(new Option(item.upazila_name, item.id, true, true));
                    })
                    upazilaSelect2.append(options).trigger('change');

                    // manually trigger the `select2:select` event
                    upazilaSelect2.trigger({
                        type: 'select2:select',
                        params: {
                            data: data
                        }
                    });
                });
            }
        }

        upazilaSelect2.select2({
            minimumInputLength: 1,
            cache: true,
            placeholder: 'Search Destination Point',
            tags: false,
            ajax: {
                url: '/api/get-upazila-select2',
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page || 1
                    };
                },
                processResults: function (data) {
                    let results = [];
                    data.data.forEach((item) => {
                        results.push({
                            id: item.id,
                            upazila_name: item.upazila_name,
                        });
                    });
                    return {
                        results: results,
                        pagination: {
                            more: data.current_page < data.last_page
                        }
                    };
                }
            },
            escapeMarkup: function (m) {
                return m;
            },
            templateResult: function (data) {
                if (data.text) return data.text;
                return data.upazila_name;
            },
            templateSelection: function (data) {
                if (data.upazila_name) {
                    return data.upazila_name;
                } else {
                    return data.text;
                }
            }
        }).trigger('change');
    }
})(exports, $garivara);
