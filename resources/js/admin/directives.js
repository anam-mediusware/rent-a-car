import Vue from 'vue';

// jQuery Select2 trigger onChange
Vue.directive('select2', {
    inserted(el) {
        $(el).on('select2:select', () => {
            const event = new Event('change', {bubbles: true, cancelable: true});
            el.dispatchEvent(event);
        });

        $(el).on('select2:unselect', () => {
            const event = new Event('change', {bubbles: true, cancelable: true})
            el.dispatchEvent(event)
        })
    },
});

// Hide Element
Vue.directive('hide', {
    bind(el) {
        el.style.display = 'none';
    },
});

// Show Element
Vue.directive('show', {
    bind(el) {
        el.style.display = 'block';
    },
});
