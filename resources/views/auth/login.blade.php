@extends('layouts.frontend')

@section('content')
    <div class="x_partner_main_wrapper float_left padding_tb_100">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_offer_car_heading_wrapper float_left">
                        <h4>Please Login To Access Car Booking</h4>
                        <h3>LOGIN TO YOUR ACCOUNT</h3>
                    </div>
                </div>
            </div>
            <div class="row form-div">
                <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-12 col-sm-12 col-12">
                    <!-- login_wrapper -->
                    <div class="login_wrapper">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                <a href="#" class="fb_btn btn btn-primary" style="background: white;color: #3578E5;border: 1px solid #3578E5;">
                                    <span>Login with Facebook</span><i class="fa fa-facebook-f"></i>
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
                                <a href="#" class="btn btn-primary google-plus"> Login with Google
                                    <img src="{{ asset('web-assets/images/google.png') }}" alt="google logo" style="width: 100px;float: right;height: 50px;margin-top: -5px;margin-left: -10px;margin-right: -30px;">
                                </a>
                            </div>
                        </div>
                        <h2>or</h2>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="formsix-pos">
                                <div class="form-group i-email">
                                    <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="off" autofocus placeholder="mobile *">
                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="formsix-e">
                                <div class="form-group i-password">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="off" placeholder="Password *">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="login_remember_box">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} style="margin-left: 1px;">
                                <label class="form-check-label" for="remember" style="margin-left: 20px;margin-top: 1px;">
                                    {{ __('Remember Me') }}
                                </label>
                                @if (Route::has('password.request'))
                                    <a class="forget_password" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                            <div class="login_btn_wrapper">
                                <button type="submit" class="login_btn">
                                    {{ __('Login') }}
                                </button>
                            </div>
                            <div class="login_message">
                                @if (Route::has('register'))
                                    <p>Don’t have an account ?
                                        <a class="" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </p>
                                @endif
                            </div>
                        </form>
                    </div>
                    <p>
                        In case you are using a public/shared computer we recommend that you logout to prevent any
                        un-authorized access to your account
                    </p>
                    <!-- /.login_wrapper-->
                </div>
            </div>
        </div>
    </div><!-- xs offer car tabs Start -->
@endsection
