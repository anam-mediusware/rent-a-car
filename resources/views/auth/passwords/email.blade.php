@extends('layouts.frontend')

@section('content')
    <div class="x_partner_main_wrapper float_left padding_tb_100">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_offer_car_heading_wrapper float_left">
                        <h4>Please Provide Your Email Address</h4>
                        <h3>RESET ACCOUNT PASSWORD</h3>
                    </div>
                </div>
            </div>
            <div class="row form-div">
                <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-12 col-sm-12 col-12">
                    <!-- login_wrapper -->
                    <div class="login_wrapper">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="formsix-pos">
                                <div class="form-group i-email">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="email* ">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="login_btn_wrapper">
                                <button type="submit" class="login_btn">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </form>
                    </div>
                    <!-- /.login_wrapper-->
                </div>
            </div>
        </div>
    </div>
    <!-- xs offer car tabs Start -->
@endsection
