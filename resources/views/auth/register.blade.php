@extends('layouts.frontend')
@section('content')
    <div class="x_partner_main_wrapper float_left padding_tb_100">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_offer_car_heading_wrapper float_left">
                        <h4>Register Got Your Account</h4>
                        <h3>NEW CUSTOMER REGISTER HERE</h3>
                    </div>
                </div>
            </div>
            <div class="row form-div">
                <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-12 col-sm-12 col-12">
                    <!-- login_wrapper -->
                    <div class="register_wrapper_box">
                        <div class="register_left_form">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="jp_regiter_top_heading">
                                    <p>Fields with * are mandetory </p>
                                </div>
                                <div class="row clearfix">
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input id="name" type="text" class="@error('name') is-invalid @enderror" placeholder="Name*" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input id="email" type="email" class="@error('email') is-invalid @enderror" placeholder="Email*" name="email" value="{{ old('email') }}" required autocomplete="email">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input id="password" type="password" placeholder="password*" class="@error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                        @error('password')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input id="password-confirm" type="password" placeholder="re-enter password*" class="" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input id="mobile" type="text" placeholder="Mobile *" class="@error('mobile') is-invalid @enderror" name="mobile" required autocomplete="mobile">
                                        @error('mobile')
                                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>
                                    <!--Form Group-->
                                    <!-- <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                       <input type="text" name="field-name" value="" placeholder="company name">
                                    </div> -->
                                    <!--Form Group-->
                                    <!-- <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                       <input type="text" name="field-name" value="" placeholder="website">
                                    </div> -->
                                    <!--Form Group-->
                                    <!-- <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                       <input type="text" name="field-name" value="" placeholder="address line">
                                    </div> -->
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="check-box text-center">
                                            <input type="checkbox" name="shipping-option" id="account-option_2">  
                                            <label for="account-option_2" class="label_2">I agreed to the
                                                <a href="#" class="check_box_anchr">Terms and Conditions</a> governing
                                                the use of weapon store</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="login_btn_wrapper register_btn_wrapper login_wrapper register_wrapper_btn">
                                    <button type="submit" class="login_btn">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </form>
                            <div class="login_message">
                                <p>
                                    Already a member?
                                    <a href="{{ route('login') }}">Login Here</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <p class="btm_txt_register_form">In case you are using a public/shared computer we recommend that
                        you logout to prevent any un-authorized access to your account</p>
                    <!-- /.login_wrapper-->
                </div>
            </div>
        </div>
    </div>
@endsection
