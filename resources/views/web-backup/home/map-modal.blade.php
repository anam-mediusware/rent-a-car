<!-- Modal -->
<div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="mapModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title font-weight-bold" id="mapModalTitle">Is this your exact location?</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="map-box">
                    <div id="map_canvas"></div>
                </div>
            </div>
            <div class="modal-footer">
                <a-button type="button" class="search-btn-group" data-dismiss="modal">OK</a-button>
            </div>
        </div>
    </div>
</div>
