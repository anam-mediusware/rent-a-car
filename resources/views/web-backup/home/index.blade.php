@extends('layouts.frontend')

@push('styles')
    @include('web.home.styles')
@endpush

@section('content')
    <div id="home-page">
    <!-- hs Slider Start -->
    <div class="slider-area float_left">
        <div id="carousel-example-generic" class="carousel slide" data-interval="false" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="carousel-captions caption-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div class="content">
                                        <h2 data-animation="animated fadeInLeft">CHEAP CAR RENTAL IN<br>
                                            your desired destination
                                        </h2>
                                        <p data-animation="animated bounceInUp">One of our top priorities is to
                                            adjust each package we offer to our
                                            <br>customer’s exact needs. Rental Cars / Bike / Jeeps <span>Starting @ $3 / Hrs</span>
                                        </p>
                                        <div class="hs_effect_btn">
                                            <ul>
                                                <li data-animation="animated flipInX"><a href="#">about
                                                        us<i class="fa fa-arrow-right"></i></a>
                                                </li>
                                                <li data-animation="animated flipInX">
                                                    <a href="#">contact<i class="fa fa-arrow-right"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12 d-none d-sm-none d-md-none d-lg-block d-xl-block">
                                    <form @submit.prevent="searchRequest">
                                        <div class="content_tabs">
                                            <div class="row">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" v-show="activePhase === 1">
                                                    <div class="x_slider_form_main_wrapper float_left" data-animation="animated fadeIn">
                                                        <div class="x_slider_form_heading_wrapper float_left">
                                                            <h3>Let’s find your perfect car</h3>
                                                        </div>
                                                        <br>
                                                        <h4>Select vehicle type</h4>
                                                        <div class="row mb-4">
                                                            <div class="col-md-12">
                                                                <div class="x_slider_form_input_wrapper float_left">
                                                                    <a-radio-group v-model="filter.class">
                                                                        <a-radio-button value="1">Economy</a-radio-button>
                                                                    </a-radio-group>
                                                                    <a-radio-group v-model="filter.class">
                                                                        <a-radio-button value="2">Premium</a-radio-button>
                                                                    </a-radio-group>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3 radio-btn" style="margin-left: 15px;">
                                                                <label>
                                                                    <input type="radio" name="test" class="radio" v-model="filter.vehicle_type" value="1">
                                                                    <img :src="'/web-assets/img/service/sedan.png'" class="service-img" alt="">
                                                                </label>
                                                                <p style="margin-left: -3px;">4 Seater</p>
                                                            </div>
                                                            <div class="col-md-3 radio-btn" style="margin-left: 15px;">
                                                                <label>
                                                                    <input type="radio" name="test" class="radio" v-model="filter.vehicle_type" value="2">
                                                                    <img :src="'/web-assets/img/service/noah.png'" class="service-img" alt="">
                                                                </label>
                                                                <p style="margin-left: -3px;">7 Seater</p>
                                                            </div>
                                                            <div class="col-md-3 radio-btn" style="margin-left: 15px;">
                                                                <label>
                                                                    <input type="radio" name="test" class="radio" v-model="filter.vehicle_type" value="3">
                                                                    <img :src="'/web-assets/img/service/hiace.png'" class="service-img" alt="">
                                                                </label>
                                                                <p style="margin-left: -9px;">10 Seater</p>
                                                            </div>
                                                            <div class="col-md-3 radio-btn" style="margin-left: 15px;">
                                                                <label>
                                                                    <input type="radio" name="test" class="radio" v-model="filter.vehicle_type" value="4">
                                                                    <img :src="'/web-assets/img/service/hiace.png'" class="service-img" alt="">
                                                                </label>
                                                                <p style="margin-left: -9px;">14 Seater</p>
                                                            </div>
                                                            <div class="col-md-3 radio-btn" style="margin-left: 15px;">
                                                                <label>
                                                                    <input type="radio" name="test" class="radio" v-model="filter.vehicle_type" value="5">
                                                                    <img :src="'/web-assets/img/service/hiace.png'" class="service-img" alt="">
                                                                </label>
                                                                <p style="margin-left: -9px;">21++ Seater</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12" v-if="carModels.length > 0">
                                                                <div class="x_slider_form_input_wrapper float_left" v-if="carModels.length > 0">
                                                                    <a-radio-group v-model="filter.car_model_id" v-for="(carModel, index) in carModels" :key="index">
                                                                        <a-radio-button :value="carModel.id">
                                                                            @{{ carModel.car_model }}
                                                                        </a-radio-button>
                                                                    </a-radio-group>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="x_slider_checkbox_bottom float_left">
                                                                    <div class="x_slider_checout_left">
                                                                    </div>
                                                                    <div class="x_slider_checout_right">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;" @click="proceedToPhase(2)">
                                                                                    Next <i class="fa fa-arrow-right"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" v-show="activePhase === 2">
                                                    <div class="x_slider_form_main_wrapper float_left">
                                                        <div class="x_slider_form_heading_wrapper float_left" style="padding: 10px 10px 10px 10px;">
                                                            <a href="javascript:;" style="color:white;margin-left: -63%;" @click="proceedToPhase(1)">Back</a>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="x_slider_form_input_wrapper float_left">
                                                                    <h3>Select a service category</h3>
                                                                    <a-select
                                                                        default-value="select" style="width: 100%"
                                                                        @change="SetServiceCategory"
                                                                        v-model="filter.service_category_id"
                                                                    >
                                                                        <a-select-option v-for="category in ServiceCategoryList"
                                                                                         :key="(category.index)"
                                                                                         :value="category.id">
                                                                            @{{ category.category_name }}
                                                                        </a-select-option>
                                                                    </a-select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12" v-if="isCategorySelected === true">
                                                                <div class="x_slider_form_input_wrapper float_left" v-if="ServiceList.length > 1">
                                                                    <h3>Select a service</h3>
                                                                    <a-radio-group v-model="filter.service_type_id" @change="onChangeValue" v-for="(service, index) in ServiceList" :key="index">
                                                                        <a-radio-button :value="service.id">
                                                                            @{{ service.service_name }}
                                                                        </a-radio-button>
                                                                    </a-radio-group>
                                                                </div>
                                                            </div>
                                                            <template v-if="+filter.service_category_id === 3">
                                                                <div class="col-md-12" v-if="filter.service_type_id === 8">
                                                                    <div class="x_slider_form_input_wrapper float_left">
                                                                        <h3>Select City</h3>
                                                                        <a-select
                                                                            default-value="select" style="width: 100%"
                                                                            @change="handleChangeAirport"
                                                                            v-model="filter.airport_city_id"
                                                                        >
                                                                            <a-select-option v-for="row in airportCites"
                                                                                             :key="(row.index)"
                                                                                             :value="row.id">
                                                                                @{{ row.city_title }}
                                                                            </a-select-option>
                                                                        </a-select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" v-if="filter.service_type_id === 9">
                                                                    <div class="x_slider_form_input_wrapper float_left">
                                                                        <h3>Select Airport</h3>
                                                                        <a-select
                                                                            default-value="select" style="width: 100%"
                                                                            @change="handleChangeAirport"
                                                                            v-model="filter.airport_city_id"
                                                                        >
                                                                            <a-select-option v-for="row in airportCites"
                                                                                             :key="(row.index)"
                                                                                             :value="row.id">
                                                                                @{{ row.airport }}
                                                                            </a-select-option>
                                                                        </a-select>
                                                                    </div>
                                                                </div>
                                                            </template>
                                                            <div class="col-md-12">
                                                                <div class="x_slider_checkbox_bottom float_left">
                                                                    <div class="x_slider_checout_left">
                                                                    </div>
                                                                    <div class="x_slider_checout_right">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;" @click="proceedToPhase(3)">
                                                                                    Next <i class="fa fa-arrow-right"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" v-show="activePhase === 3">
                                                    <div class="x_slider_form_main_wrapper float_left">
                                                        <div class="x_slider_form_heading_wrapper float_left" style="padding: 10px 10px 10px 10px;">
                                                            <a href="javascript:;" style="color:white;margin-left: -63%;" @click="proceedToPhase(2)">Back</a>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12" v-if="!(filter.service_type_id === 9)">
                                                                <div class="x_slider_form_input_wrapper float_left">
                                                                    <h3>Pick-up Location</h3>
                                                                    <input type="text" id="starting_point" name="starting_point" v-model="filter.starting_point" placeholder="City, Airport, Station, etc.">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12" v-if="!(filter.service_type_id === 8)">
                                                                <div class="x_slider_form_input_wrapper float_left">
                                                                    <h3>Drop-off Location</h3>
                                                                    <input type="text" id="ending_point" name="ending_point" v-model="filter.ending_point" placeholder="City, Airport, Station, etc.">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-sec-header">
                                                                    <h3>Pick-up Date</h3>
                                                                    <date-picker v-model="filter.pick_up_date" name="pick_up_date" type="date" format="DD-MM-YYYY"></date-picker>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-sec-header">
                                                                    <h3>Drop-Off Date</h3>
                                                                    <date-picker v-model="filter.drop_off_date" name="drop_off_date" type="date" format="DD-MM-YYYY"></date-picker>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="">
                                                                    <small>Pickup time</small>
                                                                    <date-picker v-model="filter.pick_up_time" name="pick_up_time" type="time" lang="en" format="HH:mm a" :minute-step="5"></date-picker>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="">
                                                                    <small>Drop-Off time</small>
                                                                    <date-picker v-model="filter.drop_off_time" name="drop_off_time" type="time" lang="en" format="HH:mm a" :minute-step="5"></date-picker>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="col-md-12">
                                                               <div class="x_slider_checkbox float_left">
                                                                   <input type="checkbox" id="c1" name="cb">
                                                                   <label for="c1">Driver age is between 30-65 &nbsp;<i class="fa fa-question-circle"></i>
                                                                   </label>
                                                               </div>
                                                               </div> -->
                                                            <div class="col-md-12">
                                                                <div class="x_slider_checkbox_bottom float_left">
                                                                    <div class="x_slider_checout_left">
                                                                    </div>
                                                                    <div class="x_slider_checout_right">
                                                                        <ul>
                                                                            <li>
                                                                                <button type="button" class="search-btn" @click="searchRequest">
                                                                                    Search<i class="fa fa-arrow-right"></i>
                                                                                </button>
                                                                                <!-- <a type="submit" href="javascript:;">search <i class="fa fa-arrow-right"></i></a> -->
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" v-show="activePhase === 4">
                                                    <div class="x_slider_form_main_wrapper float_left">
                                                        <div class="x_slider_form_heading_wrapper float_left">
                                                            <h3>
                                                                <a href="javascript:;" style="color:white;margin-left: -45%;" @click="GoBackToFirstPhase">
                                                                    Search again?
                                                                </a>
                                                            </h3>
                                                        </div>
                                                        <br>
                                                        <h4>Search Result...</h4>
                                                        <hr>
                                                        <div class="row" v-if="SearchedPrice">
                                                            <div class="row" style="padding: 10px;margin-left: -5px;" v-if="SearchedPrice">
                                                                <div class="col-md-12">
                                                                    <table class="table table-borderless">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>Price: </td>
                                                                            <td class="text-right">@{{ parseFloat(SearchedPrice.price) }}</td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr v-for="additionalPrice in SearchedPrice.additional_pricings" :key="additionalPrice.id">
                                                                            <td>@{{ additionalPrice.price_name }}: </td>
                                                                            <td class="text-right">@{{ parseFloat(additionalPrice.price) }}</td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr style="font-weight: bold;">
                                                                            <td>Total Price: </td>
                                                                            <td class="text-right">@{{ parseFloat(totalPrice) }}</td>
                                                                            <td>
                                                                                <a-button type="dashed"
                                                                                          href="/check-auth-booking/fixed/"
                                                                                          style="margin-top:-15px" class="search-btn-group">Book Now</a-button>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h4 style="margin-left: 20px;">
                                                                        Not satisfied with the price?
                                                                    </h4>
                                                                    <br>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <a-button type="dashed" href="/check-auth-booking/bid/" class="search-btn-group" style="margin-left: 20px;">
                                                                        Publish for Bidding
                                                                    </a-button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" v-else>
                                                            <div class="col-md-8">
                                                                <h3>No Result Found .. </h3>
                                                                <br>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <h4>Go for bid?</h4>
                                                                <a-button type="dashed" href="/check-auth-booking/bid/" class="search-btn-group" style="margin-left: 20px;">
                                                                    Bid Now
                                                                </a-button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active">
                        <span class="number"></span>
                    </li>
                </ol>
                <div class="carousel-nevigation">
                    <a class="prev" href="#carousel-example-generic" role="button" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="next" href="#carousel-example-generic" role="button" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- hs Slider End -->

    <!-- xs Slider bottom title Start -->
    <div class="x_slider_bottom_title_main_wrapper">
        <div class="x_slider_bottom_box_wrapper">
            <i class="flaticon-magnifying-glass"></i>
            <h3><a href="#">24 / 7 CAR SUPPORT</a></h3>
            <p>Proin gravida nibh vel velit auctor
                <br>aliquet. Aenean sollicitudin, lorem
                <br>quis bibendum auctor.
            </p>
        </div>
        <div class="x_slider_bottom_box_wrapper">
            <i class="flaticon-world"></i>
            <h3><a href="#">LOTS OF LOCATION</a></h3>
            <p>Proin gravida nibh vel velit auctor
                <br>aliquet. Aenean sollicitudin, lorem
                <br>quis bibendum auctor.
            </p>
        </div>
        <div class="x_slider_bottom_box_wrapper">
            <i class="flaticon-checklist"></i>
            <h3><a href="#">RESERVATION ANYTIME</a></h3>
            <p>Proin gravida nibh vel velit auctor
                <br>aliquet. Aenean sollicitudin, lorem
                <br>quis bibendum auctor.
            </p>
        </div>
        <div class="x_slider_bottom_box_wrapper">
            <i class="flaticon-car-trip"></i>
            <h3><a href="#">Rentals Cars</a></h3>
            <p>Proin gravida nibh vel velit auctor
                <br>aliquet. Aenean sollicitudin, lorem
                <br>quis bibendum auctor.
            </p>
        </div>
    </div>
    <!-- xs Slider bottom title End -->
    <!-- xs offer car tabs Start -->
    <div class="x_offer_car_main_wrapper float_left padding_tb_100">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_offer_car_heading_wrapper float_left">
                        <h4>What We Offer</h4>
                        <h3>Choose your Car</h3>
                        <p>Morbi mollis vestibulum sollicitudin. Nunc in eros a justo facilisis rutrum. Aenean id
                            ullamcorper libero
                            <br>Vestibulum imperdiet nibh vel magna lacinia commodo ultricies,
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="tab-content">
                        <div id="home" class="tab-pane active">
                            <div class="row">
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="x_car_offer_main_boxes_wrapper float_left">
                                        <div class="portfolio">
                                            <img src="/web-assets/img/homepage/GARIVARA_RENT_A_CAR_BANGLADESH.jpg" alt="img">
                                        </div>
                                        <div class="x_car_offer_price float_left">
                                            <div class="x_car_offer_price_inner">
                                                <h5>Need a Car in Dhaka?</h5>
                                            </div>
                                        </div>
                                        <div class="x_car_offer_heading float_left">
                                            <h2>
                                                <a href="#"><i class="fa fa-pencil-square-o portfolio-icons" aria-hidden="true"></i></a>
                                            </h2>
                                            <h5>Check Price</h5>
                                        </div>
                                        <div class="x_car_offer_bottom_btn float_left">
                                            <ul>
                                                <li><a href="#" style="margin-left:50%">Check Price</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="x_car_offer_main_boxes_wrapper float_left">
                                        <div class="portfolio">
                                            <img src="/web-assets/img/homepage/Offer.jpg" alt="img">
                                        </div>
                                        <div class="x_car_offer_price float_left">
                                            <div class="x_car_offer_price_inner">
                                                <h5>Offers & Packages</h5>
                                            </div>
                                        </div>
                                        <div class="x_car_offer_heading float_left">
                                            <h2>
                                                <a href="#"><i class="fa fa-gift portfolio-icons" aria-hidden="true"></i></a>
                                            </h2>
                                            <h5>Our Car Rental packages that will cover you well!</h5>
                                        </div>
                                        <div class="x_car_offer_bottom_btn float_left">
                                            <ul>
                                                <li><a href="#" style="margin-left:50%">More Details</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="x_car_offer_main_boxes_wrapper float_left">
                                        <div class="portfolio">
                                            <img src="/web-assets/img/homepage/MakeMoney.jpg" alt="img">
                                        </div>
                                        <div class="x_car_offer_price float_left">
                                            <div class="x_car_offer_price_inner">
                                                <h5>Make Money From Your Car</h5>
                                            </div>
                                        </div>
                                        <div class="x_car_offer_heading float_left">
                                            <h2>
                                                <a href="#"><i class="fa fa-gift portfolio-icons" aria-hidden="true"></i></a>
                                            </h2>
                                            <h5>RENT YOUR CAR WITH US</h5>
                                        </div>
                                        <div class="x_car_offer_bottom_btn float_left">
                                            <ul>
                                                <li><a href="#" style="margin-left:50%">Register your car</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="x_car_offer_main_boxes_wrapper float_left">
                                        <div class="portfolio">
                                            <img src="/web-assets/img/homepage/Destination.jpg" alt="img">
                                        </div>
                                        <div class="x_car_offer_price float_left">
                                            <div class="x_car_offer_price_inner">
                                                <h5>Destination Anywhere</h5>
                                            </div>
                                        </div>
                                        <div class="x_car_offer_heading float_left">
                                            <h2>
                                                <a href="#"><i class="fa fa-car portfolio-icons" aria-hidden="true"></i></a>
                                            </h2>
                                            <h5>The best way to explore Bangladesh is by car! Book your car from our
                                                various packages.</h5>
                                        </div>
                                        <div class="x_car_offer_bottom_btn float_left">
                                            <ul>
                                                <li><a href="#" style="margin-left:50%">More Details</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="x_car_offer_main_boxes_wrapper float_left">
                                        <div class="portfolio">
                                            <img src="/web-assets/img/homepage/Tourism.jpg" alt="img">
                                        </div>
                                        <div class="x_car_offer_price float_left">
                                            <div class="x_car_offer_price_inner">
                                                <h5>Tourism Bangladesh</h5>
                                            </div>
                                        </div>
                                        <div class="x_car_offer_heading float_left">
                                            <h2>
                                                <a href="#"><i class="fa fa-street-view portfolio-icons" aria-hidden="true"></i></a>
                                            </h2>
                                            <h5>Don’t Spoil the Beauty of Your tours! Traveling is easy when you
                                                have right options on your plate. </h5>
                                        </div>
                                        <div class="x_car_offer_bottom_btn float_left">
                                            <ul>
                                                <li><a href="#" style="margin-left:50%">More Details</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="x_car_offer_main_boxes_wrapper float_left">
                                        <div class="portfolio">
                                            <img src="/web-assets/img/homepage/Corporate.jpg" alt="img">
                                        </div>
                                        <div class="x_car_offer_price float_left">
                                            <div class="x_car_offer_price_inner">
                                                <h5>Corporate Deal?</h5>
                                            </div>
                                        </div>
                                        <div class="x_car_offer_heading float_left">
                                            <h2>
                                                <a href="#"><i class="fa fa-handshake-o portfolio-icons" aria-hidden="true"></i></a>
                                            </h2>
                                            <h5>Our car rental services are designed to meet your official
                                                transportation needs. Just email us.</h5>
                                        </div>
                                        <div class="x_car_offer_bottom_btn float_left">
                                            <ul>
                                                <li><a href="#" style="margin-left:50%">Contact Us</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- xs offer car tabs End -->
    <!-- btc team Wrapper Start -->
    <div class="btc_team_main_wrapper">
        <div class="btc_team_img_overlay"></div>
        <div class="container">
            <div class="btc_team_left_wrapper">
                <h3>Weekend Deal
                </h3>
                <p>Enjoy weekend rates starting from Tk.3100 per day when you rent on Fridayand Saturday. Special
                    rate includes 100 kilometer per day</p>
                <div class="x_car_offer_bottom_btn float_left">
                    <ul>
                        <li><a href="#" style="margin-left: -25px;">Check Price</a></li>
                    </ul>
                </div>
            </div>
            <div class="btc_team_left_wrapper">
                <h3>Why GARIVARA.com.bd?
                </h3>
                <div class="x_car_offer_bottom_btn float_left">
                    <ul>
                        <li><a href="#" style="margin-left: -25px;">Check Price</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- btc team Wrapper End -->
    <!-- x counter Wrapper Start -->
    <div class="x_counter_main_wrapper">
        <div class="x_counter_img_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_counter_car_heading_wrapper float_left">
                        <h4>Work Process</h4>
                        <h3>How it works?</h3>
                        <p>Morbi mollis vestibulum sollicitudin. Nunc in eros a justo facilisis rutrum. Aenean id
                            ullamcorper libero
                            <br>Vestibulum imperdiet nibh vel magna lacinia commodo ultricies,
                        </p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="x_cou_main_box_wrapper">
                        <div class="x_icon"><i class="flaticon-airplane-shape"></i>
                        </div>
                        <h5><span>1.</span> <a href="#">pick destination</a></h5>
                        <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="x_cou_main_box_wrapper">
                        <div class="x_icon"><i class="flaticon-calendar"></i>
                        </div>
                        <h5><span>2.</span> <a href="#">select term</a></h5>
                        <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="x_cou_main_box_wrapper">
                        <div class="x_icon"><i class="flaticon-sedan-car-front"></i>
                        </div>
                        <h5><span>3.</span> <a href="#">choose a car</a></h5>
                        <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="x_cou_main_box_wrapper x_cou_main_box_wrapper_last">
                        <div class="x_icon"><i class="flaticon-emoticon-square-smiling-face-with-closed-eyes"></i>
                        </div>
                        <h5><span>4.</span> <a href="#">enjoy the ride</a></h5>
                        <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- x counter Wrapper End -->
    <!-- x booking Wrapper Start -->
    <div class="x_booking_main_wrapper float_left">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="x_book_logo_wrapper float_left">
                        <img src="/web-assets/images/white_logo.png" alt="logo">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="x_book_logo_heading_wrapper float_left">
                        <h3>Book on AutoRez Now!</h3>
                        <p>The Most User Centric Rental Theme on the Market.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="x_book_logo_btn float_left">
                        <ul>
                            <li><a href="#">See All Cars <i class="fa fa-arrow-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- x booking Wrapper End -->
    <!-- xs offer car tabs Start -->
    <div class="x_offer_car_main_wrapper float_left padding_tb_100">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_offer_car_heading_wrapper float_left">
                        <h4>Testimonials</h4>
                        <h3>Top Reviews</h3>
                        <p>Morbi mollis vestibulum sollicitudin. Nunc in eros a justo facilisis rutrum. Aenean id
                            ullamcorper libero
                            <br>Vestibulum imperdiet nibh vel magna lacinia commodo ultricies,
                        </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="screenshot">
                        <div class="owl-carousel screen nplr screen-loop">
                            <div>
                                <div class="card  valign-wrapper">
                                    <!-- Client's image -->
                                    <div class="card-image">
                                        <img src="/web-assets/images/client_1.jpg" alt="img">
                                    </div>
                                    <!-- /Client's image -->
                                    <div class="card-content center-align valign">
                                        <div class="testi_slide_star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <!-- Client's Feedback -->
                                        <p>“ I don't always clap, but when I do, it's because of Sella. We can't
                                            understand how we've been living without Sella. ”</p>
                                        <!-- /Client's Feedback -->
                                        <!-- Client's Name -->
                                        <p class="card-title">Irene Marita <span>Support Manager @ Echo</span>
                                        </p>
                                        <!-- /Client's Name -->
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="card  valign-wrapper">
                                    <!-- Client's image -->
                                    <div class="card-image">
                                        <img src="/web-assets/images/client_2.jpg" alt="img">
                                    </div>
                                    <!-- /Client's image -->
                                    <div class="card-content center-align valign">
                                        <div class="testi_slide_star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <!-- Client's Feedback -->
                                        <p>“ I don't always clap, but when I do, it's because of Sella. We can't
                                            understand how we've been living without Sella. ”</p>
                                        <!-- /Client's Feedback -->
                                        <!-- Client's Name -->
                                        <p class="card-title">Irene Marita <span>Support Manager @ Echo</span>
                                        </p>
                                        <!-- /Client's Name -->
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="card  valign-wrapper">
                                    <!-- Client's image -->
                                    <div class="card-image">
                                        <img src="/web-assets/images/client_3.jpg" alt="img">
                                    </div>
                                    <!-- /Client's image -->
                                    <div class="card-content center-align valign">
                                        <div class="testi_slide_star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <!-- Client's Feedback -->
                                        <p>“ I don't always clap, but when I do, it's because of Sella. We can't
                                            understand how we've been living without Sella. ”</p>
                                        <!-- /Client's Feedback -->
                                        <!-- Client's Name -->
                                        <p class="card-title">Irene Marita <span>Support Manager @ Echo</span>
                                        </p>
                                        <!-- /Client's Name -->
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="card  valign-wrapper">
                                    <!-- Client's image -->
                                    <div class="card-image">
                                        <img src="/web-assets/images/client_4.jpg" alt="img">
                                    </div>
                                    <!-- /Client's image -->
                                    <div class="card-content center-align valign">
                                        <div class="testi_slide_star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <!-- Client's Feedback -->
                                        <p>“ I don't always clap, but when I do, it's because of Sella. We can't
                                            understand how we've been living without Sella. ”</p>
                                        <!-- /Client's Feedback -->
                                        <!-- Client's Name -->
                                        <p class="card-title">Irene Marita <span>Support Manager @ Echo</span>
                                        </p>
                                        <!-- /Client's Name -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- btc team Wrapper Start -->
    <div class="x_why_main_wrapper">
        <div class="x_why_img_overlay"></div>
        <div class="container">
            <div class="x_why_left_main_wrapper">
                <img src="/web-assets/images/w1.png" alt="car_img">
            </div>
            <div class="x_why_right_main_wrapper">
                <h3>Why Choose Us ?</h3>
                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, rem a quis bibendum auctor,
                    nisi elit consequat ipsum, nec sagittis sem nibh id elit. Dssed odio sit amet nibh vulputate
                    cursus a sit amt mauris. Morbi accumsan ipsum velit.
                    <br>
                    <br>This is Photoshop's version of Lorem Ipsum. Proin gravida n vel velit auctor aliquet. Aenean
                    sollicitudin, lorem quis bibendum tor. This is Photoshop's version of Lorem Ipsum.
                </p>
                <ul>
                    <li><a href="#">read more <i class="fa fa-arrow-right"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- btc team Wrapper End -->
    <!-- xs offer car tabs Start -->
    <div class="x_ln_car_main_wrapper float_left padding_tb_100">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_ln_car_heading_wrapper float_left">
                        <h3>Latest cars</h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="btc_ln_slider_wrapper">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="btc_team_slider_cont_main_wrapper">
                                    <div class="btc_ln_img_wrapper float_left">
                                        <img src="/web-assets/images/ln1.jpg" alt="team_img1">
                                    </div>
                                    <div class="btc_ln_img_cont_wrapper float_left">
                                        <h4><a href="#">Autoweek in review: Everything you missed Sept. 11-15</a>
                                        </h4>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-calendar"></i> &nbsp; September 19, 2017</a>
                                            </li>
                                            <li><a href="#"><i class="fa fa-user"></i> &nbsp;by Admin</a>
                                            </li>
                                        </ul>
                                        <p>What's your favorite game? Nam a diam tincidunt, condimentum nisi et,
                                            fringilla lectus. Nullam nec lectus..</p>
                                        <span><a href="#">Read More &nbsp;<i class="fa fa-angle-double-right"></i></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="btc_team_slider_cont_main_wrapper">
                                    <div class="btc_ln_img_wrapper float_left">
                                        <img src="/web-assets/images/ln2.jpg" alt="team_img1">
                                    </div>
                                    <div class="btc_ln_img_cont_wrapper float_left">
                                        <h4><a href="#">Rakish Tokyo concept signals hope for Mitsubishi's
                                                lineup</a></h4>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-calendar"></i> &nbsp; September 21, 2017</a>
                                            </li>
                                            <li><a href="#"><i class="fa fa-user"></i> &nbsp;by Admin</a>
                                            </li>
                                        </ul>
                                        <p>What's your favorite game? Nam a diam tincidunt, condimentum nisi et,
                                            fringilla lectus. Nullam nec lectus..</p>
                                        <span><a href="#">Read More &nbsp;<i class="fa fa-angle-double-right"></i></a></span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="btc_team_slider_cont_main_wrapper">
                                    <div class="btc_ln_img_wrapper float_left">
                                        <img src="/web-assets/images/ln3.jpg" alt="team_img1">
                                    </div>
                                    <div class="btc_ln_img_cont_wrapper float_left">
                                        <h4><a href="#">Dinan BMW S2 M4 first drive: Not for everyone's lineup</a>
                                        </h4>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-calendar"></i> &nbsp; September 23, 2017</a>
                                            </li>
                                            <li><a href="#"><i class="fa fa-user"></i> &nbsp;by Admin</a>
                                            </li>
                                        </ul>
                                        <p>What's your favorite game? Nam a diam tincidunt, condimentum nisi et,
                                            fringilla lectus. Nullam nec lectus..</p>
                                        <span><a href="#">Read More &nbsp;<i class="fa fa-angle-double-right"></i></a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('web.home.map-modal')

    <!--js Start-->
    <!-- xs offer car tabs Start -->
    @include('web.components.partners-component')
    <!-- btc team Wrapper Start -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript" src='https://maps.google.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&sensor=false&libraries=places'></script>
    <script src="{{ mix('js/pages/home-page.js') }}"></script>
@endpush
