@extends('web-view.layout')

@section('content')
    <div class="x_partner_main_wrapper float_left padding_tb_100">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="x_offer_car_heading_wrapper float_left">
                    {{--<h3>Success</h3>--}}
                </div>
            </div>
        </div>
        <div class="row form-div">
            <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-12 col-sm-12 col-12">
                <div class="register_wrapper_box">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="alert alert-success">
                            {{ $transaction_message }}
                        </div>
                    </div>
                </div>
                <div class="back_btn_box" id="back_to_my_trips">
                    <button class="btn">Back To My Trips</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
