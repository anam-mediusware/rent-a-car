@extends('web-view.layout')

@section('content')
    <form action="{{ url('/api/user/pay') }}" method="post">
        <input type="hidden" name="user" value='{!! $user ? json_encode($user) : null !!}'>
        <input type="hidden" name="search_params" value='{!! $data ? json_encode($data) : null !!}'>
        <input type="hidden" name="search_result" value='{!! $search_result ? json_encode($search_result) : null !!}'>
        <input type="hidden" name="bid_id" value="{{ $bid_id }}">
{{--        <input type="hidden" name="pricing_type" value="bid">--}}
        <input type="hidden" name="pricing_type" value="{{ $pricing_type }}">
        <div class="pay_btn_box" id="pay_now_btn">
            <button class="btn">Pay Now</button>
        </div>
    </form>
@endsection

@push('scripts')
@endpush
