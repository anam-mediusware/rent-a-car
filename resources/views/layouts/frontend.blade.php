<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> --}}
    @if (isset($commonData) && $commonData->route_title)
        <title>{{ $commonData->route_title }}</title>
    @else
        <title>{{ config('app.name') }}</title>
    @endif

    <meta name="keywords" content="{{ (isset($commonData))?$commonData->route_keyword:'' }}"/>
    <meta name="description" content="{{ (isset($commonData))?$commonData->route_description:'' }}"/>

    <!-- Scripts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('web-assets/css/xpedia.css')}}"/>
    <link rel="stylesheet" href="{{ asset('web-assets/css/app.css')}}"/>
    @stack('styles')
</head>
<body>
<div id="preloader">
    <div id="status">
        <img src="{{ asset('web-assets/images/pulse-trns.gif') }}" id="preloader_image" alt="loader" style="color:red">
    </div>
</div>
<div>
    @include('layouts.inc.header')
    @yield('content')
    <!-- <app-footer></app-footer> -->
</div>
@include('layouts.inc.footer')
<script src="{{ mix('web-assets/js/app.js')}}"></script>
@stack('scripts')
@include('layouts.inc.scripts')
</body>
</html>
