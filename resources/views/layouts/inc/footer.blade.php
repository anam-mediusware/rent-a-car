<div class="x_news_letter_main_wrapper">
    <div class="container">
        <div class="x_news_contact_wrapper">
            <img src="{{ asset('web-assets/images/nl1.png') }}" alt="news_img">
            <h4>Call Us <br> <span>{{ $web_settings->phone }}</span></h4>
        </div>
        <div class="x_news_contact_second_wrapper">
            <h4>Newsletter</h4>
        </div>
        <div class="x_news_contact_search_wrapper">
            <input type="text" placeholder="Email Address">
            <button>read more <i class="fa fa-arrow-right"></i></button>
        </div>
    </div>
</div>
<!-- x news latter Wrapper End -->
