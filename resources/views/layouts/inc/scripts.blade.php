{{--<script src="{{ mix('web-assets/js/manifest.js')}}"></script>--}}
{{--<script src="{{ mix('web-assets/js/vendor.js')}}"></script>--}}
<script src="{{ mix('web-assets/js/custom.js')}}"></script>

{{--<script src="{{ asset('web-assets/js/jquery-3.3.1.min.js')}}"></script>--}}
<script src="{{ asset('web-assets/js/modernizr.js')}}"></script>
<script src="{{ asset('web-assets/js/select2.min.js')}}"></script>
<script src="{{ asset('web-assets/js/jquery.menu-aim.js')}}"></script>
<script src="{{ asset('web-assets/js/jquery-ui.js')}}"></script>
<script src="{{ asset('web-assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{ asset('web-assets/js/owl.carousel.js')}}"></script>
<script src="{{ asset('web-assets/js/own-menu.js')}}"></script>
<script src="{{ asset('web-assets/js/jquery.magnific-popup.js')}}"></script>
<script src="{{ asset('web-assets/js/xpedia.mod.js')}}"></script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#success-booking').hide();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".confirm_btn_close").click(function (e) {
            e.preventDefault();
            booking_service_id = $('#booking_service_id').val();
            booking_vehicle_type_id = $('#booking_vehicle_type_id').val();
            starting_point = $('#starting_point').val();
            destination_point = $('#destination_point').val();
            price = $('#price').val();
            service_category_id = $('#service_category_id').val();
            pickup_date_time = $('#pickup_date_time').val();
            dropoff_date_time = $('#dropoff_date_time').val();
            pricing_type = $('#pricing_type').val();

            $.ajax({
                type: 'POST',
                url: '/booking-confirm',
                data: {
                    service_type_id: booking_service_id,
                    vehicle_type_id: booking_vehicle_type_id,
                    pickup_location: starting_point,
                    drop_off_location: destination_point,
                    price: price,
                    service_category_id: service_category_id,
                    pickup_date_time: pickup_date_time,
                    drop_off_date_time: dropoff_date_time,
                    pricing_type: pricing_type,
                },
                success: function (data) {
                    console.log(data.success);
                    if (data.success) {
                        $message = "Booking confirmed!! Thank you for staying with us";
                        $('#success-booking').html($message);
                        $('#success-booking').show();
                    }
                },
                error: function (err) {
                    console.log(err)
                }
            });
        });

        $('html, body').animate({
            scrollTop: $("body").offset().top
        }, 2000);
    });
</script>
