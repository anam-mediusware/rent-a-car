<div class="serach-header">
    <div class="searchbox">
        <button class="close">×</button>
        <form>
            <input type="search" placeholder="Search …">
            <button type="submit"><i class="fa fa-search"></i>
            </button>
        </form>
    </div>
</div>
<!-- x top header_wrapper Start -->
<div class="x_top_header_wrapper float_left">
    <div class="container">
        <div class="x_top_header_left_side_wrapper float_left">
            <p>Call Us : {{ $web_settings->phone }}</p>
        </div>
        <div class="x_top_header_right_side_wrapper float_left">
            <div class="x_top_header_social_icon_wrapper">
                <ul>
                    @if ($web_settings->facebook)<li><a href="{{ $web_settings->facebook }}"><i class="fa fa-facebook-square"></i></a></li>@endif
                    @if ($web_settings->twitter)<li><a href="{{ $web_settings->twitter }}"><i class="fa fa-twitter-square"></i></a></li>@endif
                    @if ($web_settings->instagram)<li><a href="{{ $web_settings->instagram }}"><i class="fa fa-instagram"></i></a></li>@endif
                    @if ($web_settings->linkedin)<li><a href="{{ $web_settings->linkedin }}"><i class="fa fa-linkedin-square"></i></a></li>@endif
                </ul>
            </div>
            <div class="x_top_header_all_select_box_wrapper">
                <ul>
                    <li class="language">
                        <select class="myselect">
                            <option>EN</option>
                            <option>RO</option>
                            <option>IT</option>
                        </select>
                        <i class="fa fa-globe"></i>
                    </li>
                    <li class="usd">
                        <select class="myselect">
                            <option>USD</option>
                            <option>EUR</option>
                            <option>CAD</option>
                        </select>
                        <i class="fa fa-money"></i>
                    </li>
                    @guest
                        <li class="login">
                            <a href="{{ route('login') }}"><i class="fa fa-power-off"></i> &nbsp;&nbsp;{{ __('Login') }}
                            </a>
                        </li>
                        @if (Route::has('register'))
                            <li class="login">
                                <a href="{{ route('register') }}"><i class="fa fa-plus-circle"></i>
                                    &nbsp;&nbsp;{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="login">
                            <a href="/account"><i class="fa fa-plus-circle"></i> &nbsp;&nbsp;Account</a>
                            <i class="fa fa-user"></i>
                        </li>
                        <li class="login">
                            <a class="javascript:;" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    @endguest
                    <!-- <li>
                        <button class="searchd"><i class="fa fa-search"></i>
                        </button>
                     </li> -->
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- x top header_wrapper End -->
<!-- hs Navigation Start -->
<div class="hs_navigation_header_wrapper">
    <div class="container">
        <div class="row">
            <div class=" col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="hs_logo_wrapper d-none d-sm-none d-xs-none d-md-block">
                    <a href="{{ url('/') }}">
                        <img style="width:110%" src="{{ asset('web-assets/images/logo.png') }}" class="img-responsive" alt="logo" title="Logo"/>
                    </a>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                <nav class="hs_main_menu d-none d-sm-none d-xs-none d-md-block">
                    <ul>
                        <li>
                            <a href="{{url('/')}}" class="menu-button single_menu">Home</a>
                        <li>
                            <div class="dropdown-wrapper menu-button">
                                <a href="{{url('/about-us')}}" class="menu-button">About us</a>
                                <div class="drop-menu">
                                    <a class="menu-button" href="{{url('/management-team')}}">Management Team</a>

                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="{{url('/services')}}" class="menu-button single_menu">services</a>
                        </li>

                        <li>
                            <div class="dropdown-wrapper menu-button">
                                <a class="menu-button" href="#">Pricing</a>
                                <div class="drop-menu">
                                    <a class="menu-button" href="">Pricing download</a>

                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown-wrapper menu-button">
                                <a class="menu-button" href="#">Packages</a>
                                <div class="drop-menu">
                                    <a class="menu-button" href="">Package dropdown</a>
                                </div>
                            </div>
                        </li>


                        <li><a class="menu-button single_menu" href="team.html">Tourism in Bangladesh</a>
                        </li>
                        <li>
                            <button class="searchd" style="color: black;margin-top: 35px;"><i class="fa fa-search"></i>
                            </button>
                        </li>

                    </ul>
                </nav>
                <header class="mobail_menu d-none d-block d-xs-block d-sm-block d-md-none d-lg-none d-xl-none">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-6">
                                <div class="hs_logo">
                                    <a href="index-2.html">
                                        <img src="{{ asset('web-assets/images/logo.png') }}" alt="Logo" title="Logo">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-6">
                                <div class="cd-dropdown-wrapper">
                                    <a class="house_toggle" href="#0">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="511.63px" height="511.631px" viewBox="0 0 511.63 511.631" style="enable-background:new 0 0 511.63 511.631;" xml:space="preserve">
                                       <g>
                                           <g>
                                               <path d="M493.356,274.088H18.274c-4.952,0-9.233,1.811-12.851,5.428C1.809,283.129,0,287.417,0,292.362v36.545
                                                c0,4.948,1.809,9.236,5.424,12.847c3.621,3.617,7.904,5.432,12.851,5.432h475.082c4.944,0,9.232-1.814,12.85-5.432
                                                c3.614-3.61,5.425-7.898,5.425-12.847v-36.545c0-4.945-1.811-9.233-5.425-12.847C502.588,275.895,498.3,274.088,493.356,274.088z"/>
                                               <path d="M493.356,383.721H18.274c-4.952,0-9.233,1.81-12.851,5.427C1.809,392.762,0,397.046,0,401.994v36.546
                                                c0,4.948,1.809,9.232,5.424,12.854c3.621,3.61,7.904,5.421,12.851,5.421h475.082c4.944,0,9.232-1.811,12.85-5.421
                                                c3.614-3.621,5.425-7.905,5.425-12.854v-36.546c0-4.948-1.811-9.232-5.425-12.847C502.588,385.53,498.3,383.721,493.356,383.721z"/>
                                               <path d="M506.206,60.241c-3.617-3.612-7.905-5.424-12.85-5.424H18.274c-4.952,0-9.233,1.812-12.851,5.424
                                                C1.809,63.858,0,68.143,0,73.091v36.547c0,4.948,1.809,9.229,5.424,12.847c3.621,3.616,7.904,5.424,12.851,5.424h475.082
                                                c4.944,0,9.232-1.809,12.85-5.424c3.614-3.617,5.425-7.898,5.425-12.847V73.091C511.63,68.143,509.82,63.861,506.206,60.241z"/>
                                               <path d="M493.356,164.456H18.274c-4.952,0-9.233,1.807-12.851,5.424C1.809,173.495,0,177.778,0,182.727v36.547
                                                c0,4.947,1.809,9.233,5.424,12.845c3.621,3.617,7.904,5.429,12.851,5.429h475.082c4.944,0,9.232-1.812,12.85-5.429
                                                c3.614-3.612,5.425-7.898,5.425-12.845v-36.547c0-4.952-1.811-9.231-5.425-12.847C502.588,166.263,498.3,164.456,493.356,164.456z
                                                "/>
                                           </g>
                                       </g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                            <g></g>
                                    </svg>
                                    </a>
                                    <!-- .cd-dropdown -->
                                </div>
                                <nav class="cd-dropdown">
                                    <h2><a href="index-2.html">Xpedia</a></h2>
                                    <a href="#0" class="cd-close">Close</a>
                                    <ul class="cd-dropdown-content">
                                        <li>
                                            <form class="cd-search">
                                                <input type="search" placeholder="Search...">
                                            </form>
                                        </li>
                                        <li class="has-children">
                                            <a href="#">Home</a>
                                            <ul class="cd-secondary-dropdown is-hidden">
                                                <li class="go-back"><a href="#0">Menu</a>
                                                </li>
                                                <li><a href="index-2.html">Home-I</a>
                                                </li>
                                                <!-- .has-children -->
                                                <li><a href="index_II.html">Home_II</a>
                                                </li>
                                                <!-- .has-children -->
                                            </ul>
                                            <!-- .cd-secondary-dropdown -->
                                        </li>
                                        <li class="has-children">
                                            <a href="#">Car</a>
                                            <ul class="cd-secondary-dropdown is-hidden">
                                                <li class="go-back"><a href="#0">Menu</a>
                                                </li>
                                                <li><a href="car_accessories.html">Car-Accessories</a>
                                                </li>
                                                <li><a href="car_booking.html">Car-Booking</a>
                                                </li>
                                                <li><a href="car_booking_done.html">Car-Booking-Done</a>
                                                </li>
                                                <li><a href="car_checkout.html">Car-Checkout</a>
                                                </li>
                                                <li><a href="car_detail_left.html"> Car-Detail-Left</a>
                                                </li>
                                                <li><a href="car_detail_right.html"> Car-Detail-Right</a>
                                                </li>
                                                <!-- .has-children -->
                                            </ul>
                                            <!-- .cd-secondary-dropdown -->
                                        </li>
                                        <li><a href="about.html">About</a>
                                        </li>
                                        <li><a href="team.html">Our Team</a>
                                        </li>
                                        <li><a href="services.html">Services</a>
                                        </li>
                                        <li class="has-children">
                                            <a href="#">Blog</a>
                                            <ul class="cd-secondary-dropdown is-hidden">
                                                <li class="go-back"><a href="#0">Menu</a>
                                                </li>
                                                <li><a href="blog_category.html">Blog Categories</a>
                                                </li>
                                                <!-- .has-children -->
                                                <li><a href="blog_single.html">Blog Single</a>
                                                </li>
                                                <!-- .has-children -->
                                            </ul>
                                            <!-- .cd-secondary-dropdown -->
                                        </li>
                                        <li><a href="{{route('contact-us')}}">Contact</a>
                                        </li>
                                    </ul>
                                    <!-- .cd-dropdown-content -->
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- .cd-dropdown-wrapper -->
                </header>
            </div>
        </div>
    </div>
</div>
<!-- hs Navigation End -->
