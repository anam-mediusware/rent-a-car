@component('mail::message')
# Registration Successful

Congratulations!
Your have successfully registered to {{ config('app.name') }}.

## Login Details:
Mobile: {{ $merchant->contact }}<br>
Password: {{ $merchant->password }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
