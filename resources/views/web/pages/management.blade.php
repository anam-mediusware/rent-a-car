@extends('layouts.frontend')

@section('content')
    <div>
        <div class="x_ln_car_main_wrapper float_left padding_tb_100">
            <div class="container">
                @include('web.components.team-component')
            </div>
        </div>
    </div>
@endsection
