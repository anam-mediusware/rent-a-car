window.vueApp = new Vue({
    el: '#home-page',
    data: {
        ServiceCategoryList: [],
        ServiceList: [],
        carModels: [],
        airportCites: [],
        airportCity: {},
        activePhase: 1,
        isCategorySelected: false,
        ServiceCat: '',
        SearchedPrice: false,

        filter: {
            vehicle_type: '1',
            car_model_id: '',
            class: '1',
            pick_up_date: '',
            pick_up_time: '',
            drop_off_date: '',
            drop_off_time: '',
            airport_city_id: 1,
            starting_point: '',
            starting_point_geo: {},
            ending_point: '',
            ending_point_geo: {},
            service_type_id: '',
            service_category_id: 1,
        },

        map: null,
        mapCenter: null,
        whichMap: null,
        isOutOfCity: false,
    },
    computed: {
        totalPrice() {
            let price = parseFloat(this.SearchedPrice.price);
            if (this.SearchedPrice.additional_pricings) {
                this.SearchedPrice.additional_pricings.forEach(additionalPrice => {
                    price += parseFloat(additionalPrice.price);
                })
            }
            return price;
        }
    },
    watch: {
        'filter.class': {
            handler(val, oldVal) {
                this.getCarModels();
            },
            deep: true
        },
        'filter.vehicle_type': {
            handler(val, oldVal) {
                this.getCarModels();
            },
            deep: true
        }
    },
    mounted() {
        this.getCarModels();
        this.$nextTick(() => {
            this.googleAutocompleteFrom();
            this.googleAutocompleteTo();
            this.SetServiceCategory();
            // document.addEventListener('scroll', this.handleScroll)
        });
        this.initMap();
    },
    created() {
        // window.addEventListener('scroll', this.handleScroll);
        this.GetServiceCategories();
    },
    methods: {
        handleChange(value) {
            console.log(`Selected: ${value}`);
        },
        onChangeValue(e) {
            console.log(`checked = ${e.target.value}`);
            this.handleChangeAirport();
        },
        handleChangeAirport() {
            const id = this.filter.airport_city_id;
            const airport = this.airportCites.filter(item => {
                return item.id === id;
            })[0];
            this.airportCity = airport;
            if (this.filter.service_type_id === 8) {
                this.filter = {
                    ...this.filter,
                    starting_point: '',
                    starting_point_geo: {},
                    ending_point: airport.airport,
                    ending_point_geo: airport.geo,
                }
            } else if (this.filter.service_type_id === 9) {
                this.filter = {
                    ...this.filter,
                    starting_point: airport.airport,
                    starting_point_geo: airport.geo,
                    ending_point: '',
                    ending_point_geo: {},
                }
            }
        },
        searchRequest() {
            let filter = {...this.filter};
            try {
                filter.pick_up_time = filter.pick_up_time.toLocaleTimeString('en-US');
            } catch (e) {
                toastr.error('Invalid Pickup time');
                return;
            }
            try {
                filter.drop_off_time = filter.drop_off_time.toLocaleTimeString('en-US');
            } catch (e) {
                toastr.error('Invalid Drop off time');
                return;
            }
            if (this.isOutOfCity) {
                toastr.error('Out Of City');
                return;
            }

            axios.get('/api/services', {
                params: filter
            }).then(response => {
                console.log(response)
                if (response.data.success) {
                    this.activePhase = 4;
                    this.SearchedPrice = response.data.data
                } else {
                    if (response.data.errors) {
                        Object.keys(response.data.errors).forEach((index) => {
                            toastr.error(response.data.errors[index][0])
                        })
                    } else {
                        this.activePhase = 4;
                        this.SearchedPrice = false
                    }
                }
            }).catch(error => this.toastError(error));
        },
        GetServiceCategories() {
            axios.get('/service-categories').then(response => {
                this.ServiceCategoryList = response.data.serviceCategories
            }).catch(error => this.toastError(error));
        },
        SetServiceCategory() {
            if (this.filter.service_category_id) {
                this.isCategorySelected = true;
                axios.get('/services-by-category/' + this.filter.service_category_id).then(response => {
                    this.ServiceList = response.data.Services
                }).catch(error => this.toastError(error));
                if (this.filter.service_category_id === 3) {
                    axios.get('/api/get-airport-cities').then(response => {
                        this.airportCites = response.data.data
                    }).catch(error => this.toastError(error));
                }
            }
        },
        proceedToPhase(phaseNumber = 1) {
            this.activePhase = phaseNumber;
            if (this.filter.service_category_id) {
                this.activePhase = phaseNumber;
            } else {
                return 0;
            }
            if (this.activePhase === 3) {
                this.$nextTick(() => {
                    this.googleAutocompleteFrom();
                    this.googleAutocompleteTo();
                });
            }
        },
        GoBackToFirstPhase() {
            if (this.filter.service_category_id) {
                this.activePhase = 1;
            }
        },
        getCarModels() {
            axios.get('/api/get-car-models', {
                params: {
                    service_quality_type: this.filter.class,
                    vehicle_type_id: this.filter.vehicle_type,
                }
            }).then(response => {
                if (response.data.success) {
                    this.carModels = response.data.data
                } else {
                    this.carModels = [];
                }
            }).catch(error => this.toastError(error));
        },
        getReverseLocation(lat, lng) {
            axios.get('/api/google/reverse/', {
                params: {lat, lng}
            }).then(response => {
                if (response.data.success) {
                    if (this.whichMap === 'from') {
                        this.filter.starting_point = response.data.data.formatted_address;
                    } else if (this.whichMap === 'to') {
                        this.filter.ending_point = response.data.data.formatted_address;
                    }
                } else {
                    this.filter.starting_point = '';
                }
            }).catch(error => this.toastError(error));
        },
        initMap() {
            var mapOptions = {
                zoom: 17,
                center: new google.maps.LatLng(0, 0),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false
            };
            this.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
            $('<div/>').addClass('centerMarker').appendTo(this.map.getDiv());
            google.maps.event.addListener(this.map, 'dragend', () => {
                this.mapCenter = {lat: this.map.center.lat(), lng: this.map.center.lng()};
                console.log(this.mapCenter);
                this.getReverseLocation(this.mapCenter.lat, this.mapCenter.lng)
            });
        },
        openMapModal(center) {
            console.log(center.lat, center.lon)
            this.map.setCenter(new google.maps.LatLng(center.lat, center.lon));
            $('#mapModal').modal();
        },
        googleAutocompleteFrom() {
            const _this = this;
            let element = document.getElementById('starting_point');
            if (!element) return;
            var options = {componentRestrictions: {country: 'bd'}, types: ['geocode']}
            let autocomplete_from = new google.maps.places.Autocomplete(element, options);
            autocomplete_from.addListener('place_changed', async function () {
                const place = autocomplete_from.getPlace();
                _this.filter.starting_point = place.formatted_address;
                const geo = {
                    lat: place.geometry.location.lat(),
                    lon: place.geometry.location.lng()
                };
                await _this.isInsideRange(geo).then((response) => {
                    _this.isOutOfCity = false;
                    if (response) {
                        _this.filter.starting_point_geo = geo;
                        _this.whichMap = 'from';
                        _this.openMapModal(geo);
                    } else {
                        _this.isOutOfCity = true;
                        toastr.error('Out of City')
                    }
                })
            })
        },
        googleAutocompleteTo() {
            const _this = this;
            let element = document.getElementById('ending_point');
            if (!element) return;
            var options = {componentRestrictions: {country: 'bd'}, types: ['geocode']}
            let autocomplete_to = new google.maps.places.Autocomplete(element, options);
            autocomplete_to.addListener('place_changed', async function () {
                const place = autocomplete_to.getPlace();
                _this.filter.ending_point = place.formatted_address;
                const geo = {
                    lat: place.geometry.location.lat(),
                    lon: place.geometry.location.lng()
                };
                await _this.isInsideRange(geo).then((response) => {
                    _this.isOutOfCity = false;
                    if (response) {
                        _this.filter.ending_point_geo = geo;
                        _this.whichMap = 'to';
                        _this.openMapModal(geo);
                    } else {
                        _this.isOutOfCity = true;
                        toastr.error('Out of City')
                    }
                })
            })
        },
        async isInsideRange(region) {
            return await axios.get('/api/is-inside-district', {
                params: {
                    district: this.airportCity.Dist_ID,
                    lat: region.lat,
                    lon: region.lon,
                },
            }).then((response) => response.data.success);
        },
        handleScroll(event) {
            console.log('ssssssssss')
            this.googleAutocompleteFrom();
            this.googleAutocompleteTo();
        }
    }
});
