<style>
    .service-img {
        vertical-align: middle;
        border-style: none;
        width: 50px;
        padding: 5px 5px 5px 5px;
    }

    .radio {
        position: absolute;
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* IMAGE STYLES */
    .radio + img {
        cursor: pointer;
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
        color: red;
        font-weight: bold;
    }

    .radio:checked + img {
        -webkit-filter: grayscale(0%);
        filter: grayscale(0%);
        border: 2px solid red;
        border-radius: 8px;
    }

    .portfolio img {
        width: 100%;
        border-top-right-radius: 10px;
        border-top-left-radius: 10px;
    }

    .x_car_offer_price_inner {
        width: 85% !important;
    }

    .x_car_offer_price_inner h5 {
        padding: 10px;
    }

    .portfolio-icons {
        font-size: 46px;
        padding: 10px;
        color: red;
    }

    .ant-radio-button-wrapper {
        margin-left: 5px !important;
        color: red !important;
    }

    .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled):hover {
        border-color: red !important;
    }

    .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled):active {
        border-color: red !important;
    }

    .search-btn {
        float: left;
        width: 150px;
        height: 50px;
        line-height: 45px;
        text-align: center;
        background: red;
        color: #ffffff;
        border: 1px solid transparent;
        text-transform: uppercase;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
    }

    .ant-radio-button-wrapper {
        margin-top: 7px !important;
    }

    .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled):first-child {
        border-color: white !important;
        box-shadow: none !important;
        background: red !important;
    }

    .ant-checkbox-checked .ant-checkbox-inner {
        background-color: red;
        color: white;
        border-color: red;
    }

    .ant-radio-button-wrapper-checked span {
        color: white;
    }

    .search-btn-group {
        background: red !important;
        color: white !important;
        font-weight: bold !important;
        width: auto !important;
    }

    .mx-datepicker {
        position: relative;
        display: inline-block;
        width: 100% !important;
        margin-top: 10px !important;
    }

    .mp-input {
        color: red !important;
    }

    .mx-input:hover, .mx-input:focus {
        border-color: red;
    }

    .mx-time-column .mx-time-item.active {
        color: red !important;
        background-color: transparent;
        font-weight: 700;
    }

    .mx-datepicker-main {
        font: 14px/1.5 "Helvetica Neue", Helvetica, Arial, "Microsoft Yahei", sans-serif;
        color: red !important;
    }

    .mx-icon-calendar, .mx-icon-clear {
        color: red !important;
    }

    /*
    customize input search google
    https://developers.google.com/maps/documentation/javascript/places-autocomplete#style-autocomplete
    */
    .pac-container {
        border-radius: 10px;
    }

    .pac-container .pac-item {
        padding: 8px 4px 8px 32px !important;
        font-size: 12px !important;
        position: relative;
    }

    .pac-container .pac-item:before {
        font-family: "FontAwesome";
        content: "\f276";
        font-size: 18px;
        position: absolute;
        left: 8px;
        color: #00a651;
    }

    .pac-icon.pac-icon-marker {
        display: none !important;
    }

    .pac-container .pac-item-query {
        font-size: 14px !important;
        padding-right: 5px;
        color: red !important;
    }

    .pac-logo:after {
        display: none !important;
    }

    .pac-matched {
        font-weight: bold;
    }

    .map-modal-ok-btn {
        float: left;
        width: 150px;
        height: 50px;
        line-height: 50px;
        text-align: center;
        background: red;
        color: #ffffff;
        text-transform: uppercase;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
    }

    .map-box {
        z-index: 9000
    }

    #map_canvas {
        height: 400px;
        margin: 0;
        z-index: 9001
    }

    #map_canvas .centerMarker {
        position: absolute;
        /*url of the marker*/
        background: url(http://maps.gstatic.com/mapfiles/markers2/marker.png) no-repeat;
        /*center the marker*/
        top: 50%;
        left: 50%;
        z-index: 9002;
        /*fix offset when needed*/
        margin-left: -10px;
        margin-top: -34px;
        /*size of the image*/
        height: 34px;
        width: 20px;
        cursor: pointer;
    }
</style>
