<div class="row">
    <div class="col-md-12">
        <div class="x_ln_car_heading_wrapper float_left">
            <h3>Our Management</h3>
        </div>
    </div>
    <div class="col-md-12">
        <div class="btc_ln_slider_wrapper">
            <div class="owl-carousel owl-theme">
                @foreach($records as $row)
                <div class="item">
                    <div class="btc_team_slider_cont_main_wrapper">
                        <div class="btc_team_img_wrapper">
                            <img src="{{ $row->avatar }}" alt="{{ $row->name }}" height="330" width="370">
                            <div class="btc_team_social_wrapper">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="btc_team_img_cont_wrapper">
                            <h4><a href="#">{{ $row->name }}</a></h4>
                            <p>({{ $row->designation }})</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
