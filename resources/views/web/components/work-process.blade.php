<!-- x counter Wrapper Start -->
<div class="x_counter_main_wrapper">
    <div class="x_counter_img_overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="x_counter_car_heading_wrapper float_left">
                    <h4>Work Process</h4>
                    <h3>How it works?</h3>
                    <p>Morbi mollis vestibulum sollicitudin. Nunc in eros a justo facilisis rutrum. Aenean id
                        ullamcorper libero
                        <br>Vestibulum imperdiet nibh vel magna lacinia commodo ultricies,</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="x_cou_main_box_wrapper">
                    <div class="x_icon"><i class="flaticon-airplane-shape"></i>
                    </div>
                    <h5><span>1.</span> <a href="#">pick destination</a></h5>
                    <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="x_cou_main_box_wrapper">
                    <div class="x_icon"><i class="flaticon-calendar"></i>
                    </div>
                    <h5><span>2.</span> <a href="#">select term</a></h5>
                    <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="x_cou_main_box_wrapper">
                    <div class="x_icon"><i class="flaticon-sedan-car-front"></i>
                    </div>
                    <h5><span>3.</span> <a href="#">choose a car</a></h5>
                    <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <div class="x_cou_main_box_wrapper x_cou_main_box_wrapper_last">
                    <div class="x_icon"><i class="flaticon-emoticon-square-smiling-face-with-closed-eyes"></i>
                    </div>
                    <h5><span>4.</span> <a href="#">enjoy the ride</a></h5>
                    <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- x counter Wrapper End -->
