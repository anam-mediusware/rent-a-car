@extends('admin.layouts.master')

@php
    $pageName = 'Car Models';
    $pageResource = 'admin.car-models';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li {{ (isset($lists))?'class=active':'' }}>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif

                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') . qString() }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                        <div class="box-header text-right">
                            <div class="row">

                                <div class="form-group">
                                    <select class="form-control select2" name="car_brand">
                                        <option value="">Car Brands</option>
                                        @if (!empty($car_brands))
                                            @foreach ($car_brands as $car_brand)
                                                <option value="{{$car_brand}}">{{$car_brand}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Search">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-flat">Search</button>
                                    <a class="btn btn-warning btn-flat" href="{{ route($pageResource.'.index') }}">X</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="box-body table-responsive">
                        <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                        <table class="table table-bordered table-hover dataTable">
                            <caption class="hidden"><h3>{{ $pageName }} List</h3></caption>
                            <thead>
                            <tr>
                                <th>SL.</th>
                                <th>Passenger</th>
                                <th>Car Maker</th>
                                <th>Car Model</th>
                                <th>Created at</th>
                                <th class="not-export-col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $key => $record)
                                <tr>
                                    <td>{{$serial++}}</td>
                                    <td>{{$record->vehicleType->vehicle_type_english}}</td>
                                    <td>{{$record->car_brand}}</td>
                                    <td>{{$record->car_model}}</td>
                                    <td>{{formatDateTime($record->created_at)}}</td>
                                    <td>
                                        @php
                                            $access = 1;
                                            listAction([
                                                actionLi(route($pageResource.'.show', $record->id).qString(), 'show', $access),
                                                actionLi(route($pageResource.'.edit', $record->id).qString(), 'edit', $access),
                                                actionLi(route($pageResource.'.destroy', $record->id).qString(), 'delete', $access),
                                            ]);
                                        @endphp
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-right">
                            {{ $records->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
