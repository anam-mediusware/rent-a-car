<div class="form-group{{ $errors->has('vehicle_type_id') ? ' has-error' : '' }}">
    <label for="vehicle_type_id" class="control-label col-sm-3 required">Passenger:</label>
    <div class="col-sm-9">
        @php($vehicle_type_id = old('vehicle_type_id', $data->vehicle_type_id))
        <select class="form-control" name="vehicle_type_id" id="vehicle_type_id" required>
            <option value="">Select Passenger</option>
            @if (!empty($vehicle_types))
                @foreach ($vehicle_types as $vehicle_type)
                    <option value="{{$vehicle_type->id}}" {{ ($vehicle_type_id==$vehicle_type->id)?'selected':'' }}>{{ $vehicle_type->vehicle_type_english }}</option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('vehicle_type_id'))
            <span class="help-block"><strong>{{ $errors->first('vehicle_type_id') }}</strong></span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('car_brand') ? ' has-error' : '' }}">
    <label for="car_brand" class="control-label col-sm-3 required">Car Maker:</label>
    <div class="col-sm-9">
        <?php $brand = (isset($data->car_brand)) ? $data->car_brand : old('car_brand'); ?>
        <select class="form-control select2" id="car_brand" name="car_brand" required>
            <option value="">Select Brand</option>
            @if ($car_brands)
                @foreach ($car_brands as $car_brand)
                    <option value="{{ $car_brand }}" {{ $brand==$car_brand?'selected':''}}>{{ $car_brand }}</option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('car_brand'))
            <span class="help-block">
                <strong>{{ $errors->first('car_brand') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('car_model', $data->car_model) ? ' has-error' : '' }}">
    <label for="car_model" class="control-label col-sm-3 required">Car Model:</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" name="car_model" id="car_model" value="{{ old('car_model', $data->car_model) }}" required>

        @if ($errors->has('car_model'))
            <span class="help-block"><strong>{{ $errors->first('car_model') }}</strong></span>
        @endif
    </div>
</div>
