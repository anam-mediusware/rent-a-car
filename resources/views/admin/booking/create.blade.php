@extends('admin.layouts.master')

@push('styles')
    <style>
        #re_pass_error{
            color: #a94442;
            font-style: italic;
        }
    </style>
    {!! (!isset($inv->booking_id))?'<style>.invoiceDiv{display:none;}</style>':'' !!}
@endpush

<?php
$pageName = 'Booking Create';
$pageResource = 'admin.booking';

if (!isset($data)) {
    $data = new \App\Models\Booking();
}
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="{{ route($pageResource.'.create') . qString() }}">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                </a>
            </li>
        </ul>

        <div class="tab-content" id="booking-form">
            <div class="tab-pane active">
                <div class="box-body">
                    <form method="POST" action="{{ url('admin/booking') }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data" onsubmit="return bookingSubmit()">
                        @csrf

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('customer_type') ? ' has-error' : '' }}">
                                    <label class="control-label col-sm-3 required">Customer Type:</label>
                                    <div class="col-sm-9">
                                        <select name="customer_type" id="customer_type" class="form-control" onchange="customerType(this.value)">
                                            <option value="">Select Customer Type</option>
                                            <option value="1" {{ old('customer_type') == 1 ? 'selected' : '' }}>New
                                            </option>
                                            <option value="2" {{ old('customer_type') == 2 ? 'selected' : '' }}>Existing
                                            </option>
                                        </select>
                                        @if ($errors->has('customer_type'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('customer_type') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div id="new_client_box" style="display: {{ old('customer_type') == 1 ? 'block' : 'none' }}">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3 required">Name:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3 required">Mobile:</label>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" name="mobile" value="{{ old('mobile') }}" required>

                                            @if ($errors->has('mobile'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('mobile') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3">Address:</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="address" rows="5">{{ old('address') }}</textarea>

                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3 required">Email:</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" autocomplete="off" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3 required">Password:</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" autocomplete="off" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('retype_password') ? ' has-error' : '' }}">
                                        <label class="control-label col-sm-3 required">Retype&nbsp;Password:</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" name="retype_password" id="retype_password" value="{{ old('retype_password') }}" required>
                                            <span id="re_pass_error" style="display: none"></span>
                                            @if ($errors->has('retype_password'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('retype_password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('existing_client') ? ' has-error' : '' }}" style="display: {{ old('customer_type') == 2 ? 'block' : 'none' }}" id="existing_client_box">
                                    <label class="control-label col-sm-3 required">Existing Client:</label>
                                    <div class="col-sm-9">
                                        <select name="existing_client" id="existing_client" class="form-control select2" required>
                                            <option value="">Select Existing Client</option>
                                            @if (!empty($client))
                                                @foreach ($client as $val)
                                                    <option value="{{$val->id}}" {{ old('existing_client') == $val->id ? 'selected' : 'none' }}>{{ucwords($val->name)}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('existing_client'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('existing_client') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                @include('admin.booking.partials.booking-common-input')

                                <div class="form-group{{ $errors->has('package_additional_price') ? ' has-error' : '' }}">
                                    <div class="col-sm-3 text-right">
                                        <label class="control-label">Package Additional Price:</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <table class="table table-bordered table-additional" id="dynamic_field">
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control" name="additional_price[0][label]" id="additional_label_0" placeholder="Price label"/>
                                                </td>
                                                <td>
                                                    <input type="number" class="form-control" name="additional_price[0][price]" id='additional_price_0' placeholder="Price" value=""/>
                                                </td>
                                                <td>
                                                    <button type="button" name="add" id="add" class="btn btn-success">
                                                        Add More
                                                    </button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @include('admin.booking.partials.payment-disbursement-form')

                            @include('admin.booking.partials.invoice-form')
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success btn-flat btn-lg">Book</button>
                                    <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
    @include('admin.booking.js.form-scripts')
    @include('admin.booking.js.vue-js')
    @include('admin.booking.partials.form-scripts')
@endpush
