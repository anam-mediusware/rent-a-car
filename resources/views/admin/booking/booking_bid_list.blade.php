@extends('admin.layouts.master')

<?php
$pageName = 'Booking Bid';
$pageResource = 'admin.booking';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.index') . qString() }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>

         <li>
            <a href="{{ route($pageResource.'.create') . qString() }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
            </a>
        </li>

    </ul>

    <div class="tab-content">

        <div class="tab-pane active">
            <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">

                        <div class="form-group">
                            <select class="form-control select2" name="customer_id">
                                <option value="">Customer Name</option>
                                @if (!empty($customer))
                                    @foreach ($customer as $cust)
                                        <option value="{{$cust->id}}">{{$cust->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control select2" name="service_category_id">
                                <option value="">Service Category</option>
                                @if (!empty($serviceCategorie))
                                    @foreach ($serviceCategorie as $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control select2" name="service_id">
                                <option value="">Service</option>
                                @if (!empty($services))
                                    @foreach ($services as $service)
                                        <option value="{{$service->id}}">{{$service->service_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <select class="form-control select2" name="vehicle_type_id">
                                <option value="">Vehicle Type</option>
                                @if (!empty($vehicle_type))
                                    @foreach ($vehicle_type as $vehicle)
                                        <option value="{{$vehicle->id}}">{{$vehicle->vehicle_type_english}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Search">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ route($pageResource.'.index') }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Customer Name</th>
                            <th>Service category</th>
                            <th>Service Type</th>
                            <th>Vehicle Type</th>
                            <th>Price Type</th>
                            <th>Pickup Location</th>
                            <th>Drop off Location</th>
                            <th>Pickup Date Time</th>
                            <th>Drop Off Date Time</th>
                            <th>Trip Status</th>
                            <th>Payment Status</th>
                            <th>Created at</th>
                            <th class="not-export-col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $key => $val)
                        <tr>
                            <td>{{$serial++}}</td>
                            <td>{{$val->user->name}}</td>
                            <td>{{$val->serviceCategory->category_name??null}}</td>
                            <td>{{$val->service->service_name}}</td>
                            <td>{{$val->vehicleType->vehicle_type_english}}</td>
                            <td>{{ucfirst($val->pricing_type)}}</td>
                            <td>{{ucwords($val->upazilaBoundaryFrom ? $val->upazilaBoundaryFrom->Upaz_name : '')}}</td>
                            <td>{{ucwords($val->upazilaBoundaryTo ? $val->upazilaBoundaryTo->Upaz_name : '')}}</td>
                            <td>{{formatDateTime($val->pick_up_date_time)}}</td>
                            <td>{{formatDateTime($val->drop_off_date_time)}}</td>
                            <td>{{$val->booking_status}}</td>
                            <td>{{$val->status}}</td>
                            <td>{{formatDateTime($val->created_at)}}</td>
                            <td>
                            <?php
                            $access = 1;
                            listAction([
                                actionLi(route($pageResource.'.edit', $val->id).qString(), 'edit', $access),
                            ]);
                            ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>

    </div>
</section>

@endsection
