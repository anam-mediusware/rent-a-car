<script type="application/javascript">
    function customerType(id)
    {
        $('#new_client_box').hide();
        $('#existing_client_box').hide();

        if (id == 1) {
            $('#new_client_box').show();
        }

        if (id == 2) {
            $('#existing_client_box').show();
        }
    }
    $(document).ready(function () {
        var i = 0;
        $('#add').click(function () {

            var ad_label = $('#additional_label_0').val();
            var ad_price = $('#additional_price_0').val();
            $('#additional_label_0').css('border', '1px solid #d2d6de');
            $('#additional_price_0').css('border', '1px solid #d2d6de');
            if (ad_label == "") {
                $('#additional_label_0').css('border', '1px solid red');
                $('#additional_price_0').css('border', '1px solid red');
            } else if (ad_price == "") {
                $('#additional_price_0').css('border', '1px solid red');
            } else {
                i++;
                $('#dynamic_field').append('<tr id="row' + i + '"><td><input type="text" class="form-control" name="additional_price[' + i + '][label]" id="additional_label_' + i + '" placeholder="Price label" required/></td><td><input type="number" class="form-control" name="additional_price[' + i + '][price]" id="additional_price_' + i + '" placeholder="Price" value="" required/></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
            }
        });

        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

    });

    $(function () {
        $("#pick_up_date_time").datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y H:i',
            formatDate: 'd/m/Y H:i',
            timepicker: true,
            datepicker: true,
        });

        $("#drop_off_date_time").datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y H:i',
            formatDate: 'd/m/Y H:i',
            timepicker: true,
            datepicker: true,
        });
    });

    function bookingSubmit() {
        $('#re_pass_error').hide();
        var customer_type = $('#customer_type').val();

        if (customer_type == 1) {
            var password = $('#password').val();
            var retype_password = $('#retype_password').val();
            if (password != retype_password) {
                $('#re_pass_error').show();
                $('#re_pass_error').html('Password does not match')
                return false;
            }
        }

        return true;
    }
</script>
