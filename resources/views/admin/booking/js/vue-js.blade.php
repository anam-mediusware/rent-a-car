<script type="text/javascript" src='https://maps.google.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&sensor=false&libraries=places'></script>

<script>
    const drop_off_point = '{!! old("drop_off_point", $data->drop_off_point) !!}';
    const pick_up_point = '{!! old("pick_up_point", $data->pick_up_point) !!}';
    const service_category = '{!! old("service_category_id", $data->service_category_id) !!}';
    const service_type = '{!! old("service_type_id", $data->service_type_id) !!}';
    const airports = {!! $airports !!};
    const service_types = {!! isset($services) ? $services : json_encode([]) !!};
    window.vueApp = new Vue({
        el: '#booking-form',
        data: {
            drop_off_point: drop_off_point,
            pick_up_point: pick_up_point,
            airports: airports,
            service_category: service_category,
            service_type: service_type,
            service_types: service_types,
        },
        computed: {
            pick_up_readOnly: function () {
                return this.service_category === '3' && (this.service_type === '' || this.service_type === '8');
            },
            drop_off_readOnly: function () {
                return this.service_category === '3' && this.service_type === '9';
            },
        },
        mounted() {
            if (this.service_category === '3' && this.service_type === '9') {
                $('#pick_up_location_div').hide();
                $('#airport_div').show();
            }
            if (this.service_category === '3' && (this.service_type === '' || this.service_type === '8')) {
                $('#drop_off_location_div').hide();
                $('#airport_div').show();
            }
            this.googleAutocompleteFrom();
            this.googleAutocompleteTo();
        },
        methods: {
            googleAutocompleteFrom() {
                let element = document.getElementById('pick_up_location');
                if (!element) return;
                var options = {componentRestrictions: {country: 'bd'}, types: ['geocode']}
                let autocomplete_from = new google.maps.places.Autocomplete(element, options);
                autocomplete_from.addListener('place_changed', async () => {
                    const place = autocomplete_from.getPlace();
                    const geo = {
                        lat: place.geometry.location.lat(),
                        lon: place.geometry.location.lng()
                    };
                    this.pick_up_point = JSON.stringify(geo);
                })
            },
            googleAutocompleteTo() {
                let element = document.getElementById('drop_off_location');
                if (!element) return;
                var options = {componentRestrictions: {country: 'bd'}, types: ['geocode']}
                let autocomplete_to = new google.maps.places.Autocomplete(element, options);
                autocomplete_to.addListener('place_changed', async () => {
                    const place = autocomplete_to.getPlace();
                    const geo = {
                        lat: place.geometry.location.lat(),
                        lon: place.geometry.location.lng()
                    };
                    this.drop_off_point = JSON.stringify(geo);
                })
            },
            handleChangeServiceCategory(id) {
                $('#pick_up_location_div').show();
                $('#drop_off_location_div').show();
                $('#airport_div').hide();
                if (id === '3') {
                    $('#airport_div').show();
                    $('#pick_up_location_div').hide();
                    $('#drop_off_location_div').hide();
                }
                axios.get(base_url + `/common-api/services/service-category/${id}`).then((response) => {
                    this.service_types = response.data.data;
                    this.service_type = '';
                });
            },
            handleChangeServiceType(id) {
                this.pick_up_point = this.drop_off_point = '';
                $('#airport_id').val('');
                $('#pick_up_location_div').show();
                $('#drop_off_location_div').show();
                if (id === '8') {
                    $('#drop_off_location_div').hide();
                } else if (id === '9') {
                    $('#pick_up_location_div').hide();
                }
            },
            handleChangeAirport(id) {
                const selectedAirport = this.airports.filter(item => {
                    return item.id === parseInt(id);
                });
                let geo = '';
                if (selectedAirport.length) {
                    geo = JSON.stringify(selectedAirport[0].geo);
                }
                this.pick_up_point = this.drop_off_point = '';
                $('#pick_up_location').val('');
                $('#drop_off_location').val('');
                console.log('this.service_type', this.service_type)
                if (this.service_type.toString() === '8') {
                    this.drop_off_point = geo;
                    $('#drop_off_location').val(selectedAirport[0].airport);
                } else if (this.service_type.toString() === '9') {
                    this.pick_up_point = geo;
                    $('#pick_up_location').val(selectedAirport[0].airport);
                }
            }
        }
    });
</script>
