<script>
    {{--var parentServices = @json($parentServices);--}}
    {{--var bookServices = @json($bookServices);--}}
    function showService() {
        var key = $('#service_parent').val();
        var htmlOpt = '<option value="">Choose Package</option>';
        if (bookServices[key] != undefined) {
            for (let i = 0; i < bookServices[key].length; i++) {
                htmlOpt += '<option value="'+bookServices[key][i].id+'">'+bookServices[key][i].service_name+'</option>';
            }
        }
        $('#service_sub').html(htmlOpt);

        if (parentServices[key].service_group=='Inter District') {
            $('#district-inter').show();
            $('#district-inter select').attr('required', true);
            getInterDis();
        } else {
            $('#district-inter').hide();
            $('#district-inter select').val('');
            $('#district-inter select').attr('required', false);
        }

        var serviceParentTxt = $('#service_parent option:selected').text();
        if(serviceParentTxt=='Airport Transportation') {
            $('#airport_serviceDIV').show();
        } else {
            $('#airport_serviceDIV').hide();
        }
    }

    var interDisRes = [];
    function getInterDis() {
        $.ajax({
            type: "POST",
            url: '/inter-district-ajax',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( response ) {
                if (response.status==1) {
                    var pickUpLocHtml = '';
                    var pickUpLoc = response.pickUpLoc;
                    for (let i = 0; i < pickUpLoc.length; i++) {
                        pickUpLocHtml += '<option value="'+pickUpLoc[i]+'">'+pickUpLoc[i]+'</option>';
                    };
                    $('#inter_district_from').html(pickUpLocHtml);


                    var dropUpLocHtml = '';
                    var dropUpLoc = response.dropUpLoc[pickUpLoc[0]];
                    for (let i = 0; i < dropUpLoc.length; i++) {
                        dropUpLocHtml += '<option value="'+dropUpLoc[i]+'">'+dropUpLoc[i]+'</option>';
                    };
                    $('#inter_district_to').html(dropUpLocHtml);

                    interDisRes = response.dropUpLoc;
                } else {
                    $('#inter_district_from').html('<option value="">Select</option>');
                    $('#inter_district_to').html('<option value="">Select</option>');
                    alerts(response.message);
                }
            }
        });
    }

    function getUnion() {
        var inter_district_from = $('#inter_district_from').val();

        var dropUpLocHtml = '';
        var dropUpLoc = interDisRes[inter_district_from];
        for (let i = 0; i < dropUpLoc.length; i++) {
            dropUpLocHtml += '<option value="'+dropUpLoc[i]+'">'+dropUpLoc[i]+'</option>';
        };
        $('#inter_district_to').html(dropUpLocHtml);
    }

    function getUser() {
        var user_mobile = $('#user_mobile').val();
        if (user_mobile!='' && user_mobile.length>10) {
            $.ajax({
                type: "POST",
                url: '/admin/booking/ajax-find-user',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: { user_mobile:user_mobile },
                success: function(data) {
                    $('#user_id').val(data.id);
                    $('#user_name').val(data.name);
                    $('#user_email').val(data.email);
                    $('#user_address').val(data.address);
                    $('#pick_location').val(data.address);
                    console.log(data);
                },
                error: function() {
                    alerts('System error. Please try after sometimes.');
                }
            });
        }
    }

    function countDay(k) {
        var startDate = $('#pick_datetime').val();
        var endDate = $('#drop_datetime').val();

        if (k==1) {
            $('#drop_datetime').val(startDate);
            endDate = startDate;
        }

        var date1 = new Date(startDate);
        var date2 = new Date(endDate);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        if (diffDays>0) {
            //console.log(date1+'----'+date2+'=='+diffDays);
            // var diffDays = (diffDays+1);
        } else {
            var diffDays = 1;
        }
        $('#rent_days').val(diffDays);
    }

    function totalAmount() {
        var rent_amount = Number($('#rent_amount').val());
        var driver_cost = Number($('#driver_cost').val());
        var total_amount = (rent_amount+driver_cost);
        $('#total_amount').val(total_amount.toFixed(2));
    }

    //Invoice...
    function invoiceChk(){
        if (document.getElementById('invoice').checked==true) {
            $('.invoiceDiv').slideDown();
        } else {
            $('.invoiceDiv').slideUp();
        }
    }

    function setInvoice() {
        var rental_amount = Number($('#rental_amount').val());
        if (isNaN(rental_amount)) {
            $('#rental_amount').val('');
            $('#rental_amount').focus();
            alert('Rental Amount: Enter Valid Amount');
            rental_amount = 0;
        }

        var extra_uses_cost = Number($('#extra_uses_cost').val());
        if (isNaN(extra_uses_cost)) {
            $('#extra_uses_cost').val('');
            $('#extra_uses_cost').focus();
            alert('Extra Uses Charge: Enter Valid Amount.');
            extra_uses_cost = 0;

        }
        var driver_food_cost = Number($('#driver_food_cost').val());
        if (isNaN(driver_food_cost)) {
            $('#driver_food_cost').val('');
            $('#driver_food_cost').focus();
            alert('Driver Charge: Enter Valid Amount.');
            driver_food_cost = 0;
        }

        var driver_overtime_cost = Number($('#driver_overtime_cost').val());
        if (isNaN(driver_overtime_cost)) {
            $('#driver_overtime_cost').val('');
            $('#driver_overtime_cost').focus();
            alert('Driver’s Overtime: Enter Valid Amount.');
            driver_overtime_cost = 0;
        }

        var fuel_cost = Number($('#fuel_cost').val());
        if (isNaN(fuel_cost)) {
            $('#fuel_cost').val('');
            $('#fuel_cost').focus();
            alert('Fuel Cost (Mileage): Enter Valid Amount.');
            fuel_cost = 0;
        }

        var toll_cost = Number($('#toll_cost').val());
        if (isNaN(toll_cost)) {
            $('#toll_cost').val('');
            $('#toll_cost').focus();
            alert('Toll-Parking: Enter Valid Amount.');
            toll_cost = 0;
        }

        var other_cost = Number($('#other_cost').val());
        if (isNaN(other_cost)) {
            $('#other_cost').val('');
            $('#other_cost').focus();
            alert('Other: Enter Valid Amount.');
            other_cost = 0;
        }

        var vat_cost = Number($('#vat_cost').val());
        if (isNaN(vat_cost)) {
            $('#vat_cost').val('');
            $('#vat_cost').focus();
            alert('VAT: Enter Valid Amount.');
            vat_cost = 0;
        }

        var invoice_amount = (rental_amount+extra_uses_cost+driver_food_cost+driver_overtime_cost+fuel_cost+toll_cost+other_cost+vat_cost);
        $('#invoice_amount').val(invoice_amount);

        //billing();
    }

    function setBill() {
        var bill_amount = Number($('#bill_amount').val());
        if (isNaN(bill_amount)) {
            $('#bill_amount').val('');
            $('#bill_amount').focus();
            alert('Rental Amount: Enter Valid Amount');
            bill_amount = 0;
        }

        //Set Booking Amount...
        $('#rent_amount').val(bill_amount);
        totalAmount();

        var customer_advance = Number($('#customer_advance').val());
        if (isNaN(customer_advance)) {
            $('#customer_advance').val('');
            $('#customer_advance').focus();
            alert('Customer Advance: Enter Valid Amount');
            customer_advance = 0;
        }

        if (document.getElementById('cod_chk').checked==true) {
            var cash_on_delivery = Number($('#cash_on_delivery').val());
            if (isNaN(cash_on_delivery)) {
                $('#cash_on_delivery').val('');
                $('#cash_on_delivery').focus();
                alert('Customer Advance: Enter Valid Amount');
                cash_on_delivery = 0;
            }
        } else {
            var cash_on_delivery = (bill_amount-customer_advance);
            $('#cash_on_delivery').val(cash_on_delivery);
        }

        var driver_rate = Number($('#driver_rate').val());
        if (isNaN(driver_rate)) {
            $('#driver_rate').val('');
            $('#driver_rate').focus();
            alert('Driver Rate: Enter Valid Amount');
            driver_rate = 0;
        }

        var admin_commission = (bill_amount-driver_rate);
        $('#admin_commission').val(admin_commission);


        //Driver Payment Details
        var driver_receivable_amount = (cash_on_delivery-driver_rate);
        driver_receivable_amount = (driver_receivable_amount>0)?driver_receivable_amount:0;
        $('#driver_receivable_amount').val(driver_receivable_amount);

        var driver_receive_amount = Number($('#driver_receive_amount').val());
        if (isNaN(driver_receive_amount)) {
            $('#driver_receive_amount').val('');
            $('#driver_receive_amount').focus();
            alert('Driver Amount Received: Enter Valid Amount');
            driver_receive_amount = 0;
        }
        if (driver_receive_amount>driver_receivable_amount) {
            $('#driver_receive_amount').val('');
            $('#driver_receive_amount').focus();
            alert('Can not receive greater than receivable amount! ('+driver_receivable_amount+')');
            driver_receive_amount = 0;
        }

        var driver_payable_amount = (driver_rate-cash_on_delivery);
        driver_payable_amount = (driver_payable_amount>0)?driver_payable_amount:0;
        $('#driver_payable_amount').val(driver_payable_amount);

        var driver_paid_amount = Number($('#driver_paid_amount').val());
        if (isNaN(driver_paid_amount)) {
            $('#driver_paid_amount').val('');
            $('#driver_paid_amount').focus();
            alert('Driver Amount Received: Enter Valid Amount');
            driver_paid_amount = 0;
        }
        if (driver_paid_amount>driver_payable_amount) {
            $('#driver_paid_amount').val('');
            $('#driver_paid_amount').focus();
            alert('Can not paid greater than payable amount!');
        }


        //Customer Payment Details
        var user_receivable_amount = (bill_amount-(customer_advance+cash_on_delivery));
        user_receivable_amount = (user_receivable_amount>0)?user_receivable_amount:0;
        $('#user_receivable_amount').val(user_receivable_amount);

        var user_receive_amount = Number($('#user_receive_amount').val());
        if (isNaN(user_receive_amount)) {
            $('#user_receive_amount').val('');
            $('#user_receive_amount').focus();
            alert('Customer Amount Received: Enter Valid Amount');
            user_receive_amount = 0;
        }
        if (user_receive_amount>user_receivable_amount) {
            $('#user_receive_amount').val('');
            $('#user_receive_amount').focus();
            alert('Can not receive greater than receivable amount!');
        }
    }

    function codChk() {
        if (document.getElementById('cod_chk').checked==true) {
            $('#cash_on_delivery').prop('readonly', false);
        } else {
            $('#cash_on_delivery').prop('readonly', true);
        }
    }
    invoiceChk();
</script>
