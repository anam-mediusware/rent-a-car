{{--Payment Disbursement ::Start --}}
<input type="hidden" name="bill" id="bill" value="1">
<div class="col-sm-6">
    <div class="form-group">
        <label class="control-label col-sm-4">Rental Amount:</label>
        <div class="col-sm-8">
            <input type="number" step="any" min="0" class="form-control" name="bill_amount" id="bill_amount" value="{{ isset($bill->bill_amount)?$bill->bill_amount:(isset($data->total_amount)?$data->total_amount:old('bill_amount')) }}" onkeyup="setBill()">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Customer Advance:</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="number" step="any" min="0" class="form-control" name="customer_advance" id="customer_advance" value="{{ isset($bill->customer_advance)?$bill->customer_advance:old('customer_advance') }}" onkeyup="setBill()">

                @if (isset($bill->customer_advance) && $bill->customer_advance>0)
                    <a class="input-group-addon" href="{{ url('admin/'.$pageResource.'/send-advance/'.$data->id) }}">Send Link</a>
                @else
                    <span class="input-group-addon"></span>
                @endif
            </div>

        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Cash on Delivery:</label>
        <div class="col-sm-8">
            <div class="input-group">
                <input type="number" step="any" min="0" class="form-control" name="cash_on_delivery" id="cash_on_delivery" value="{{ isset($bill->cash_on_delivery)?$bill->cash_on_delivery:old('cash_on_delivery') }}" onkeyup="setBill()" readonly>
                <span class="input-group-addon"><input type="checkbox" id="cod_chk" value="1" onclick="codChk()"></span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Driver Rate:</label>
        <div class="col-sm-8">
            <input type="number" step="any" min="0" class="form-control" name="driver_rate" id="driver_rate" value="{{ isset($bill->driver_rate)?$bill->driver_rate:old('driver_rate') }}" onkeyup="setBill()">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Commission:</label>
        <div class="col-sm-8">
            <input type="number" step="any" min="0" class="form-control" name="admin_commission" id="admin_commission" value="{{ isset($bill->admin_commission)?$bill->admin_commission:old('admin_commission') }}" readonly>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Remarks:</label>
        <div class="col-sm-8">
            <textarea class="form-control" name="bill_remark">{{ isset($bill->bill_remark)?$bill->bill_remark:old('bill_remark') }}</textarea>
        </div>
    </div>




    {{-- Driver Payment Details::START --}}
    <div class="col-sm-offset-4 col-sm-8">
        <label class="text-yellow">Driver Payment Details</label>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Amount Receivable:</label>
        <div class="col-sm-8">
            <input type="number" step="any" min="0" class="form-control" name="driver_receivable_amount" id="driver_receivable_amount" value="{{ (isset($bill) && ($bill->cash_on_delivery-$bill->driver_rate)>0)?($bill->cash_on_delivery-$bill->driver_rate):'0' }}" readonly>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Amount Received:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="driver_receive_amount" id="driver_receive_amount" value="{{ isset($bill->driver_receive_amount)?$bill->driver_receive_amount:old('driver_receive_amount') }}" onkeyup="setBill()">
        </div>

        <div class="col-sm-4">
            <?php $driver_receive_by = (isset($data->driver_receive_by))?$data->driver_receive_by:old('driver_receive_by'); ?>
            <select name="driver_receive_by" class="form-control">
                <option value="">Received By</option>
                @foreach($payMethod as $pay)
                    <option value="{{ $pay }}" {{ ($driver_receive_by==$pay)?'selected':'' }}>{{ $pay }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-sm-offset-4 col-sm-8"><br>
            <input type="text" class="form-control" name="driver_receive_remark" value="{{ isset($bill->driver_receive_remark)?$bill->driver_receive_remark:old('driver_receive_remark') }}" placeholder="Remark">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Amount Payable:</label>
        <div class="col-sm-8">
            <input type="number" step="any" min="0" class="form-control" name="driver_payable_amount" id="driver_payable_amount" value="{{ (isset($bill) && ($bill->driver_rate-$bill->cash_on_delivery)>0)?($bill->driver_rate-$bill->cash_on_delivery):'0' }}" readonly>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Amount Paid:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="driver_paid_amount" id="driver_paid_amount" value="{{ isset($bill->driver_paid_amount)?$bill->driver_paid_amount:old('driver_paid_amount') }}" onkeyup="setBill()">
        </div>

        <div class="col-sm-4">
            <?php $driver_paid_by = (isset($data->driver_paid_by))?$data->driver_paid_by:old('driver_paid_by'); ?>
            <select name="driver_paid_by" class="form-control">
                <option value="">Paid By</option>
                @foreach($payMethod as $pay)
                    <option value="{{ $pay }}" {{ ($driver_paid_by==$pay)?'selected':'' }}>{{ $pay }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-sm-offset-4 col-sm-8"><br>
            <input type="text" class="form-control" name="driver_paid_remark" value="{{ isset($bill->driver_paid_remark)?$bill->driver_paid_remark:old('driver_paid_remark') }}" placeholder="Remark">
        </div>
    </div>
    {{-- Driver Payment Details::END --}}


    {{-- Customer Payment Details::START --}}
    <div class="col-sm-offset-4 col-sm-8">
        <label class="text-blue">Customer Payment Details</label>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Amount Receivable:</label>
        <div class="col-sm-8">
            <input type="number" step="any" min="0" class="form-control" name="user_receivable_amount" id="user_receivable_amount" value="{{ (isset($bill) && ($bill->bill_amount-($bill->customer_advance+$bill->cash_on_delivery))>0)?($bill->bill_amount-($bill->customer_advance+$bill->cash_on_delivery)):'0' }}" readonly>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Amount Received:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="user_receive_amount" id="user_receive_amount" value="{{ isset($bill->user_receive_amount)?$bill->user_receive_amount:old('user_receive_amount') }}" onkeyup="setBill()">
        </div>

        <div class="col-sm-4">
            <?php $user_receive_by = (isset($data->user_receive_by))?$data->user_receive_by:old('user_receive_by'); ?>
            <select name="user_receive_by" class="form-control">
                <option value="">Received By</option>
                @foreach($payMethod as $pay)
                    <option value="{{ $pay }}" {{ ($user_receive_by==$pay)?'selected':'' }}>{{ $pay }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-sm-offset-4 col-sm-8"><br>
            <input type="text" class="form-control" name="user_receive_remark" value="{{ isset($bill->user_receive_remark)?$bill->user_receive_remark:old('user_receive_remark') }}" placeholder="Remark">
        </div>
    </div>
    {{-- Customer Payment Details::END --}}

    @if (isset($data))
        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-4">
                <p>Booking Added By: {{ ($data->booking_from==1)?$data->user_name:$data->created_name }} {{ dateFormat($data->created_at, 1) }}</p>

                @if ($data->updated_name!='')
                    <p>Booking Modified By: {{ $data->updated_name }} {{ dateFormat($data->updated_at, 1) }}</p>
                @endif
            </div>
        </div>
    @endif
</div>
{{--Payment Disbursement ::End --}}
