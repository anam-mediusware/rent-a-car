{{-- Invoice::Start --}}
<div class="col-sm-6">
    <div class="form-group">
        <label class="control-label col-sm-3">&nbsp;</label>
        <div class="col-sm-9">
            <label><input type="checkbox" name="invoice" id="invoice" value="1" onchange="invoiceChk()" {{ isset($inv->booking_id)?'checked':'' }}> Create An Invoice</label>
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Rental Amount:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="rental_amount" id="rental_amount" value="{{ isset($inv->rental_amount)?$inv->rental_amount:old('rental_amount') }}" onkeyup="setInvoice()">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="rental_remark" value="{{ isset($inv->rental_remark)?$inv->rental_remark:'@ 3,500/= Per day X 1 day' }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Extra Uses Charge:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="extra_uses_cost" id="extra_uses_cost" value="{{ isset($inv->extra_uses_cost)?$inv->extra_uses_cost:old('extra_uses_cost') }}" onkeyup="setInvoice()">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="extra_uses_remark" value="{{ isset($inv->extra_uses_remark)?$inv->extra_uses_remark:'@ 200/= Per Hour X 1 hour' }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Driver Charge:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="driver_food_cost" id="driver_food_cost" value="{{ isset($inv->driver_food_cost)?$inv->driver_food_cost:old('driver_food_cost') }}" onkeyup="setInvoice()">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="driver_food_remark" value="{{ isset($inv->driver_food_remark)?$inv->driver_food_remark:'@ 150/= Per day X 1 day' }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Driver’s Overtime:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="driver_overtime_cost" id="driver_overtime_cost" value="{{ isset($inv->driver_overtime_cost)?$inv->driver_overtime_cost:old('driver_overtime_cost') }}" onkeyup="setInvoice()">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="driver_overtime_remark" value="{{ isset($inv->driver_overtime_remark)?$inv->driver_overtime_remark:'@ 200/= Per Hour X 1 hour' }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Fuel Cost (Mileage):</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="fuel_cost" id="fuel_cost" value="{{ isset($inv->fuel_cost)?$inv->fuel_cost:old('fuel_cost') }}" onkeyup="setInvoice()">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="fuel_remark" value="{{ isset($inv->fuel_remark)?$inv->fuel_remark:'@ 12/- per KM X 160' }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Toll-Parking:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="toll_cost" id="toll_cost" value="{{ isset($inv->toll_cost)?$inv->toll_cost:old('toll_cost') }}" onkeyup="setInvoice()">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="toll_remark" value="{{ isset($inv->toll_remark)?$inv->toll_remark:'N/A' }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Other:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="other_cost" id="other_cost" value="{{ isset($inv->other_cost)?$inv->other_cost:old('other_cost') }}" onkeyup="setInvoice()">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="other_remark" value="{{ isset($inv->other_remark)?$inv->other_remark:'N/A' }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">VAT:</label>
        <div class="col-sm-4">
            <input type="number" step="any" min="0" class="form-control" name="vat_cost" id="vat_cost" value="{{ isset($inv->vat_cost)?$inv->vat_cost:old('vat_cost') }}" onkeyup="setInvoice()">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="vat_remark" value="{{ isset($inv->vat_remark)?$inv->vat_remark:'N/A' }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Total Invoiced Amount:</label>
        <div class="col-sm-9">
            <input type="number" step="any" min="0" class="form-control" name="invoice_amount" id="invoice_amount" value="{{ isset($inv->invoice_amount)?$inv->invoice_amount:old('invoice_amount') }}">
        </div>
    </div>

    <div class="form-group invoiceDiv">
        <label class="control-label col-sm-3">Invoice Date:</label>
        <div class="col-sm-4">
            <input type="text" class="form-control datepicker" name="invoice_date" value="{{ isset($inv->invoice_date)?$inv->invoice_date:date('Y-m-d') }}">
        </div>
        <div class="col-sm-5">
            <input type="text" class="form-control" name="invoice_number" value="{{ isset($inv->invoice_number)?$inv->invoice_number:old('invoice_number') }}">
        </div>
    </div>
</div>
{{-- Invoice::Start --}}
