<div class="form-group{{ $errors->has('vehicle_type_id') ? ' has-error' : '' }}">
    <label for="vehicle_type_id" class="control-label col-sm-3 required">Vehicle Type:</label>
    <div class="col-sm-9">
        <?php $vehicle_type_id = old('vehicle_type_id', $data->vehicle_type_id); ?>
        <select class="form-control" name="vehicle_type_id" id="vehicle_type_id" required>
            <option value="">Select Vehicle Type</option>
            @if (!empty($vehicleType))
                @foreach ($vehicleType as $vehicle)
                    <option value="{{$vehicle->id}}" {{ ($vehicle_type_id==$vehicle->id)?'selected':'' }}>{{ $vehicle->vehicle_type_english }}</option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('vehicle_type_id'))
            <span class="help-block"><strong>{{ $errors->first('vehicle_type_id') }}</strong></span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('service_category_id') ? ' has-error' : '' }}">
    <label for="service_category_id" class="control-label col-sm-3 required">Service Category:</label>
    <div class="col-sm-9">
        <?php $category_id = old('service_category_id', $data->service_category_id); ?>
        <select class="form-control" name="service_category_id" id="service_category_id" v-model="service_category" @change="handleChangeServiceCategory($event.target.value)" required>
            <option value="">Select Service Category</option>
            @if (!empty($serviceCategories))
                @foreach ($serviceCategories as $category)
                    @if($category->id !='5' && $category->id !='6')
                        <option value="{{$category->id}}" {{ ($category_id==$category->id)?'selected':'' }}>{{ $category->category_name }}</option>
                    @endif
                @endforeach
            @endif
        </select>

        @if ($errors->has('service_category_id'))
            <span class="help-block"><strong>{{ $errors->first('service_category_id') }}</strong></span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('rent_type') ? ' has-error' : '' }}">
    <label for="rent_type" class="control-label col-sm-3 required">Service Type:</label>
    <div class="col-sm-9">
        <select class="form-control" name="rent_type" id="rent_type" v-model="service_type" @change="handleChangeServiceType($event.target.value)" required>
            <option value="">Select Rent Type</option>
            <option v-for="item in service_types" :value="item.id">@{{ item.service_name }}</option>
        </select>

        @if ($errors->has('rent_type'))
            <span class="help-block"><strong>{{ $errors->first('rent_type') }}</strong></span>
        @endif
    </div>
</div>

@php
    $service_id = old('rent_type', $data->service_type_id);
    $airport_name = '';
    if ($service_id == 8){
    $airport_name = old('drop_off_location', $data->drop_off_location);
    }
    if ($service_id == 9) {
    $airport_name = old('pick_up_location', $data->pick_up_location);
    }
@endphp
<div class="form-group{{ $errors->has('airport_id') ? ' has-error' : '' }}" id="airport_div" style="display: none;">
    <label for="airport_id" class="control-label col-sm-3 required">Airport:</label>
    <div class="col-sm-9">
        <select class="form-control" name="airport_id" id="airport_id" @change="handleChangeAirport($event.target.value)" required>
            <option value="">Select Airport</option>
            @if ($airports)
                @foreach ($airports as $airport)
                    <option value="{{ $airport->id }}" {{$airport_name==$airport->airport?'selected':''}}>{{ $airport->airport }}</option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('airport_id'))
            <span class="help-block"><strong>{{ $errors->first('airport_id') }}</strong></span>
        @endif
    </div>
</div>

<input type="hidden" name="pick_up_point" id="pick_up_point" v-model="pick_up_point">
<?php
$pickup_point = old('pick_up_location', $data->pick_up_location);
?>
<div class="form-group{{ $errors->has('pick_up_location') || $errors->has('pick_up_point') ? ' has-error' : '' }}" id="pick_up_location_div">
    <label for="pick_up_location" class="control-label col-sm-3 required">Pick Up Location:</label>
    <div class="col-sm-9">
        <input type="text" name="pick_up_location" id="pick_up_location" value="{{$pickup_point}}" class="form-control" required>
        @if ($errors->has('pick_up_location'))
            <span class="help-block"><strong>{{ $errors->first('pick_up_location') }}</strong></span>
        @endif
        @if ($errors->has('pick_up_point'))
            <span class="help-block"><strong>Invalid Pick Up Location</strong></span>
        @endif
    </div>
</div>

<input type="hidden" name="drop_off_point" id="drop_off_point" v-model="drop_off_point">
<?php
$destination_point = old('drop_off_location', $data->drop_off_location);
?>
<div class="form-group{{ $errors->has('drop_off_location') || $errors->has('drop_off_point') ? ' has-error' : '' }}" id="drop_off_location_div">
    <label for="drop_off_location" class="control-label col-sm-3 required">Drop Off Location:</label>
    <div class="col-sm-9">
        <input type="text" name="drop_off_location" id="drop_off_location" value="{{$destination_point}}" class="form-control" required>
        @if ($errors->has('drop_off_location'))
            <span class="help-block"><strong>{{ $errors->first('drop_off_location') }}</strong></span>
        @endif
        @if ($errors->has('drop_off_point'))
            <span class="help-block"><strong>Invalid Drop Off Location</strong></span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('pick_up_date_time') ? ' has-error' : '' }}">
    <label for="pick_up_date_time" class="control-label col-sm-3 required">Pickup Date and Time:</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" name="pick_up_date_time" id='pick_up_date_time' value="{{ old('pick_up_date_time', $data->pick_up_date_time) ? formatDatePickerFull(old('pick_up_date_time', $data->pick_up_date_time)) : '' }}" autocomplete="off">
        @error('pick_up_date_time')
            <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group{{ $errors->has('drop_off_date_time') ? ' has-error' : '' }}">
    <label for="drop_off_date_time" class="control-label col-sm-3 required">Drop-off Date and
        Time:</label>
    <div class="col-sm-9">
        <input type="text" class="form-control" name="drop_off_date_time" id='drop_off_date_time' value="{{ old('drop_off_date_time', $data->drop_off_date_time) ? formatDatePickerFull(old('drop_off_date_time', $data->drop_off_date_time)) : '' }}" autocomplete="off">
        @error('drop_off_date_time')
            <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group{{ $errors->has('pricing_type') ? ' has-error' : '' }}">
    <label class="control-label col-sm-3 required">Pricing Type:</label>
    <div class="col-sm-9">
        <ul class="list-unstyled">
            <li>
                <input type="radio" name="pricing_type" id="fixed" value="fixed" {{old('pricing_type', $data->pricing_type) == 'fixed' ? 'checked' : ''}} required>
                <label class="radio-title" for="fixed"> Fixed </label>
                <input type="radio" name="pricing_type" id="bid" value="bid" {{old('pricing_type', $data->pricing_type) == 'bid' ? 'checked' : ''}} required>
                <label class="radio-title" for="bid"> Bid</label>
            </li>
        </ul>

        @error('pricing_type')
            <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
    <label for="price" class="control-label col-sm-3 required">Price:</label>
    <div class="col-sm-9">
        <input type="number" class="form-control" name="price" id='price' value="{{ old('price', $data->price) }}" required/>

        @error('price')
            <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group{{ $errors->has('decoration_cost') ? ' has-error' : '' }}" id="decoration_cost_div" style="display:none">
    <label for="decoration_cost" class="control-label col-sm-3 required">Decoration Cost:</label>
    <div class="col-sm-9">
        <input type="number" class="form-control" name="decoration_cost" id='decoration_cost' value="{{ old('decoration_cost', $data->decoration_cost) }}" required/>

        @error('decoration_cost')
            <span class="help-block"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>
