@extends('admin.layouts.master')

@push('styles')
    <style>
        #re_pass_error{
            color: #a94442;
            font-style: italic;
        }
    </style>
    {!! (!isset($inv->booking_id))?'<style>.invoiceDiv{display:none;}</style>':'' !!}
@endpush

<?php
$pageName = 'Booking';
$pageResource = 'admin.booking';
?>

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#">
                        <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
                    </a>
                </li>
            </ul>

            <div class="tab-content" id="booking-form">
                <div class="tab-pane active">
                    @if (isset($data))
                        <div class="box-body">
                            <form method="POST" action="{{ route('admin.booking.show', $data->id) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data" onsubmit="return bookingSubmit()">
                                @csrf
                                @method('PUT')

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label col-sm-3">Booking Number:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" value="{{ isset($data->book_number)?$data->book_number:old('book_number') }}" placeholder="Auto Generated" readonly>
                                            </div>
                                        </div>

                                        <div id="new_client_box">
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label class="control-label col-sm-3 required">Name:</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="name" value="{{ old('name', $data->user->name) }}" readonly>

                                                    @if ($errors->has('name'))
                                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                                                <label class="control-label col-sm-3 required">Mobile:</label>
                                                <div class="col-sm-9">
                                                    <input type="number" class="form-control" name="mobile" value="{{ old('mobile', $data->user->mobile) }}" required>

                                                    @if ($errors->has('mobile'))
                                                        <span class="help-block"><strong>{{ $errors->first('mobile') }}</strong></span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                <label class="control-label col-sm-3">Address:</label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control" name="address" rows="5">{{ old('address', $data->user->address) }}</textarea>

                                                    @if ($errors->has('address'))
                                                        <span class="help-block"><strong>{{ $errors->first('address') }}</strong></span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="control-label col-sm-3 required">Email:</label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" value="{{ old('email', $data->user->email) }}" autocomplete="off" required>
                                                    @if ($errors->has('email'))
                                                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @include('admin.booking.partials.booking-common-input')

                                        <div class="form-group{{ $errors->has('package_additional_price') ? ' has-error' : '' }}">
                                            <div class="col-sm-3 text-right">
                                                <label class="control-label">Package Additional Price:</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <table class="table table-bordered table-additional" id="dynamic_field">
                                                    @foreach($data->additionalPricings as $additionalPricing)
                                                        <tr>
                                                            <td>
                                                                <input type="text" class="form-control" value="{{$additionalPricing->price_name}}" readonly/>
                                                            </td>
                                                            <td>
                                                                <input type="number" class="form-control" value="{{$additionalPricing->price}}" readonly/>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    @include('admin.booking.partials.payment-disbursement-form')

                                    @include('admin.booking.partials.invoice-form')

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn btn-success btn-flat btn-lg">Update
                                                    Booking
                                                </button>
                                                <a href="{{ route('admin.booking.index') }}" class="btn btn-warning btn-flat btn-lg">Back</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </section>
@endsection

@push('scripts')
    @include('admin.booking.js.form-scripts')
    @include('admin.booking.js.vue-js')
    @include('admin.booking.partials.form-scripts')
@endpush
