@extends('admin.layouts.master')

<?php
$pageName = 'Slide';
$pageResource = 'admin.slide';
?>

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li {{ (isset($lists))?'class=active':'' }}>
                    <a href="{{ route($pageResource.'.index') . qString() }}">
                        <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                    </a>
                </li>
                <li {{ (isset($create))?'class=active':'' }}>
                    <a href="{{ route($pageResource.'.create') . qString() }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                    </a>
                </li>

                @if (isset($edit))
                    <li class="active">
                        <a href="#">
                            <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
                        </a>
                    </li>
                @endif

                @if (isset($show))
                    <li class="active">
                        <a href="#">
                            <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
                        </a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                @if(isset($show))
                    <div class="tab-pane active">
                        @if (isset($data))
                            <div class="box-body table-responsive">
                                <table class="table table-bordered">
                                    <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                                    <thead>
                                    <tr class="hide">
                                        <th style="width:120px;"></th>
                                        <th style="width:10px;"></th>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="3">
                                            {!! viewImg('slides', $data->image, ['popup' => 1, 'class' => 'img-circle', 'style' =>'width:100px;', 'fakeimg' => 'no-img']) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="width:120px;">Title</th>
                                        <th style="width:10px;">:</th>
                                        <td>{{ $data->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Small Title</th>
                                        <th>:</th>
                                        <td>{{ $data->title_small }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <th>:</th>
                                        <td>{{ ($data->status==1)?'Active':(($data->status==2)?'Inactive':'Pending') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="box-body">
                                {!! notFoundText() !!}
                            </div>
                        @endif
                    </div>

                @elseif(isset($edit) || isset($create))
                    <div class="tab-pane active">
                        <div class="box-body">
                            <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                {!! (isset($edit))?'<input name="_method" type="hidden" value="PUT">':'' !!}
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label class="control-label col-sm-3 required">Title:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="title" value="{{ isset($data->title)?$data->title:old('title') }}" required>

                                                @if ($errors->has('title'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('title_small') ? ' has-error' : '' }}">
                                            <label class="control-label col-sm-3 required">Small Title:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="title_small" value="{{ isset($data->title_small)?$data->title_small:old('title_small') }}" required>

                                                @if ($errors->has('title_small'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('title_small') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                            <label class="control-label col-sm-3">Image (600x330):</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control" name="image">

                                                @if ($errors->has('image'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                            <label class="control-label col-sm-3">Status:</label>
                                            <div class="col-sm-9">
                                                <?php $status = (isset($data->status)) ? $data->status : old('status'); ?>
                                                <select name="status" class="form-control">
                                                    @foreach([1 => 'Active', 2 => 'Inactive'] as $sK => $sV)
                                                        <option value="{{ $sK }}" {{ ($status==$sK)?'selected':'' }}>{{ $sV }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Update':'Create' }}</button>
                                            <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                @elseif (isset($lists))
                    <div class="tab-pane active">
                        <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                            <div class="box-header text-right">
                                <div class="row">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Write your search text...">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Search</button>
                                        <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="box-body table-responsive">
                            <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                            <?php //echo $pagination->msg; ?>
                            <table class="table table-bordered table-hover dataTable">
                                <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                                <thead>
                                <tr>
                                    <th>SL.</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Small Title</th>
                                    <th>Status</th>
                                    <th class="not-export-col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($records as $key => $val)
                                    <tr>
                                        <td>{{$serial++}}</td>
                                        <td>{!! viewImg('slides', $val->image, ['popup' => 1, 'class' => 'img-circle', 'style' =>'width:30px;', 'fakeimg' => 'no-img']) !!}</td>
                                        <td>{{$val->title}}</td>
                                        <td>{{$val->title_small}}</td>
                                        <td>{{ ($val->status==1)?'Active':(($val->status==2)?'Inactive':'Pending') }}</td>
                                        <td>
                                            <?php
                                            $access = 1;
                                            listAction([
                                                actionLi(route($pageResource . '.show', $val->id) . qString(), 'show', $access),
                                                actionLi(route($pageResource . '.edit', $val->id) . qString(), 'edit', $access),
                                                actionLi(route($pageResource . '.destroy', $val->id) . qString(), 'delete', $access),
                                            ]);
                                            ?>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-right">
                                {{ $records->appends(Request::except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
