@extends('admin.layouts.master')

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-4 col-sm-3 col-xs-6">
                <a class="small-box bg-aqua" href="{{ url('/admin/booking') }}">
                    <div class="inner text-center">
                        <h4>Booking List</h4>
                    </div>
                    <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span></a>
            </div>

            <div class="col-md-4 col-sm-3 col-xs-6">
                <a class="small-box bg-red" href="{{ url('/admin/crm') }}">
                    <div class="inner text-center">
                        <h4>Customer Insights</h4>
                    </div>
                    <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span></a>
            </div>

            <div class="col-md-4 col-sm-3 col-xs-6">
                <a class="small-box bg-green" href="{{ url('/admin/crm/followup') }}">
                    <div class="inner text-center">
                        <h4>Follow Ups</h4>
                    </div>
                    <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span></a>
            </div>

            <div class="col-md-4 col-sm-3 col-xs-6">
                <a class="small-box bg-yellow" href="{{ url('/admin/booking/create') }}">
                    <div class="inner text-center">
                        <h4>Add New Booking</h4>
                    </div>
                    <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span></a>
            </div>

            <div class="col-md-4 col-sm-3 col-xs-6">
                <a class="small-box bg-blue" href="{{ url('/admin/partner') }}">
                    <div class="inner text-center">
                        <h4>Driver & Cars</h4>
                    </div>
                    <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span></a>
            </div>

            <div class="col-md-4 col-sm-3 col-xs-6">
                <a class="small-box bg-navy" href="{{ url('/admin/sms') }}">
                    <div class="inner text-center">
                        <h4>Send SMS</h4>
                    </div>
                    <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span></a>
            </div>
        </div>

        <img src="{{ url('web-assets/img/logo.png') }}" style="position:absolute; bottom:30px; right:0;">
    </section>
@endsection
