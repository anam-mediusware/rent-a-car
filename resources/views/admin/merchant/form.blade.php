<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="control-label col-sm-4 required">Name:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="name" name="name" value="{{ isset($data->name)?$data->name:old('name') }}" required>

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
    <label for="contact" class="control-label col-sm-4 required">Mobile Number:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="contact" name="contact" value="{{ isset($data->contact)?$data->contact:old('contact') }}" required>

        @if ($errors->has('contact'))
            <span class="help-block">
                <strong>{{ $errors->first('contact') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('second_contact_name') ? ' has-error' : '' }}">
    <label for="second_contact_name" class="control-label col-sm-4">Second Contact Name:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="second_contact_name" name="second_contact_name" value="{{ isset($data->second_contact_name)?$data->second_contact_name:old('second_contact_name') }}">

        @if ($errors->has('second_contact_name'))
            <span class="help-block">
                <strong>{{ $errors->first('second_contact_name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('second_contact_mobile_number') ? ' has-error' : '' }}">
    <label for="second_contact_mobile_number" class="control-label col-sm-4">Second Contact Mobile Number:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="second_contact_mobile_number" name="second_contact_mobile_number" value="{{ isset($data->second_contact_mobile_number)?$data->second_contact_mobile_number:old('second_contact_mobile_number') }}">

        @if ($errors->has('second_contact_mobile_number'))
            <span class="help-block">
                <strong>{{ $errors->first('second_contact_mobile_number') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    <label for="address" class="control-label col-sm-4 required">Address:</label>
    <div class="col-sm-8">
        <textarea class="form-control" id="address" name="address" rows="5" required>{{ isset($data->address)?$data->address:old('address') }}</textarea>

        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
    <label for="district" class="control-label col-sm-4 required">Partner District:</label>
    <div class="col-sm-8">
        <?php $district = (isset($data->district))?$data->district:old('district'); ?>
        <select class="form-control" id="district" name="district" required>
            <option value="">Select District</option>
            @foreach($disArray as $div => $disArr)
                <optgroup label="{{ $div }}">
                    @foreach($disArr as $dis)
                        <option value="{{ $dis }}" {{ ($district==$dis)?'selected':'' }}>{{ $dis }}</option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>

        @if ($errors->has('district'))
            <span class="help-block">
                <strong>{{ $errors->first('district') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="control-label col-sm-4">E-Mail Address:</label>
    <div class="col-sm-8">
        <input type="email" class="form-control" id="email" name="email" value="{{ isset($data->email)?$data->email:old('email') }}">

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('photo_id') ? ' has-error' : '' }}">
    <label for="photo_id" class="control-label col-sm-4">NID/Passport/Any Photo ID:</label>
    <div class="col-sm-6">
        <input type="file" class="form-control" id="photo_id" name="photo_id">
    </div>
    <div class="col-sm-2">
        {!! viewImg('merchants/id', isset($data->photo_id)?$data->photo_id:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
    </div>

    @if ($errors->has('photo_id'))
        <span class="help-block">
            <strong>{{ $errors->first('photo_id') }}</strong>
        </span>
    @endif
</div>
