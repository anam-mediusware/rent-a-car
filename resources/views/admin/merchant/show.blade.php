@extends('admin.layouts.master')

@php
    $pageName = 'Merchant';
    $pageResource = 'admin.merchant';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif
                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') . qString() }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif

                <li class="active">
                    <a href="#">
                        <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    @if (isset($data))
                        <div class="box-body table-responsive">
                            <table class="table table-bordered">
                                <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                                <thead>
                                <tr class="hide">
                                    <th style="width:230px;"></th>
                                    <th style="width:10px;"></th>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th style="width:230px;">Name</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{ $data->name }}</td>
                                </tr>
                                <tr>
                                    <th>Second Contact Name</th>
                                    <th>:</th>
                                    <td>{{ $data->second_contact_name }}</td>
                                </tr>
                                <tr>
                                    <th>Second Contact Mobile Number</th>
                                    <th>:</th>
                                    <td>{{ $data->second_contact_mobile_number }}</td>
                                </tr>

                                <tr>
                                    <th colspan="3"></th>
                                </tr>

                                <tr>
                                    <th>Address</th>
                                    <th>:</th>
                                    <td>{!! nl2br($data->address) !!}</td>
                                </tr>
                                <tr>
                                    <th>District</th>
                                    <th>:</th>
                                    <td>{{ $data->district }}</td>
                                </tr>
                                <tr>
                                    <th>Mobile number</th>
                                    <th>:</th>
                                    <td>{{ $data->contact }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <th>:</th>
                                    <td>{{ $data->email ? $data->email : 'N/A' }}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="box-body">
                            {!! notFoundText() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
