@extends('admin.layouts.master')

@php
    $pageName = 'Bidding';
    $pageResource = 'admin.bidding-trips';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#">
                        <i class="fa fa-list" aria-hidden="true"></i> Bid List
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <form method="GET" action="{{ route($pageResource.'.bids', $records->first()->bidding_trip_id) }}" class="form-inline">
                        <div class="box-header text-right">
                            <div class="row">

                                <div class="form-group">
                                    <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Search">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-flat">Search</button>
                                    <a class="btn btn-warning btn-flat" href="{{ route($pageResource.'.bids', $records->first()->bidding_trip_id) }}">X</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="box-body table-responsive">
                        <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                        <table class="table table-bordered table-hover dataTable">
                            <caption class="hidden"><h3>{{ $pageName }} List</h3></caption>
                            <thead>
                            <tr>
                                <th>SL.</th>
                                <th scope="col">Driver</th>
                                <th scope="col">Price</th>
                                <th>Created at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($serial = ($records->perPage() * ($records->currentPage() - 1)) + 1)
                            @foreach($records as $key => $record)
                                <tr>
                                    <td>{{ $serial++ }}</td>
                                    <td>{{ $record->driver->full_name }}</td>
                                    <td>{{ $record->price }}</td>
                                    <td>{{formatDateTime($record->created_at)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-right">
                            {{ $records->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
