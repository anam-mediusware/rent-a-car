@extends('admin.layouts.master')

@php
    $pageName = 'Bidding';
    $pageResource = 'admin.bidding-trips';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li {{ (isset($lists))?'class=active':'' }}>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif

                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') . qString() }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                        <div class="box-header text-right">
                            <div class="row">

                                <div class="form-group">
                                    <select class="form-control select2" name="service_id">
                                        <option value="">Service</option>
                                        @if (!empty($services))
                                            @foreach ($services as $service)
                                                <option value="{{$service->id}}">{{$service->service_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Search">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-flat">Search</button>
                                    <a class="btn btn-warning btn-flat" href="{{ route($pageResource.'.index') }}">X</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="box-body table-responsive">
                        <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                        <table class="table table-bordered table-hover dataTable">
                            <caption class="hidden"><h3>{{ $pageName }} List</h3></caption>
                            <thead>
                            <tr>
                                <th>SL.</th>
                                <th>Customer Name</th>
                                <th>Vehicle Type</th>
                                <th>Service Category</th>
                                <th>Service Type</th>
                                <th>Pick Up Location</th>
                                <th>Drop Off Location</th>
                                <th>Pick Up Date Time</th>
                                <th>Drop Off Date Time</th>
                                <th>Price</th>
                                <th>Created at</th>
                                <th class="not-export-col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $key => $record)
                                <tr>
                                    <td>{{$serial++}}</td>
                                    <td>{{ $record->user->name }}</td>
                                    <td>{{ $record->vehicleType->vehicle_type_english }}</td>
                                    <td>{{$val->serviceCategory->category_name??null}}</td>
                                    <td>{{ $record->service->service_name }}</td>
                                    <td>{{ucwords($record->upazilaBoundaryFrom ? $record->upazilaBoundaryFrom->Upaz_name : '')}}</td>
                                    <td>{{ucwords($record->upazilaBoundaryTo ? $record->upazilaBoundaryTo->Upaz_name : '')}}</td>
                                    <td>{{ $record->pick_up_date_time }}</td>
                                    <td>{{ $record->drop_off_date_time }}</td>
                                    <td>{{ $record->price }}</td>
                                    <td>{{formatDateTime($record->created_at)}}</td>
                                    <td>
                                        @php
                                            $access = 1;
                                            $actions = [
                                                '<li><a href="'.route($pageResource.'.show', $record->id).'"><i class="fa fa-eye"></i> Show Details</a></li>'
                                                ];
                                            if ($record->bids->count()) {
                                                $actions[] = '<li><a href="'.route($pageResource.'.bids', $record->id).'"><i class="fa fa-eye"></i> Show Bids</a></li>';
                                            }
                                            listAction($actions);
                                        @endphp
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-right">
                            {{ $records->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
