@extends('admin.layouts.master')

@php
    $pageName = 'Show';
    $pageResource = 'admin.bidding-trips';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                <li>
                    <a href="{{ route($pageResource.'.index') . qString() }}">
                        <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                    </a>
                </li>
                @endif
                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif

                <li class="active">
                    <a href="#">
                        <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    @if (isset($data))
                        <div class="box-body table-responsive">
                            <table class="table table-bordered">
                                <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                                <thead>
                                <tr class="hide">
                                    <th style="width:230px;"></th>
                                    <th style="width:10px;"></th>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th style="width:230px;">Customer Name</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{ $data->user->name }}</td>
                                </tr>
                                <tr>
                                    <th>Vehicle Type</th>
                                    <th>:</th>
                                    <td>{{ $data->vehicleType->vehicle_type_english }}</td>
                                </tr>
                                <tr>
                                    <th>Service Category</th>
                                    <th>:</th>
                                    <td>{{ $data->serviceCategory->category_name }}</td>
                                </tr>
                                <tr>
                                    <th>Service Type</th>
                                    <th>:</th>
                                    <td>{{ $data->service->service_name }}</td>
                                </tr>
                                <tr>
                                    <th>Pick Up Location</th>
                                    <th>:</th>
                                    <td>{{ucwords($data->upazilaBoundaryFrom ? $data->upazilaBoundaryFrom->Upaz_name : '')}}</td>
                                </tr>
                                <tr>
                                    <th>Drop Off Location</th>
                                    <th>:</th>
                                    <td>{{ucwords($data->upazilaBoundaryTo ? $data->upazilaBoundaryTo->Upaz_name : '')}}</td>
                                </tr>
                                <tr>
                                    <th>Pick Up Date Time</th>
                                    <th>:</th>
                                    <td>{{ $data->pick_up_date_time }}</td>
                                </tr>
                                <tr>
                                    <th>Drop Off Date Time</th>
                                    <th>:</th>
                                    <td>{{ $data->drop_off_date_time }}</td>
                                </tr>
                                <tr>
                                    <th>Price</th>
                                    <th>:</th>
                                    <td>{{ $data->price }}</td>
                                </tr>
                                <tr>
                                    <th>Created At</th>
                                    <th>:</th>
                                    <td>{{ $data->created_at }}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                        <h2>Bid for a driver</h2>
                        <hr>
                        <div class="box-body">
                            <form method="POST" action="{{ route('admin.bidding-trips.submit-bid', $data->id) }}" class="form-horizontal" enctype="multipart/form-data">
                                @csrf

                                <div class="row">
                                    <div class="col-sm-6">

                                        <div class="form-group{{ $errors->has('driver_id') ? ' has-error' : '' }}">
                                            <label for="driver_id" class="control-label col-sm-3 required">Driver Name:</label>
                                            <div class="col-sm-9">
                                                @php($driver_id = old('driver_id'))
                                                <select class="form-control" name="driver_id" id="driver_id" required>
                                                    <option value="">Select Driver</option>
                                                    @if (!empty($drivers))
                                                        @foreach ($drivers as $driver)
                                                            <option value="{{$driver->id}}" {{ ($driver_id==$driver->id)?'selected':'' }}>{{ $driver->full_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                                @if ($errors->has('driver_id'))
                                                    <span class="help-block"><strong>{{ $errors->first('driver_id') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('price', $data->price) ? ' has-error' : '' }}">
                                            <label for="price" class="control-label col-sm-3 required">Bidding Price:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="price" id="price" value="{{ old('price', $data->price) }}" required>

                                                @if ($errors->has('price'))
                                                    <span class="help-block"><strong>{{ $errors->first('price') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                            <label for="note" class="control-label col-sm-3">Note:</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="note" id="note" rows="5">{{ old('note', $data->user->note) }}</textarea>

                                                @if ($errors->has('note'))
                                                    <span class="help-block"><strong>{{ $errors->first('note') }}</strong></span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-success btn-flat btn-lg">Place Bid</button>
                                            <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    @else
                        <div class="box-body">
                            {!! notFoundText() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('#driver_id').select2();
        });
    </script>
@endpush
