@extends('admin.layouts.master')

@php
    $pageName = 'Create';
    $pageResource = 'admin.bidding-trips';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="{{ route($pageResource.'.create') . qString() }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                    </a>
                </li>

            </ul>

            <div class="tab-content">

                <div class="tab-pane active">
                    <div class="box-body">
                        <form method="POST" action="{{ route($pageResource.'.index') }}" class="form-horizontal" enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="col-sm-6">

                                    @include('admin.bidding-trip.form')

                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-success btn-flat btn-lg">Book</button>
                                        <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
