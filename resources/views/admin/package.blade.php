@extends('admin.layouts.master')

<?php
$pageName = 'Package';
$pageResource = 'admin.package';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.index') . qString() }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>

        <li {{ (isset($create))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.create') . qString() }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
            </a>
        </li>

        @if (isset($images))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Image Or Video uplaod {{ $pageName }}
            </a>
        </li>
        @endif

        @if (isset($edit))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
            </a>
        </li>
        @endif

        @if (isset($show))
        <li class="active">
            <a href="#">
                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
            </a>
        </li>
        @endif
    </ul>

    <div class="tab-content">
        @if(isset($show))
        <div class="tab-pane active">
            @if (isset($data))
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                        <thead>
                            <tr class="hide">
                                <th style="width:230px;"></th>
                                <th style="width:10px;"></th>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Banner Image</th>
                                <th>:</th>
                                <td>{!! viewImg('package/banner_image', $data[0]->banner_image, ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}</td>
                            </tr>

                            <tr>
                                <th>Gallery Image</th>
                                <th>:</th>
                                <td>
                                    @if(!empty($data[0]->PackageImageVideo))
                                        @foreach($data[0]->PackageImageVideo as $packageImage)
                                            @if ($packageImage->data_type == '1')
                                                {!! viewImg('package/gallery', $packageImage->image, ['popup' => 1, 'style' =>'height:30px;']) !!}
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <th>Gallery Video</th>
                                <th>:</th>
                                <td>
                                    @if(!empty($data[0]->PackageImageVideo))
                                        @foreach($data[0]->PackageImageVideo as $packageImage)
                                            @if ($packageImage->data_type == '2')
                                                @php $url = preg_replace("#.*youtube\.com/watch\?v=#","",$packageImage->image); @endphp
                                                <iframe width="100px" height="100px" src="https://www.youtube.com/embed/{{$url}}" frameborder="0" allowfullscreen></iframe>
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <th>Package Title</th>
                                <th>:</th>
                                <td>{{$data[0]->package_title}}</td>
                            </tr>
                            <tr>
                                <th>Package Description</th>
                                <th>:</th>
                                <td><body>{{htmlspecialchars_decode($data[0]->package_description)}}</body></td>
                            </tr>
                            <tr>
                                <th style="width:230px;">Vehicle Type</th>
                                <th style="width:10px;">:</th>
                                <td>{{$data[0]->VehicleType->vehicle_type_english}}</td>
                            </tr>
                            <tr>
                                <th>From Location</th>
                                <th>:</th>
                                <td>{{$data[0]->from_location}}</td>
                            </tr>
                            <tr>
                                <th>To Location</th>
                                <th>:</th>
                                <td>{{$data[0]->to_location}}</td>
                            </tr>
                            <tr>
                                <th>Trip Type</th>
                                <th>:</th>
                                <td>{{$data[0]->day_trip_status == 'yes' ? 'Day Trip' : 'Night Stay'}}</td>
                            </tr>

                            <tr>
                                <th>Trip Start Date</th>
                                <th>:</th>
                                <td>{{$data[0]->trip_start_date !="" ? date('d/m/Y',strtotime($data[0]->trip_start_date)) : ''}}</td>
                            </tr>

                            <tr>
                                <th>Trip End Date</th>
                                <th>:</th>
                                <td>{{$data[0]->trip_end_date !="" ? date('d/m/Y',strtotime($data[0]->trip_end_date)) : ''}}</td>
                            </tr>

                            <tr>
                                <th>Package Start Date</th>
                                <th>:</th>
                                <td>{{$data[0]->package_start_date !="" ? date('d/m/Y',strtotime($data[0]->package_start_date)) : ''}}</td>
                            </tr>

                            <tr>
                                <th>Package End Date</th>
                                <th>:</th>
                                <td>{{$data[0]->package_end_date !="" ? date('d/m/Y',strtotime($data[0]->package_end_date)) : ''}}</td>
                            </tr>

                            <tr>
                                <th>Package Pricing Type</th>
                                <th>:</th>
                                <td>{{ucwords($data[0]->ServieSetting->pricing_type)}}</td>
                            </tr>

                            <tr>
                                <th>Package Price</th>
                                <th>:</th>
                                <td>{{$data[0]->ServieSetting->price}}</td>
                            </tr>

                            <tr>
                                <th>Package Additional Price</th>
                                <th>:</th>
                                <td>
                                    @if (!empty($data['additional_pricing']))
                                        <ul class="list-unstyled">
                                            @foreach ($data['additional_pricing'] as $additional)
                                                <li>{{$additional->price_name}} : {{$additional->price}}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <th>Package SEO Title</th>
                                <th>:</th>
                                <td>{{$data[0]->package_seo_title}}</td>
                            </tr>

                            <tr>
                                <th>Package SEO Keywords</th>
                                <th>:</th>
                                <td>{{$data[0]->package_seo_keywords}}</td>
                            </tr>

                            <tr>
                                <th>Package SEO Description</th>
                                <th>:</th>
                                <td>{{$data[0]->package_seo_description}}</td>
                            </tr>

                            <tr>
                                <th>Created at</th>
                                <th>:</th>
                                <td>{{$data[0]->created_at}}</td>
                            </tr>

                            <tr>
                                <th>Updated at</th>
                                <th>:</th>
                                <td>{{$data[0]->updated_at}}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            @else
                <div class="box-body">
                    {!! notFoundText() !!}
                </div>
            @endif
        </div>

        @elseif(isset($edit) || isset($create))
        <div class="tab-pane active">
            <div class="box-body">
                <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data" onsubmit="return createEdit()">
                    @csrf
                    {!! (isset($edit))?'<input name="_method" type="hidden" value="PUT">':'' !!}
                    <div class="row">
                        <div class="col-sm-10">

                            <div class="form-group{{ $errors->has('package_title') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Package Title:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="package_title" id='package_title' value="{{ isset($data[0]->package_title)?$data[0]->package_title:old('package_title') }}" required/>

                                    @if ($errors->has('package_title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('banner_image') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Banner Image:</label>
                                <div class="col-sm-6">
                                    <input type="file" class="form-control" name="banner_image" id="banner_image">
                                </div>
                                <div class="col-sm-2">
                                    {!! viewImg('package/banner_image', isset($data[0]->banner_image)?$data[0]->banner_image:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
                                </div>

                                @if ($errors->has('banner_image'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('banner_image') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('package_description') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Package Description:</label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control" name="package_description" id='post_content'>{{ isset($data[0]->package_description)?html_entity_decode($data[0]->package_description):old('package_description') }}</textarea>

                                    @if ($errors->has('package_description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('vehicle_type_id') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Vehicle Type:</label>
                                <div class="col-sm-8">
                                    <?php $vehicle_type_id = (isset($data[0]->vehicle_type_id))?$data[0]->vehicle_type_id:old('vehicle_type_id'); ?>
                                    <select class="form-control" name="vehicle_type_id" required>
                                        <option value="">Select Vehicle Type</option>
                                        @if (!empty($vehicleType))
                                            @foreach ($vehicleType as $vehicle)
                                                <option value="{{$vehicle->id}}" {{ ($vehicle_type_id==$vehicle->id)?'selected':'' }}>{{ $vehicle->vehicle_type_english }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('vehicle_type_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('vehicle_type_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('from_location') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">From Location:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="from_location" value="{{ isset($data[0]->from_location)?$data[0]->from_location:old('from_location') }}" required>

                                    @if ($errors->has('from_location'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('from_location') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('to_location') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">To Location:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="to_location" value="{{ isset($data[0]->to_location)?$data[0]->to_location:old('to_location') }}" required>

                                    @if ($errors->has('to_location'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('to_location') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('day_trip_status') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Trip Type:</label>
                                <div class="col-sm-8">
                                    <?php $day_trip = (isset($data[0]->day_trip_status))?$data[0]->day_trip_status:old('day_trip_status'); ?>
                                    <select class="form-control" name="day_trip_status" onchange="dayTrip(this.value)" required>
                                        <option value="">Select Trip Type</option>
                                            <option value="yes" {{ ($day_trip=='yes')?'selected':'' }}>Day Trip</option>
                                            <option value="no" {{ ($day_trip=='no')?'selected':'' }}>Night Stay</option>
                                    </select>

                                    @if ($errors->has('day_trip_status'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('day_trip_status') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('trip_start_date') ? ' has-error' : '' }}" id="trip_start_date_div" style="display: {{$day_trip=='no'?'block':'none'}}">
                                <label class="control-label col-sm-4">Trip Start Date:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="trip_start_date" id='datetimepicker1' value="{{ isset($data[0]->trip_start_date) && $data[0]->trip_start_date!=""?date('d/m/Y',strtotime($data[0]->trip_start_date)):old('trip_start_date') }}">

                                    @if ($errors->has('trip_start_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('trip_start_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('trip_end_date') ? ' has-error' : '' }}" id="trip_end_date_div" style="display: {{$day_trip=='no'?'block':'none'}}">
                                <label class="control-label col-sm-4">Trip End Date:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="trip_end_date" id='datetimepicker2' value="{{ isset($data[0]->trip_end_date) && $data[0]->trip_end_date!=""?date('d/m/Y',strtotime($data[0]->trip_end_date)):old('trip_end_date') }}">

                                    @if ($errors->has('trip_end_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('trip_end_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('package_start_date') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Package Start Date:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="package_start_date" id='datetimepicker3' value="{{ isset($data[0]->package_start_date) && $data[0]->package_start_date !=""?date('d/m/Y',strtotime($data[0]->package_start_date)):old('trip_end_date') }}" required>

                                    @if ($errors->has('package_start_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_start_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('package_end_date') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Package End Date:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="package_end_date" id='datetimepicker4' value="{{ isset($data[0]->package_end_date) && $data[0]->package_end_date !="" ?date('d/m/Y',strtotime($data[0]->package_end_date)):old('package_end_date') }}" required>

                                    @if ($errors->has('package_end_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_end_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('package_setting') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Package Pricing Type:</label>
                                <div class="col-sm-8">
                                    <ul class="list-unstyled">
                                        <?php $package_setting = (isset($data[0]->ServieSetting->pricing_type))?$data[0]->ServieSetting->pricing_type:old('flat_bit'); ?>
                                        <li>
                                            <input type="radio" name="flat_bit" id="fixed" value="fixed" {{$package_setting == 'fixed' ? 'checked' : ''}} required>
                                            <label class="radio-title" for="fixed"> Fixed </label>
                                            <input type="radio" name="flat_bit" id="bid" value="bid" {{$package_setting == 'bid' ? 'checked' : ''}} required>
                                            <label class="radio-title" for="bid"> Bid</label>
                                        </li>
                                    </ul>
                                    @if ($errors->has('package_setting'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_setting') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('package_price') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Package Price:</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="package_price" id='package_price' value="{{ isset($data[0]->ServieSetting->price)?$data[0]->ServieSetting->price:old('package_price') }}" required/>

                                    @if ($errors->has('package_price'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('package_additional_price') ? ' has-error' : '' }}">
                                <div class="col-sm-4 text-right">
                                    <label class="control-label">Package Additional Price:</label>
                                </div>
                                <div class="col-sm-8">
                                    <table class="table table-bordered table-additional" id="dynamic_field">
                                        @if(isset($edit))
                                            <input type="hidden" name="service_pricing_id" value="{{$data[0]->ServieSetting->service_id}}"
                                        @endif
                                        @if (isset($data) && !$data['additional_pricing']->isEmpty())
                                            @php $count = 0 @endphp
                                            @foreach ($data['additional_pricing'] as $additionalPricings)
                                                <tr id="row{{$count}}">
                                                    <td>
                                                        <input type="text" class="form-control" name="additional_price[{{$count}}][label]" id="additional_label_{{$count}}" placeholder="Price label" value="{{$additionalPricings->price_name}}"/>
                                                    </td>
                                                    <td>
                                                        <input type="number" class="form-control" name="additional_price[{{$count}}][price]" id='additional_price_{{$count}}' placeholder="Price"  value="{{$additionalPricings->price}}"/>
                                                    </td>
                                                    <td>
                                                        @if ($count == 0)
                                                            <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                                                        @else
                                                        <button type="button" name="remove" id="{{$count}}" class="btn btn-danger btn_remove">X</button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php $count++ @endphp
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control" name="additional_price[0][label]" id="additional_label_0" placeholder="Price label"/>
                                                </td>
                                                <td>
                                                    <input type="number" class="form-control" name="additional_price[0][price]" id='additional_price_0' placeholder="Price" value=""/>
                                                </td>
                                                <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('package_seo_title') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Package SEO Title:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="package_seo_title" id='package_seo_title' value="{{ isset($data[0]->package_description)?$data[0]->package_seo_title:old('package_seo_title') }}"/>

                                    @if ($errors->has('package_seo_title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_seo_title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('package_seo_keywords') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Package SEO Keywords:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="package_seo_keywords" id='package_seo_keywords' value="{{ isset($data[0]->package_seo_keywords)?$data[0]->package_seo_keywords:old('package_seo_keywords') }}"/>

                                    @if ($errors->has('package_seo_keywords'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_seo_keywords') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('package_seo_description') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Package SEO Description:</label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control" name="package_seo_description" id='package_seo_description'>{{ isset($data[0]->package_seo_description)?$data[0]->package_seo_description:old('package_seo_description') }}</textarea>

                                    @if ($errors->has('package_seo_description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('package_seo_description') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Update':'Create' }}</button>
                                <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @elseif (isset($lists))
        <div class="tab-pane active">
            <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <select class="form-control select2" name="vehicle_type">
                                <option value="">Vehicle Type</option>
                                @if (!empty($vehicle_type))
                                    @foreach ($vehicle_type as $vehicle)
                                        <option value="{{$vehicle->id}}"  {{ (Request::get('vehicle_type')== $vehicle->id)?'selected' : '' }}>{{$vehicle->vehicle_type_english}}<option>
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Search">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ route($pageResource.'.index') }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Package</th>
                            <th>Vehicle Type</th>
                            <th>From Location - To Location</th>
                            <th>Package Start Date</th>
                            <th>Price</th>
                            <th>Price Type</th>
                            <th>Status</th>
                            <th>Created at</th>
                            <th class="not-export-col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $key => $val)
                        <tr>
                        <td>{{$serial++}}</td>
                            <td>{{$val->package_title}}</td>
                            <td>{{$val->VehicleType->vehicle_type_english}}</td>
                            <td>{{$val->from_location.' - '.$val->to_location}}</td>
                            <td>{{date('d/m/Y',strtotime($val->package_start_date))}}</td>
                            <td>{{$val->ServieSetting !="" ? $val->ServieSetting->price : ''}}</td>
                            <td>{{$val->ServieSetting !="" ? $val->ServieSetting->pricing_type : ''}}</td>
                            <td>{{$val->status == '1' ? 'Active' : 'Inactive'}}</td>
                            <td>{{$val->created_at}}</td>
                            <td>
                            <?php
                            $access = 1;
                            listAction([
                                actionLi(route($pageResource.'.show', $val->id).qString(), 'show', $access),
                                actionLi(route($pageResource.'.edit', $val->id).qString(), 'edit', $access),
                                actionLi(route($pageResource.'.imageVideoUpload', $val->id).qString(), 'media', $access),
                                actionLi(route($pageResource.'.destroy', $val->id).qString(), 'delete', $access),
                            ]);
                            ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>

        @elseif (isset($images))
        <div class="tab-pane active">
            <div class="row">
                <div class="col-sm-10">
                    <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{$id}}" name="id"/>

                        <div class="form-group{{ $errors->has('image_upload') ? ' has-error' : '' }}" id="image_upload_div">
                            <label class="control-label col-sm-4">Image:</label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="image_upload[]" multiple>
                                <span>300px - 600px</span>
                                @if ($errors->has('image_upload'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image_upload') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('video_upload') ? ' has-error' : '' }}" id="video_upload_div">
                            <label class="control-label col-sm-4">Video Link:</label>
                            <div class="col-sm-8">
                                <table class="table table-bordered table-additional" id="dynamic_image_field">
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" name="video_upload[0]" placeholder="Vidro URL">
                                        </td>
                                        <td><button type="button" name="add" id="add_image" class="btn btn-success">Add More</button></td>
                                    </tr>
                                </table>

                                @if ($errors->has('video_upload'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('video_upload') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success btn-flat btn-lg">Create</button>
                            <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
  </div>
</section>

<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    @if(isset($edit) || isset($create))
        CKEDITOR.replace('post_content');
    @endif

    $(function () {
        $("#datetimepicker1").datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'Y-m-d',
            formatDate: 'Y-m-d',
            timepicker: false,
            datepicker: true,
        });

        $('#datetimepicker2').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'Y-m-d',
            formatDate: 'Y-m-d',
            timepicker: false,
            datepicker: true,
        });
    });

    $(document).ready(function(){
        @if(isset($edit))
            var i=0+{{(count($data['additional_pricing'])-1)}};
        @else
            var i=0;
        @endif
        $('#add').click(function(){
            var ad_label = $('#additional_label_0').val();
            var ad_price = $('#additional_price_0').val();
            $('#additional_label_0').css('border','1px solid #d2d6de');
            $('#additional_price_0').css('border','1px solid #d2d6de');
            if (ad_label == "") {
                $('#additional_label_0').css('border','1px solid red');
                $('#additional_price_0').css('border','1px solid red');
            } else if (ad_price == "") {
                $('#additional_price_0').css('border','1px solid red');
            }else{
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" class="form-control" name="additional_price['+i+'][label]" id="additional_label_'+i+'" placeholder="Price label" required/></td><td><input type="number" class="form-control" name="additional_price['+i+'][price]" id="additional_price_'+i+'" placeholder="Price" value="" required/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
            }
        });

        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });

    });

    $(document).ready(function(){
        var j=0;
        $('#add_image').click(function(){
            j++;
            $('#dynamic_image_field').append('<tr id="row'+j+'"><td><input type="text" class="form-control" name="video_upload['+j+']" placeholder="Vidro URL" required></td><td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_remove">X</button></td></tr>');
        });

        $(document).on('click', '.btn_remove', function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });

    });

    function dayTrip(value){
        if (value == 'no') {
            $('#trip_start_date_div').show();
            $('#trip_end_date_div').show();
        }else if (value == 'yes') {
            $('#trip_start_date_div').hide();
            $('#trip_end_date_div').hide();
        }

        $("#datetimepicker1").datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });

        $('#datetimepicker2').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });
    }
    $(function () {
        $('#datetimepicker3').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });

        $('#datetimepicker4').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });
    });
</script>
<style>
table.table-additional{
    margin-bottom: 0px;
}
</style>
@endsection
