@extends('admin.layouts.master')

<?php
$pageName = 'Page & Policy';
$pageResource = 'admin.policy';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.index') . qString() }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>
        <li {{ (isset($create))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.create') . qString() }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
            </a>
        </li>

        @if (isset($edit))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
            </a>
        </li>
        @endif

        @if (isset($show))
        <li class="active">
            <a href="#">
                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
            </a>
        </li>
        @endif
    </ul>

    <div class="tab-content">
        @if(isset($show))
        <div class="tab-pane active">
            @if (isset($data))
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                        <thead>
                            <tr class="hide">
                                <th style="width:120px;"></th>
                                <th style="width:10px;"></th>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th style="width:120px;">Type</th>
                                <th style="width:10px;">:</th>
                                <td>{{ $data->policy_type }}</td>
                            </tr>
                            <tr>
                                <th>Content</th>
                                <th>:</th>
                                <td>{!! $data->policy_details !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @else
                <div class="box-body">
                    {!! notFoundText() !!}
                </div>
            @endif
        </div>

        @elseif(isset($edit) || isset($create))
        <div class="tab-pane active">
            <div class="box-body">
                <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    {!! (isset($edit))?'<input name="_method" type="hidden" value="PUT">':'' !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group{{ $errors->has('policy_type') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Type:</label>
                                <div class="col-sm-10">
                                    <?php $policy_type = (isset($data->policy_type))?$data->policy_type:old('policy_type'); ?>
                                    <select name="policy_type" class="form-control" required>
                                        <option value="">Select One</option>
                                        @foreach(['Home', 'About Us', 'Fleet', 'Careers', 'News', 'Cancellation Policy', 'Wait Time Policy', 'Privacy Policy', 'Refund And Return Policy', 'Terms And Conditions', 'FAQ'] as $policy)
                                            <option value="{{ $policy }}" {{ ($policy_type==$policy)?'selected':'' }}>{{ $policy }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('policy_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('policy_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('policy_details') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Content:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="editor" name="policy_details" required rows="10">{{ isset($data->policy_details)?$data->policy_details:old('policy_details') }}</textarea>

                                    @if ($errors->has('policy_details'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('policy_details') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Update':'Create' }}</button>
                                <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @elseif (isset($lists))
        <div class="tab-pane active">
            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Type</th>
                            <th>Content</th>
                            <th class="not-export-col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $key => $val)
                        <tr>
                            <td>{{$serial++}}</td>
                            <td>{{$val->policy_type}}</td>
                            <td>{{ excerpt($val->policy_details) }}</td>
                            <td>
                            <?php
                            $access = 1;
                            listAction([
                                actionLi(route($pageResource.'.show', $val->id).qString(), 'show', $access),
                                actionLi(route($pageResource.'.edit', $val->id).qString(), 'edit', $access),
                                actionLi(route($pageResource.'.destroy', $val->id).qString(), 'delete', $access),
                            ]);
                            ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
        @endif
    </div>
  </div>
</section>
@endsection
