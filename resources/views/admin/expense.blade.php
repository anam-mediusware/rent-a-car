@extends('admin.layouts.master')

<?php
$pageName = 'Expense';
$pageResource = 'admin.expense';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.index') . qString() }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>

        <li {{ (isset($create))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.create') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
            </a>
        </li>

        @if (isset($edit))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
            </a>
        </li>
        @endif

        @if (isset($show))
        <li class="active">
            <a href="#">
                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
            </a>
        </li>
        @endif
    </ul>

    <div class="tab-content">
        @if(isset($edit) || isset($create))
        <div class="tab-pane active">
            <div class="box-body">
                <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    {!! (isset($edit))?'<input name="_method" type="hidden" value="PUT">':'' !!}
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Category :</label>
                                <div class="col-sm-10">
                                    <?php $category_id = (isset($data->category_id))?$data->category_id:old('category_id'); ?>
                                    <select name="category_id" class="form-control" required>
                                        <option value="">Select One</option>
                                        @foreach($categoryData as $cat)
                                            <option value="{{ $cat->id }}" {{ ($category_id==$cat->id)?'selected':'' }}>{{ $cat->category_name }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('category_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('category_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('expense_amount') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Amount :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="expense_amount" value="{{ isset($data->expense_amount)?$data->expense_amount:old('expense_amount') }}" required>

                                    @if ($errors->has('expense_amount'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('expense_amount') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('expense_date') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Date :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control datepicker" name="expense_date" value="{{ isset($data->expense_date)?$data->expense_date:date('Y-m-d') }}" required>

                                    @if ($errors->has('expense_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('expense_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('expense_remark') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2">Remark:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="expense_remark" rows="10">{{ isset($data->expense_remark)?$data->expense_remark:old('expense_remark') }}</textarea>

                                    @if ($errors->has('expense_remark'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('expense_remark') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Update':'Create' }}</button>
                                <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @elseif (isset($lists))
        <div class="tab-pane active">
            <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Write your search text...">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th style="width:30px;">SL.</th>
                            <th>Date</th>
                            <th>Category</th>
                            <th>Remark</th>
                            <th>Amount</th>
                            <th class="not-export-col" style="width:100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $key => $val)
                        <tr>
                            <td>{{$serial++}}</td>
                            <td>{{$val->expense_date}}</td>
                            <td>{{$val->category_name}}</td>
                            <td>{!! nl2br($val->expense_remark) !!}</td>
                            <td>{{$val->expense_amount}}</td>
                            <td>
                            <?php
                            $access = 1;
                            listAction([
                                actionLi(route($pageResource.'.edit', $val->id), 'edit', $access),
                                actionLi(route($pageResource.'.destroy', $val->id), 'delete', $access),
                            ]);
                            ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
        @endif
    </div>
  </div>
</section>
@endsection
