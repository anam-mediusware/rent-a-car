@extends('admin.layouts.master')

<?php
$pageName = 'Career';
$pageResource = 'admin.career';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.index') . qString() }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>
        <li {{ (isset($create))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.create') . qString() }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
            </a>
        </li>

        @if (isset($edit))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
            </a>
        </li>
        @endif

        @if (isset($show))
        <li class="active">
            <a href="#">
                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
            </a>
        </li>
        @endif
    </ul>

    <div class="tab-content">
        @if(isset($show))
        <div class="tab-pane active">
            @if (isset($data))
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                        <thead>
                            <tr class="hide">
                                <th style="width:120px;"></th>
                                <th style="width:10px;"></th>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th style="width:120px;">Name</th>
                                <th style="width:10px;">:</th>
                                <td>{{ $data->position_name }}</td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    {!! viewImg('careers', $data->image, ['popup' => 1, 'class' => 'img-circle', 'style' =>'width:100px;', 'fakeimg' => 'no-img']) !!}
                                </td>
                            </tr>
                            <tr>
                                <th>Details</th>
                                <th>:</th>
                                <td>{!! $data->position_details !!}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <td>{{ ($data->status==1)?'Active':(($data->status==2)?'Inactive':'Pending') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @else
                <div class="box-body">
                    {!! notFoundText() !!}
                </div>
            @endif
        </div>

        @elseif(isset($edit) || isset($create))
        <div class="tab-pane active">
            <div class="box-body">
                <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    {!! (isset($edit))?'<input name="_method" type="hidden" value="PUT">':'' !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group{{ $errors->has('position_name') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Name:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="position_name" value="{{ isset($data->position_name)?$data->position_name:old('position_name') }}" required>

                                    @if ($errors->has('position_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('position_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2">Image:</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="image">

                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('position_details') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Details:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="editor" name="position_details" required rows="10">{{ isset($data->position_details)?$data->position_details:old('position_details') }}</textarea>

                                    @if ($errors->has('position_details'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('position_details') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2">Status:</label>
                                <div class="col-sm-10">
                                    <?php $status = (isset($data->status))?$data->status:old('status'); ?>
                                    <select name="status" class="form-control">
                                        @foreach([1 => 'Active', 2 => 'Inactive'] as $sK => $sV)
                                            <option value="{{ $sK }}" {{ ($status==$sK)?'selected':'' }}>{{ $sV }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Update':'Create' }}</button>
                                <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @elseif (isset($lists))
        <div class="tab-pane active">
            <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Write your search text...">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Details</th>
                            <th>Status</th>
                            <th class="not-export-col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $key => $val)
                        <tr>
                            <td>{{$serial++}}</td>
                            <td>{!! viewImg('careers', $val->image, ['popup' => 1, 'class' => 'img-circle', 'style' =>'width:30px;', 'fakeimg' => 'no-img']) !!}</td>
                            <td>{{$val->position_name}}</td>
                            <td>{{ excerpt($val->position_details) }}</td>
                            <td>{{ ($val->status==1)?'Active':(($val->status==2)?'Inactive':'Pending') }}</td>
                            <td>
                            <?php
                            $access = 1;
                            listAction([
                                actionLi(route($pageResource.'.show', $val->id).qString(), 'show', $access),
                                actionLi(route($pageResource.'.edit', $val->id).qString(), 'edit', $access),
                                actionLi(route($pageResource.'.destroy', $val->id).qString(), 'delete', $access),
                            ]);
                            ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
        @endif
    </div>
  </div>
</section>
@endsection
