@extends('admin.layouts.master')

<?php $pageName = 'User'; ?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ url('/admin/user'.qString()) }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>
        <li {{ (isset($create))?'class=active':'' }}>
            <a href="{{ route('admin.users.create') . qString() }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
            </a>
        </li>

        @if (isset($edit))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
            </a>
        </li>
        @endif

        @if (isset($show))
        <li class="active">
            <a href="#">
                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
            </a>
        </li>
        @endif
    </ul>

    <div class="tab-content">
        @if(isset($show))
        <div class="tab-pane active">
            @if (isset($data))
                <div class="box-body table-responsive">
                    <table class="table table-bordered">
                        <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                        <thead>
                            <tr class="hide">
                                <th style="width:120px;"></th>
                                <th style="width:10px;"></th>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3">
                                {!! viewImg('uploads/users', $data->image, ['thumb' => 1, 'popup' => 1, 'class' => 'img-circle', 'style' =>'width:100px;', 'fakeimg' => 'avatar']) !!}
                                </td>
                            </tr>
                            <tr>
                                <th style="width:120px;">Name</th>
                                <th style="width:10px;">:</th>
                                <td>{{ $data->name }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <th>:</th>
                                <td>{{ $data->email }}</td>
                            </tr>
                            <tr>
                                <th>Type</th>
                                <th>:</th>
                                <td>{{ accountType($data->account_type) }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <td>{{ ($data->status==1)?'Active':(($data->status==2)?'Inactive':'Pending') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @else
                <div class="box-body">
                    {!! notFoundText() !!}
                </div>
            @endif
        </div>

        @elseif(isset($edit) || isset($create))
        <div class="tab-pane active">
            <div class="box-body">
                <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    {!! (isset($edit))?'<input name="_method" type="hidden" value="PUT">':'' !!}
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="border-bottom">User Information:</h3>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-3 required">Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="name" value="{{ isset($data->name)?$data->name:old('name') }}" required>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-3">Avatar:</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control" name="image">

                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                            <h3 class="border-bottom">Login Information:</h3>
                            <div class="form-group{{ $errors->has('account_type') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-3 required">Account Type:</label>
                                <div class="col-sm-9">
                                    <?php $account_type = (isset($data->account_type))?$data->account_type:old('account_type'); ?>
                                    <select name="account_type" class="form-control" required>
                                        <option value="">Select Type</option>
                                        @foreach(accountType() as $typK => $typV)
                                            <option value="{{ $typK }}" {{ ($account_type==$typK)?'selected':'' }}>{{ $typV }}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('account_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('account_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-3 required">Email:</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" name="email" value="{{ isset($data->email)?$data->email:old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-3 {{ (isset($create))?'required':'' }}">Password:</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" {{ (isset($create))?'required':'' }}>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('retype_password') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-3 {{ (isset($create))?'required':'' }}">Retype&nbsp;Password:</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="retype_password" {{ (isset($create))?'required':'' }}>

                                    @if ($errors->has('retype_password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('retype_password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-3">Status:</label>
                                <div class="col-sm-9">
                                    <?php $status = (isset($data->status))?$data->status:old('status'); ?>
                                    <select name="status" class="form-control">
                                        @foreach([0 => 'Pending', 1 => 'Active', 2 => 'Inactive'] as $sK => $sV)
                                            <option value="{{ $sK }}" {{ ($status==$sK)?'selected':'' }}>{{ $sV }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Update':'Create' }}</button>
                                <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @elseif (isset($lists))
        <div class="tab-pane active">
            <form method="GET" action="{{ route('user.index') }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <select name="type" class="form-control">
                                <option value="">Select Type</option>
                                @foreach(accountType() as $typK => $typV)
                                    <option value="{{ $typK }}" {{ (Request::get('type')==$typK)?'selected':'' }}>{{ $typV }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="status" class="form-control">
                                <option value="">Select Status</option>
                                @foreach([0 => 'Pending', 1 => 'Active', 2 => 'Inactive'] as $sK => $sV)
                                    <option value="{{ $sK }}" {{ (Request::get('status')==$sK)?'selected':'' }}>{{ $sV }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="email">Email address:</label>
                            <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Write your search text...">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ route($pageResource.'.index') }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Avatar</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Account Type</th>
                            <th>Status</th>
                            <th class="not-export-col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $key => $val)
                        <tr>
                            <td>{{$serial++}}</td>
                            <td>{!! viewImg('uploads/users', $val->image, ['thumb' => 1, 'popup' => 1, 'class' => 'img-circle', 'style' =>'width:30px;', 'fakeimg' => 'avatar']) !!}</td>
                            <td>{{$val->name}}</td>
                            <td>{{$val->email}}</td>
                            <td>{{accountType($val->account_type)}}</td>
                            <td>{{ ($val->status==1)?'Active':(($val->status==2)?'Inactive':'Pending') }}</td>
                            <td>
                            <?php
                            $access = 1;
                            listAction([
                                actionLi(route('user.show', $val->id).qString(), 'show', $access),
                                actionLi(route('user.edit', $val->id).qString(), 'edit', $access),
                                actionLi(route('user.destroy', $val->id).qString(), 'delete', $access),
                                actionLi(url('admin/user/activity', $val->id).qString(), 'activity', $access, ['status' => $val->status]),
                            ]);
                            ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
        @endif
    </div>
  </div>
</section>
@endsection
