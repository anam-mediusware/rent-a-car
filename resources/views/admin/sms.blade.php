@extends('admin.layouts.master')

<?php
$pageName = 'SMS';
$pageResource = 'admin.sms';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.index') . qString() }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>
        <li {{ (isset($create))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.create') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Send A New {{ $pageName }}
            </a>
        </li>

        @if (isset($edit))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Resend A {{ $pageName }}
            </a>
        </li>
        @endif

        @if (isset($show))
        <li class="active">
            <a href="#">
                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
            </a>
        </li>
        @endif
    </ul>

    <div class="tab-content">
        @if(isset($edit) || isset($create))
        <div class="tab-pane active">
            <div class="box-body">
                <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    {!! (isset($edit))?'<input name="_method" type="hidden" value="PUT">':'' !!}
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Mobile Number :</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="mobile_number" value="{{ isset($data->mobile_number)?$data->mobile_number:old('mobile_number') }}" required>

                                    @if ($errors->has('mobile_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mobile_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-2 required">Message:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="message" required rows="10">{{ isset($data->message)?$data->message:old('message') }}</textarea>

                                    @if ($errors->has('message'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('message') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Resend':'Send' }}</button>
                                <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        @elseif (isset($lists))
        <div class="tab-pane active">
            <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Write your search text...">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th style="width:30px;">SL.</th>
                            <th style="width:150px;">Mobile Number </th>
                            <th>Message</th>
                            <th class="not-export-col" style="width:100px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $key => $val)
                        <tr>
                            <td>{{$serial++}}</td>
                            <td>{{$val->mobile_number}}</td>
                            <td>{{ excerpt($val->message) }}</td>
                            <td>
                            <?php
                            $access = 1;
                            listAction([
                                actionLi(route($pageResource.'.edit', $val->id).qString(), 'edit', $access),
                                actionLi(route($pageResource.'.destroy', $val->id).qString(), 'delete', $access),
                            ]);
                            ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
        @endif
    </div>
  </div>
</section>
@endsection
