@extends('admin.layouts.master')

<?php
$pageName = 'SEO';
$pageResource = 'admin.seo';
?>

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li {{ (isset($lists))?'class=active':'' }}>
                    <a href="{{ route($pageResource.'.index') . qString() }}">
                        <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                    </a>
                </li>
                <li {{ (isset($create))?'class=active':'' }}>
                    <a href="{{ route($pageResource.'.create') . qString() }}">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                    </a>
                </li>

                @if (isset($edit))
                    <li class="active">
                        <a href="#">
                            <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
                        </a>
                    </li>
                @endif

                @if (isset($show))
                    <li class="active">
                        <a href="#">
                            <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
                        </a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                @if(isset($show))
                    <div class="tab-pane active">
                        @if (isset($data))
                            <div class="box-body table-responsive">
                                <table class="table table-bordered">
                                    <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                                    <thead>
                                    <tr class="hide">
                                        <th style="width:150px;"></th>
                                        <th style="width:10px;"></th>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th style="width:150px;">Route Name</th>
                                        <th style="width:10px;">:</th>
                                        <td>{{ $data->route_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Route Title</th>
                                        <th>:</th>
                                        <td>{{ $data->route_title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Route Keyword</th>
                                        <th>:</th>
                                        <td>{{ $data->route_keyword }}</td>
                                    </tr>
                                    <tr>
                                        <th>Route Description</th>
                                        <th>:</th>
                                        <td>{{ $data->route_description }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="box-body">
                                {!! notFoundText() !!}
                            </div>
                        @endif
                    </div>

                @elseif(isset($edit) || isset($create))
                    <div class="tab-pane active">
                        <div class="box-body">
                            <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                {!! (isset($edit))?'<input name="_method" type="hidden" value="PUT">':'' !!}
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group{{ $errors->has('route_name') ? ' has-error' : '' }}">
                                            <label class="control-label col-sm-3 required">Route Name:</label>
                                            <div class="col-sm-9">
                                                <?php $route_name = (isset($data->route_name)) ? $data->route_name : old('route_name'); ?>
                                                <select name="route_name" class="form-control" required>
                                                    <option value="">Select One</option>
                                                    @foreach($routeArray as $rK => $rV)
                                                        <option value="{{ $rK }}" {{ ($route_name==$rK)?'selected':'' }}>{{ $rV }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('route_title') ? ' has-error' : '' }}">
                                            <label class="control-label col-sm-3 required">Route Title:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="route_title" value="{{ isset($data->route_title)?$data->route_title:old('route_title') }}" required>

                                                @if ($errors->has('route_title'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('route_title') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('route_keyword') ? ' has-error' : '' }}">
                                            <label class="control-label col-sm-3">Route Keyword:</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="route_keyword" value="{{ isset($data->route_keyword)?$data->route_keyword:old('route_keyword') }}">

                                                @if ($errors->has('route_keyword'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('route_keyword') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('route_description') ? ' has-error' : '' }}">
                                            <label class="control-label col-sm-3">Route Description:</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="route_description" rows="10">{{ isset($data->route_description)?$data->route_description:old('route_description') }}</textarea>

                                                @if ($errors->has('route_description'))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('route_description') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Update':'Create' }}</button>
                                            <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                @elseif (isset($lists))
                    <div class="tab-pane active">
                        <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                            <div class="box-header text-right">
                                <div class="row">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Write your search text...">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Search</button>
                                        <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="box-body table-responsive">
                            <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                            <?php //echo $pagination->msg; ?>
                            <table class="table table-bordered table-hover dataTable">
                                <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                                <thead>
                                <tr>
                                    <th>SL.</th>
                                    <th>Route Name</th>
                                    <th>Route Title</th>
                                    <th>Route Keyword</th>
                                    <th>Route Description</th>
                                    <th class="not-export-col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($records as $key => $val)
                                    <tr>
                                        <td>{{$serial++}}</td>
                                        <td>{{$val->route_name}}</td>
                                        <td>{{$val->route_title}}</td>
                                        <td>{{$val->route_keyword}}</td>
                                        <td>{{ excerpt($val->route_description) }}</td>
                                        <td>
                                            <?php
                                            $access = 1;
                                            listAction([
                                                actionLi(route($pageResource . '.show', $val->id) . qString(), 'show', $access),
                                                actionLi(route($pageResource . '.edit', $val->id) . qString(), 'edit', $access),
                                                actionLi(route($pageResource . '.destroy', $val->id) . qString(), 'delete', $access),
                                            ]);
                                            ?>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="text-right">
                                {{ $records->appends(Request::except('page'))->links() }}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
