<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/bootstrap/css/bootstrap.min.css') }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/font-awesome/css/font-awesome.min.css') }}">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/select2/css/select2.min.css') }}">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/datatables/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/datatables/export/buttons.dataTables.min.css') }}">

    <!-- Date Time Picker -->
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/datetimepicker/jquery.datetimepicker.css') }}">

    <!-- fancybox -->
    <link rel="stylesheet" href="{{ asset('admin-assets/plugins/fancybox-3.0/jquery.fancybox.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin-assets/css/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin-assets/css/skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin-assets/css/styles.css') }}">
    @stack('styles')

    <!-- jQuery 3 -->
    <script src="{{ asset('admin-assets/plugins/jquery/jquery.min.js') }}"></script>
    <script> var base_url = '{{ url('') }}' </script>
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="{{ route('admin.dashboard') }}" class="logo">
            <span class="logo-mini">
                {{ config('app.name', 'Laravel') }}
            </span>
            <span class="logo-lg">
                {{ config('app.name', 'Laravel') }}
            </span>
        </a>

        @include('admin.layouts.inc.navbar')
    </header>


    @include('admin.layouts.inc.sidebar')

    <div class="content-wrapper">
        @yield('content')
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            Developed by <a href="#" target="_blank">Tanvir</a>
        </div>
        <strong>
            Copyright &copy; {{date('Y')}} {{ config('app.name', 'Laravel') }}.
        </strong> All rights reserved.
    </footer>
</div>

<script src="{{ mix('admin-assets/js/app.js')}}"></script>

<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('admin-assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('admin-assets/plugins/select2/js/select2.full.min.js') }}"></script>

<!-- Date Time Picker -->
<script src="{{ asset('admin-assets/plugins/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>

<!-- fancybox -->
<script src="{{ asset('admin-assets/plugins/fancybox-3.0/jquery.fancybox.min.js') }}"></script>

<!-- Form validate -->
<script src="{{ asset('admin-assets/plugins/validate/jquery.validate.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>

<script src="{{ asset('admin-assets/plugins/datatables/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/plugins/datatables/export/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/plugins/datatables/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/plugins/datatables/export/buttons.print.min.js') }}"></script>

<!-- Editor -->
<script src="{{ asset('admin-assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('admin-assets/plugins/ckeditor/samples/js/sample.js') }}"></script>
<script> if ($("#editor").length > 0) { CKEDITOR.replace("editor"); } </script>

<!-- App -->
<script src="{{ asset('admin-assets/js/app.min.js') }}"></script>

<script>
    var current_url = window.location.href;

    function setActive(init = false) {
        if (!init) {
            $('ul.treeview-menu.menu-open').filter(function () {
                return !$(this).children('li').children('a').filter(function () {
                    return this.href === current_url
                }).length;
            }).slideUp().removeClass('menu-open').closest('li').removeClass('active');
        }
        // for sidebar menu entirely but not cover treeview
        $('ul.sidebar-menu a').filter(function () {
            return this.href === current_url;
        }).parent('li').addClass('active');

        // for treeview
        $('ul .treeview a').filter(function () {
            return this.href === current_url;
        }).parentsUntil(".sidebar-menu > .treeview").slideDown().addClass('menu-open').closest('li').addClass('active');
    }

    setActive(true);
    $(".sidebar-menu").mouseleave(function () {
        setActive(false);
    });
</script>
@stack('scripts')
</body>
</html>
