<nav class="navbar navbar-static-top" role="navigation">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <span class="project-name-header"></span>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ (Auth::user()->image!='')?asset('uploads/users/thumb_'.Auth::user()->image):asset('admin-assets/img/avatar.png') }}" class="user-image" alt="{{ Auth::user()->name }}">
                    <span class="hidden-xs">
                        {{ Auth::user()->name }}
                    </span>
                </a>

                <ul class="dropdown-menu">
                    <li class="user-header">
                        <img src="{{ (Auth::user()->image!='')?asset('uploads/users/thumb_'.Auth::user()->image):asset('admin-assets/img/avatar.png') }}" class="img-circle" alt="{{ Auth::user()->name }}">
                        <p>
                            {{ Auth::user()->name }}
                            <small>
                                {{ Auth::user()->email }}
                            </small>
                        </p>
                    </li>

                    <li class="user-footer">
                        <div class="pull-left">
                            <a class="btn btn-default btn-flat" href="{{route('admin.profile.index')}}">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-default btn-flat" href="{{ route('admin.logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" class="non-validate" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
