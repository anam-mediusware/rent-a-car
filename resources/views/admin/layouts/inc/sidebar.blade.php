<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ (Auth::user()->image!='')?asset('uploads/users/thumb_'.Auth::user()->image):asset('admin-assets/img/avatar.png') }}" class="img-circle" alt="{{ Auth::user()->name }}">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="header" style="padding:2px;"></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-handshake-o"></i> <span>Cars & Drivers</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li id="menu_partner">
                        <a href="{{url('/admin/cars')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Cars</span>
                        </a>
                    </li>
                    <li id="menu_partner_car">
                        <a href="{{url('/admin/car-models')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Car Models</span>
                        </a>
                    </li>
                    <li id="menu_partner_car">
                        <a href="{{url('/admin/drivers')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Drivers</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="{{url('/admin/client')}}">
                    <i class="fa fa-users"></i>
                    <span>Client</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-handshake-o"></i> <span>Merchants</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li id="menu_merchant">
                        <a href="{{url('/admin/merchant')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Merchants</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-car"></i> <span>Booking</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('admin/booking/create')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>New Booking</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/booking')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Booking List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/booking/booking-bid-list')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Booking Bid List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/bidding-trips')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Bidding Trip List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/booking/booking-cancelled-list')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Cancelled Trips</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/service-setting')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Cancelled Booking</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/service-setting')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Payment Pending</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/service-setting')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Payment Cancelled</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/service-setting')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Failed to Pay</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-bar-chart"></i> <span>Report</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li id="menu_report_receivable">
                        <a href="{{url('/admin/report/receivable')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Amount Receivable</span>
                        </a>
                    </li>
                    <li id="menu_report_payable">
                        <a href="{{url('/admin/report/payable')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Amount Payable</span>
                        </a>
                    </li>
                    <li id="menu_report_income">
                        <a href="{{url('/admin/report/income')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Income (Commission)</span>
                        </a>
                    </li>
                    <li id="menu_report_expense">
                        <a href="{{url('/admin/report/expense')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Expenses</span>
                        </a>
                    </li>
                    <li id="menu_report_profit-loss">
                        <a href="{{url('/admin/report/profit-loss')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Net Income</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-coffee"></i> <span>Expense</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li id="menu_expense">
                        <a href="{{url('/admin/expense')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Expense</span>
                        </a>
                    </li>
                    <li id="menu_expense_category">
                        <a href="{{url('/admin/expense-category')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Expense Category</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-user-circle"></i> <span>CRM</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li id="menu_crm">
                        <a href="{{url('/admin/crm')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>CRM</span>
                        </a>
                    </li>
                    <li id="menu_crm_followup">
                        <a href="{{url('/admin/crm/followup')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Followup</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li id="menu_sms">
                <a href="{{url('/admin/sms')}}">
                    <i class="fa fa-comment-o"></i>
                    <span>SMS</span>
                </a>
            </li>

            <li id="menu_contact-mail">
                <a href="{{url('/admin/contact-mail')}}">
                    <i class="fa fa-envelope"></i>
                    <span>Contact Mail</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-cogs"></i> <span>Service Setting</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('/admin/package')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Packages</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{url('/admin/service-setting')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Service Pricing</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-globe"></i> <span>Website</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li id="menu_tourism">
                        <a href="{{url('/admin/tourism')}}">
                            <i class="fa fa-rocket"></i>
                            <span>Tourism</span>
                        </a>
                    </li>
                    <li id="menu_management">
                        <a href="{{url('/admin/management')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Management</span>
                        </a>
                    </li>
                    <li id="menu_career">
                        <a href="{{url('/admin/career')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Career</span>
                        </a>
                    </li>
                    <li id="menu_news">
                        <a href="{{url('/admin/news')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>News</span>
                        </a>
                    </li>
                    <li id="menu_policy">
                        <a href="{{url('/admin/policy')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Page & Policy</span>
                        </a>
                    </li>
                    <li id="menu_contact-type">
                        <a href="{{url('/admin/contact-type')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Contact Type</span>
                        </a>
                    </li>
                    <li id="menu_slide">
                        <a href="{{url('/admin/slide')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Slide</span>
                        </a>
                    </li>
                    <li id="menu_seo">
                        <a href="{{url('/admin/seo')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>SEO</span>
                        </a>
                    </li>
                    <li id="menu_web_settings">
                        <a href="{{url('/admin/web-settings')}}">
                            <i class="fa fa-arrow-right"></i>
                            <span>Website Settings</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </section>
</aside>
