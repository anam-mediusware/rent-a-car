@extends('admin.layouts.master')

@php
    $pageName = 'Drivers';
    $pageResource = 'admin.drivers';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content" id="driver-form">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif
                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') . qString() }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif
                <li class="active">
                    <a href="#">
                        <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    @if (isset($data))
                        <div class="box-body">
                            <form method="POST" action="{{ route($pageResource.'.update', $data->id) }}" class="form-horizontal" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="row">
                                    <div class="col-sm-10">

                                        @include('admin.drivers.form')

                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-success btn-flat btn-lg">Update
                                            </button>
                                            <a href="{{ route($pageResource.'.index') }}" class="btn btn-warning btn-flat btn-lg">Back</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </section>
@endsection

@push('scripts')
    @include('admin.drivers.script')
@endpush
