@extends('admin.layouts.master')

@php
    $pageName = 'Drivers';
    $pageResource = 'admin.drivers';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li {{ (isset($lists))?'class=active':'' }}>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif

                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') . qString() }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <form method="GET" action="{{ route($pageResource.'.store') }}" class="form-inline">
                        <div class="box-header text-right">
                            <div class="row">
                                <div class="form-group">
                                    <select class="form-control select2" name="driver">
                                        <option value="">Select One</option>
                                        @foreach($drivers as $value)
                                            <option value="{{ $value->id }}" {{ (Request::get('driver')== $value->id)?'selected' : '' }}>{{ $value->full_name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="email address">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-flat">Search</button>
                                    <a class="btn btn-warning btn-flat" href="{{ route($pageResource.'.index') }}">X</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="box-body table-responsive">
                        <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                        <table class="table table-bordered table-hover dataTable">
                            <caption class="hidden"><h3>{{ $pageName }} List</h3></caption>
                            <thead>
                            <tr>
                                <th>SL.</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>City</th>
                                <th>Payment Method</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th class="not-export-col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $key => $val)
                                <tr>
                                    <td>{{$serial++}}</td>
                                    <td>{{$val->full_name}}</td>
                                    <td>{{$val->email_address}}</td>
                                    <td>{{$val->gender == 'M' ? 'Male' : 'Female'}}</td>
                                    <td>{{$val['DrivingCitie']['city_name_english']}}</td>
                                    <td>{{$val['DriversPayoutMethod']['method_name_english']}}</td>
                                    <td>{{$val->status == 1 ? 'Active' : 'Inactive'}}</td>
                                    <td>{{$val->created_at}}</td>
                                    <td>
                                        <?php
                                        $access = 1;
                                        listAction([
                                            actionLi(route($pageResource.'.show', $val->id).qString(), 'show', $access),
                                            actionLi(route($pageResource.'.edit', $val->id).qString(), 'edit', $access),
                                            actionLi(route($pageResource.'.destroy', $val->id).qString(), 'delete', $access),
                                        ]);
                                        ?>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-right">
                            {{ $records->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
