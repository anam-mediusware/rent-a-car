<script type="text/javascript">
    $(function () {
        $("#datetimepicker1").datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });

        $('#datetimepicker2').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });
    });

    function createEdit()
    {
        var dv_li_front = document.getElementById('driveing_license_front');
        var dv_li_back = document.getElementById('driveing_license_back');
        var expiry_date = $('#datetimepicker2').val();

        $('#datetimepicker2'). prop("required", false);
        if (dv_li_front.files.length > 0 || dv_li_back.files.length > 0){
            if(expiry_date == "") {
                $('#datetimepicker2'). prop("required", true);
                return false;
            }
        }
        return true;
    }

    function paymentDetails(id){
        //console.log(id);
        var data = { "id": id,  "_token": "{{ csrf_token() }}" }
        $.ajax({
            type: "POST",
            url: '{{ url("admin/drivers/getPaymentDetails") }}',
            data: data,
            success: function (response) {
                //console.log(response.length);
                var paymentHTML = '';
                for (var i = 0;  i < response.length; i++) {
                    paymentHTML += '<div class="form-group">'+
                        '<label class="control-label col-sm-4 required">'+response[i]+'</label>'+
                        '<div class="col-sm-8">'+
                        '<input type="text" class="form-control" name="payment_details['+i+'][value]" required>'+
                        '<input type="hidden" value="'+response[i]+'" name="payment_details['+i+'][field]">'+
                        '</div>'+
                        '</div>';
                }
                $('#payment_detials').html(paymentHTML);
            }
        });
    }

    window.vueApp = new Vue({
        el: '#driver-form',
        data: {
            addCar: false
        },
        mounted() {
        },
        methods: {
        }
    });
</script>
