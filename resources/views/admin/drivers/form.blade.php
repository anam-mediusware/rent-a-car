<div class="form-group{{ $errors->has('merchant_id') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4">Merchants:</label>
    <div class="col-sm-8">
        <?php $merchant_id = (isset($data->merchants_id))?$data->merchants_id:old('merchant_id');?>
        <select class="form-control" name="merchant_id">
            <option value="">No Merchant</option>
            @if (!empty($merchants))
                @foreach ($merchants as $merchant)
                    <option value="{{ $merchant->id}}" {{ ($merchant_id==$merchant->id)?'selected':'' }}>{{ $merchant->name }}</option>
                @endforeach
            @endif
        </select>
        @if ($errors->has('merchant_id'))
            <span class="help-block">
                <strong>{{ $errors->first('merchant_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Full Name:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="full_name" value="{{ isset($data->full_name)?$data->full_name:old('full_name') }}" required>

        @if ($errors->has('full_name'))
            <span class="help-block">
                <strong>{{ $errors->first('full_name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('mobile_number') ? ' has-error' : '' }}">
    <label for="mobile_number" class="control-label col-sm-4 required">Mobile:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="mobile_number" name="mobile_number" value="{{ isset($data->mobile_number)?$data->mobile_number:old('mobile_number') }}" required>

        @if ($errors->has('mobile_number'))
            <span class="help-block">
                <strong>{{ $errors->first('mobile_number') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('emergency_contact_number') ? ' has-error' : '' }}">
    <label for="emergency_contact_number" class="control-label col-sm-4">Emergency Contact Number:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="emergency_contact_number" name="emergency_contact_number" value="{{ isset($data->emergency_contact_number)?$data->emergency_contact_number:old('emergency_contact_number') }}">

        @if ($errors->has('emergency_contact_number'))
            <span class="help-block">
                <strong>{{ $errors->first('emergency_contact_number') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('email_address') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4">Email:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="email_address" value="{{ isset($data->email_address)?$data->email_address:old('email_address') }}">

        @if ($errors->has('email_address'))
            <span class="help-block">
                <strong>{{ $errors->first('email_address') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Gender:</label>
    <div class="col-sm-8">
        <?php $gender = (isset($data->gender))?$data->gender:old('gender'); ?>
        <select class="form-control" name="gender" required>
            <option value="">Select Gender</option>
            <option value="M" {{ ($gender=='M')?'selected':'' }}>Male</option>
            <option value="F" {{ ($gender=='F')?'selected':'' }}>Female</option>
        </select>

        @if ($errors->has('gender'))
            <span class="help-block">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Date Of Birth:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="date_of_birth" id='datetimepicker1' value="{{ isset($data->date_of_birth)?$data->date_of_birth:old('date_of_birth') }}" autocomplete="off" required>

        @if ($errors->has('date_of_birth'))
            <span class="help-block">
                <strong>{{ $errors->first('date_of_birth') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
    <label for="city_id" class="control-label col-sm-4 required">City:</label>
    <div class="col-sm-8">
        <?php $city_id = (isset($data->city_id))?$data->city_id:old('city_id'); ?>
        <select id="city_id" class="form-control select2" name="city_id" required>
            <option value="">Select City</option>
            @foreach($driver_cities as $city)
                <option value="{{ $city->id }}" {{ ($city_id==$city->id)?'selected':'' }}>{{ $city->Upaz_name }}</option>
            @endforeach
        </select>

        @if ($errors->has('city_id'))
            <span class="help-block">
                <strong>{{ $errors->first('city_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('payment_method_id') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Payment Method:</label>
    <div class="col-sm-8">
        <?php $payment_method_id = (isset($data->payment_method_id))?$data->payment_method_id:old('payment_method_id'); ?>
        <select class="form-control select2" name="payment_method_id" onchange="paymentDetails(this.value)" required>
            <option value="">Select Payment Method</option>
            @foreach($driver_payout_method as $payout)
                <option value="{{ $payout->id }}" {{ ($payment_method_id==$payout->id)?'selected':'' }}>{{ $payout->method_name_english }}</option>
            @endforeach
        </select>

        @if ($errors->has('payment_method_id'))
            <span class="help-block">
                <strong>{{ $errors->first('payment_method_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div id="payment_detials">
    @if(isset($edit))
        @if(!empty($data->payment_details))
            @foreach ($data->payment_details as $key => $payment)
                <div class="form-group">
                    <label class="control-label col-sm-4 required">{{ $payment['field'] }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="payment_details[{{ $key }}][value]" value="{{ $payment['value'] }}" required>
                        <input type="hidden" value="{{ $payment['field'] }}" name="payment_details[{{ $key }}][field]">
                    </div>
                </div>
            @endforeach
        @endif
    @endif
</div>

<div class="form-group{{ $errors->has('driver_photo') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4">Photo of Driver:</label>
    <div class="col-sm-6">
        <input type="file" class="form-control" name="driver_photo">
    </div>
    <div class="col-sm-2">
        {!! viewImg('drivers/photo', isset($data->image)?$data->image:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
    </div>

    @if ($errors->has('driver_photo'))
        <span class="help-block">
            <strong>{{ $errors->first('driver_photo') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('nid_front') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4">NID Front:</label>
    <div class="col-sm-6">
        <input type="file" class="form-control" name="nid_front">
    </div>
    <div class="col-sm-2">
        {!! viewImg('drivers/nid', isset($data->driver_document['nid_front_img'])?$data->driver_document['nid_front_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
    </div>

    @if ($errors->has('nid_front'))
        <span class="help-block">
        <strong>{{ $errors->first('nid_front') }}</strong>
    </span>
    @endif
</div>

<div class="form-group{{ $errors->has('nid_back') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4">NID Back:</label>
    <div class="col-sm-6">
        <input type="file" class="form-control" name="nid_back">
    </div>
    <div class="col-sm-2">
        {!! viewImg('drivers/nid', isset($data->driver_document['nid_back_img'])?$data->driver_document['nid_back_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
    </div>

    @if ($errors->has('nid_back'))
        <span class="help-block">
        <strong>{{ $errors->first('nid_back') }}</strong>
    </span>
    @endif
</div>

<div class="form-group{{ $errors->has('driveing_license_front') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4">Driving License Front:</label>
    <div class="col-sm-6">
        <input type="file" class="form-control" name="driveing_license_front" id="driveing_license_front">
    </div>
    <div class="col-sm-2">
        {!! viewImg('drivers/license', isset($data->driver_document['dv_lic_front_img'])?$data->driver_document['dv_lic_front_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
    </div>

    @if ($errors->has('driveing_license_front'))
        <span class="help-block">
        <strong>{{ $errors->first('driveing_license_front') }}</strong>
    </span>
    @endif
</div>

<div class="form-group{{ $errors->has('driveing_license_back') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4">Driving License Back:</label>
    <div class="col-sm-6">
        <input type="file" class="form-control" name="driveing_license_back" id="driveing_license_back">
    </div>
    <div class="col-sm-2">
        {!! viewImg('drivers/license', isset($data->driver_document['dv_lic_back_img'])?$data->driver_document['dv_lic_back_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
    </div>

    @if ($errors->has('driveing_license_back'))
        <span class="help-block">
        <strong>{{ $errors->first('driveing_license_back') }}</strong>
    </span>
    @endif
</div>

<div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4">Driving License Expiry Date:</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="expiry_date" id="datetimepicker2" value="{{ isset($data->driver_document['dv_lic_expiry_date']) && $data->driver_document['dv_lic_expiry_date'] !="" ? date('d/m/Y',strtotime($data->driver_document['dv_lic_expiry_date'])):old('expiry_date') }}" autocomplete="off">

        @if ($errors->has('expiry_date'))
            <span class="help-block">
                <strong>{{ $errors->first('expiry_date') }}</strong>
            </span>
        @endif
    </div>
</div>
@if(isset($edit))
    <input type="hidden" name="nid_front_id" value="{{ $data->driver_document['nid_front_id'] > 0 ? $data->driver_document['nid_front_id'] : 0 }}"/>

    <input type="hidden" name="nid_back_id" value="{{ $data->driver_document['nid_back_id'] > 0 ? $data->driver_document['nid_back_id'] : 0 }}"/>

    <input type="hidden" name="dv_lic_front_id" value="{{ $data->driver_document['dv_lic_front_id'] > 0 ? $data->driver_document['dv_lic_front_id'] : 0 }}"/>

    <input type="hidden" name="dv_lic_back_id" value="{{ $data->driver_document['dv_lic_back_id'] > 0 ? $data->driver_document['dv_lic_back_id'] : 0 }}"/>
@endif

@if(!isset($edit))

<div class="form-group{{ $errors->has('has_car') ? ' has-error' : '' }}">
    <label for="add_car" class="control-label col-sm-4">Add Car:</label>
    <div class="col-sm-8">
        <input id="add_car" class="form-check-input" type="checkbox" name="add_car" v-model="addCar">
    </div>
</div>

@include('admin.drivers.car-info-form')

@endif
