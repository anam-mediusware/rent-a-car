@extends('admin.layouts.master')

@php
    $pageName = 'Drivers';
    $pageResource = 'admin.drivers';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif
                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') . qString() }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif

                <li class="active">
                    <a href="#">
                        <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    @if (isset($data))
                        <div class="box-body table-responsive">
                            <table class="table table-bordered">
                                <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                                <thead>
                                <tr class="hide">
                                    <th style="width:230px;"></th>
                                    <th style="width:10px;"></th>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th style="width:230px;">Fullname</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{ $data->full_name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <th>:</th>
                                    <td>{{ $data->email_address }}</td>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <th>:</th>
                                    <td>{{ $data->gender == 'M' ? 'Male' : 'Female' }}</td>
                                </tr>
                                <tr>
                                    <th>Date Of Birth</th>
                                    <th>:</th>
                                    <td>{{ date('d/m/Y', strtotime($data->date_of_birth)) }}</td>
                                </tr>
                                <tr>
                                    <th>City</th>
                                    <th>:</th>
                                    <td>
                                        {{ $data->DrivingCity->Upaz_name }}, {{ $data->DrivingCity->Dist_name }}, {{ $data->DrivingCity->Divi_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Payment Method</th>
                                    <th>:</th>
                                    <td>
                                        {{ $data->DriversPayoutMethod->method_name_english }}
                                    </td>
                                </tr>

                                @if(!empty($data->payment_details))
                                    @foreach ($data->payment_details as $key => $payment)
                                        <tr>
                                            <th>{{ $payment['field'] }}</th>
                                            <th>:</th>
                                            <td>{{ $payment['value'] }}</td>
                                        </tr>
                                    @endforeach
                                @endif

                                @if(!empty($data->cars))
                                    <tr>
                                        <th>Car Model</th>
                                        <th>:</th>
                                        <th>{{ $data->cars->model }}, {{ $data->cars->year }}</th>
                                    </tr>
                                @endif

                                <tr>
                                    <th>Driver Photo</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('drivers/photo', $data->image, ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Driving License Front</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('drivers/license', $data->driver_document['dv_lic_front_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Driving License Back</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('drivers/license', $data->driver_document['dv_lic_back_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>

                                <tr>
                                    <th>Expiry Date</th>
                                    <th>:</th>
                                    <td>{{$data->driver_document['dv_lic_expiry_date'] !="" ? date('d/m/Y',strtotime($data->driver_document['dv_lic_expiry_date'])) : ''}}</td>
                                </tr>

                                <tr>
                                    <th>NID Front</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('drivers/nid', $data->driver_document['nid_front_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>NID Back</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('drivers/nid', $data->driver_document['nid_back_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="box-body">
                            {!! notFoundText() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
