@if (!isset($data))
    @php($data = null)
@endif
<div v-if="addCar">
    <div class="form-group{{ $errors->has('metro') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required">Metro:</label>
        <div class="col-sm-8">
            <?php $metro = (isset($data->metro)) ? $data->metro : old('metro'); ?>
            <select class="form-control select2" name="metro" required>
                <option value="">Select Metor</option>
                @if ($division_district)
                    @foreach ($division_district as $division)
                        <option value="{{ $division[1] }}" {{ ($metro==$division[1])?'selected':'' }}>{{ $division[1] }}</option>
                    @endforeach
                @endif
            </select>

            @if ($errors->has('metro'))
                <span class="help-block">
                <strong>{{ $errors->first('metro') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('alphabetical') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required">Alphabetical Serial:</label>
        <div class="col-sm-8">
            <?php $alphabetical_s = (isset($data->alphabetical_serial)) ? $data->alphabetical_serial : old('alphabetical'); ?>
            <select class="form-control select2" name="alphabetical" required>
                <option value="">Select Alphabetical Serial</option>
                @if ($alphabetical_serial)
                    @foreach ($alphabetical_serial as $alphabetical)
                        <option value="{{ $alphabetical[0] }}" {{ ($alphabetical_s==$alphabetical[0]) ? 'selected':'' }}>{{ $alphabetical[0] }}</option>
                    @endforeach
                @endif
            </select>

            @if ($errors->has('alphabetical'))
                <span class="help-block">
                <strong>{{ $errors->first('alphabetical') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('serial_number') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required">Serial Number:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="serial_number" value="{{ isset($data->serial_number)?$data->serial_number:old('serial_number') }}" required>

            @if ($errors->has('serial_number'))
                <span class="help-block">
                <strong>{{ $errors->first('serial_number') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('vehicle_type') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required">Passenger:</label>
        <div class="col-sm-8">

            <?php $vehicle_type = (isset($data->vehicle_type)) ? $data->vehicle_type : old('vehicle_type'); ?>
            <select class="form-control select2" name="vehicle_type" required>
                <option value="">Select Vehicle</option>
                @if ($vehicle_types)
                    @foreach ($vehicle_types as $vehicle)
                        <option value="{{ $vehicle->vehicle_type_english }}" {{  $vehicle_type==$vehicle->vehicle_type_english?'selected':'' }}>{{ $vehicle->vehicle_type_english }}</option>
                    @endforeach
                @endif
            </select>

            @if ($errors->has('vehicle_type'))
                <span class="help-block">
                <strong>{{ $errors->first('vehicle_type') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('car_brand') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required">Car Maker:</label>
        <div class="col-sm-8">
            <?php $brand = (isset($data->car_brand)) ? $data->car_brand : old('car_brand'); ?>
            <select class="form-control select2" name="car_brand" required>
                <option value="">Select Brand</option>
                @if ($car_brands)
                    @foreach ($car_brands as $car_brand)
                        <option value="{{ $car_brand }}" {{ $brand==$car_brand?'selected':''}}>{{ $car_brand }}</option>
                    @endforeach
                @endif
            </select>

            @if ($errors->has('car_brand'))
                <span class="help-block">
                <strong>{{ $errors->first('car_brand') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('model') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required">Model:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="model" value="{{ isset($data->model)?$data->model:old('model') }}" required>

            @if ($errors->has('model'))
                <span class="help-block">
                <strong>{{ $errors->first('model') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required">Year:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="year" value="{{ isset($data->year)?$data->year:old('year') }}" required>

            @if ($errors->has('year'))
                <span class="help-block">
                <strong>{{ $errors->first('year') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('registration_front') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Registration Front :</label>
        <div class="col-sm-6">
            <input type="file" class="form-control" name="registration_front" id="registration_front">
        </div>
        <div class="col-sm-2">
            {!! viewImg('cars/registration', isset($data->car_document['reg_front_img'])?$data->car_document['reg_front_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
        </div>

        @if ($errors->has('registration_front'))
            <span class="help-block">
            <strong>{{ $errors->first('registration_front') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('registration_back') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Registration Back :</label>
        <div class="col-sm-6">
            <input type="file" class="form-control" name="registration_back" id="registration_back">
        </div>
        <div class="col-sm-2">
            {!! viewImg('cars/registration', isset($data->car_document['reg_back_img'])?$data->car_document['reg_back_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
        </div>

        @if ($errors->has('registration_back'))
            <span class="help-block">
        <strong>{{ $errors->first('registration_back') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('reg_expiry_date') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Registration Expiry Date:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="reg_expiry_date" id="datetimepicker2" value="{{ isset($data->car_document['reg_expiry_date']) && $data->car_document['reg_expiry_date'] !="" ?date('d/m/Y',strtotime($data->car_document['reg_expiry_date'])):old('reg_expiry_date') }}" autocomplete="off">

            @if ($errors->has('reg_expiry_date'))
                <span class="help-block">
                <strong>{{ $errors->first('reg_expiry_date') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('fitness_paper') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Fitness Paper:</label>
        <div class="col-sm-6">
            <input type="file" class="form-control" name="fitness_paper" id="fitness_paper">
        </div>
        <div class="col-sm-2">
            {!! viewImg('cars/fitness', isset($data->car_document['fitness_paper_img'])?$data->car_document['fitness_paper_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
        </div>

        @if ($errors->has('fitness_paper'))
            <span class="help-block">
            <strong>{{ $errors->first('fitness_paper') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('fit_expiry_date') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Fitness Expiry Date:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="fit_expiry_date" id="datetimepicker3" value="{{ isset($data->car_document['fit_expiry_date']) && $data->car_document['fit_expiry_date'] !="" ?date('d/m/Y',strtotime($data->car_document['fit_expiry_date'])):old('fit_expiry_date') }}">

            @if ($errors->has('fit_expiry_date'))
                <span class="help-block">
                <strong>{{ $errors->first('fit_expiry_date') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('tax_token') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Tax Token:</label>
        <div class="col-sm-6">
            <input type="file" class="form-control" name="tax_token" id="tax_token">
        </div>
        <div class="col-sm-2">
            {!! viewImg('cars/tax', isset($data->car_document['tax_token_img'])?$data->car_document['tax_token_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
        </div>

        @if ($errors->has('tax_token'))
            <span class="help-block">
            <strong>{{ $errors->first('tax_token') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('tax_expiry_date') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Tax Expiry Date:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="tax_expiry_date" id="datetimepicker4" value="{{ isset($data->car_document['tax_expiry_date']) && $data->car_document['tax_expiry_date'] !="" ?date('d/m/Y',strtotime($data->car_document['tax_expiry_date'])):old('tax_expiry_date') }}">

            @if ($errors->has('tax_expiry_date'))
                <span class="help-block">
                <strong>{{ $errors->first('tax_expiry_date') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('insurance_paper') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Insurance Paper :</label>
        <div class="col-sm-6">
            <input type="file" class="form-control" name="insurance_paper" id="insurance_paper">
        </div>
        <div class="col-sm-2">
            {!! viewImg('cars/insurance', isset($data->car_document['insurance_paper_img'])?$data->car_document['insurance_paper_img']:'', ['popup' => 1, 'style' =>'height:30px;']) !!}
        </div>

        @if ($errors->has('insurance_paper'))
            <span class="help-block">
            <strong>{{ $errors->first('insurance_paper') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('insurance_expiry_date') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Insurance Expiry Date:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="insurance_expiry_date" id="datetimepicker5" value="{{ isset($data->car_document['insurance_expiry_date']) && $data->car_document['insurance_expiry_date'] !="" ?date('d/m/Y',strtotime($data->car_document['insurance_expiry_date'])):old('insurance_expiry_date') }}">

            @if ($errors->has('insurance_expiry_date'))
                <span class="help-block">
                <strong>{{ $errors->first('insurance_expiry_date') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('car_image_outer') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Car Image - Outer:</label>
        <div class="col-sm-6">
            <input type="file" class="form-control" name="car_image_outer[]" id="car_image_outer" multiple>
        </div>
        <div class="col-sm-2">
            @if ($data)
                @foreach($data->car_image->where('type', 'outer') as $car_image)
                    {!! viewImg('cars/car-img', $car_image['image']?$car_image['image'] : '', ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                @endforeach
            @endif
        </div>

        @if ($errors->has('car_image_outer'))
            <span class="help-block">
            <strong>{{ $errors->first('car_image_outer') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('car_image_inner') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4">Car Image - Inner:</label>
        <div class="col-sm-6">
            <input type="file" class="form-control" name="car_image_inner[]" id="car_image_inner" multiple>
        </div>
        <div class="col-sm-2">
            @if ($data)
                @foreach($data->car_image->where('type', 'inner') as $car_image)
                    {!! viewImg('cars/car-img', $car_image['image']?$car_image['image'] : '', ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                @endforeach
            @endif
        </div>

        @if ($errors->has('car_image_inner'))
            <span class="help-block">
            <strong>{{ $errors->first('car_image_inner') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group{{ $errors->has('owner_name') ? ' has-error' : '' }}">
        <label for="owner_name" class="control-label col-sm-4">Owner Name:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="owner_name" name="owner_name" value="{{ isset($data->owner_name)?$data->owner_name:old('owner_name') }}">

            @if ($errors->has('owner_name'))
                <span class="help-block">
                <strong>{{ $errors->first('owner_name') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('owner_address') ? ' has-error' : '' }}">
        <label for="owner_address" class="control-label col-sm-4">Owner Address:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="owner_address" name="owner_address" value="{{ isset($data->owner_address)?$data->owner_address:old('owner_address') }}">

            @if ($errors->has('owner_address'))
                <span class="help-block">
                <strong>{{ $errors->first('owner_address') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('owner_mobile_number') ? ' has-error' : '' }}">
        <label for="owner_mobile_number" class="control-label col-sm-4">Owner Mobile Number:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="owner_mobile_number" name="owner_mobile_number" value="{{ isset($data->owner_mobile_number)?$data->owner_mobile_number:old('owner_mobile_number') }}">

            @if ($errors->has('owner_mobile_number'))
                <span class="help-block">
                <strong>{{ $errors->first('owner_mobile_number') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('owner_email') ? ' has-error' : '' }}">
        <label for="owner_email" class="control-label col-sm-4">Owner Email:</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="owner_email" name="owner_email" value="{{ isset($data->owner_email)?$data->owner_email:old('owner_email') }}">

            @if ($errors->has('owner_email'))
                <span class="help-block">
                <strong>{{ $errors->first('owner_email') }}</strong>
            </span>
            @endif
        </div>
    </div>

    @if(isset($edit))
        <input type="hidden" name="reg_front_id" value="{{ $data->car_document['reg_front_id'] > 0 ? $data->car_document['reg_front_id'] : 0 }}"/>

        <input type="hidden" name="reg_back_id" value="{{ $data->car_document['reg_back_id'] > 0 ? $data->car_document['reg_back_id'] : 0 }}"/>

        <input type="hidden" name="fitness_id" value="{{ $data->car_document['fitness_id'] > 0 ? $data->car_document['fitness_id'] : 0 }}"/>

        <input type="hidden" name="tax_id" value="{{ $data->car_document['tax_id'] > 0 ? $data->car_document['tax_id'] : 0 }}"/>

        <input type="hidden" name="insurance_id" value="{{ $data->car_document['insurance_id'] > 0 ? $data->car_document['insurance_id'] : 0 }}"/>
    @endif
</div>
