@extends('admin.layouts.master')

<?php
$pageName = 'Service Pricing';
$pageResource = 'admin.service-pricing';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.index') . qString() }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>

        @if (isset($edit))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Additional Prices {{ $pageName }}
            </a>
        </li>
        @endif
    </ul>

    <div class="tab-content">
        @if(isset($edit))
        <div class="tab-pane active">
            <div class="box-body">
                <p>Price Setting: {{ $data[0]->ServiceCategorie->category_name }} >> {{ $data[0]->service_name }}</p>
                <table class="table table-bordered">
                    <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                    <thead>
                        <tr class="hide">
                            <th style="width:230px;"></th>
                            <th style="width:10px;"></th>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>

                        @if (!$servicePricing->isEmpty())
                            <tr>
                                <th style="width:50px;">Price</th>
                                <th style="width:10px;">:</th>
                                <td style="width:178px" id="inputPrice">
                                    <label>{{$servicePricing[0]->price}}</label>
                                </td>
                                <td text-left id="update-btn">
                                    <a class="btn btn-default" href="javascript:void(0)" onclick="addUpdatePrice({{$servicePricing[0]->id}},1)">Edit</a>
                                    <input type="hidden" id="service_pricing_id" value="{{$servicePricing[0]->id}}"/>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <th style="width:50px;">Price</th>
                                <th style="width:10px;">:</th>
                                <td style="width:178px">
                                    <input type="text" name="price" id="price" class="form-control" required/>
                                </td>
                                <td text-left>
                                    <a class="btn btn-default" href="javascript:void(0)" onclick="addUpdatePrice({{$data[0]->id}},'0')">Add</a>
                                </td>
                            </tr>
                        @endif

                        @if (!$servicePricing->isEmpty())
                            <tr>
                                <td text-left colspan="4">
                                    <b><u>Additional Prices</u></b>
                                </td>
                            </tr>

                            @if (!$servicePricing[0]->additionalPricings->isEmpty())
                                @foreach ($servicePricing[0]->additionalPricings as $additional)
                                    <tr>
                                        <th style="width:100px;" id="ad_price_name_{{$additional->id}}">{{$additional->price_name}}</th>
                                        <th style="width:10px;">:</th>
                                        <td style="width:178px" id="ad_price_{{$additional->id}}">
                                            {{$additional->price}}
                                        </td>
                                        <td style="width:178px" id="ad_price_type_{{$additional->id}}"></td>
                                        <td text-left id="ad_update_btn_{{$additional->id}}">
                                            <a class="btn btn-default" href="javascript:void(0)" onclick="addiEdit({{$additional->id}}, '{{$additional->price_name}}',{{$additional->price}}, '{{$additional->price_type}}')">Edit</a>
                                            <a class="btn btn-default" href="javascript:void(0)" onclick="addiDelete({{$additional->id}})">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif

                            <tr id="addi-price-add"></tr>

                            <tr>
                                <td text-left colspan="2">
                                    <a class="btn btn-default" href="javascript:void(0)" onclick="addNewPrice()">Add New</a>
                                </td>
                                @if (!$servicePricing[0]->additionalPricings->isEmpty())
                                    <td text-left colspan="2">
                                        <a class="btn btn-default" href="javascript:void(0)" onclick="allAdditionalPrice()">Delete All Additional Pricing</a>
                                    </td>
                                @endif
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        @elseif (isset($lists))
        <div class="tab-pane active">

            <div class="box-header text-left">
                <div class="row">
                    <div class="box-body">

                        <div class="box-group" id="accordion">
                            @if (!empty($vehicle_types))
                                @foreach ($vehicle_types as $key => $vehicle)
                                    <div class="panel box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$key}}" aria-expanded="false" class="collapsed">
                                                    {{$vehicle->vehicle_type_english}}
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="collapse_{{$key}}" class="panel-collapse collapse {{$key==0?'in':''}}" aria-expanded="false">
                                            <div class="box-body">
                                                <ul class="list-unstyled">
                                                @if (!empty($service_pricing))
                                                    @foreach ($service_pricing as $setting)
                                                        <li class="parent-title">
                                                            <p class="category-title">{{ $setting->category_name }}</p>
                                                            @if (!empty($setting->service))
                                                                @foreach ($setting->service as $key => $service)
                                                                    <ul>
                                                                        <li>
                                                                            <label class="cap-title">{{ $service->service_name }}</label>
                                                                            <input type="radio" name="flat_bit_{{ $service->id }}" id="flat_{{ $service->id }}" {{ !empty($service->ServiceSetting) && $service->ServiceSetting->pricing_type == 'fixed' && $service->ServiceSetting->vehicle_type_id == $vehicle->id ? 'checked' : '' }} onclick="cheangeSetting({{ $service->id }},'fixed',{{$vehicle->id}})">
                                                                            <label class="radio-title" for="flat_{{ $service->id }}"> Fixed </label>
                                                                            <input type="radio" name="flat_bit_{{ $service->id }}" id="bit_{{ $service->id }}" {{ !empty($service->ServiceSetting) && $service->ServiceSetting->pricing_type == 'bid' && $service->ServiceSetting->vehicle_type_id == $vehicle->id ? 'checked' : '' }}  onclick="cheangeSetting({{ $service->id }},'bid',{{$vehicle->id}})">
                                                                            <label class="radio-title" for="bit_{{ $service->id }}"> Bid</label>
                                                                            <a href="{{ url('admin/service-pricing/'.$service->id.'/edit')}}" class="edit-btn">Edit</a>
                                                                        </li>
                                                                    </ul>
                                                                @endforeach
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>

                      </div>
                </div>
            </div>

        </div>
        @endif
    </div>
  </div>
</section>

<style>
label.cap-title {
    font-weight: normal;
    width: 130px;
    margin-bottom: 0;
}
label.radio-title {
    font-weight: normal;
    width: 38px;
    margin-bottom: 0;
}

li.parent-title {
    font-weight: bold;
    margin-bottom: 14px;
}
li.parent-title ol li {
    font-weight: bold;
    margin-bottom: 4px;
    font-weight: normal;
    text-decoration: none;
}
.box-body p{
    font-weight: bold;
    text-decoration: underline;
}
p.category-title{
    background: #ccc;
    text-decoration: none;
    padding: 4px 12px;
}
a.edit-btn {
    color: #656464;
    font-weight: normal;
    border: 1px solid #8c8686;
    padding: 0px 4px;
    border-radius: 2px;
}
a.edit-btn:hover {
    background: #a5a1a1;
    color: #fff;
}
.card.collapse-bg-color {
    background: #ccc;
    border-radius: 4px;
}
</style>

<script>
    function cheangeSetting(id, type, vehicle_id)
    {
        var data = { "id": id, "type": type, "vehicle_id": vehicle_id, "_token": "{{ csrf_token() }}" }
        $.ajax({
            type: "POST",
            url: '{{ url("admin/service-pricing/updateGlobalSetting") }}',
            data: data,
            success: function (response) {
                console.log(response);
            }
        });
    }

    function addUpdatePrice(id,type)
    {
        if (type == 0) {
            var price = $('#price').val();
            $('#price').css('border','1px solid #d2d6de');
            if (price == "") {
                $('#price').css('border','1px solid red');
            } else {
                var data = { "id": id, "price": price, "_token": "{{ csrf_token() }}" }
                $.ajax({
                    type: "POST",
                    url: '{{ url("admin/service-pricing") }}',
                    data: data,
                    success: function (response) {
                        if (response.status == true) {
                            location.reload();
                        }
                    }
                });
            }
        } else {
            $('#inputPrice').html('<input type="text" name="price" id="price" class="form-control" required/>');
            $('#update-btn').html('<a class="btn btn-default" href="javascript:void(0)" onclick="updatePrice('+id+')">Update</a><a class="btn btn-default" href="javascript:void(0)" onclick="closePrice()">Close</a>');
        }
    }

    function updatePrice(id)
    {
        var price = $('#price').val();
        $('#price').css('border','1px solid #d2d6de');
        if (price == "") {
            $('#price').css('border','1px solid red');
        } else {
            var data = { "id": id, "price": price, "_token": "{{ csrf_token() }}" }
            $.ajax({
                type: "POST",
                url: '{{ url("admin/service-pricing/updatePrice") }}',
                data: data,
                success: function (response) {
                    if (response.status == true) {
                        location.reload();
                    }
                }
            });
        }
    }

    function closePrice()
    {
        location.reload();
    }

    function addiEdit(id, price_name, price, price_type)
    {
        $('#ad_price_name_'+id).html('<input type="text" name="ad_price_name_'+id+'" id="ad_edit_price_name_'+id+'" placeholder="Price name" value="'+price_name+'" class="form-control" required/>');
        $('#ad_price_'+id).html('<input type="number" name="ad_price_'+id+'" id="ad_edit_price_'+id+'" value="'+price+'" placeholder="Price" class="form-control" required/>');

        var priceTypeHTML = '<select name="ad_update_price_type_'+id+'" id="ad_update_price_type_'+id+'"  class="form-control">';
        if (price_type == 'atactual') {
            priceTypeHTML += '<option value="atactual" selected>Atactual</option>'+
                             '<option value="percentage">Percentage</option>';
        } else {
            priceTypeHTML += '<option value="atactual">Atactual</option>'+
                             '<option value="percentage" selected>Percentage</option>';
        }

        $('#ad_price_type_'+id).html(priceTypeHTML);
        $('#ad_update_btn_'+id).html('<a class="btn btn-default" href="javascript:void(0)" onclick="updateAddiPrice('+id+')">Update</a><a class="btn btn-default" href="javascript:void(0)" onclick="closeAddiPrice()">Close</a>');
    }

    function closeAddiPrice()
    {
        location.reload();
    }

    function updateAddiPrice(id)
    {
        var ad_price_name = $('#ad_edit_price_name_'+id).val();
        var ad_price = $('#ad_edit_price_'+id).val();
        var ad_price_type = $('#ad_update_price_type_'+id).val();

        $('#ad_edit_price_name_'+id).css('border','1px solid #d2d6de');
        $('#ad_edit_price_'+id).css('border','1px solid #d2d6de');
        $('#ad_update_price_type_'+id).css('border','1px solid #d2d6de');
        if (ad_price_name == "") {
            $('#ad_edit_price_name_'+id).css('border','1px solid red');
        } else if (ad_price == "") {
            $('#ad_edit_price_'+id).css('border','1px solid red');
        } else if (ad_price_type == "") {
            $('#ad_update_price_type_'+id).css('border','1px solid red');
        } else {
            var data = { "id": id, "ad_price_name" : ad_price_name, "price": ad_price, "price_type": ad_price_type, "_token": "{{ csrf_token() }}" }
            $.ajax({
                type: "POST",
                url: '{{ url("admin/service-pricing/updateAdditionalPrice") }}',
                data: data,
                success: function (response) {
                    if (response.status == true) {
                        location.reload();
                    }
                }
            });
        }
    }

    function addNewPrice(){
        var HTML = '';
        HTML +='<th style="width:138px;">'+
                    '<input type="text" name="ad_price_name" id="ad_price_name" placeholder="Price name" class="form-control" required/>'+
                '</th>'+
                '<th style="width:10px;">:</th>'+
                '<td style="width:178px">'+
                    '<input type="number" name="ad_add_price" id="ad_add_price" placeholder="Price" class="form-control" required/>'+
                '</td>'+
                '<td style="width:178px">'+
                    '<select name="price_type" id="price_type" class="form-control">'+
                        '<option value="atactual">Atactual</option>'+
                        '<option value="percentage">Percentage</option>'+
                    '</select>'+
                '</td>'+
                '<td text-left id="ad_add_submit_btn">'+
                    '<a class="btn btn-default" href="javascript:void(0)" onclick="addAdditionalPrice()">Add</a>'+
                '</td>';
        $('#addi-price-add').html(HTML);
    }

    function addAdditionalPrice()
    {
        var id = $('#service_pricing_id').val();
        var add_price_name = $('#ad_price_name').val();
        var add_price = $('#ad_add_price').val();
        var price_type = $('#price_type').val();

        $('#ad_price_name').css('border','1px solid #d2d6de');
        $('#ad_add_price').css('border','1px solid #d2d6de');
        $('#price_type').css('border','1px solid #d2d6de');
        if (add_price_name == "") {
            $('#ad_price_name').css('border','1px solid red');
        } else if (add_price == "") {
            $('#ad_add_price').css('border','1px solid red');
        } else if (price_type == "") {
            $('#price_type').css('border','1px solid red');
        } else {
            var data = { "id": id, "price_name" : add_price_name, "price": add_price, "price_type": price_type, "_token": "{{ csrf_token() }}" }
            $.ajax({
                type: "POST",
                url: '{{ url("admin/service-pricing/addAdditionalPrice") }}',
                data: data,
                success: function (response) {
                    console.log(response);
                    if (response.status == true) {
                        location.reload();
                    }
                }
            });
        }
    }

    function addiDelete(id)
    {
        bootbox.confirm({
            message: "Do you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true){
                    var url = "{{URL('admin/service-pricing/')}}";
                    var dltUrl = url+"/"+id;
                    $.ajax({
                        type: "POST",
                        url: dltUrl,
                        data: { "_token": "{{ csrf_token() }}", "_method": 'DELETE' },
                        success: function (response) {
                            if (response.status == true) {
                                location.reload();
                            }
                        }
                    });
                }
            }
        });
    }

    function allAdditionalPrice()
    {
        bootbox.confirm({
            message: "Do you want to delete All Additional Price?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    var service_pricing_id = $('#service_pricing_id').val();
                    var url = "{{URL('admin/service-pricing/additionalPriceDestroy/')}}";
                    var dltUrl = url+"/"+service_pricing_id;
                    $.ajax({
                        type: "POST",
                        url: dltUrl,
                        data: { "_token": "{{ csrf_token() }}", "_method": 'DELETE' },
                        success: function (response) {
                            if (response.status == true) {
                                location.reload();
                            }
                        }
                    });
                }
            }
        });

    }

</script>

<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js') }}"></script>
@endsection
