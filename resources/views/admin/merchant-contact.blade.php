@extends('admin.layouts.master')

@php
    $pageName = 'Contact';
    $pageResource = 'admin.merchant';
@endphp

@section('content')
<section class="content">
    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <div class="nav-tabs-custom">
        <div class="tab-content">
            <div class="tab-pane active">
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="box box-info box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Send message to {{env('APP_NAME')}}</h3>
                                        </div>
                                        <div class="box-body">
                                            <form method="POST" saction="{{ route($pageResource.'.contact', $id) }}" class="form-horizontal" enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group col-sm-12">
                                                    <label class="col-sm-12">Message</label>
                                                    <div class="col-sm-12">
                                                        <textarea class="form-control" name="message" rows="5">{{ isset($data->message)?$data->message:old('message') }}</textarea>

                                                        @if ($errors->has('message'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('message') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group col-sm-12">
                                                    <label class="control-label col-sm-3">Attachment 1:</label>
                                                    <div class="col-sm-9">
                                                        <input type="file" class="form-control" name="attachment1">
                                                    </div>
                                                </div>

                                                <div class="form-group col-sm-12">
                                                    <label class="control-label col-sm-3">Attachment 2:</label>
                                                    <div class="col-sm-9">
                                                        <input type="file" class="form-control" name="attachment2">
                                                    </div>
                                                </div>

                                                <div class="form-group col-sm-12">
                                                    <label class="control-label col-sm-3">Attachment 3:</label>
                                                    <div class="col-sm-9">
                                                        <input type="file" class="form-control" name="attachment3">
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-12 text-center">
                                                    <button type="submit" class="btn btn-success btn-flat">Send message</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <div class="box box-info box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Ticket communication</h3>
                                        </div>
                                        <div class="box-body">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Created Date</th>
                                                    <th>From</th>
                                                    <th>To</th>
                                                    <th>Message</th>
                                                    <th>File</th>
                                                </tr>
                                                </thead>
                                                @foreach ($records as $value)
                                                <tbody>
                                                <tr>
                                                    <td>{{dateFormat($value->created_at, 1)}}</td>
                                                    <td>
                                                        @if($value->sender_type == 1 )
                                                            <span class="btn btn-warning btn-xs">Admin</span>
                                                        @elseif($value->sender_type == 2)
                                                            <span class="btn btn-success btn-xs">Partner</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($value->receiver_type == 1 )
                                                            <span class="btn btn-warning btn-xs">Admin</span>
                                                        @elseif($value->receiver_type == 2)
                                                            <span class="btn btn-success btn-xs">Partner</span>
                                                        @endif
                                                    </td>
                                                    <td>{{$value->message}}</td>
                                                    <td>
                                                        {!! viewFile('uploads/partner_contacts', $value->attachment1) !!}
                                                        <br>
                                                        {!! viewFile('uploads/partner_contacts', $value->attachment2) !!}
                                                        <br>
                                                        {!! viewFile('uploads/partner_contacts', $value->attachment3) !!}
                                                    </td>
                                                </tr>

                                                </tbody>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>


                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
@endsection
