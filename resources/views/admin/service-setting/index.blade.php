@extends('admin.layouts.master')

@php
    $pageName = 'Service Setting';
    $pageResource = 'admin.service-setting';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li {{ (isset($lists))?'class=active':'' }}>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif

                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                        <div class="box-header text-right">
                            <div class="row">
                                <div class="form-group">
                                    <select class="form-control select2" name="service_category_id">
                                        <option value="">Category</option>
                                        @if (!empty($serviceCategories))
                                            @foreach ($serviceCategories as $cat)
                                                <option value="{{$cat->id}}"  {{ (Request::get('service_category_id')== $cat->id)?'selected' : '' }}>{{$cat->category_name}}<option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control select2" name="vehicle_type_id">
                                        <option value="">Vehicle Type</option>
                                        @if (!empty($vehicleType))
                                            @foreach ($vehicleType as $vehicle)
                                                <option value="{{$vehicle->id}}"  {{ (Request::get('vehicle_type_id')== $vehicle->id)?'selected' : '' }}>{{$vehicle->vehicle_type_english}}<option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <select class="form-control select2" name="service_id">
                                        <option value="">Service</option>
                                        @if (!empty($service))
                                            @foreach ($service as $val)
                                                <option value="{{$val->id}}"  {{ (Request::get('service_id')== $val->id)?'selected' : '' }}>{{$val->service_name}}<option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-flat">Search</button>
                                    <a class="btn btn-warning btn-flat" href="{{ route($pageResource.'.index') }}">X</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="box-body table-responsive">
                        <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                        <table class="table table-bordered table-hover dataTable">
                            <caption class="hidden"><h3>{{ $pageName }} List</h3></caption>
                            <thead>
                            <tr>
                                <th>SL.</th>
                                <th>Category</th>
                                <th>Service</th>
                                <th>Vehicle Type</th>
                                <th>Price Type</th>
                                <th>Starting Point</th>
                                <th>Destination Point</th>
                                <th>Price</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th class="not-export-col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($records as $key => $val)
                                <tr>
                                    <td>{{$serial++}}</td>
                                    <td>{{$val->Service ? $val->Service->ServiceCategory->category_name??null : ''}}</td>
                                    <td>{{$val->Service ? $val->Service->service_name : ''}}</td>
                                    <td>{{$val->VehicleType->vehicle_type_english}}</td>
                                    <td>{{ucwords($val->pricing_type)}}</td>
                                    <td>{{ucwords($val->upazilaBoundaryFrom ? $val->upazilaBoundaryFrom->Upaz_name : '')}}</td>
                                    <td>{{ucwords($val->upazilaBoundaryTo ? $val->upazilaBoundaryTo->Upaz_name : '')}}</td>
                                    <td class="text-right">{!! currency($val->price) !!}</td>
                                    <td class="text-center">{{$val->status == '1' ? 'Active' : 'Inactive'}}</td>
                                    <td>{{formatDateTime($val->created_at)}}</td>
                                    <td>
                                        <?php
                                        $access = 1;
                                        listAction([
                                            actionLi(route($pageResource.'.show', $val->id).qString(), 'show', $access),
                                            actionLi(route($pageResource.'.edit', $val->id).qString(), 'edit', $access),
                                            actionLi(route($pageResource.'.destroy', $val->id).qString(), 'delete', $access),
                                        ]);
                                        ?>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-right">
                            {{ $records->appends(Request::except('page'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
