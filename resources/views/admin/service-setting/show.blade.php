@extends('admin.layouts.master')

@php
    $pageName = 'Service Setting';
    $pageResource = 'admin.service-setting';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif
                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') . qString() }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif

                <li class="active">
                    <a href="#">
                        <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    @if (isset($data))
                        <div class="box-body table-responsive">
                            <table class="table table-bordered">
                                <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                                <thead>
                                <tr class="hide">
                                    <th style="width:230px;"></th>
                                    <th style="width:10px;"></th>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <th style="width:230px;">Service Category</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{$data->Service->ServiceCategory->category_name}}</td>
                                </tr>

                                <tr>
                                    <th style="width:230px;">Rent Type</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{$data->Service->service_name}}</td>
                                </tr>

                                <tr>
                                    <th style="width:230px;">Vehicle Type</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{$data->VehicleType->vehicle_type_english}}</td>
                                </tr>

                                <tr>
                                    <th>Pricing Type</th>
                                    <th>:</th>
                                    <td>{{ucwords($data->pricing_type)}}</td>
                                </tr>

                                <tr>
                                    <th>Price</th>
                                    <th>:</th>
                                    <td>{{$data->price}}</td>
                                </tr>

                                <tr>
                                    <th>Starting Point</th>
                                    <th>:</th>
                                    <td>{{ucwords($data->upazilaBoundaryFrom ? $data->upazilaBoundaryFrom->Upaz_name : '')}}</td>
                                </tr>

                                <tr>
                                    <th>Destination Point</th>
                                    <th>:</th>
                                    <td>{{ucwords($data->upazilaBoundaryTo ? $data->upazilaBoundaryTo->Upaz_name : '')}}</td>
                                </tr>

                                <tr>
                                    <th>Additional Price</th>
                                    <th>:</th>
                                    <td>
                                        @if (!empty($data->Service->additionalPricings))
                                            <ul class="list-unstyled">
                                                @foreach ($data->Service->additionalPricings as $additional)
                                                    <li>{{$additional->price_name}} : {{$additional->price}}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <th>Created at</th>
                                    <th>:</th>
                                    <td>{{formatDateTime($data->created_at)}}</td>
                                </tr>

                                <tr>
                                    <th>Updated at</th>
                                    <th>:</th>
                                    <td>{{formatDateTime($data->created_at)}}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="box-body">
                            {!! notFoundText() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
