<script>
    @php
        if (isset($data) && !$data->additionalPricings->isEmpty()) {
            $count = count($data->additionalPricings);
        }
        $category_id = (isset($data->Service->ServiceCategory->id))?$data->Service->ServiceCategory->id:0;
        $destination_point = (isset($data->destination_point))?$data->destination_point:'';
        $start_point = (isset($data->starting_point))?$data->starting_point:'';
    @endphp
    $(document).ready(function () {
        var i ={{$count??0}};
        $('#add').click(function () {

            var ad_label = $('#additional_label_0').val();
            var ad_price = $('#additional_price_0').val();
            $('#additional_label_0').css('border', '1px solid #d2d6de');
            $('#additional_price_0').css('border', '1px solid #d2d6de');
            if (ad_label == "") {
                $('#additional_label_0').css('border', '1px solid red');
                $('#additional_price_0').css('border', '1px solid red');
            } else if (ad_price == "") {
                $('#additional_price_0').css('border', '1px solid red');
            } else {
                i++;
                $('#dynamic_field').append('<tr id="row' + i + '"><td><input type="text" class="form-control" name="additional_price[' + i + '][label]" id="additional_label_' + i + '" placeholder="Price label" required/></td><td><input type="number" class="form-control" name="additional_price[' + i + '][price]" id="additional_price_' + i + '" placeholder="Price" value="" required/></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
            }
        });

        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        @if($category_id)
        setTimeout(() => {
            $garivara.upazilaSelect2('#start_point', {{$start_point}});
            $garivara.upazilaSelect2('#destination_point', {{$destination_point}});
        }, 300);
        @endif
    });


    window.vueApp = new Vue({
        el: '#service-setting-form',
        data: {
            rent_types: {!! $service !!},
            airport_list: {!! $airports !!},
            formData: {
                service_category: "{!! (isset($data->Service->ServiceCategory->id))?$data->Service->ServiceCategory->id:old('service_category') !!}",
                rent_type: "{!! (isset($data->service_id))?$data->service_id:old('rent_type') !!}",
                airport_id: "{!! (isset($data->airport_id))?$data->airport_id:old('airport_id') !!}",
            },
            selected_airport: {!! (isset($data->airport_id))?$airports->where('id', $data->airport_id)->first():'{}' !!},
        },
        mounted() {
        },
        methods: {
            getServiceTypes(event) {
                const id = parseInt(event.target.value);
                axios.get(base_url + `/common-api/services/service-category/${id}`).then(response => {
                    this.formData.rent_type = '';

                    if (response.data.success) {
                        this.rent_types = response.data.data;
                    }
                    setTimeout(() => {
                        if (id === 2) {
                            $garivara.upazilaSelect2('#destination_point', {{$destination_point}});
                        }
                        if ([1, 2].includes(id)) {
                            $garivara.upazilaSelect2('#start_point', {{$start_point}});
                        }
                    }, 300);
                })
            },
            selectAirport(event) {
                const airportId = parseInt(event.target.value);
                if (airportId) {
                    this.selected_airport = this.airport_list.filter(item => {
                        return item.id === airportId
                    })[0];
                } else {
                    this.selected_airport = {};
                }
            },
        }
    });
</script>
