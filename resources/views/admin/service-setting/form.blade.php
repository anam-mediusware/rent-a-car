<div class="form-group{{ $errors->has('service_category') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Service Category:</label>
    <div class="col-sm-8">
        <?php $category_id = (isset($data->Service->ServiceCategory->id))?$data->Service->ServiceCategory->id:old('service_category'); ?>
        <select class="form-control" name="service_category" @change="getServiceTypes($event)" v-model="formData.service_category" required>
            <option value="">Select Service Category</option>
            @if (!empty($ServiceCategories))
                @foreach ($ServiceCategories as $category)
                    <option value="{{$category->id}}" {{ ($category_id==$category->id)?'selected':'' }}>{{ $category->category_name }}</option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('service_category'))
            <span class="help-block">
                <strong>{{ $errors->first('service_category') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('vehicle_type_id') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Vehicle Type:</label>
    <div class="col-sm-8">
        <?php $vehicle_type_id = (isset($data->vehicle_type_id))?$data->vehicle_type_id:old('vehicle_type_id'); ?>
        <select class="form-control" name="vehicle_type_id" required>
            <option value="">Select Vehicle Type</option>
            @if (!empty($vehicleType))
                @foreach ($vehicleType as $vehicle)
                    <option value="{{$vehicle->id}}" {{ ($vehicle_type_id==$vehicle->id)?'selected':'' }}>{{ $vehicle->vehicle_type_english }}</option>
                @endforeach
            @endif
        </select>

        @if ($errors->has('vehicle_type_id'))
            <span class="help-block">
                <strong>{{ $errors->first('vehicle_type_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('rent_type') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Service Type:</label>
    <div class="col-sm-8">
        @php
            $service_id = (isset($data->service_id))?$data->service_id:old('rent_type');
        @endphp
        <select class="form-control" name="rent_type" id="rent_type" v-model="formData.rent_type" required>
            <option value="">Select Rent Type</option>
            <option v-for="rent_type in rent_types" :key="rent_type.id" :value="rent_type.id">@{{ rent_type.service_name }}</option>
            {{--@if (!empty($service) && isset($edit))
                @foreach ($service as $val)
                    <option value="{{$val->id}}" {{ ($service_id==$val->id)?'selected':'' }}>{{ $val->service_name }}</option>
                @endforeach
            @endif--}}
        </select>

        @if ($errors->has('rent_type'))
            <span class="help-block">
                <strong>{{ $errors->first('rent_type') }}</strong>
            </span>
        @endif
    </div>
</div>

<section v-if="[1,2].includes(parseInt(formData.service_category)) && ![8,9].includes(parseInt(formData.rent_type))" id="start_destination_section">
    <div class="form-group{{ $errors->has('start_point') ? ' has-error' : '' }}">
        <label for="start_point" class="control-label col-sm-4 required">City:</label>
        <div class="col-sm-8">
            @php
                $start_point = (isset($data->start_point))?$data->start_point:'';
            @endphp
            <select class="form-control" name="start_point" id="start_point" required>
                <option value="">Select City</option>
            </select>

            @if ($errors->has('start_point'))
                <span class="help-block">
                <strong>{{ $errors->first('start_point') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div v-if="parseInt(formData.service_category) === 2" class="form-group{{ $errors->has('destination_point') ? ' has-error' : '' }}">
        <label for="destination_point" class="control-label col-sm-4 required">Destination Point:</label>
        <div class="col-sm-8">
            @php
                $destination_point = (isset($data->destination_point))?$data->destination_point:'';
            @endphp
            <select class="form-control" name="destination_point" id="destination_point" required>
                <option value="">Select Destination Point</option>
            </select>

            @if ($errors->has('destination_point'))
                <span class="help-block">
                <strong>{{ $errors->first('destination_point') }}</strong>
            </span>
            @endif
        </div>
    </div>
</section>

<section v-if="parseInt(formData.service_category) === 3 && parseInt(formData.rent_type) === 9" id="airport_to_city_section">
    @php
        $category_id = (isset($data->Service->ServiceCategory->id))?$data->Service->ServiceCategory->id:0;
        $starting_point = (isset($data->starting_point))?$data->starting_point:'';
    @endphp
    <div class="form-group{{ $errors->has('airport_id') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required" id="start_point_label">Airport:</label>
        <div class="col-sm-8">

            <select class="form-control" name="airport_id" v-model="formData.airport_id" @change="selectAirport($event)" required>
                <option value="">Select Airport</option>
                @if ($airports)
                    @foreach ($airports as $key => $airport)
                        <option value="{{$airport->id}}">{{ $airport->airport }}</option>
                    @endforeach
                @endif
            </select>

            @if ($errors->has('airport_id'))
                <span class="help-block">
                <strong>{{ $errors->first('airport_id') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('airport_city_name') ? ' has-error' : '' }}">
        <label class="control-label col-sm-4 required">Airport City:</label>
        <div class="col-sm-8">
            <input type="text" name="airport_city_name" id="airport_city_name" v-model="selected_airport.city_title" class="form-control" readonly required/>

            @if ($errors->has('airport_city_name'))
                <span class="help-block">
                <strong>{{ $errors->first('airport_city_name') }}</strong>
            </span>
            @endif
        </div>
    </div>
</section>

<section v-if="parseInt(formData.service_category) === 3 && parseInt(formData.rent_type) === 8" id="city_to_airport_section">
    <div class="form-group{{ $errors->has('airport_id') ? ' has-error' : '' }}" id="airport_city_div">
        <label class="control-label col-sm-4 required" id="airport_city_label">Airport City:</label>
        <div class="col-sm-8">
            @php
                $airport_destination = (isset($data->airport_city))?$data->airport_city:'';
            @endphp
            <select class="form-control" name="airport_id" id="airport_id" v-model="formData.airport_id" @change="selectAirport($event)" required>
                <option value="">Select City</option>
                @if ($airports)
                    @foreach ($airports as $key => $airport)
                        <option value="{{$airport->id}}">{{ $airport->city_title }}</option>
                    @endforeach
                @endif
            </select>

            @if ($errors->has('airport_id'))
                <span class="help-block">
                <strong>{{ $errors->first('airport_id') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('airport_name') ? ' has-error' : '' }}" id="airport_div">
        <label class="control-label col-sm-4 required">Airport:</label>
        <div class="col-sm-8">
            <input type="text" name="airport_name" id="airport_name" v-model="selected_airport.airport" class="form-control" readonly required/>

            @if ($errors->has('airport_name'))
                <span class="help-block">
                <strong>{{ $errors->first('airport_name') }}</strong>
            </span>
            @endif
        </div>
    </div>
</section>

{{--<div class="form-group{{ $errors->has('flat_bit') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Pricing Type:</label>
    <div class="col-sm-8">
        <ul class="list-unstyled">
            <?php $package_setting = (isset($data->pricing_type))?$data->pricing_type:old('flat_bit'); ?>
            <li>
                <input type="radio" name="flat_bit" id="fixed" value="fixed" {{$package_setting == 'fixed' ? 'checked' : ''}} required>
                <label class="radio-title" for="fixed"> Fixed </label>
                <input type="radio" name="flat_bit" id="bid" value="bid" {{$package_setting == 'bid' ? 'checked' : ''}} required>
                <label class="radio-title" for="bid"> Bid</label>
            </li>
        </ul>
        @if ($errors->has('flat_bit'))
            <span class="help-block">
                <strong>{{ $errors->first('flat_bit') }}</strong>
            </span>
        @endif
    </div>
</div>--}}

<div class="form-group{{ $errors->has('service_quality_type') ? ' has-error' : '' }}">
    @php
        $service_quality_type = isset($data->service_quality_type)?$data->service_quality_type:old('service_quality_type', 1);
    @endphp
    <label for="service_quality_type" class="control-label col-sm-4 required">Service Quality Type:</label>
    <div class="col-sm-8">
        <ul class="list-unstyled">
            <li>
                <input type="radio" name="service_quality_type" id="optionsRadios1" value="1" {{$service_quality_type == 1 ? 'checked' : ''}} required>
                <label class="radio-title" for="optionsRadios1"> Economy </label>
                <input type="radio" name="service_quality_type" id="optionsRadios2" value="2" {{$service_quality_type == 2 ? 'checked' : ''}} required>
                <label class="radio-title" for="optionsRadios2"> Premium </label>
            </li>
        </ul>

        @if ($errors->has('service_quality_type'))
            <span class="help-block">
                <strong>{{ $errors->first('service_quality_type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
    <label class="control-label col-sm-4 required">Price:</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" name="price" id='price' value="{{ isset($data->price)?$data->price:old('price') }}" required/>

        @if ($errors->has('price'))
            <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('decoration_cost') ? ' has-error' : '' }}" id="decoration_cost_div" style="display: {{ $category_id == 4 ? 'block' : 'none' }}">
    <label class="control-label col-sm-4 required">Decoration Cost:</label>
    <div class="col-sm-8">
        <input type="number" class="form-control" name="decoration_cost" id='decoration_cost' value="{{ isset($data->decoration_cost)?$data->decoration_cost:old('decoration_cost') }}" required/>

        @if ($errors->has('decoration_cost'))
            <span class="help-block">
                <strong>{{ $errors->first('decoration_cost') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('package_additional_price') ? ' has-error' : '' }}">
    <div class="col-sm-4 text-right">
        <label class="control-label">Package Additional Price:</label>
    </div>
    <div class="col-sm-8">
        <table class="table table-bordered table-additional" id="dynamic_field">

            @if (isset($data) && !$data->additionalPricings->isEmpty())
                @php $count = 0 @endphp
                @foreach ($data->additionalPricings as $additionalPricing)
                    <tr id="row{{$count}}">
                        <td>
                            <input type="text" class="form-control" name="additional_price[{{$count}}][label]" id="additional_label_{{$count}}" placeholder="Price label" value="{{$additionalPricing->price_name}}"/>
                        </td>
                        <td>
                            <input type="number" class="form-control" name="additional_price[{{$count}}][price]" id='additional_price_{{$count}}' placeholder="Price" value="{{$additionalPricing->price}}"/>
                        </td>
                        <td>
                            @if ($count == 0)
                                <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                            @else
                                <button type="button" name="remove" id="{{$count}}" class="btn btn-danger btn_remove">X</button>
                            @endif
                        </td>
                    </tr>
                    @php $count++ @endphp
                @endforeach
            @else
                <tr>
                    <td>
                        <input type="text" class="form-control" name="additional_price[0][label]" id="additional_label_0" placeholder="Price label"/>
                    </td>
                    <td>
                        <input type="number" class="form-control" name="additional_price[0][price]" id='additional_price_0' placeholder="Price" value=""/>
                    </td>
                    <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>
                </tr>
            @endif
        </table>
    </div>
</div>
@if(isset($edit))
    <input type="hidden" name="pre_service_id" value="{{$data->service_id}}"/>
@endif
