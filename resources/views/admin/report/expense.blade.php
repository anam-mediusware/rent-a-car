@extends('admin.layouts.master')

<?php
$pageName = 'Expense';
$pageResource = 'report/expense';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active">
            <a><i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }}</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active">
            <form method="GET" action="{{ url('/admin/'.$pageResource) }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" class="form-control" name="from" id="datepicker-from" value="{{ Request::get('from') }}" placeholder="Date From">
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="to" id="datepicker-to" value="{{ Request::get('to') }}" placeholder="Date To">
                        </div>

                        <div class="form-group">
                            <select name="show" class="form-control">
                                <option value="">Page Wise</option>
                                <option value="1" {{ (Request::get('show')==1)?'selected':'' }}>Show All</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Date</th>
                            <th>Category</th>
                            <th>Remark</th>
                            <th class="text-right" align="right">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php ($total = 0)
                        @foreach($records as $key => $val)
                        <tr>
                            <td>{{$serial++}}</td>
                            <td>{{dateFormat($val->expense_date)}}</td>
                            <td>{{$val->category_name}}</td>
                            <td>{{$val->expense_remark}}</td>
                            <td align="right">{{$val->expense_amount}}</td>
                        </tr>
                        @php ($total += $val->expense_amount)
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right" align="right">Total:</th>
                            <th class="text-right" align="right">{{numberFormat($total,1)}}</th>
                        </tr>
                    </tfoot>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
  </div>
</section>
@endsection
