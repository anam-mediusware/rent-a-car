@extends('admin.layouts.master')

<?php
$pageName = 'Net Income';
$pageResource = 'report/profit-loss';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active">
            <a><i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }}</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active">
            <form method="GET" action="{{ url('/admin/'.$pageResource) }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" class="form-control" name="from" id="datepicker-from" value="{{ Request::get('from') }}" placeholder="Date From" required>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="to" id="datepicker-to" value="{{ Request::get('to') }}" placeholder="Date To" required>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                @if (isset($records))
                    <table class="table table-bordered table-hover dataTable">
                        <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                        <thead>
                            <tr>
                                <th>SL.</th>
                                <th>Date</th>
                                <th>Details</th>
                                <th class="text-right" align="right">Income</th>
                                <th class="text-right" align="right">Expense</th>
                                <th class="text-right" align="right">Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $balance = 0;
                            $incomeTotal = 0;
                            $expenseTotal = 0;
                            @endphp
                            @foreach($records as $key => $val)
                                @php
                                if ($val->type=='Income') {
                                    $balance += $val->amount;
                                } else {
                                    $balance -= $val->amount;
                                }
                                @endphp
                            <tr>
                                <td>{{$serial++}}</td>
                                <td>{{dateFormat($val->date)}}</td>
                                <td>{{$val->details}}</td>
                                <td align="right">{{($val->type=='Income')?$val->amount:''}}</td>
                                <td align="right">{{($val->type=='Expense')?$val->amount:''}}</td>
                                <td align="right">{{$balance}}</td>
                            </tr>
                            @php
                            $incomeTotal += ($val->type=='Income')?$val->amount:0;
                            $expenseTotal += ($val->type=='Expense')?$val->amount:0;
                            @endphp
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th class="text-right" align="right">Total:</th>
                                <th class="text-right" align="right">{{numberFormat($incomeTotal)}}</th>
                                <th class="text-right" align="right">{{numberFormat($expenseTotal)}}</th>
                                <th class="text-right" align="right">{{numberFormat($balance)}}</th>
                            </tr>
                        </tfoot>
                    </table>
                @else
                    <h2 class="text-center">Select date range & hit search to show report</h2>
                @endif
                <div class="text-right"></div>
            </div>
        </div>
    </div>
  </div>
</section>
@endsection
