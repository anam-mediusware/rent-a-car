@extends('admin.layouts.master')

<?php
$pageName = 'Amount Receivable';
$pageResource = 'report/receivable';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active">
            <a><i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }}</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active">
            <form method="GET" action="{{ url('/admin/'.$pageResource) }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" class="form-control" name="from" id="datepicker-from" value="{{ Request::get('from') }}" placeholder="Date From">
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="to" id="datepicker-to" value="{{ Request::get('to') }}" placeholder="Date To">
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="number" value="{{ Request::get('number') }}" placeholder="Booking Number">
                        </div>

                        <div class="form-group">
                            <select name="driver" class="form-control select2">
                                <option value="">------ All Driver ------</option>
                                @foreach($drivers as $driver)
                                    <option value="{{ $driver->id }}" {{ (Request::get('driver')==$driver->id)?'selected':'' }}>{{ ($driver->merchant)?$driver->merchant->name:'' }}/{{ $driver->full_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <select name="type" class="form-control">
                                <option value="">All</option>
                                @foreach(['Driver', 'Customer'] as $typ)
                                    <option value="{{ $typ }}" {{ (Request::get('type')==$typ)?'selected':'' }}>{{ $typ }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <select name="show" class="form-control">
                                <option value="">Page Wise</option>
                                <option value="1" {{ (Request::get('show')==1)?'selected':'' }}>Show All</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Booking No</th>
                            <th>Customer Name</th>
                            <th>Customer Mobile</th>
                            <th class="text-right" align="right">Customer Receivable</th>
                            <th>Driver Name</th>
                            <th>Driver Mobile</th>
                            <th class="text-right" align="right">Driver Receivable</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $total1 = 0;
                        $total2 = 0;
                        @endphp
                        @foreach($records as $key => $val)
                        <tr onclick="window.open('{{url('admin/booking/'.$val->id)}}')" style="cursor:pointer;">
                            <td>{{$serial++}}</td>
                            <td>{{$val->book_number}}</td>
                            <td>{{$val->name}}</td>
                            <td>{{$val->mobile}}</td>
                            <td align="right">{{$val->user_receive_due}}</td>

                            <td>{{$val->full_name}}</td>
                            <td>{{$val->mobile_number}}</td>
                            <td align="right">{{$val->driver_receive_due}}</td>
                        </tr>
                        @php
                        $total1 += $val->user_receive_due;
                        $total2 += $val->driver_receive_due;
                        @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right" align="right">Total:</th>
                            <th class="text-right" align="right">{{numberFormat($total1,1)}}</th>
                            <th></th>
                            <th class="text-right" align="right">Total:</th>
                            <th class="text-right" align="right">{{numberFormat($total2,1)}}</th>
                        </tr>
                    </tfoot>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
  </div>
</section>
@endsection
