@extends('admin.layouts.master')

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li {{ (isset($show))?'class=active':'' }}>
                <a href="{{ route('admin.profile.index') }}">
                    <i class="fa fa-list-alt" aria-hidden="true"></i>My Profile
                </a>
            </li>
            <li {{ (isset($edit))?'class=active':'' }}>
                <a href="{{ route('admin.profile.self-edit') }}">
                    <i class="fa fa-edit" aria-hidden="true"></i> Edit Profile
                </a>
            </li>
            <li {{ (isset($password))?'class=active':'' }}>
                <a href="{{ route('admin.profile.password') }}">
                    <i class="fa fa-key" aria-hidden="true"></i> Change Password
                </a>
            </li>
        </ul>

        <div class="tab-content">
            @if (isset($show))
            <div class="tab-pane active">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ Auth::user()->name }}'s Profile</h3>
                </div>
                @if (isset($data))
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover">
                            <caption class="hidden"><h3>{{ Auth::user()->name }}'s Profile</h3></caption>
                            <thead>
                                <tr class="hide">
                                    <th style="width:120px;"></th>
                                    <th style="width:10px;"></th>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="3">
                                        {!! viewImg('uploads/users/', $data->image, ['fakeimg'=>'avatar', 'class' => 'img-circle','style' =>'width:200px;']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th style="width:120px;">Name</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{ $data->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <th>:</th>
                                    <td>{{ $data->email }}</td>
                                </tr>
                                <tr>
                                    <th>Type</th>
                                    <th>:</th>
                                    <td>{{ accountType($data->account_type) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="box-body">
                        {!! notFoundText() !!}
                    </div>
                @endif
            </div>

            @elseif (isset($edit))
            <div class="tab-pane active">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ Auth::user()->name }}'s Profile Change</h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <form method="POST" action="{{ route('admin.profile.self-update') }}" id="are_you_sure" class="form-horizontal">
                            @csrf
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="control-label col-sm-3 required">Name:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value="{{ isset($data->name)?$data->name:null }}" required>
                                    </div>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label col-sm-3 required">Email:</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" name="email" value="{{ isset($data->email)?$data->email:null }}" required>
                                    </div>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success btn-flat btn-lg">Update</button>
                                    <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @elseif (isset($password))
            <div class="tab-pane active">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $data->name;?>'s Password Change</h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <form method="POST" action="{{ route('admin.profile.updatePassword') }}" id="are_you_sure" class="form-horizontal">
                            @csrf

                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                    <label class="control-label col-sm-4 required">Old Password:</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="old_password" required>

                                        @if ($errors->has('old_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('old_password') }}</strong>
                                            </span>
                                        @endif

                                        @if (session('error'))
                                            <span class="help-block">
                                                <strong>{{ session('error') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                                    <label class="control-label col-sm-4 required">New Password:</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="new_password" required>

                                        @if ($errors->has('new_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('new_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                                    <label class="control-label col-sm-4 required">Confirm Password:</label>
                                    <div class="col-sm-8">
                                        <input type="password" class="form-control" name="confirm_password" required>

                                        @if ($errors->has('confirm_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('confirm_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success btn-flat btn-lg">Update</button>
                                    <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
@endsection
