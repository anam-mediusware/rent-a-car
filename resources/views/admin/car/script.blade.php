<script type="text/javascript">
    $(function () {
        $("#datetimepicker1").datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });

        $('#datetimepicker2').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });

        $('#datetimepicker3').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });

        $('#datetimepicker4').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });

        $('#datetimepicker5').datetimepicker({
            ownerDocument: document,
            contentWindow: window,
            value: '',
            rtl: false,
            format: 'd/m/Y',
            formatDate: 'd/m/Y',
            timepicker: false,
            datepicker: true,
        });
    });

    function createEdit()
    {
        var rg_front = document.getElementById('registration_front');
        var rg_back = document.getElementById('registration_back');
        var expiry_date = $('#datetimepicker2').val();

        $('#datetimepicker2'). prop("required", false);
        if (rg_front.files.length > 0 || rg_back.files.length > 0){
            if(expiry_date == "") {
                $('#datetimepicker2'). prop("required", true);
                return false;
            }
        }

        var fitness_paper = document.getElementById('fitness_paper');
        var fit_expiry_date = $('#datetimepicker3').val();

        $('#datetimepicker2'). prop("required", false);
        if (fitness_paper.files.length > 0){
            if(fit_expiry_date == "") {
                $('#datetimepicker3'). prop("required", true);
                return false;
            }
        }

        var tax_token = document.getElementById('tax_token');
        var tax_expiry_date = $('#datetimepicker4').val();

        $('#datetimepicker2'). prop("required", false);
        if (tax_token.files.length > 0){
            if(tax_expiry_date == "") {
                $('#datetimepicker4'). prop("required", true);
                return false;
            }
        }

        var insurance_paper = document.getElementById('insurance_paper');
        var insurance_expiry_date = $('#datetimepicker5').val();

        $('#datetimepicker5'). prop("required", false);
        if (insurance_paper.files.length > 0){
            if(insurance_expiry_date == "") {
                $('#datetimepicker5'). prop("required", true);
                return false;
            }
        }

        return true;
    }

    @php($driver_id = (isset($data->drivers_id)) ? $data->drivers_id : old('driver_id', ''))
    @php($vehicle_type = (isset($data->vehicle_type)) ? $data->vehicle_type : old('vehicle_type', ''))
    @php($brand = (isset($data->car_brand)) ? $data->car_brand : old('car_brand', ''))

    window.vueApp = new Vue({
        el: '#car-form',
        data: {
            drivers: {!! $drivers !!},
            selectedDriverId: '{!! $driver_id !!}',
            vehicle_type: '{!! $vehicle_type !!}',
            car_brand: '{!! $brand !!}',
            car_models: {!! $car_models !!},
        },
        mounted() {
        },
        methods: {
            getDrivers(event) {
                const merchant_id = event.target.value;
                axios.get('/admin/api/get-drivers/' + merchant_id).then(response => {
                    this.drivers = response.data.data
                })
            },
            getCarModels(event) {
                axios.get('/admin/api/get-cars', {
                    params: {
                        vehicle_type: this.vehicle_type,
                        car_brand: this.car_brand,
                    }
                }).then(response => {
                    this.car_models = response.data.data
                })
            }
        }
    });
</script>
