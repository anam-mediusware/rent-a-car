@extends('admin.layouts.master')

@php
    $pageName = 'Cars';
    $pageResource = 'admin.cars';
@endphp

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                @if (Route::has($pageResource.'.index'))
                    <li>
                        <a href="{{ route($pageResource.'.index') . qString() }}">
                            <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
                        </a>
                    </li>
                @endif
                @if (Route::has($pageResource.'.create'))
                    <li>
                        <a href="{{ route($pageResource.'.create') . qString() }}">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
                        </a>
                    </li>
                @endif

                <li class="active">
                    <a href="#">
                        <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    @if (isset($data))
                        <div class="box-body table-responsive">
                            <table class="table table-bordered">
                                <caption class="hidden"><h3>{{ $pageName }} Details</h3></caption>
                                <thead>
                                <tr class="hide">
                                    <th style="width:230px;"></th>
                                    <th style="width:10px;"></th>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th style="width:230px;">Driver Name</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{$data->driver !="" ? $data->driver->full_name : ''}}</td>
                                </tr>
                                <tr>
                                    <th>Car Number</th>
                                    <th>:</th>
                                    <td>{{ $data->metro.'-'.$data->alphabetical_serial.'-'.$data->serial_number }}</td>
                                </tr>
                                <tr>
                                    <th>Vehicle Type</th>
                                    <th>:</th>
                                    <td>{{ $data->vehicle_type }}</td>
                                </tr>
                                <tr>
                                    <th>Brand</th>
                                    <th>:</th>
                                    <td>{!! $data->car_brand !!}</td>
                                </tr>
                                <tr>
                                    <th>Model</th>
                                    <th>:</th>
                                    <td>{{ $data->model }}</td>
                                </tr>
                                <tr>
                                    <th>Car Registration Front</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('cars/registration', $data->car_document['reg_front_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>

                                <tr>
                                    <th>Car Registration Back</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('cars/registration', $data->car_document['reg_back_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Registration Expiry Date</th>
                                    <th>:</th>
                                    <td>{{ date('d/m/Y',strtotime($data->car_document['reg_expiry_date'])) }}</td>
                                </tr>
                                <tr>
                                    <th>Fitness Paper</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('cars/fitness', $data->car_document['fitness_paper_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Fitness Paper Expiry Date</th>
                                    <th>:</th>
                                    <td>{{ date('d/m/Y',strtotime($data->car_document['fit_expiry_date'])) }}</td>
                                </tr>
                                <tr>
                                    <th>Tax Token</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('cars/tax', $data->car_document['tax_token_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Tax Tokan Expiry Date</th>
                                    <th>:</th>
                                    <td>{{ date('d/m/Y',strtotime($data->car_document['tax_expiry_date'])) }}</td>
                                </tr>
                                <tr>
                                    <th>Insurance Paper</th>
                                    <th>:</th>
                                    <td>
                                        {!! viewImg('cars/insurance', $data->car_document['insurance_paper_img'], ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Insurance Paper Expiry Date</th>
                                    <th>:</th>
                                    <td>{{ date('d/m/Y',strtotime($data->car_document['insurance_expiry_date'])) }}</td>
                                </tr>

                                <tr>
                                    <th>Car Image - Outer</th>
                                    <th>:</th>
                                    <td>
                                        @foreach($data->car_image->where('type', 'outer') as $car_image)
                                        {!! viewImg('cars/car-img', $car_image['image']?$car_image['image'] : '', ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                        @endforeach
                                    </td>
                                </tr>

                                <tr>
                                    <th>Car Image - Inner</th>
                                    <th>:</th>
                                    <td>
                                        @foreach($data->car_image->where('type', 'inner') as $car_image)
                                            {!! viewImg('cars/car-img', $car_image['image']?$car_image['image'] : '', ['popup' => 1, 'style' =>'width:200px;', 'fakeimg' => 'no-img']) !!}
                                        @endforeach
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="box-body">
                            {!! notFoundText() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
