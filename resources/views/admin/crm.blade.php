@extends('admin.layouts.master')

<?php
$pageName = 'Customer Insight';
$pageResource = 'admin.crm';
?>

@section('content')

@if (session('message'))
<section class="content-header">
    <div class="alert alert-success" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      {{ session('message') }}
    </div>
</section>
@endif

<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li {{ (isset($lists))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.index') . qString() }}">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }} List
            </a>
        </li>
        <li {{ (isset($create))?'class=active':'' }}>
            <a href="{{ route($pageResource.'.create') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> Add {{ $pageName }}
            </a>
        </li>

        @if (isset($edit))
        <li class="active">
            <a href="#">
                <i class="fa fa-edit" aria-hidden="true"></i> Edit {{ $pageName }}
            </a>
        </li>
        @endif

        @if (isset($show))
        <li class="active">
            <a href="#">
                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ $pageName }} Details
            </a>
        </li>
        @endif
    </ul>

    <div class="tab-content">
        @if(isset($show))
        <div class="tab-pane active">
            @if (isset($data))
                <div class="box-body">
                    <div class="col-sm-6 table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th style="width:120px;">Calling Number</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{ $data->customer_number }}</td>
                                </tr>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>:</th>
                                    <td>{{ $data->customer_name }}</td>
                                </tr>
                                <tr>
                                    <th>Customer Email</th>
                                    <th>:</th>
                                    <td>{{ $data->customer_email }}</td>
                                </tr>
                                <tr>
                                    <th>Customer Address</th>
                                    <th>:</th>
                                    <td>{!! nl2br($data->customer_address) !!}</td>
                                </tr>
                                <tr>
                                    <th>Date & Time</th>
                                    <th>:</th>
                                    <td>{{ dateFormat($data->created_at, 1) }}</td>
                                </tr>
                                <tr>
                                    <th>Ticket Number</th>
                                    <th>:</th>
                                    <td>{{ $data->ticket_number }}</td>
                                </tr>
                                <tr>
                                    <th>Agent Name</th>
                                    <th>:</th>
                                    <td>{{ $data->name }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-6 table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th style="width:120px;">Customer Query</th>
                                    <th style="width:10px;">:</th>
                                    <td>{{ nl2br($data->customer_query) }}</td>
                                </tr>
                                <tr>
                                    <th>Travel Date & Time</th>
                                    <th>:</th>
                                    <td>{{ $data->travel_datetime }}</td>
                                </tr>
                                <tr>
                                    <th>Vehicel Type</th>
                                    <th>:</th>
                                    <td>{{ $data->vehicle_type }}</td>
                                </tr>
                                <tr>
                                    <th>Our Feedback</th>
                                    <th>:</th>
                                    <td>{!! nl2br($data->feedback) !!}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <th>:</th>
                                    <td>{{ ($data->status==1)?'Solved':(($data->status==2)?'Booking':'Followup') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div class="box-body">
                    {!! notFoundText() !!}
                </div>
            @endif
        </div>

        @elseif(isset($edit) || isset($create))
        <div class="tab-pane active">
            <div class="box-body">
                <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    @if ((isset($edit)))@method('PUT')@endif
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('customer_number') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Calling Number:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="customer_number" id="customer_number" value="{{ isset($data->customer_number)?$data->customer_number:old('customer_number') }}" required onblur="getCalling()">

                                    @if ($errors->has('customer_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('customer_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('customer_name') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Customer Name:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="customer_name" value="{{ isset($data->customer_name)?$data->customer_name:old('customer_name') }}">

                                    @if ($errors->has('customer_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('customer_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('customer_email') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Customer Email:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="customer_email" value="{{ isset($data->customer_email)?$data->customer_email:old('customer_email') }}">

                                    @if ($errors->has('customer_email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('customer_email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('customer_address') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Customer Address:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="customer_address" rows="5">{{ isset($data->customer_address)?$data->customer_address:old('customer_address') }}</textarea>

                                    @if ($errors->has('customer_address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('customer_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            {{-- <div class="form-group">
                                <label class="control-label col-sm-4">Date & Time:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ isset($data->created_at)?$data->created_at:date('Y-m-d H:i:s') }}" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Ticket Number:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ isset($data->ticket_number)?$data->ticket_number:'' }}" readonly placeholder="Auto Generated">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4">Agent Name:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="{{ isset($data->created_name)?$data->created_name:Auth::user()->name }}" readonly>
                                </div>
                            </div> --}}
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('customer_query') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Customer Query:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="customer_query" rows="5">{{ isset($data->customer_query)?$data->customer_query:old('customer_query') }}</textarea>

                                    @if ($errors->has('customer_query'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('customer_query') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('travel_datetime') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Travel Date & Time:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control datetimepicker" name="travel_datetime" value="{{ isset($data->travel_datetime)?$data->travel_datetime:old('travel_datetime') }}">
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('vehicle_type') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Vehicel Type:</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="vehicle_type" value="{{ isset($data->vehicle_type)?$data->vehicle_type:old('vehicle_type') }}">
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('feedback') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4">Our Feedback:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="feedback" rows="5">{{ isset($data->feedback)?$data->feedback:old('feedback') }}</textarea>

                                    @if ($errors->has('feedback'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('feedback') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="control-label col-sm-4 required">Status:</label>
                                <div class="col-sm-8">
                                    <?php $status = (isset($data->status))?$data->status:old('status'); ?>
                                    <select name="status" class="form-control" required>
                                        <option value="">Select Status
                                        @foreach([1 => 'Solved', 2 => 'Booking', 3 => 'Followup'] as $sK => $sV)
                                            <option value="{{ $sK }}" {{ ($status==$sK)?'selected':'' }}>{{ $sV }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success btn-flat btn-lg">{{ (isset($edit))?'Update':'Create' }}</button>
                                <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Ticket Number</th>
                                <th>Ticket Date</th>
                                <th>Calling Number</th>
                                <th>Customer Name</th>
                                <th>Customer Query</th>
                                <th>Feedback</th>
                            </tr>
                        </thead>
                        <tbody id="callingData">
                            @if(isset($callings) && !empty($callings))
                            @foreach($callings as $key => $val)
                            <tr>
                                <td>{{$val->ticket_number}}</td>
                                <td>{{ dateFormat($val->created_at, 1) }}</td>
                                <td>{{$val->customer_number}}</td>
                                <td>{{ excerpt($val->customer_name) }}</td>
                                <td>{{ excerpt($val->customer_query) }}</td>
                                <td>{{ excerpt($val->feedback) }}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script>
            function getCalling() {
                var customer_number = $('#customer_number').val();
                $.ajax({
                    type: "GET",
                    dataType: "JSON",
                    url: '/admin/crm/calling',
                    data: {'customer_number':customer_number},
                    success: function( response ) {
                        var html = '';
                        if (response.status==1) {
                            var callings = response.callings;
                            for (let i = 0; i < callings.length; i++) {
                                html += '<tr>'+
                                '<td>'+callings[i].ticket_number+'</td>'+
                                '<td>'+callings[i].created_at+'</td>'+
                                '<td>'+callings[i].customer_number+'</td>'+
                                '<td>'+callings[i].customer_name+'</td>'+
                                '<td>'+callings[i].customer_query+'</td>'+
                                '<td>'+callings[i].feedback+'</td>'+
                            '</tr>';
                            }

                            $('#callingData').html(html);
                        } else {
                            $('#callingData').html('');
                        }
                    }
                });
            }
        </script>

        @elseif (isset($lists))
        <div class="tab-pane active">
            <form method="GET" action="{{ route($pageResource.'.index') }}" class="form-inline">
                <div class="box-header text-right">
                    <div class="row">
                        <div class="form-group">
                            <input type="text" class="form-control" name="q" value="{{ Request::get('q') }}" placeholder="Write your search text...">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Search</button>
                            <a class="btn btn-warning btn-flat" href="{{ url('/admin/'.$pageResource) }}">X</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="box-body table-responsive">
                <span class="text-muted">Showing {{$records->currentPage()*$records->perPage()-$records->perPage()+1}} to {{ ($records->currentPage()*$records->perPage()>$records->total())?$records->total():$records->currentPage()*$records->perPage()}} of {{$records->total()}} data(s)</span>
                <?php //echo $pagination->msg; ?>
                <table class="table table-bordered table-hover dataTable">
                    <caption class="hidden"><h3><?php echo $pageName; ?> List</h3></caption>
                    <thead>
                        <tr>
                            <th>SL.</th>
                            <th>Ticket Number</th>
                            <th>Ticket Date</th>
                            <th>Calling Number</th>
                            <th>Customer Name</th>
                            <th>Customer Query</th>
                            <th>Feedback</th>
                            <th class="not-export-col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($records as $key => $val)
                        <tr>
                            <td>{{$serial++}}</td>
                            <td>{{$val->ticket_number}}</td>
                            <td>{{ dateFormat($val->created_at, 1) }}</td>
                            <td>{{$val->customer_number}}</td>
                            <td>{{ excerpt($val->customer_name) }}</td>
                            <td>{{ excerpt($val->customer_query) }}</td>
                            <td>{{ excerpt($val->feedback) }}</td>
                            <td>
                            <?php
                            $access = 1;
                            listAction([
                                actionLi(route($pageResource.'.show', $val->id).qString(), 'show', $access),
                                actionLi(route($pageResource.'.edit', $val->id).qString(), 'edit', $access),
                                actionLi(route($pageResource.'.destroy', $val->id).qString(), 'delete', $access),
                            ]);
                            ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
        @endif
    </div>
  </div>
</section>
@endsection
