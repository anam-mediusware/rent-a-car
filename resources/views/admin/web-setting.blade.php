@extends('admin.layouts.master')

<?php
$pageName = 'Website Settings';
$pageResource = 'admin.web-settings';
?>

@section('content')

    @if (session('message'))
        <section class="content-header">
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                {{ session('message') }}
            </div>
        </section>
    @endif

    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li {{ (isset($lists))?'class=active':'' }}>
                    <a href="{{ route($pageResource.'.index') . qString() }}">
                        <i class="fa fa-list" aria-hidden="true"></i> {{ $pageName }}
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <div class="box-body">
                        <form method="POST" action="{{ url($actionLink) }}" id="are_you_sure" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="control-label col-sm-3 required">Phone:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="phone" id="phone" value="{{ isset($data->phone)?$data->phone:old('phone') }}" required>

                                            @error('phone')
                                            <span class="help-block"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="control-label col-sm-3 required">Email:</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" name="email" id="email" value="{{ isset($data->email)?$data->email:old('email') }}" required>

                                            @error('email')
                                            <span class="help-block"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
                                        <label for="facebook" class="control-label col-sm-3">Facebook:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="facebook" id="facebook" value="{{ isset($data->facebook)?$data->facebook:old('facebook') }}">

                                            @error('facebook')
                                            <span class="help-block"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
                                        <label for="twitter" class="control-label col-sm-3">Twitter:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="twitter" id="twitter" value="{{ isset($data->twitter)?$data->twitter:old('twitter') }}">

                                            @error('twitter')
                                            <span class="help-block"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }}">
                                        <label for="instagram" class="control-label col-sm-3">Instagram:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="instagram" id="instagram" value="{{ isset($data->instagram)?$data->instagram:old('instagram') }}">

                                            @error('instagram')
                                            <span class="help-block"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('linkedin') ? ' has-error' : '' }}">
                                        <label for="linkedin" class="control-label col-sm-3">Linked In:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="linkedin" id="linkedin" value="{{ isset($data->linkedin)?$data->linkedin:old('linkedin') }}">

                                            @error('linkedin')
                                            <span class="help-block"><strong>{{ $message }}</strong></span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-success btn-flat btn-lg">Save</button>
                                        <button type="reset" class="btn btn-warning btn-flat btn-lg">Clear</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
