@extends('layouts.frontend')
@section('content')
    <div id="app">
        <div class="x_partner_main_wrapper float_left padding_tb_100">
            <div class="container">
                <div class="row">
                    <div class="col-2">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
                            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Bookings</a>
                            <a class="nav-link" id="v-pills-bidding-trip-tab" data-toggle="pill" href="#v-pills-bidding-trip" role="tab" aria-controls="v-pills-bidding-trip" aria-selected="false">Bidding Trips</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Payments</a>
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <profile-component></profile-component>
                            </div>
                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                <booking-component></booking-component>
                            </div>
                            <div class="tab-pane fade" id="v-pills-bidding-trip" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <bidding-trip-component></bidding-trip-component>
                            </div>
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <payment-component></payment-component>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
