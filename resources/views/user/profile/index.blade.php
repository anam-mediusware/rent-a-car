@extends('user.layouts.user')

@section('user.content')
    <div id="app">
        <profile-component></profile-component>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('#account-profile').addClass('active');
        });
    </script>
@endpush
