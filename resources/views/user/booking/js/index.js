window.vueApp = new Vue({
    el: '#booking-index',
    data: {
        ratingId: null,
        ratingData: {
            overall_rating: 0,
            car_condition: 0,
            driver_behavior: 0,
            driver_punctuality: 0,
            comment: '',
        }
    },
    mounted() {
        const _this = this
        this.$nextTick(() => {
            $(".rating").rating({
                "click": function (e) {
                    const id = $(e.event.target).parent().attr('id')
                    _this.ratingData[id] = e.stars
                }
            });
        })
    },
    methods: {
        submitUpdateStatus(id, status, review = false) {
            let data = {
                action: status
            }
            if (review) {
                data['ratingData'] = this.ratingData
            }
            axios.post('/bookings/update-status/' + id, data).then(response => {
                if (response.data.success) {
                    window.location.reload()
                } else {
                    if (response.data.errors) {
                        Object.keys(response.data.errors).forEach(key => {
                            toastr.error(response.data.errors[key][0])
                        })
                    } else {
                        toastr.error(response.data.error)
                    }
                }
            })
        },
        onchangeActionHandler(event, id) {
            this.ratingId = id;
            if (!event.target.value) return;
            console.log(event.target.value)
            if (event.target.value === 'Complete') {
                $('#tripCompleteModal').modal()
                return;
            }
            swal({
                title: "Are you sure?",
                text: "Once done, You will not able to rollback!",
                icon: false,
                buttons: true,
                dangerMode: false,
            }).then((confirmedSelection) => {
                if (confirmedSelection) {
                    this.submitUpdateStatus(id, event.target.value)
                } else {
                    console.log('Canceled!')
                }
            });
        },
        handleSkipAndComplete() {
            this.submitUpdateStatus(this.ratingId, 'Complete')
        },
        handleSubmitAndComplete() {
            this.submitUpdateStatus(this.ratingId, 'Complete', true)
        }
    }
});
