@extends('user.layouts.user')

@section('user.content')
    <div id="booking-index">
        @if (session('message'))
            <section class="content-header">
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('message') }}
                </div>
            </section>
        @elseif (session('errors'))
            @foreach(session('errors') as $error)
                <section class="content-header">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ $error->first() }}
                    </div>
                </section>
            @endforeach
        @endif

        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Your Bookings...</h5>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Service</th>
                        <th scope="col">Location</th>
                        <th scope="col">Booking Time</th>
                        <th scope="col">Status</th>
                        <th scope="col">Operations</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($serial = ($records->perPage() * ($records->currentPage() - 1)) + 1)
                    @foreach($records as $key => $record)
                        <tr>
                            <th scope="row">{{$serial++}}</th>
                            <td>
                                <img src="{{ asset($record->vehicleType->vehicle_image) }}" style="margin-top:-12px" class="service-img" alt="">
                                {{ $record->vehicleType->vehicle_type_english }}({{ $record->service->service_name }})
                            </td>
                            <td>
                                <b>{{ ucwords($record->upazilaBoundaryFrom ? $record->upazilaBoundaryFrom->Upaz_name : 'N/A') }}</b>
                                to
                                <b>{{ ucwords($record->upazilaBoundaryTo ? $record->upazilaBoundaryTo->Upaz_name : 'N/A') }}</b>
                            </td>
                            <td>{{ formatDateTime($record->created_at) }}</td>
                            <td>{{ $record->booking_status }}</td>
                            <td>
                                @if ($bookingStatus->availableOptions($record->booking_status, 'customer')->count() > 0)
                                    <select id="action-{{$record->id}}" name="action" class="form-control" @change="onchangeActionHandler($event, {{$record->id}})">
                                        <option value="">Change Operation</option>
                                        @foreach($bookingStatus->availableOptions($record->booking_status, 'customer') as $key => $status)
                                            <option value="{{ $key }}">{{ $status }}</option>
                                        @endforeach
                                    </select>
                                @else
                                    <p>No Operations Available</p>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    {{ $records->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="tripCompleteModal" tabindex="-1" role="dialog" aria-labelledby="tripCompleteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="tripCompleteModalTitle">Rating</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="mr-5">
                            {{ makeLabel('overall_rating') }} :
                            <div class="float-right">
                                <div id="overall_rating" class="rating" data-rating-stars="5" data-rating-value="0" data-rating-readonly="false"></div>
                            </div>
                        </div>
                        <div class="mr-5">
                            {{ makeLabel('car_condition_rating') }} :
                            <div class="float-right">
                                <div id="car_condition" class="rating" data-rating-stars="5" data-rating-value="0" data-rating-readonly="false"></div>
                            </div>
                        </div>
                        <div class="mr-5">
                            {{ makeLabel('driver_behavior_rating') }} :
                            <div class="float-right">
                                <div id="driver_behavior" class="rating" data-rating-stars="5" data-rating-value="0" data-rating-readonly="false"></div>
                            </div>
                        </div>
                        <div class="mr-5">
                            {{ makeLabel('driver_punctuality_rating') }} :
                            <div class="float-right">
                                <div id="driver_punctuality" class="rating" data-rating-stars="5" data-rating-value="0" data-rating-readonly="false"></div>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label for="comment">Comment</label>
                            <textarea v-model="ratingData.comment" class="form-control" name="comment" id="comment" cols="30" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" @click="handleSkipAndComplete">Skip & Complete
                        </button>
                        <button type="button" class="btn btn-success" @click="handleSubmitAndComplete">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
    <style>
        .checked {
            color: orange;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(() => {
            $('#bookings').addClass('active');
        });
    </script>
    <script src="{{ asset('web-assets/js/rating.js') }}"></script>
    <script src="{{ mix('js/pages/user/booking.index.js') }}"></script>
@endpush
