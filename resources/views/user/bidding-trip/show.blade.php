@extends('user.layouts.user')

@section('user.content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Bids of Your Bidding Trips...</h5>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Driver</th>
                            <th scope="col">Price</th>
                            <th scope="col">Operations</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($biddingTrip)
                            @foreach($biddingTrip->bids as $bid)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>
                                        <div class="d-flex">
                                            {{ $bid->driver->full_name }}
                                            <div class="rating-tooltip ml-2">
                                                <div class="rating-tooltips-outer">
                                                    <div id="dataReadonlyReview"
                                                         data-rating-stars="5"
                                                         data-rating-readonly="true"
                                                         data-rating-value="{{$bid->driver->overall_rating}}"
                                                         data-rating-input="#dataReadonlyInput">
                                                    </div>
                                                </div>
                                                <div class="rating-tooltips-inner">
                                                    <div>
                                                        {{ makeLabel('overall_rating') }} :
                                                        <div class="float-right">
                                                            <div id="dataReadonlyReview"
                                                                 data-rating-stars="5"
                                                                 data-rating-readonly="true"
                                                                 data-rating-value="{{$bid->driver->overall_rating}}"
                                                                 data-rating-input="#dataReadonlyInput">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        {{ makeLabel('car_condition_rating') }} :
                                                        <div class="float-right">
                                                            <div id="dataReadonlyReview"
                                                                 data-rating-stars="5"
                                                                 data-rating-readonly="true"
                                                                 data-rating-value="{{$bid->driver->car_condition_rating}}"
                                                                 data-rating-input="#dataReadonlyInput">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        {{ makeLabel('driver_behavior_rating') }} :
                                                        <div class="float-right">
                                                            <div id="dataReadonlyReview"
                                                                 data-rating-stars="5"
                                                                 data-rating-readonly="true"
                                                                 data-rating-value="{{$bid->driver->driver_behavior_rating}}"
                                                                 data-rating-input="#dataReadonlyInput">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        {{ makeLabel('driver_punctuality_rating') }} :
                                                        <div class="float-right">
                                                            <div id="dataReadonlyReview"
                                                                 data-rating-stars="5"
                                                                 data-rating-readonly="true"
                                                                 data-rating-value="{{$bid->driver->driver_punctuality_rating}}"
                                                                 data-rating-input="#dataReadonlyInput">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $bid->price }}</td>
                                    <td>
                                        <a href="{{ route('user.bids.show', $bid->id) }}" class="btn btn-primary btn-sm">Accept</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">
                                    <div class="box-body">
                                        {!! notFoundText() !!}
                                    </div>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" />
    <style type="text/css">
        .rating-tooltip {
            text-decoration:none;
            position:relative;
        }

        .rating-tooltip .rating-tooltips-inner {
            display:none;
            -moz-border-radius:6px;
            -webkit-border-radius:6px;
            border-radius:6px;
            color:black;
            background:white;
        }

        .rating-tooltip .rating-tooltips-inner img {
            float:left;
            margin:0px 8px 8px 0;
        }

        .rating-tooltip:hover .rating-tooltips-inner {
            display:block;
            position:absolute;
            top:0;
            left:0;
            z-index:1000;
            width:600px;
            max-width:320px;
            min-height:128px;
            border:1px solid black;
            margin-top:12px;
            margin-left:32px;
            overflow:hidden;
            padding:8px;
        }

        .rating-tooltips-inner .float-right {
            display: inline;
        }

        .checked {
            color: orange;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{ asset('web-assets/js/rating.js') }}"></script>
    <script>
        $(document).ready(() => {
            $('#bidding-trips').addClass('active');
            $(".rating").rating({
                "emptyStar": "far fa-star",
                "halfStar": "fas fa-star-half-alt",
                "filledStar": "fas fa-star"
            });
        });
    </script>
@endpush
