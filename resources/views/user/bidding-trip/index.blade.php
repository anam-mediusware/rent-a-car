@extends('user.layouts.user')

@section('user.content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Your Bidding Trips...</h5>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Service</th>
                    <th scope="col">Location</th>
                    <th scope="col">Booking Time</th>
                    <th scope="col">Bids</th>
                    <th scope="col">Operations</th>
                </tr>
                </thead>
                <tbody>
                @php($serial = ($records->perPage() * ($records->currentPage() - 1)) + 1)
                @foreach($records as $key => $record)
                    <tr>
                        <th scope="row">{{$serial++}}</th>
                        <td>
                            <img src="{{ asset($record->vehicleType->vehicle_image) }}" style="margin-top:-12px" class="service-img" alt="">
                            {{ $record->vehicleType->vehicle_type_english }}({{ $record->service->service_name }})
                        </td>
                        <td>
                            <b>{{ ucwords($record->upazilaBoundaryFrom ? $record->upazilaBoundaryFrom->Upaz_name : 'N/A') }}</b>
                            to
                            <b>{{ ucwords($record->upazilaBoundaryTo ? $record->upazilaBoundaryTo->Upaz_name : 'N/A') }}</b>
                        </td>
                        <td>{{ formatDateTime($record->created_at) }}</td>
                        <td>{{ $record->bids_count }}</td>
                        <td>
                            <a href="{{ route('user.bidding-trips.show', $record->id) }}" class="btn btn-primary btn-sm">
                                View <i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {{ $records->appends(Request::except('page'))->links() }}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('#bidding-trips').addClass('active');
        });
    </script>
@endpush
