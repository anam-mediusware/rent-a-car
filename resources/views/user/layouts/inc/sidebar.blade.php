<div class="nav flex-column nav-pills">
    <a class="nav-link" id="account-profile" href="{{ url('/account/profile') }}"><i class="fa fa-chevron-right"></i> Profile</a>
    <a class="nav-link" id="bookings" href="{{ url('/bookings') }}"><i class="fa fa-chevron-right"></i> Bookings</a>
    <a class="nav-link" id="bidding-trips" href="{{ url('/bidding-trips') }}"><i class="fa fa-chevron-right"></i> Bidding Trips</a>
    <a class="nav-link" id="payments" href="{{ url('/payments') }}"><i class="fa fa-chevron-right"></i> Payments</a>
</div>
