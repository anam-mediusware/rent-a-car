@extends('layouts.frontend')

@section('content')
    <div>
        <div class="x_partner_main_wrapper float_left padding_tb_100">
            <div class="container">
                <div class="row">
                    <div class="col-2">
                        @include('user.layouts.inc.sidebar')
                    </div>
                    <div class="col-10">
                        @yield('user.content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
