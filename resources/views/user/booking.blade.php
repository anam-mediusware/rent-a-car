@extends('layouts.frontend')

@section('content')
    <div class="x_partner_main_wrapper float_left padding_tb_100">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_offer_car_heading_wrapper float_left">
                        <!-- <h4>Booking your car now</h4> -->
                        @if ($booking_type == 'fixed')
                            <h3>Book Your Car Now</h3>
                        @else
                            <h3>Bid For Your Car Now</h3>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row form-div">
                <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-12 col-sm-12 col-12">
                    <!-- login_wrapper -->
                    <div class="register_wrapper_box">

                        <form action="{{ $booking_type == 'fixed' ? url('/pay') : route('user.bidding-trips.store') }}" method="post">
                            @csrf
                            <div class="register_left_form">
                                <div class="jp_regiter_top_heading">
                                    <p style="color:red"><u>User Details:</u></p>
                                </div>
                                <div class="row clearfix">
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name}}">
                                    </div>
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}">
                                    </div>
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input id="mobile" type="text" class="form-control" name="mobile" value="{{ Auth::user()->mobile }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <p style="color:red; margin-left:15px"><u>Booking Details:</u></p>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                    <!-- <p>Starting Point: {{ $data['pick_up_location'] }}</p> -->
                                        <ul class="list-group">
                                            <li class="list-group-item">Vehicle Type: <br>
                                                <img src="{{ $vehicle_type['vehicle_image'] }}" class="service-img" alt="">
                                                <br>
                                                {{ $data['vehicle_name'] }}
                                            </li>
                                            @if ($booking_type == 'fixed')
                                                <li class="list-group-item">Service Type: {{ $data['service_name'] }}</li>
                                            @endif
                                            <li class="list-group-item">
                                                Pickup Point: {{ $data['pick_up_location'] }}
                                            </li>
                                            <li class="list-group-item">
                                                Drop-Off Point: {{ $data['drop_off_location'] }}
                                            </li>
                                            <li class="list-group-item">Pickup date:
                                                {{ carbonParse($data['pick_up_date'] . ' ' . $data['pick_up_time'])->format('j F, Y, h:i A') }}
                                            </li>
                                            <li class="list-group-item">Drop-Off date:
                                                {{ carbonParse($data['drop_off_date'] . ' ' . $data['drop_off_time'])->format('j F, Y, h:i A') }}
                                            </li>
                                            @if ($booking_type == 'fixed')
                                                <li class="list-group-item">Price: {{ $data['price'] }}</li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="check-box text-center">
                                        <input type="checkbox" name="shipping-option" id="account-option_2">  
                                        <label for="account-option_2" class="label_2">I agreed to the
                                            <a href="#" class="check_box_anchr">Terms and Conditions</a></label>
                                    </div>
                                </div>
                            </div>

                            <!-- Hidden values -->
                            <input type="hidden" name="pricing_type" id="pricing_type" value="{{ $booking_type}}">

                            <div class="login_btn_wrapper">
                                <button type="submit" class="confirm_btn">Confirm</button>
                            </div>
                        </form>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="alert alert-success" id="success-booking"></div>
                        </div>
                    </div>
                </div>
                <!-- /.login_wrapper-->
            </div>
        </div>
    </div>
@endsection
