<?php

namespace Tests\Feature;

use App\Models\Driver;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NewBidBroadcast extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function newBidCreated()
    {
        $trip = \App\Models\BiddingTrip::create([
            "customer_type" => 2,
            "customer_id" => 2,
            "vehicle_type_id" => 1,
            "service_category_id" => 1,
            "service_type_id" => 10,
            "airport_id" => null,
            "pick_up_city" => 519,
            "pick_up_location" => "Adabar, Dhaka",
            "pick_up_point" => null,
            "drop_off_city" => 453,
            "drop_off_location" => "Mirpur, Dhaka",
            "drop_off_point" => null,
            "pick_up_date_time" => "2020-10-16 17:58:34",
            "drop_off_date_time" => "2020-10-18 07:58:34",
            "price" => "0.00",
            "status" => 1,
        ]);

        $drivers = Driver::select(['id', 'city_id'])
            ->whereHas('DrivingCity', function ($city) use ($trip) {
                $city->where('Dist_ID', $trip->upazilaBoundaryFrom->Dist_ID);
            })
            ->whereHas('cars', function ($car) use ($trip) {
                $cmp = $trip->service_quality_type == 1 ? '<' : '>=';
                $car->where([
                    'car_model_id' => $trip->car_model_id,
                    'vehicle_type' => $trip->vehicle_type_id,
                ])
                    ->where('year', $cmp, 2015);
            })
            ->get();
        \App\Events\NewBidEvent::dispatch($trip, $drivers->pluck('id'));
    }
}
