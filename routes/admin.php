<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin', 'as' => 'admin.'], function () {

    Auth::routes();

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/', function () { return redirect()->route('admin.dashboard'); });
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
        Route::get('/profile/edit', 'ProfileController@edit')->name('profile.self-edit');
        Route::post('/profile/update', 'ProfileController@update')->name('profile.self-update');
        Route::get('/profile/password', 'ProfileController@password')->name('profile.password');
        Route::post('/profile/updatePassword', 'ProfileController@updatePassword')->name('profile.updatePassword');
        Route::resource('/profile', 'ProfileController');

        Route::resource('/car-models', 'CarModelController');
        Route::get('/api/get-cars', 'CarsController@availableCars');

        Route::post('/drivers/getPaymentDetails', 'DriversController@getPaymentDetails')->name('DriversController.getPaymentDetails');
        Route::resource('/drivers', 'DriversController');
        Route::resource('/cars', 'CarsController');

        Route::resource('/service-pricing', 'ServicePricingController');
        Route::post('/service-pricing/updateGlobalSetting', 'ServicePricingController@updateGlobalSetting')->name('ServicePricingController.updateGlobalSetting');
        Route::post('/service-pricing/updatePrice', 'ServicePricingController@updatePrice')->name('ServicePricingController.updatePrice');
        Route::post('/service-pricing/updateAdditionalPrice', 'ServicePricingController@updateAdditionalPrice')->name('ServicePricingController.updateAdditionalPrice');
        Route::post('/service-pricing/addAdditionalPrice', 'ServicePricingController@addAdditionalPrice')->name('ServicePricingController.addAdditionalPrice');
        Route::delete('/service-pricing/additionalPriceDestroy/{id}', 'ServicePricingController@additionalPriceDestroy')->name('ServicePricingController.additionalPriceDestroy');

        Route::resource('/client', 'ClientController');

        Route::get('/merchant/{id}/contact', 'MerchantController@contact')->name('merchant.contact');
        Route::post('/merchant/{id}/contact', 'MerchantController@contactStore');
        Route::resource('/merchant', 'MerchantController');

        Route::post('/package/package-image-upload', 'PackageController@packageImageUpload')->name('package.packageImageUpload');
        Route::get('/package/{id}/image-video-upload', 'PackageController@imageVideoUpload')->name('package.imageVideoUpload');
        Route::resource('/package', 'PackageController');

        Route::resource('/service-setting', 'ServiceSettingController');

        Route::get('/booking/booking-bid-list', 'BookingController@bookingBidList')->name('booking.bookingBidList');
        Route::get('/booking/booking-cancelled-list', 'BookingController@bookingCancelledList')->name('booking.bookingCancelledList');
        Route::post('/booking/assign-driver/{booking_id}', 'BookingController@assignDriver')->name('booking.assign-driver');
        Route::resource('/booking', 'BookingController');

        // Report Routes Start
        Route::get('/report/receivable', 'ReportController@index');
        Route::get('/report/payable', 'ReportController@payable');
        Route::get('/report/income', 'ReportController@income');
        Route::get('/report/expense', 'ReportController@expense');
        Route::get('/report/profit-loss', 'ReportController@profitLoss');
        // Report Routes End

        Route::get('/bidding-trips/bids/{bidding_trip_id}', 'BiddingTripController@bids')->name('bidding-trips.bids');
        Route::post('/bidding-trips/bid/{id}', 'BiddingTripController@submitBid')->name('bidding-trips.submit-bid');
        Route::resource('/bidding-trips', 'BiddingTripController')->only('index', 'show', 'delete');

        Route::resource('/expense-category', 'ExpenseCategoryController');
        Route::resource('/expense', 'ExpenseController');

        Route::get('/crm/calling', 'CrmController@calling');
        Route::get('/crm/followup', 'CrmController@followup');
        Route::resource('/crm', 'CrmController');

        Route::resource('/sms', 'SmsController');

        Route::resource('/contact-mail', 'ContactMailController');

        // CMS Routes Start
        Route::resource('/tourism', 'TourismController');
        Route::resource('/management', 'ManagementController');
        Route::resource('/slide', 'SlideController');
        Route::resource('/career', 'CareerController');
        Route::resource('/news', 'NewsController');
        Route::resource('/policy', 'PolicyController');
        Route::resource('/contact-type', 'ContactTypeController');
        Route::resource('/seo', 'SeoController');
        Route::resource('/web-settings', 'WebSettingController');
        // CMS Routes End
    });
});
