<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['namespace' => 'Web'], function () {
    // Pages Routes Start
    Route::get('/', 'PagesController@index')->name('home');
    Route::get('/home', 'PagesController@index')->name('home');
    Route::get('/about-us', 'PagesController@aboutUs')->name('about-us');
    Route::get('/fleet', 'PagesController@fleet')->name('fleet');
    Route::get('/management-team', 'PagesController@management')->name('management-team');
    Route::get('/careers', 'PagesController@careers')->name('careers');
    Route::get('/news', 'PagesController@news')->name('news');
    Route::get('/news/{slug}','PagesController@news')->name('news');
    Route::get('/contact-us', 'PagesController@contactUs')->name('contact-us');
    Route::post('/contact-send-mail', 'PagesController@contactSendMail')->name('contact-send-mail');
    Route::get('/cancellation-policy', 'PagesController@cancellationPolicy')->name('cancellation-policy');
    Route::get('/wait-time-policy', 'PagesController@waitTimePolicy')->name('wait-time-policy');
    Route::get('/privacy-policy', 'PagesController@privacyPolicy')->name('privacy-policy');
    Route::get('/refund-and-return-policy', 'PagesController@refundPolicy')->name('refund-and-return-policy');
    Route::get('/terms-and-conditions', 'PagesController@termsConditions')->name('terms-and-conditions');
    Route::get('/faq', 'PagesController@faq')->name('faq');
    Route::get('/sitemap', 'PagesController@sitemap')->name('sitemap');
    Route::get('/partner-with-us', 'PagesController@partnerWithUs')->name('partner-with-us');
    Route::post('/partner-make', 'PagesController@partnerMade')->name('partner-make');
    Route::get('/services', 'PagesController@services')->name('services');
    // Pages Routes End

    Route::get('/api/google/autocomplete', 'Api\GoogleApiController@autocomplete');
    Route::get('/api/google/place-details', 'Api\GoogleApiController@getPlaceDetails');
    Route::get('/api/google/reverse', 'Api\GoogleApiController@getReversePlaceDetails');

    Route::get('/api/get-car-models', 'CarController@getCarByVehicleType');

    Route::get('/api/get-airport-cities', 'AirportController@getCities');

    Route::get('/api/get-upazila', 'UpazilaBoundaryController@index')->name('upazila.index');
    Route::get('/api/get-upazila-select2', 'UpazilaBoundaryController@getUpazilaNameSelect2')->name('upazila.getUpazilaNameSelect2');
    Route::get('/api/get-upazilas-by-upazila-boundary-ids', 'UpazilaBoundaryController@getUpazilasByUpazilaBoundaryIds')->name('upazila.getUpazilaNameSelect2');
});

Route::group(['namespace' => 'User'], function () {
    Route::get('/service-categories', 'ServiceController@getServiceCategoryList');
    Route::get('/services-by-category/{id}', 'ServiceController@getServicesList');
    Route::post('/search-for-car', 'ServiceController@fetchServicePricingByCustomerRequest');
    Route::get('/check-auth-booking/{value}', 'ServiceController@checkAuthUserForBooking');
    Route::get('/booking-details/{booking_type}', 'ServiceController@bookingConfirmation')->name('booking.details');
    Route::group(['middleware' => 'auth'], function () {
        Route::post('/booking-confirm', 'BookingController@bookingConfirmationDetails')->name('booking.confirm');
        Route::patch('/update-profile', 'AccountsController@update_profile');

        Route::group(['as' => 'user.'], function () {
            Route::get('/account/bookings', 'AccountsController@bookings')->name('bookings');
            Route::get('/account/my-profile', 'AccountsController@myProfile');
            Route::get('/account/profile', 'AccountsController@profile')->name('profile');
            Route::get('/account', 'AccountsController@index')->name('account');

            Route::post('/bookings/update-status/{booking_id}', 'BookingController@updateStatus');
            Route::resource('/bookings', 'BookingController');
            Route::resource('/bids', 'BidController')->only('show');
            Route::resource('/bidding-trips', 'BiddingTripController')->only('index', 'show', 'store');
            Route::resource('/payments', 'PaymentController');
        });
    });

    Route::get('/transaction-success', 'PaymentMessageController@success')->name('transaction.success');
    Route::get('/transaction-failed', 'PaymentMessageController@failed')->name('transaction.failed');

    // SSLCOMMERZ Start
    Route::post('/pay', 'SslCommerzPaymentController@index');

    Route::post('/success', 'SslCommerzPaymentController@success');
    Route::post('/fail', 'SslCommerzPaymentController@fail');
    Route::post('/cancel', 'SslCommerzPaymentController@cancel');

    Route::post('/ipn', 'SslCommerzPaymentController@ipn');
    //SSLCOMMERZ END

    // Vue JS Route
    // Route::get('/{any}', 'HomeController@index')->where('any', '.*');
});
