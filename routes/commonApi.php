<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Common API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Common API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'common-api', 'namespace' => 'CommonApi'], function () {
    Route::apiResource('/service-categories', 'ServiceCategoryController')->except('store', 'update', 'destroy');
    Route::get('/services/service-category/{service_category_id}', 'ServiceController@getByServiceCategory')->name('services.service-category');
    Route::apiResource('/services', 'ServiceController')->except('store', 'update', 'destroy');
});
