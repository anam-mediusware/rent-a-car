<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Web API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web" middleware group. Enjoy building your Web API!
|
*/

Route::group(['namespace' => 'User'], function () {
    Route::apiResource('/services', 'ServiceController');
    Route::apiResource('/bids', 'Api\BiddingTripController');
});
