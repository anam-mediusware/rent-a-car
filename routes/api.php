<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'User'], function () {
    Route::get('/service-categories', 'ServiceController@getServiceCategoryList');
    Route::get('/services-by-category/{id}', 'ServiceController@getServicesList');
});

Route::group(['namespace' => 'Web'], function () {
    Route::get('/is-inside-district', 'UpazilaBoundaryController@isInsideDistrict');
});

Route::group(['namespace' => 'Api'], function () {

    // Customers Routes Begin
    Route::get('/', function () { echo 'worked!'; });
    Route::post('/register', 'AuthController@register');
    Route::post('/verify', 'AuthController@verify');
    Route::post('/forgot-password', 'AuthController@forgotPassword');
    Route::post('/reset-password', 'AuthController@resetPassword');
    Route::post('/login', 'AuthController@login');
    Route::group(['middleware' => ['auth:sanctum']], function() {
        Route::get('/me', 'AuthController@me');
        Route::post('/change-password', 'AuthController@changePassword');
        Route::post('/logout', 'AuthController@logout');
        Route::post('/twofa', 'AuthController@twofa');
        Route::post('/twofa-resend', 'AuthController@twofaResend');

//        Route::get('/profile', 'AuthController@profile');

        Route::apiResource('/profile', 'ProfileController');
    });
    // Customers Routes End


    // Drivers Routes Begin
    Route::group(['namespace' => 'Driver', 'prefix' => 'driver'], function () {
        Route::get('/', function () { echo 'worked!'; });
        Route::post('/register', 'AuthController@register');
        Route::post('/verify', 'AuthController@verify');
        Route::post('/forgot-password', 'AuthController@forgotPassword');
        Route::post('/reset-password', 'AuthController@resetPassword');
        Route::post('/login', 'AuthController@login');
        Route::group(['middleware' => ['auth:sanctum']], function() {
            Route::get('/me', 'AuthController@me');
            Route::post('/change-password', 'AuthController@changePassword');
            Route::post('/logout', 'AuthController@logout');
            Route::post('/twofa', 'AuthController@twofa');
            Route::post('/twofa-resend', 'AuthController@twofaResend');

            Route::apiResource('/profile', 'ProfileController');

            Route::apiResource('/trips/fixed-price', 'FixedPriceTripController');
            Route::apiResource('/trips/bidding-trip', 'BiddingTripController');
            Route::apiResource('/trips/bids', 'BidController');
            Route::get('/driver-trips/get-running', 'DriverTripController@getRunningTrip');
            Route::post('/driver-trips/update-status/{booking_id}', 'DriverTripController@changeBookingStatus');
            Route::apiResource('/driver-trips', 'DriverTripController');
        });
    });
    // Drivers Routes End


    // User Routes Begin
    Route::group(['namespace' => 'User', 'prefix' => 'user', 'as' => 'api.user.'], function () {
        Route::get('/booking-confirmation', 'BookingController@bookingConfirmation');
        Route::get('/pay-now', 'SslCommerzPaymentController@payNow');
        Route::group(['middleware' => ['auth:sanctum']], function() {
            Route::apiResource('/bookings', 'BookingController');
            Route::post('/trips/update-status/{id}', 'CustomerTripController@changeBookingStatus');
            Route::apiResource('/trips', 'CustomerTripController');
            Route::get('/bidding-trips/show-bids/{id}', 'BiddingTripController@showBids');
            Route::apiResource('/bidding-trips', 'BiddingTripController')->only('index', 'show', 'store');
        });

        Route::get('/transaction-success', 'PaymentMessageController@success')->name('transaction.success');
        Route::get('/transaction-failed', 'PaymentMessageController@failed')->name('transaction.failed');

        // SSLCOMMERZ Start
        Route::post('/pay', 'SslCommerzPaymentController@index');

        Route::post('/success', 'SslCommerzPaymentController@success');
        Route::post('/fail', 'SslCommerzPaymentController@fail');
        Route::post('/cancel', 'SslCommerzPaymentController@cancel');

        Route::post('/ipn', 'SslCommerzPaymentController@ipn');
        //SSLCOMMERZ END
    });
    // User Routes End
});

Route::get('/pusher-test', function (Request $request) {
//    \App\Events\TestMessage::broadcast('Yeah Success!');
//    $trip = \App\Models\BiddingTrip::first();
//    dd($trip->toArray());
    \App\Models\BiddingTrip::create([
      "customer_type" => 2,
      "customer_id" => 2,
      "vehicle_type_id" => 1,
      "service_category_id" => 1,
      "service_type_id" => 10,
      "airport_id" => null,
      "pick_up_city" => 519,
      "pick_up_location" => "Adabar, Dhaka",
      "pick_up_point" => null,
      "drop_off_city" => 453,
      "drop_off_location" => "Mirpur, Dhaka",
      "drop_off_point" => null,
      "pick_up_date_time" => "2020-10-16 17:58:34",
      "drop_off_date_time" => "2020-10-18 07:58:34",
      "price" => "0.00",
      "status" => 1,
    ]);
});
