<?php


namespace App\Channels;


use App\Services\SendSMSService;
use Illuminate\Notifications\Notification;

class SmsChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toSms($notifiable);

        (new SendSMSService($notifiable->contact, $message))->sendSimpleMessage();

        // Send notification to the $notifiable instance...
    }
}
