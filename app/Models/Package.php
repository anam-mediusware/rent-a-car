<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'vehicle_type_id', 'from_location', 'to_location', 'day_trip_status', 'day_count', 'night_count', 'trip_start_date', 'trip_end_date', 'package_start_date', 'package_end_date', 'package_description', 'package_title', 'package_seo_title', 'package_seo_keywords', 'package_seo_description', 'banner_image','status', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function ServieSetting() {
        return $this->hasOne('App\Models\ServiceSetting', 'service_id', 'id');
    }

    public function ServicePricing() {
        return $this->hasOne('App\Models\ServicePricing', 'service_id', 'id')->with('additionalPricings');
    }

    public function VehicleType() {
        return $this->hasOne('App\Models\VehicleType', 'id', 'vehicle_type_id');
    }

    public function PackageImageVideo() {
        return $this->hasMany('App\Models\PackageImageVideo', 'package_id', 'id');
    }
}
