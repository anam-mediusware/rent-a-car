<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tourism extends Model
{
    use HasFactory;

    protected $fillable = [
        'tourism_type', 'tourism_name', 'tourism_slug', 'tourism_thumb', 'tourism_image', 'tourism_details', 'sorting', 'route_title', 'route_keyword', 'route_description', 'status', 'created_by', 'updated_by',
    ];
}
