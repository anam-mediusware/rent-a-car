<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
        'category_id', 'expense_amount', 'expense_date', 'expense_remark', 'created_by', 'updated_by',
    ];
}
