<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BiddingTrip extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    public function user() {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function vehicleType() {
        return $this->hasOne(VehicleType::class, 'id', 'vehicle_type_id');
    }

    public function serviceCategory(){
        return $this->hasOne(ServiceCategory::class, 'id', 'service_category_id');
    }

    public function service(){
        return $this->hasOne(Service::class, 'id', 'service_type_id');
    }

    public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    public function upazilaBoundaryFrom()
    {
        return $this->belongsTo(UpazilaBoundary::class, 'pick_up_city', 'id');
    }

    public function upazilaBoundaryTo()
    {
        return $this->belongsTo(UpazilaBoundary::class, 'drop_off_city', 'id');
    }

    public function airport()
    {
        return $this->belongsTo(Airport::class, 'airport_id', 'id');
    }
}
