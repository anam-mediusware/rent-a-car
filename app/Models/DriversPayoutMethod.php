<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriversPayoutMethod extends Model
{
    protected $fillable = [
        'method_name_english', 'method_name_bangla', 'fields_english', 'fields_bangla', 'status', 'created_at', 'updated_at'
    ];
}
