<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Merchant extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name', 'second_contact_name', 'second_contact_mobile_number', 'company_name', 'address', 'thana', 'district', 'contact', 'phone_number', 'email', 'photo_id', 'email_cc', 'email_verified_at', 'password', 'remember_token', 'account_name', 'account_number', 'bank_name', 'branch_name', 'routing_number', 'bkash_number', 'rocket_number', 'password', 'status', 'created_by', 'updated_by', ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
