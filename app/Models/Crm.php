<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crm extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_number', 'customer_name', 'customer_email', 'customer_address', 'ticket_number', 'customer_query', 'travel_datetime', 'vehicle_type', 'feedback', 'status', 'created_by', 'updated_by',
    ];
}
