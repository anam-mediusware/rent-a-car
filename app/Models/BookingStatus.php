<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookingStatus extends Model
{
    protected $table = 'booking_status';
    protected $fillable = [
        'booking_id', '	booking_status', 'created_at', 'updated_at'
    ];
}
