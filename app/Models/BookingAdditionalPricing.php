<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingAdditionalPricing extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'booking_id', 'price_name', 'price', 'price_type', 'status', 'created_at', 'updated_at'
    ];
}
