<?php

namespace App\Models;

use App\Models\Traits\HasActiveStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    use HasFactory, HasActiveStatus;

    protected $guarded = ['id'];

    protected $casts = [
        'geo' => 'json',
    ];
}
