<?php

namespace App\Models;

use App\Models\Traits\HasActiveStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceSetting extends Model
{
    use HasFactory, SoftDeletes, HasActiveStatus;

    private $service_quality = [
        1 => 'Economy',
        2 => 'Premium',
    ];

    protected $fillable = [
        'service_id', 'vehicle_type_id', 'pricing_type', 'service_quality_type', 'price', 'decoration_cost', 'starting_point', 'destination_point', 'airport_id', 'status', 'created_at', 'updated_at'
    ];

    protected $appends = [
        'service_quality_type_title'
    ];

    public function getServiceQualityTypeTitleAttribute()
    {
        return $this->service_quality[$this->service_quality_type];
    }

    public function Service() {
        return $this->hasOne('App\Models\Service', 'id', 'service_id')->OrderBy('id','desc')->with('ServiceCategory','additionalPricings');
    }

    public function VehicleType() {
        return $this->hasOne('App\Models\VehicleType', 'id', 'vehicle_type_id');
    }

    public function ServicePricing() {
        return $this->hasOne('App\Models\ServicePricing', 'id', 'service_id')->with('additionalPricings');
    }

    public function additionalPricings()
    {
        return $this->hasMany(AdditionalPricing::class);
    }

    public function upazilaBoundaryFrom()
    {
        return $this->belongsTo(UpazilaBoundary::class, 'starting_point', 'id');
    }

    public function upazilaBoundaryTo()
    {
        return $this->belongsTo(UpazilaBoundary::class, 'destination_point', 'id');
    }

    public function airport()
    {
        return $this->belongsTo(Airport::class, 'airport_id', 'id');
    }
}
