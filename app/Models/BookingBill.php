<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingBill extends Model
{
    use HasFactory;

    protected $fillable = [
        'booking_id', 'bill_amount', 'customer_advance', 'cash_on_delivery', 'driver_rate', 'admin_commission', 'bill_remark', 'driver_receivable_amount', 'driver_receive_amount', 'driver_receive_due', 'driver_receive_by', 'driver_receive_remark', 'driver_payable_amount', 'driver_paid_amount', 'driver_paid_due', 'driver_paid_by', 'driver_paid_remark', 'user_receivable_amount', 'user_receive_amount', 'user_receive_due', 'user_receive_by', 'user_receive_remark', 'created_by',
    ];
}
