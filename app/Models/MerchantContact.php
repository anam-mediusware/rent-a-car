<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MerchantContact extends Model
{
    protected $fillable = ['partner_id', 'receiver_type', 'sender_type', 'message', 'attachment1', 'attachment2', 'attachment3', 'status', ];
}
