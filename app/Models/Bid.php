<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function biddingTrip()
    {
        return $this->belongsTo(BiddingTrip::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function driverWithRatings()
    {
        return $this->belongsTo(DriverWithRating::class, 'driver_id', 'id');
    }
}
