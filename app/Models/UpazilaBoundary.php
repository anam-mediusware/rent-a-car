<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UpazilaBoundary extends Model
{
    use HasFactory;

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('mySelect', function ($builder) {
            $builder->addSelect(['id', 'geometry_name', 'Dist_ID', 'Dist_name', 'Upz_ID', 'Upaz_name', 'Div_ID', 'Divi_name', 'Upz_UID', 'Area']);
        });
    }

    protected $guarded = ['id'];
}
