<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Booking extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($booking) {
            if ($booking->getOriginal('booking_status') && $booking->isDirty('booking_status')) {
                BookingStatusChangeLog::create([
                    'booking_id' => $booking->id,
                    'loggable_type' => get_class(auth()->user()),
                    'loggable_id' => auth()->id(),
                    'old_status' => $booking->getOriginal('booking_status'),
                    'new_status' => $booking->booking_status,
                ]);
            }
        });

        static::creating(function ($booking) {
            $booking->book_number = Booking::bookingNo();
        });
    }

    public static function bookingNo()
    {
        $data = Booking::select('book_number')
            ->where(DB::raw("YEAR(created_at)"), date('Y'))
            ->where(DB::raw("MONTH(created_at)"), date('m'))
            ->orderBy('book_number', 'DESC')
            ->first();

        $lastPart = (isset($data))?intval(substr($data->book_number, 5)):0;

        $number = '1';
        $number .= date('ym');
        $number .= substr("0000", 0, -strlen($lastPart+1));
        $number .= $lastPart+1;
        return $number;
    }

    public function serviceSetting()
    {
        return $this->belongsTo(ServiceSetting::class);
    }

    public function bid()
    {
        return $this->belongsTo(Bid::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function vehicleType()
    {
        return $this->hasOne(VehicleType::class, 'id', 'vehicle_type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function serviceCategory()
    {
        return $this->hasOne(ServiceCategory::class, 'id', 'service_category_id');
    }

    public function service()
    {
        return $this->hasOne(Service::class, 'id', 'service_type_id');
    }

    public function additionalPricings()
    {
        return $this->hasMany(BookingAdditionalPricing::class);
    }

    public function upazilaBoundaryFrom()
    {
        return $this->belongsTo(UpazilaBoundary::class, 'pick_up_city', 'id');
    }

    public function upazilaBoundaryTo()
    {
        return $this->belongsTo(UpazilaBoundary::class, 'drop_off_city', 'id');
    }

    public function airport()
    {
        return $this->belongsTo(Airport::class, 'airport_id', 'id');
    }

    public function rating()
    {
        return $this->hasOne(DriverRating::class, 'booking_id', 'id');
    }
}
