<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriversDocument extends Model
{
    protected $fillable = [
        'drivers_id', 'document_type', 'file_name', 'expiry_date', 'verification_status', 'status', 'created_at', 'updated_at'
    ];
}
