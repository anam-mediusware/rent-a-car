<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingInvoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'booking_id', 'invoice_number', 'invoice_date', 'rental_amount', 'rental_remark', 'extra_uses_cost', 'extra_uses_remark', 'driver_food_cost', 'driver_food_remark', 'driver_overtime_cost', 'driver_overtime_remark', 'fuel_cost', 'fuel_remark', 'toll_cost', 'toll_remark', 'other_cost', 'other_remark', 'vat_cost', 'vat_remark', 'invoice_amount',
    ];
}
