<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    use HasFactory;

    protected $fillable = [
        'route_name', 'route_title', 'route_keyword', 'route_description', 'created_by', 'updated_by',
    ];
}
