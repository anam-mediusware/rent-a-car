<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $fillable = [
        'news_heading', 'news_slug', 'news_image', 'news_date', 'news_details', 'status', 'created_by', 'updated_by',
    ];
}
