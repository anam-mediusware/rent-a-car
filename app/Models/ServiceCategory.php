<?php

namespace App\Models;

use App\Models\Traits\HasActiveStatus;
use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    use HasActiveStatus;

    protected $fillable = [
        'serial_no', 'category_name', 'image', 'status', 'created_at', 'updated_at'
    ];

    protected $appends = ['image_url'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('serialized', function ($model) {
            return $model->orderBy('serial_no');
        });
    }

    public function service() {
        return $this->hasMany(Service::class, 'category_id', 'id')->with('ServiceSetting');
    }

    public function getImageUrlAttribute()
    {
        return '/web-assets/img/service-category/' . $this->image;
    }
}
