<?php

namespace App\Models;

use App\Models\Traits\HasActiveStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cars extends Model
{
    use SoftDeletes;
    use HasActiveStatus;

    protected $fillable = [
        'drivers_id', 'merchants_id', 'metro', 'alphabetical_serial', 'serial_number', 'vehicle_type', 'car_brand', 'car_model_id', 'model', 'year', 'owner_name', 'owner_address', 'owner_mobile_number', 'owner_email', 'status', 'created_by', 'updated_by',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($driver) {
            $driver->merchants_id = $driver->merchants_id == '0' ? null : $driver->merchants_id;
        });
    }

    /**
     * Scope a query to only include active cars.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', '1');
    }

    public function driver()
    {
        return $this->hasOne('App\Models\Driver', 'id', 'drivers_id');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchants_id', 'id');
    }

    public function cars_image()
    {
        return $this->hasOne('App\Models\CarsImage', 'id', 'cars_id');
    }
}
