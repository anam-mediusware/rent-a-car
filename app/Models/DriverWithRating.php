<?php

namespace App\Models;

class DriverWithRating extends Driver
{
    protected $table = 'drivers';

    protected $with = ['ratings'];

    protected $appends = [
        'overall_rating',
        'car_condition_rating',
        'driver_behavior_rating',
        'driver_punctuality_rating',
    ];
}
