<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Management extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'image', 'designation', 'details', 'status', 'created_by', 'updated_by',
    ];

    protected $appends = ['avatar'];

    public function getAvatarAttribute()
    {
        return Storage::url('public/managements/' . $this->image);
    }
}
