<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackageImageVideo extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'package_id', 'data_type', 'image', 'video_link', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];
}
