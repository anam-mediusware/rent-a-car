<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingStatusChangeLog extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * Get the owning loggable model.
     */
    public function loggable()
    {
        return $this->morphTo();
    }

    public function bookings()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'id');
    }
}
