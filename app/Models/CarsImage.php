<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarsImage extends Model
{
    protected $fillable = [
        'cars_id', 'type', 'image', 'status', 'created_at', 'updated_at'
    ];
}
