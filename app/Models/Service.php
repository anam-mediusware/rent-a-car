<?php

namespace App\Models;

use App\Models\Traits\HasActiveStatus;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasActiveStatus;

    protected $fillable = [
        'category_id', 'service_name', 'status', 'created_at', 'updated_at'
    ];

    public function serviceSetting() {
        return $this->hasOne(ServiceSetting::class, 'service_id', 'id' )->OrderBy('id','desc');
    }

    public function serviceCategory(){
        return $this->hasOne(ServiceCategory::class, 'id', 'category_id');
    }

    public function additionalPricings(){
        return $this->hasMany(AdditionalPricing::class, 'service_pricing_id', 'id');
    }
}
