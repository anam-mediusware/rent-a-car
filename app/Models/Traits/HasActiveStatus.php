<?php


namespace App\Models\Traits;


use App\Scopes\ActiveStatusScope;

trait HasActiveStatus
{
    /**
     * Boot the has active status trait for a model.
     *
     * @return void
     */
    public static function bootHasActiveStatus()
    {
        static::addGlobalScope(new ActiveStatusScope());
    }
}
