<?php


namespace App\Models\Traits;


use Biscolab\GoogleMaps\Api\Geocoding;
use Biscolab\GoogleMaps\Enum\GoogleMapsApiConfigFields;
use Biscolab\GoogleMaps\Fields\LatLngFields;
use Biscolab\GoogleMaps\Object\LatLng;

trait GeoCodingAttributes
{
    protected static $geocoding;

    /**
     * Boot the geo coding attributes trait for a model.
     *
     * @return void
     */
    public static function bootGeoCodingAttributes()
    {
        static::$geocoding = new Geocoding([
            GoogleMapsApiConfigFields::KEY => env('GOOGLE_API_KEY')
        ]);
    }

    /**
     * Initialize the geo coding attributes trait for an instance.
     *
     * @return void
     */
    public function initializeGeoCodingAttributes()
    {
        //
    }

    private function getFormattedAddress($lat, $lng)
    {
        try {
            return self::$geocoding->getByLatLng(new LatLng([
                LatLngFields::LAT => $lat,
                LatLngFields::LNG => $lng,
            ]))->first()->getFormattedAddress();
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getFromFormattedAddressAttribute()
    {
        $geo = json_decode($this->pick_up_point);
        if ($geo) {
            return $this->getFormattedAddress($geo->lat, $geo->lon);
        }
        return null;
    }

    public function getToFormattedAddressAttribute()
    {
        $geo = json_decode($this->drop_off_point);
        if ($geo) {
            return $this->getFormattedAddress($geo->lat, $geo->lon);
        }
        return null;
    }
}
