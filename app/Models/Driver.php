<?php

namespace App\Models;

use App\Models\Traits\HasActiveStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Driver extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, HasActiveStatus;

    protected $fillable = [
        'merchants_id', 'full_name', 'mobile_number', 'emergency_contact_number', 'email_address', 'PIN', 'gender', 'date_of_birth', 'city_id', 'payment_method_id', 'payment_details', 'image', 'status', 'password', 'created_at', 'updated_at'
    ];

    protected $casts = [
        'payment_details' => 'json'
    ];

    protected $hidden = ['password'];

    private function calcRating($num)
    {
        if (count(explode('.', $num)) > 1) {
            list($whole, $decimal) = explode('.', $num);
            $decimalCalculated = 0.00;
            if ($decimal >= 0 && $decimal <= 25) {
                $decimalCalculated = 0.00;
            } elseif ($decimal >= 26 && $decimal <= 75) {
                $decimalCalculated = 0.50;
            } elseif ($decimal >= 76 && $decimal <= 99) {
                $decimalCalculated = 0.50;
            }
            return $whole + $decimalCalculated;
        }
        return $num;
    }

    public function getOverallRatingAttribute()
    {
        $rating = 0;
        if ($this->ratings) {
            $count = $this->ratings->count();
            if ($count) {
                $rating = $this->ratings->sum('overall_rating') / $this->ratings->count();
            } else {
                $rating = 0;
            }
        }
        return $this->calcRating($rating);
    }

    public function getCarConditionRatingAttribute()
    {
        $rating = 0;
        if ($this->ratings) {
            $count = $this->ratings->count();
            if ($count) {
                $rating = $this->ratings->sum('car_condition') / $this->ratings->count();
            } else {
                $rating = 0;
            }
        }
        return $this->calcRating($rating);
    }

    public function getDriverBehaviorRatingAttribute()
    {
        $rating = 0;
        if ($this->ratings) {
            $count = $this->ratings->count();
            if ($count) {
                $rating = $this->ratings->sum('driver_behavior') / $this->ratings->count();
            } else {
                $rating = 0;
            }
        }
        return $this->calcRating($rating);
    }

    public function getDriverPunctualityRatingAttribute()
    {
        $rating = 0;
        if ($this->ratings) {
            $count = $this->ratings->count();
            if ($count) {
                $rating = $this->ratings->sum('driver_punctuality') / $this->ratings->count();
            } else {
                $rating = 0;
            }
        }
        return $this->calcRating($rating);
    }

    public function DrivingCitie()
    {
        return $this->hasOne('App\Models\DrivingCitie', 'id', 'city_id');
    }

    public function DrivingCity()
    {
        return $this->hasOne(UpazilaBoundary::class, 'id', 'city_id');
    }

    public function cars()
    {
        return $this->hasOne(Cars::class, 'drivers_id', 'id');
    }

    public function DriversPayoutMethod()
    {
        return $this->hasOne('App\Models\DriversPayoutMethod', 'id', 'payment_method_id');
    }

    public function bids()
    {
        return $this->hasMany(Bid::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function ratings()
    {
        return $this->hasMany(DriverRating::class, 'driver_id', 'id');
    }

    /**
     * Get all of the driver logs.
     */
    public function bookingStatusLogs()
    {
        return $this->morphMany(BookingStatusChangeLog::class, 'loggable');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchants_id', 'id');
    }
}
