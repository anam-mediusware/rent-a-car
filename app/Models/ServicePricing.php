<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServicePricing extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'service_id', 'price', 'status', 'created_at', 'updated_at'
    ];

    public function additionalPricings(){
        return $this->hasMany(AdditionalPricing::class, 'service_pricing_id', 'id');
    }
}
