<?php

namespace App\Models;

use App\Models\Traits\HasActiveStatus;
use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    use HasActiveStatus;

    protected $fillable = [
        'vehicle_type_english', 'vehicle_type_bangla', 'status', 'created_at', 'updated_at'
    ];

    protected $appends = ['vehicle_image'];

    public function getVehicleImageAttribute()
    {
        $images = [
            '4 Seater' => '/web-assets/img/service/sedan.png',
            '7 Seater' => '/web-assets/img/service/noah.png',
            '10 Seater' => '/web-assets/img/service/hiace.png',
            '14 Seater' => '/web-assets/img/service/hiace.png',
            '21++ Seater' => '/web-assets/img/service/hiace.png',
        ];
        return $images[$this->vehicle_type_english];
    }
}
