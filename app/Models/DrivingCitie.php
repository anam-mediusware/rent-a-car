<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DrivingCitie extends Model
{
    protected $fillable = [
        'city_name_english', 'city_name_bangla', 'status', 'created_at', 'updated_at'
    ];
}
