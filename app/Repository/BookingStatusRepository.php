<?php


namespace App\Repository;


use Illuminate\Support\Collection;

class BookingStatusRepository
{
    private $statuses;

    public function __construct()
    {
        $this->statuses = collect([
            'Pending', // Payment done but booking not accepted by driver / admin
            'Confirmed', // Driver accepted
            // 'Accepted by Customer',
            // 'Accepted by Driver',
            'Cancelled by Customer',
            'Cancelled by Driver',
            'Trip Started',
            'Complete'
        ]);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->statuses;
    }

    public function availableOptions($current_status, $user_type = 'customer')
    {
        $optionsAvailable = [];
        if ($user_type == 'customer') { // Options for Customer
            if ($current_status == 'Pending') {
                $optionsAvailable = [
                    'Cancelled by Customer' => 'Cancel'
                ];
            } else if ($current_status == 'Confirmed') {
                $optionsAvailable = [
                    'Cancelled by Customer' => 'Cancel'
                ];
            } else if ($current_status == 'Accepted by Driver') { // Not used currently
                $optionsAvailable = [
                    'Cancelled by Customer' => 'Cancel',
                    'Accepted by Customer' => 'Accept'
                ];
            } else if ($current_status == 'Accepted by Customer') { // Not used currently
                $optionsAvailable = [
                    'Cancelled by Customer' => 'Cancel'
                ];
            } else if ($current_status == 'Trip Started') {
                $optionsAvailable = [
                    'Complete' => 'Complete'
                ];
            } else if (in_array($current_status, ['Complete', 'Cancelled by Customer', 'Cancelled by Driver'])) {
                $optionsAvailable = [];
            }
        } else if ($user_type == 'driver') { // Options for Driver
            if ($current_status == 'Pending') {
                $optionsAvailable = [
                    'Cancelled by Driver' => 'Cancel',
                    'Confirmed' => 'Confirm',
                ];
            } else if ($current_status == 'Confirmed') {
                $optionsAvailable = [
                    'Cancelled by Driver' => 'Cancel',
                    'Trip Started' => 'Start Trip',
                ];
            } else if ($current_status == 'Accepted by Driver') { // Not used currently
                $optionsAvailable = [
                    'Cancelled by Driver' => 'Cancel'
                ];
            } else if ($current_status == 'Accepted by Customer') { // Not used currently
                $optionsAvailable = [
                    'Cancelled by Driver' => 'Cancel',
                    'Trip Started' => 'Start Trip',
                ];
            } else if ($current_status == 'Trip Started') {
                $optionsAvailable = [];
            } else if (in_array($current_status, ['Complete', 'Cancelled by Customer', 'Cancelled by Driver'])) {
                $optionsAvailable = [];
            }
        }
        return collect($optionsAvailable);
    }
}
