<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\CarTrait;
use App\Http\Controllers\Admin\Traits\DataTrait;
use App\Models\CarModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cars;
use App\Models\CarsDocument;
use App\Models\CarsImage;
use App\Models\Driver;
use App\Models\Merchant;
use App\Models\VehicleType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Image;

class CarsController extends Controller
{
    use DataTrait, CarTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $sql = Cars::with(['driver', 'merchant'])->orderBy('cars.id', 'ASC');

        $input = $request->all();
        if (!empty($input['car'])) {
            $sql->Where('cars.id', $input['car']);
        }

        if (!empty($input['q'])) {
            $sql->Where('cars.model', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('cars.metro', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('cars.alphabetical_serial', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('cars.serial_number', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('cars.vehicle_type', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('cars.car_brand', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('cars.model', 'LIKE', '%' . $input['q'] . '%');
        }

        $perPage = 25;
        $records = $sql->paginate($perPage);
        return view('admin.car.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $merchants = Merchant::where('status', '1')->get();
        $division_district = $this->division_district();
        $alphabetical_serial = $this->alphabetical_serial();
        $car_brands = $this->car_brands();
        $car_models = collect();

        $drivers = Driver::select(['id', 'full_name'])->whereNull('merchants_id')->get();
        $vehicle_types = VehicleType::where('status', '1')->get();
        $create = 1;
        $actionLink = 'admin/cars';

        return view('admin.car.create', compact('create', 'actionLink', 'drivers', 'vehicle_types', 'division_district', 'alphabetical_serial', 'car_brands', 'merchants', 'car_models'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'metro' => 'required',
            'alphabetical' => 'required',
            'serial_number' => 'required',
            'vehicle_type' => 'required',
            'car_brand' => 'required',
            'model' => 'required',
            'year' => 'required|numeric',

            'reg_expiry_date' => 'required',
            'fit_expiry_date' => 'required',
            'tax_expiry_date' => 'required',
            'insurance_expiry_date' => 'required',

            'registration_front' => 'required',
            'registration_back' => 'required',
            'fitness_paper' => 'required',
            'tax_token' => 'required',
            'insurance_paper' => 'required',
            'car_image_outer' => 'required',
            'car_image_inner' => 'required',
        ]);

        $this->createCar($request, $request->driver_id);

        $request->session()->flash('message', 'Car was successful added!');
        return redirect('admin/cars/create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $show = 1;
        $data = Cars::with(['driver', 'merchant'])->find($id);
        $data['car_document'] = $this->car_document($id);
        $data['car_image'] = CarsImage::where('cars_id', $id)->get();
        return view('admin.car.show', compact('show', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $edit = 1;
        $merchants = Merchant::where('status', '1')->get();
        $division_district = $this->division_district();
        $alphabetical_serial = $this->alphabetical_serial();
        $car_brands = $this->car_brands();

        $vehicle_types = VehicleType::where('status', '1')->get();

        $data = Cars::findOrFail($id);
        $merchants_id = $data->merchants_id;
        $drivers = Driver::select(['id', 'full_name'])
            ->when($merchants_id, function ($query, $merchants_id) {
                return $query->where('merchants_id', $merchants_id);
            })->get();
        $data['car_document'] = $this->car_document($id);
        $data['car_image'] = CarsImage::where('cars_id', $id)->get();
        $car_models = CarModel::where(['vehicle_type_id' => $data->vehicle_type, 'car_brand' => $data->car_brand])->get();

        // return $data;
        return view('admin.car.edit', compact('edit', 'data', 'drivers', 'vehicle_types', 'division_district', 'alphabetical_serial', 'car_brands', 'merchants', 'car_models'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'metro' => 'required',
            'alphabetical' => 'required',
            'serial_number' => 'required',
            'vehicle_type' => 'required',
            'car_brand' => 'required',
            'model' => 'required',

            'reg_expiry_date' => 'required',
            'fit_expiry_date' => 'required',
            'tax_expiry_date' => 'required',
            'insurance_expiry_date' => 'required',
        ]);

        $input = $request->all();
        $carModel = CarModel::where('car_model', $request->model)->first();
        $updateData = [
            'drivers_id' => $input['driver_id'] != "" ? $input['driver_id'] : 0,
            'merchants_id' => $input['merchant_id'] != "" ? $input['merchant_id'] : 0,
            'metro' => $input['metro'],
            'alphabetical_serial' => $input['alphabetical'],
            'serial_number' => $input['serial_number'],
            'vehicle_type' => $input['vehicle_type'],
            'car_brand' => $input['car_brand'],
            'car_model_id' => $carModel ? $carModel->id : null,
            'model' => $input['model'],
            'owner_name' => $input['owner_name'],
            'owner_address' => $input['owner_address'],
            'owner_mobile_number' => $input['owner_mobile_number'],
            'owner_email' => $input['owner_email'],
        ];

        $data = Cars::find($id);
        $data->update($updateData);

        if ($input['reg_expiry_date'] != "") :
            $reg_expiry_date = $input['reg_expiry_date'];
            $reg_year = substr($reg_expiry_date, 6, 4);
            $reg_month = substr($reg_expiry_date, 3, 2);
            $reg_day = substr($reg_expiry_date, 0, 2);
            $reg_full_date = $reg_year . '-' . $reg_month . '-' . $reg_day;
        endif;

        if ($request->hasFile('registration_front') != '') {

            $prevImage = CarsDocument::find($input['reg_front_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/cars/registration/' . $prevImage->file_name)) {
                    File::delete('storage/cars/registration/' . $prevImage->file_name);
                }
            }

            $image = $request->file('registration_front');
            $fileName = 'reg-front-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point

            $dir = public_path('storage/app/public/cars/registration/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('cars/registration/' . $fileName, $img);

            if ($input['reg_front_id'] > 0) :
                CarsDocument::where('id', $input['reg_front_id'])->update(['file_name' => $fileName, 'expiry_date' => $reg_full_date]);
            else :
                $insert_rg_front = [
                    'cars_id' => $id,
                    'document_type' => 'registration_front',
                    'expiry_date' => $reg_full_date,
                    'file_name' => $fileName,
                    'updated_at' => date('Y-m-d h:i:s')
                ];
                CarsDocument::create($insert_rg_front);
            endif;
        }

        if (isset($input['reg_front_id']) && $input['reg_front_id'] > 0) :
            CarsDocument::where('id', $input['reg_front_id'])->update(['expiry_date' => $reg_full_date]);
        endif;

        if ($request->hasFile('registration_back') != '') {

            $prevImage = CarsDocument::find($input['reg_back_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/cars/registration/' . $prevImage->file_name)) {
                    File::delete('storage/cars/registration/' . $prevImage->file_name);
                }
            }

            $image = $request->file('registration_back');
            $fileName = 'reg-back-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point

            $dir = public_path('storage/app/public/cars/registration/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('cars/registration/' . $fileName, $img);

            if ($input['reg_back_id'] > 0) :
                CarsDocument::where('id', $input['reg_back_id'])->update(['file_name' => $fileName, 'expiry_date' => $reg_full_date]);
            else :
                $insert_reg_back = [
                    'cars_id' => $id,
                    'document_type' => 'registration_back',
                    'expiry_date' => $reg_full_date,
                    'file_name' => $fileName,
                ];
                CarsDocument::create($insert_reg_back);
            endif;
        }

        if (isset($input['fit_expiry_date']) && $input['fit_expiry_date'] != "") :
            $fit_expiry_date = $input['fit_expiry_date'];
            $fit_year = substr($fit_expiry_date, 6, 4);
            $fit_month = substr($fit_expiry_date, 3, 2);
            $fit_day = substr($fit_expiry_date, 0, 2);
            $fit_full_date = $fit_year . '-' . $fit_month . '-' . $fit_day;
        endif;

        if ($request->hasFile('fitness_paper') != '') {

            $prevImage = CarsDocument::find($input['fitness_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/cars/fitness/' . $prevImage->file_name)) {
                    File::delete('storage/cars/fitness/' . $prevImage->file_name);
                }
            }

            $image = $request->file('fitness_paper');
            $fileName = 'fit-paper-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/cars/fitness/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('cars/fitness/' . $fileName, $img);

            if ($input['fitness_id'] > 0) :
                CarsDocument::where('id', $input['fitness_id'])->update(['file_name' => $fileName, 'expiry_date' => $fit_full_date]);
            else :

                $insert_fitness = [
                    'cars_id' => $id,
                    'document_type' => 'fitness_paper',
                    'expiry_date' => $fit_full_date,
                    'file_name' => $fileName,
                ];
                CarsDocument::create($insert_fitness);
            endif;
        }

        if (isset($input['fitness_id']) && $input['fitness_id'] > 0) :
            CarsDocument::where('id', $input['fitness_id'])->update(['expiry_date' => $fit_full_date]);
        endif;

        if ($input['tax_expiry_date'] != "") :
            $tax_expiry_date = $input['tax_expiry_date'];
            $tax_year = substr($tax_expiry_date, 6, 4);
            $tax_month = substr($tax_expiry_date, 3, 2);
            $tax_day = substr($tax_expiry_date, 0, 2);
            $tax_full_date = $tax_year . '-' . $tax_month . '-' . $tax_day;
        endif;

        if ($request->hasFile('tax_token') != '') {

            $prevImage = CarsDocument::find($input['tax_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/cars/tax/' . $prevImage->file_name)) {
                    File::delete('storage/cars/tax/' . $prevImage->file_name);
                }
            }

            $image = $request->file('tax_token');
            $fileName = 'tax-token-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/cars/tax/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('cars/tax/' . $fileName, $img);

            if ($input['tax_id'] > 0) :
                CarsDocument::where('id', $input['tax_id'])->update(['file_name' => $fileName, 'expiry_date' => $tax_full_date]);
            else :
                $insert_tax = [
                    'cars_id' => $id,
                    'document_type' => 'tax_token',
                    'expiry_date' => $tax_full_date,
                    'file_name' => $fileName,
                ];
                CarsDocument::create($insert_tax);
            endif;
        }

        if (isset($input['tax_id']) && $input['tax_id'] > 0) :
            CarsDocument::where('id', $input['tax_id'])->update(['expiry_date' => $tax_full_date]);
        endif;

        if ($input['insurance_expiry_date'] != "") :
            $insurance_expiry_date = $input['insurance_expiry_date'];
            $ins_year = substr($insurance_expiry_date, 6, 4);
            $ins_month = substr($insurance_expiry_date, 3, 2);
            $ins_day = substr($insurance_expiry_date, 0, 2);
            $ins_full_date = $ins_year . '-' . $ins_month . '-' . $ins_day;
        endif;

        if ($request->hasFile('insurance_paper') != '') {

            $prevImage = CarsDocument::find($input['insurance_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/cars/insurance/' . $prevImage->file_name)) {
                    File::delete('storage/cars/insurance/' . $prevImage->file_name);
                }
            }

            $image = $request->file('insurance_paper');
            $fileName = 'insurance-paper-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/cars/insurance/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('cars/insurance/' . $fileName, $img);

            if ($input['insurance_id'] > 0) :
                CarsDocument::where('id', $input['insurance_id'])->update(['file_name' => $fileName, 'expiry_date' => $ins_full_date]);
            else :
                $insert_insurance = [
                    'cars_id' => $id,
                    'document_type' => 'insurance_paper',
                    'expiry_date' => $ins_full_date,
                    'file_name' => $fileName,
                ];
                CarsDocument::create($insert_insurance);
            endif;
        }

        if (isset($input['insurance_id']) && $input['insurance_id'] > 0) :
            CarsDocument::where('id', $input['insurance_id'])->update(['expiry_date' => $ins_full_date]);
        endif;

        if ($request->hasFile('car_image_outer')) {
            $prevImage = CarsImage::where('cars_id', $id)->where('type', 'outer');
            foreach ($prevImage->get() as $img) {
                if (File::exists('storage/cars/car-img/' . $img->image)) {
                    File::delete('storage/cars/car-img/' . $img->image);
                }
            }
            if ($prevImage) $prevImage->delete();

            foreach ($request->file('car_image_outer') as $key => $image) {
                $fileName = 'car-image-' . Auth::user()->id . time() . '-outer-' . $key . '.' . $image->getClientOriginalExtension();

                $img = image::make($image->getRealPath());
                $img->resize(324, 204, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->stream(); // <-- Key point

                Storage::disk('public')->put('cars/car-img/' . $fileName, $img);

                $car_img = [
                    'cars_id' => $id,
                    'type' => 'outer',
                    'image' => $fileName
                ];
                CarsImage::create($car_img);
            }
        }

        if ($request->hasFile('car_image_inner')) {
            $prevImage = CarsImage::where('cars_id', $id)->where('type', 'outer');
            foreach ($prevImage->get() as $img) {
                if (File::exists('storage/cars/car-img/' . $img->image)) {
                    File::delete('storage/cars/car-img/' . $img->image);
                }
            }
            if ($prevImage) $prevImage->delete();

            foreach ($request->file('car_image_inner') as $key => $image) {
                $fileName = 'car-image-' . Auth::user()->id . time() . '-inner-' . $key . '.' . $image->getClientOriginalExtension();

                $img = image::make($image->getRealPath());
                $img->resize(324, 204, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->stream(); // <-- Key point

                Storage::disk('public')->put('cars/car-img/' . $fileName, $img);

                $car_img = [
                    'cars_id' => $id,
                    'type' => 'inner',
                    'image' => $fileName
                ];
                CarsImage::create($car_img);
            }
        }

        $request->session()->flash('message', 'Car was successful updated!');
        return redirect('admin/cars/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        Cars::find($id)->delete();
        $request->session()->flash('message', 'Car was successful deleted!');
        return redirect('admin/cars');
    }

    static function driver_name($id)
    {
        $driver = Driver::find($id);
        return $driver->full_name;
    }

    public function availableCars(Request $request)
    {
        $vehicle_type_id = $request->input('vehicle_type_id');
        $car_brand = $request->input('car_brand');
        $data = CarModel::when($vehicle_type_id, function ($query, $vehicle_type_id) {
                return $query->where('vehicle_type_id', $vehicle_type_id);
            })->when($car_brand, function ($query, $car_brand) {
                return $query->where('car_brand', $car_brand);
            })->get();
        if ($data) {
            return response()->json(['success' => true, 'data' => $data]);
        }
        return response()->json(['success' => false, 'data' => $data, 'error' => 'Data Not Found']);
    }
}
