<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;

class NewsController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = News::orderBy('news_heading');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('news_heading', 'LIKE', $input['q'].'%')
                ->orWhere('news_details', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.news', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $create = 1;
        $actionLink = 'admin/news';
        return view('admin.news', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'news_heading' => 'required',
            'news_date' => 'required',
            'news_details' => 'required',
            'status' => 'required|numeric',
            'news_image' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $fileName = '';
        if ($request->hasFile('news_image')) {
            $image = $request->file('news_image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('news/' . $fileName, $img);
        }

        $input = $request->all();
        $insert = News::create([
            'news_heading' => $input['news_heading'],
            'news_slug' => urlSlug($input['news_heading']),
            'news_date' => $input['news_date'],
            'news_details' => $input['news_details'],
            'status' => $input['status'],
            'news_image' => $fileName,
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'News was successful added!');
        return redirect('admin/news/create');
    }

    public function show($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $show = 1;
        $data = News::find($id);

        return view('admin.news', compact('show', 'data'));
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $edit = 1;
        $actionLink = 'admin/news/'.$id;
        $data = News::find($id);
        return view('admin.news', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'news_heading' => 'required',
            'news_date' => 'required',
            'news_details' => 'required',
            'status' => 'required|numeric',
            'news_image' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $input = $request->all();
        $updateData = [
            'news_heading' => $input['news_heading'],
            'news_slug' => urlSlug($input['news_heading']),
            'news_date' => $input['news_date'],
            'news_details' => $input['news_details'],
            'status' => $input['status'],
            'updated_by' => Auth::user()->id,
        ];

        $data = News::find($id);
        if ($request->hasFile('news_image')) {
            deleteStorageFile('news', $data->news_image);

            $image = $request->file('news_image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('news/' . $fileName, $img);

            $updateData['news_image'] = $fileName;
        }
        $data->update($updateData);

        $request->session()->flash('message', 'News was successful updated!');
        return redirect('admin/news/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = News::find($id);
        $data->delete();
        deleteStorageFile('news', $data['news_image']);

        $request->session()->flash('message', 'News was successful deleted!');
        return redirect('admin/news');
    }
}
