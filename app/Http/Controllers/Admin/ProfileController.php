<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $show = 1;
        $id = Auth::id();
        $data = Admin::find($id);

        return view('admin.profile', compact('show', 'data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $show = 1;
        $id = $id ? $id : Auth::id();
        $data = Admin::findOrFail($id);

        return view('admin.profile', compact('show', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id = null)
    {
        $edit = 1;
        $id = $id ? $id : Auth::id();
        $data = Admin::find($id);

        return view('admin.profile', compact('edit', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id = null)
    {
        $id = $id ? $id : Auth::id();
        $input = $request->all();
        $this->validate($request, [
            'name' => 'required|max:100',
            'email' => 'required|email|max:100|unique:users,email,'.$id.',id',
        ]);

        $formData = [
            'name' => $input['name'],
            'email' => $input['email'],
        ];

        $request->session()->flash('message', $input['name']."'s account was successful updated!");
        $data = Admin::find($id);
        $data->update($formData);
        return redirect('admin/profile');
    }

    public function password()
    {
        $password = 1;
        $id = Auth::id();
        $data = Admin::find($id);

        return view('admin.profile', compact('password', 'data'));
    }

    public function updatePassword(Request $request)
    {
        $id = Auth::id();
        $input = $request->all();
        $this->validate($request, [
            'old_password'     => 'required',
            'new_password'     => 'required|max:20|min:6',
            'confirm_password' => 'required|same:new_password',
        ]);
        $userData = Admin::find($id);

        if (!Hash::check($input['old_password'], $userData->password)) {
            return back()->with('error', 'The specified password does not match the database password');
        } else {
            $formData = [
                'password' => bcrypt($input['new_password']),
            ];

            $request->session()->flash('message', $userData->name."'s password was successful updated!");
            $data = Admin::find($id);
            $data->update($formData);
            return redirect('admin/profile');
        }
    }
}
