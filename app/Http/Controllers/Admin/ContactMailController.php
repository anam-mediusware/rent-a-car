<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ContactMail;

class ContactMailController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = ContactMail::orderBy('created_at', 'DESC');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->where('name', 'LIKE', $input['q'].'%')
            ->orWhere('email', 'LIKE', $input['q'].'%')
            ->orWhere('phone_no', 'LIKE', $input['q'].'%')
            ->orWhere('support_type', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.contact-mail', compact('lists', 'serial', 'records'));
    }

    public function show($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $show = 1;
        $data = ContactMail::find($id);

        return view('admin.contact-mail', compact('show', 'data'));
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = ContactMail::find($id);
        $data->delete();

        //ContactMail::destroy($id);
        $request->session()->flash('message', 'Contact Mail was successful deleted!');
        return redirect('admin/contact-mail');
    }
}
