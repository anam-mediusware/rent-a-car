<?php

namespace App\Http\Controllers\Admin;

use App\Mail\MerchantRegistration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Merchant;
use App\Models\MerchantContact;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MerchantController extends Controller
{
    public function index(Request $request)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $sql = Merchant::orderBy('id', 'DESC');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('name', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('contact', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('email', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('address', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('district', 'LIKE', '%' . $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.merchant.index', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $disArray = [
            "Barisal" => ["Barguna", "Barisal", "Bhola", "Jhalokati", "Patuakhali", "Pirojpur"],
            "Chittagong" => ["Bandarban", "Brahmanbaria", "Chandpur", "Chittagong", "Comilla", "Cox's Bazar", "Feni", "Khagrachhari", "Lakshmipur", "Noakhali", "Rangamati"],
            "Dhaka" => ["Dhaka", "Faridpur", "Gazipur", "Gopalganj", "Kishoreganj", "Madaripur", "Manikganj", "Munshiganj", "Narayanganj", "Narsingdi", "Rajbari", "Shariatpur", "Tangail"],
            "Khulna" => ["Bagerhat", "Chuadanga", "Jessore", "Jhenaidah", "Khulna", "Kushtia", "Magura", "Meherpur", "Narail", "Satkhira"],
            "Mymensingh" => ["Jamalpur", "Mymensingh", "Netrakona", "Sherpur"],
            "Rajshahi" => ["Bogra", "Chapainawabganj", "Joypurhat", "Naogaon", "Natore", "Pabna", "Rajshahi", "Sirajganj"],
            "Rangpur" => ["Dinajpur", "Gaibandha", "Kurigram", "Lalmonirhat", "Nilphamari", "Panchagarh", "Rangpur", "Thakurgaon"],
            "Sylhet" => ["Habiganj", "Moulvibazar", "Sunamganj", "Sylhet"]
        ];

        $create = 1;
        $actionLink = 'admin/merchant';
        return view('admin.merchant.create', compact('create', 'actionLink', 'disArray'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'contact' => 'required|string|max:255|unique:merchants,contact',
            'email' => 'nullable|string|email|max:255|unique:merchants',
        ], [
            'contact.unique' => 'The mobile number has already been taken.'
        ]);

        $input = $request->all();
        $password = Str::upper(Str::random(8));
        $insertData = [
            'name' => $input['name'],
            'contact' => $input['contact'],
            'email' => $input['email'],
            'second_contact_name' => $input['second_contact_name'],
            'second_contact_mobile_number' => $input['second_contact_mobile_number'],
            'address' => $input['address'],
            'district' => $input['district'],
            'password' => Hash::make($password),
        ];

        if ($request->hasFile('photo_id') != '') {
            $image = $request->file('photo_id');
            $fileName = 'merchant-nid-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::put('public/merchants/id/' . $fileName, $img);
            $insertData['photo_id'] = $fileName;
        }

        $insert = Merchant::create($insertData);
        if ($insert && $insertData['email']) {
            // Send Email to Merchant
            Mail::to($insertData['email'])
                ->send(new MerchantRegistration(
                    new Merchant(array_merge($insertData, ['password' => $password]))
                ));
        } else if ($insert && $insertData['contact']) {
            // Send SMS to Merchant
            Notification::send($insert, new \App\Notifications\MerchantRegistration(
                new Merchant(array_merge($insertData, ['password' => $password]))
            ));
        }

        $request->session()->flash('message', 'Merchant was successful added!');
        return redirect('admin/merchant/create');
    }

    public function show($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $show = 1;
        $data = Merchant::find($id);

        return view('admin.merchant.show', compact('show', 'data'));
    }

    public function edit($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $disArray = district();

        $edit = 1;
        $actionLink = 'admin/merchant/' . $id;
        $data = Merchant::find($id);
        return view('admin.merchant.edit', compact('edit', 'actionLink', 'disArray', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'contact' => 'required|string|max:255|unique:merchants,contact,' . $id . ',id',
            'email' => 'required|string|email|max:255|unique:merchants,email,' . $id . ',id',
        ]);

        $input = $request->all();
        $updateData = [
            'name' => $input['name'],
            'contact' => $input['contact'],
            'email' => $input['email'],
            'second_contact_name' => $input['second_contact_name'],
            'second_contact_mobile_number' => $input['second_contact_mobile_number'],
            'address' => $input['address'],
            'district' => $input['district'],
        ];
        if (isset($input['password']) && $input['password'] != '') {
            $this->validate($request, [
                'password' => 'required|string|min:6',
            ]);
            $updateData['password'] = Hash::make($input['password']);
        }

        $data = Merchant::find($id);
        if ($request->hasFile('photo_id') != '') {
            $prevImage = $data;
            if (!empty($prevImage) && $prevImage->photo_id != "") {
                if (File::exists('storage/merchants/id/' . $prevImage->photo_id)) {
                    File::delete('storage/merchants/id/' . $prevImage->photo_id);
                }
            }

            $image = $request->file('photo_id');
            $fileName = 'merchant-nid-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::put('public/merchants/id/' . $fileName, $img);
            $updateData['photo_id'] = $fileName;
        }

        $data->update($updateData);

        $request->session()->flash('message', 'Merchant was successful updated!');
        return redirect('admin/merchant/' . $id . '/edit');
    }

    public function destroy(Request $request, $id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $data = Merchant::find($id);
        $data->delete();

        $request->session()->flash('message', 'Merchant was successful deleted!');
        return redirect('admin/merchant');
    }

    public function contact(Request $request, $id)
    {
        $sql = MerchantContact::orderBy('id', 'DESC');
        $sql->Where('partner_id', '=', $id);

        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.merchant-contact', compact('id', 'serial', 'records'));
    }

    public function contactStore(Request $request, $id)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);

        $input = $request->all();
        $insertData = [
            'partner_id' => $id,
            'receiver_type' => 2,
            'sender_type' => 1,
            'message' => $input['message'],
            'status' => 1,
        ];

        if ($request->file('attachment1') != '') {
            $file = $request->file('attachment1');
            $attachment1 = 'a1' . time() . Auth::user()->id . '-' . $file->getClientOriginalName();

            $dir = public_path('storage/app/public/merchant/merchant_contacts/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->putFileAs('merchant/merchant_contacts/' . $attachment1, $file, $attachment1);
            $insertData['attachment1'] = $attachment1;
        }

        if ($request->file('attachment2') != '') {
            $file = $request->file('attachment2');
            $attachment2 = 'a2' . time() . Auth::user()->id . '-' . $file->getClientOriginalName();

            $dir = public_path('storage/app/public/merchant/merchant_contacts/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->putFileAs('merchant/merchant_contacts/' . $attachment2, $file, $attachment2);
            $insertData['attachment2'] = $attachment2;
        }

        if ($request->file('attachment3') != '') {
            $file = $request->file('attachment3');
            $attachment3 = 'a3' . time() . Auth::user()->id . '-' . $file->getClientOriginalName();

            $dir = public_path('storage/app/public/merchant/merchant_contacts/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->putFileAs('merchant/merchant_contacts/' . $attachment3, $file, $attachment3);

            $insertData['attachment3'] = $attachment3;
        }
        $insert = MerchantContact::create($insertData);

        $request->session()->flash('message', 'Message has been sent successfully!');
        return redirect()->route('merchant.contact', $id);
    }
}
