<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = \App\Models\Booking::select(['bookings.id', 'bookings.book_number', 'users.name', 'users.mobile', 'drivers.full_name', 'drivers.mobile_number', 'booking_bills.driver_receive_due', 'booking_bills.user_receive_due']);
        $sql->join('users', 'bookings.customer_id', '=', 'users.id');
        $sql->join('booking_bills', 'bookings.id', '=', 'booking_bills.booking_id');
        $sql->leftJoin('drivers', 'bookings.driver_id', '=', 'drivers.id');
        $sql->orderBy('bookings.id', 'ASC');

        $input = $request->all();

        if (!empty($input['from'])) {
            $sql->where(DB::raw('DATE(bookings.pick_up_date_time)'), '>=', $input['from']);
        }
        if (!empty($input['to'])) {
            $sql->where(DB::raw('DATE(bookings.pick_up_date_time)'), '<=', $input['to']);
        }

        if (!empty($input['number'])) {
            $sql->where('bookings.book_number', 'LIKE', $input['number'].'%');
        }

        if (!empty($input['driver'])) {
            $sql->where('bookings.driver_id', $input['driver']);
        }

        if (!empty($input['type'])) {
            if ($input['type']=='Driver') {
                $sql->where('booking_bills.driver_receive_due', '>', 0);
            } elseif ($input['type']=='Customer') {
                $sql->where('booking_bills.user_receive_due', '>', 0);
            }
        } else {
            $sql->where( function($q) {
                $q->where('booking_bills.driver_receive_due', '>', 0)->orWhere('booking_bills.user_receive_due', '>', 0);
            });
        }

        $perPage = (!empty($input['show']))?$sql->count():25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        $drivers = \App\Models\Driver::with(['merchant'])->get();

        return view('admin.report.receivable', compact('serial', 'records', 'drivers'));
    }

    public function payable(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = \App\Models\Booking::select(['bookings.id', 'bookings.book_number', 'drivers.full_name', 'drivers.mobile_number', 'booking_bills.driver_paid_due']);
        $sql->join('booking_bills', 'bookings.id', '=', 'booking_bills.booking_id');
        $sql->leftJoin('drivers', 'bookings.driver_id', '=', 'drivers.id');
        $sql->orderBy('bookings.id', 'ASC');

        $input = $request->all();

        if (!empty($input['from'])) {
            $sql->where(DB::raw('DATE(bookings.pick_up_date_time)'), '>=', $input['from']);
        }
        if (!empty($input['to'])) {
            $sql->where(DB::raw('DATE(bookings.pick_up_date_time)'), '<=', $input['to']);
        }

        if (!empty($input['number'])) {
            $sql->where('bookings.book_number', 'LIKE', $input['number'].'%');
        }

        if (!empty($input['driver'])) {
            $sql->where('bookings.driver_id', $input['driver']);
        }

        $sql->where('booking_bills.driver_paid_due', '>', 0);

        $perPage = (!empty($input['show']))?$sql->count():25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        $drivers = \App\Models\Driver::with(['merchant'])->get();

        return view('admin.report.payable', compact('serial', 'records', 'drivers'));
    }

    public function income(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = \App\Models\Booking::select(['bookings.id', 'bookings.book_number', 'service_categories.category_name', 'booking_bills.bill_amount', 'booking_bills.admin_commission']);
        $sql->join('booking_bills', 'bookings.id', '=', 'booking_bills.booking_id');
        $sql->join('service_categories', 'bookings.service_category_id', '=', 'service_categories.id');
        $sql->orderBy('bookings.id', 'ASC');

        $input = $request->all();

        if (!empty($input['from'])) {
            $sql->where(DB::raw('DATE(bookings.pick_datetime)'), '>=', $input['from']);
        }
        if (!empty($input['to'])) {
            $sql->where(DB::raw('DATE(bookings.pick_datetime)'), '<=', $input['to']);
        }

        if (!empty($input['number'])) {
            $sql->where('bookings.id', 'LIKE', $input['number'].'%');
        }

        $sql->where('booking_bills.admin_commission', '>', 0);

        $perPage = (!empty($input['show']))?$sql->count():25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.report.income', compact('serial', 'records'));
    }

    public function expense(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = \App\Models\Expense::select(['expenses.*', 'expense_categories.category_name']);
        $sql->join('expense_categories', 'expenses.category_id', '=', 'expense_categories.id');
        $sql->orderBy('expenses.id', 'ASC');

        $input = $request->all();

        if (!empty($input['from'])) {
            $sql->where('expense_date', '>=', $input['from']);
        }
        if (!empty($input['to'])) {
            $sql->where('expense_date', '<=', $input['to']);
        }

        $perPage = (!empty($input['show']))?$sql->count():25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.report.expense', compact('serial', 'records'));
    }

    public function profitLoss(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $input = $request->all();
        $serial = 1;

        $income = \App\Models\BookingBill::selectRaw('"Income" AS type, booking_bills.created_at AS date, booking_bills.admin_commission AS amount, booking_bills.bill_remark AS details');

        $expense  = \App\Models\Expense::selectRaw('"Expense" AS type, expenses.expense_date AS date, expenses.expense_amount AS amount, expense_categories.category_name AS details');
        $expense->join('expense_categories', 'expenses.category_id', '=', 'expense_categories.id');

        if (!empty($input['from']) && !empty($input['to'])) {
            if (!empty($input['from'])) {
                $income->where(DB::raw('DATE(booking_bills.created_at)'), '>=', $input['from']);
            }
            if (!empty($input['to'])) {
                $income->where(DB::raw('DATE(booking_bills.created_at)'), '<=', $input['to']);
            }

            if (!empty($input['from'])) {
                $expense->where('expense_date', '>=', $input['from']);
            }
            if (!empty($input['to'])) {
                $expense->where('expense_date', '<=', $input['to']);
            }
        }
        $records = $income->unionAll($expense)->orderBy('date', 'ASC')->get();
        return view('admin.report.profit-loss', compact('serial', 'records'));
    }
}
