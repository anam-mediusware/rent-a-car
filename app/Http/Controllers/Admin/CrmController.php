<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Crm;
use Auth;

class CrmController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = Crm::orderBy('id', 'DESC');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('customer_number', 'LIKE', $input['q'].'%')
            ->orWhere('customer_name', 'LIKE', $input['q'].'%')
            ->orWhere('customer_email', 'LIKE', $input['q'].'%')
            ->orWhere('ticket_number', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.crm', compact('lists', 'serial', 'records'));
    }

    public function followup(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = Crm::orderBy('id', 'DESC');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->where(function ($q) use ($input) {
                $q->Where('customer_number', 'LIKE', $input['q'].'%')
                ->orWhere('customer_name', 'LIKE', $input['q'].'%')
                ->orWhere('customer_email', 'LIKE', $input['q'].'%')
                ->orWhere('ticket_number', 'LIKE', $input['q'].'%');
            });
        }

        $sql->where('status', 3);

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.crm', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $create = 1;
        $actionLink = 'admin/crm';
        return view('admin.crm', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_number' => 'required|max:11',
        ]);

        $ticketNo = time();

        $input = $request->all();
        $insert = Crm::create([
            'customer_number' => $input['customer_number'],
            'customer_name' => $input['customer_name'],
            'customer_email' => $input['customer_email'],
            'customer_address' => $input['customer_address'],
            'ticket_number' => $ticketNo,
            'customer_query' => $input['customer_query'],
            'travel_datetime' => $input['travel_datetime'],
            'vehicle_type' => $input['vehicle_type'],
            'feedback' => $input['feedback'],
            'status' => $input['status'],
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'CRM was successful added!');
        return redirect('admin/crm/create');
    }

    public function show($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $show = 1;
        $data = Crm::select('crms.*', 'users.name')
            ->join('users', 'crms.created_by', '=', 'users.id')
            ->find($id);

        return view('admin.crm', compact('show', 'data'));
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $edit = 1;
        $actionLink = 'admin/crm/'.$id;
        $data = Crm::find($id);
        $callings = Crm::Where('customer_number', $data->customer_number)->Where('id', '!=',$id)->orderBy('id', 'DESC')->get();
        return view('admin.crm', compact('edit', 'actionLink', 'data', 'callings'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'customer_number' => 'required|max:100',
        ]);

        $input = $request->all();
        $updateData = [
            'customer_number' => $input['customer_number'],
            'customer_name' => $input['customer_name'],
            'customer_email' => $input['customer_email'],
            'customer_address' => $input['customer_address'],
            'customer_query' => $input['customer_query'],
            'travel_datetime' => $input['travel_datetime'],
            'vehicle_type' => $input['vehicle_type'],
            'feedback' => $input['feedback'],
            'status' => $input['status'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Crm::find($id);
        $data->update($updateData);

        $request->session()->flash('message', 'CRM was successful updated!');
        return redirect('admin/crm/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = Crm::find($id);
        $data->delete();

        //Crm::destroy($id);
        $request->session()->flash('message', 'CRM was successful deleted!');
        return redirect('admin/crm');
    }

    public function calling(Request $request)
    {
        $input = $request->all();
        $callings = Crm::Where('customer_number', $input['customer_number'])->orderBy('id', 'DESC')->get();
        $data = [
            'status' => 1,
            'callings' => $callings
        ];
        return response()->json($data, 200);
    }
}
