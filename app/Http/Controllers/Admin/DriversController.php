<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\CarTrait;
use App\Http\Controllers\Admin\Traits\DataTrait;
use App\Models\UpazilaBoundary;
use App\Models\VehicleType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\DriversDocument;
use App\Models\DriversPayoutMethod;
use App\Models\DrivingCitie;
use App\Models\Merchant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;

class DriversController extends Controller
{
    use DataTrait, CarTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $drivers = Driver::OrderBy('drivers.full_name', 'ASC')->where('drivers.status', '1')->get();

        $sql = Driver::orderBy('drivers.id', 'DESC')->with('DrivingCitie', 'DriversPayoutMethod');

        $input = $request->all();
        if (!empty($input['driver'])) {
            $sql->Where('drivers.id', $input['driver']);
        }

        if (!empty($input['q'])) {
            $sql->Where('drivers.email_address', 'LIKE', '%' . $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;
        return view('admin.drivers.index', compact('lists', 'serial', 'records', 'drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $merchants = Merchant::where('status', '1')->get();
        $driver_cities = UpazilaBoundary::orderBy('Upaz_name', 'ASC')->get();
        $driver_payout_method = DriversPayoutMethod::orderBy('id', 'ASC')->where('status', '1')->get();

        $division_district = $this->division_district();
        $alphabetical_serial = $this->alphabetical_serial();
        $car_brands = $this->car_brands();
        $vehicle_types = VehicleType::all();

        $create = 1;
        $actionLink = 'admin/drivers';
        return view('admin.drivers.create', compact('create', 'actionLink', 'driver_cities', 'driver_payout_method', 'merchants', 'vehicle_types', 'division_district', 'alphabetical_serial', 'car_brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'email_address' => 'required|email',
            'gender' => 'required|in:M,F',
            'date_of_birth' => 'required',
            'city_id' => 'required|integer',
            'payment_method_id' => 'required|integer',
        ]);

        $input = $request->all();
        $password = Str::upper(Str::random(8));
        $insertData = [
            'merchants_id' => $input['merchant_id'] != "" ? $input['merchant_id'] : 0,
            'full_name' => $input['full_name'],
            'mobile_number' => $input['mobile_number'],
            'emergency_contact_number' => $input['emergency_contact_number'],
            'email_address' => $input['email_address'],
            'gender' => $input['gender'],
            'date_of_birth' => Carbon::createFromFormat('d/m/Y', $input['date_of_birth'])->format('Y-m-d'),
            'city_id' => $input['city_id'],
            'payment_method_id' => $input['payment_method_id'],
            'payment_details' => json_encode($input['payment_details']),
            'password' => Hash::make($password),
        ];

        if ($request->hasFile('driver_photo') != '') {
            $image = $request->file('driver_photo');
            $fileName = 'driver-photo-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/photo/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/photo/' . $fileName, $img);
            $insertData['image'] = $fileName;
        }

        $driver = Driver::create($insertData);

        if ($request->hasFile('nid_front') != '') {
            $image = $request->file('nid_front');
            $fileName = 'nid-front-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/nid/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/nid/' . $fileName, $img);
            $insert_nf_nid = [
                'drivers_id' => $driver->id,
                'document_type' => 'NID_front',
                'file_name' => $fileName,
            ];
            DriversDocument::create($insert_nf_nid);
        }

        if ($request->hasFile('nid_back') != '') {
            $image = $request->file('nid_back');
            $fileName = 'nid-back-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/nid/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/nid/' . $fileName, $img);
            $insert_fb_nid = [
                'drivers_id' => $driver->id,
                'document_type' => 'NID_back',
                'file_name' => $fileName,
            ];
            DriversDocument::create($insert_fb_nid);
        }

        if ($request->hasFile('driveing_license_front') != '') {
            $image = $request->file('driveing_license_front');
            $fileName = 'driveing-license-front-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/license/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/license/' . $fileName, $img);
            $insert_dlf_nid = [
                'drivers_id' => $driver->id,
                'document_type' => 'driving_license_front',
                'file_name' => $fileName,
                'expiry_date' => Carbon::createFromFormat('d/m/Y', $input['expiry_date'])->format('Y-m-d'),
            ];
            DriversDocument::create($insert_dlf_nid);
        }

        if ($request->hasFile('driveing_license_back') != '') {
            $image = $request->file('driveing_license_back');
            $fileName = 'driveing-license-back-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/license/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/license/' . $fileName, $img);

            $expiry_date = $input['expiry_date'];
            $year = substr($expiry_date, 6, 4);
            $month = substr($expiry_date, 3, 2);
            $day = substr($expiry_date, 0, 2);
            $f_expiry_date = $year . '-' . $month . '-' . $day;

            $insert_dlb_nid = [
                'drivers_id' => $driver->id,
                'document_type' => 'driving_license_back ',
                'file_name' => $fileName,
                'expiry_date' => $f_expiry_date,
            ];
            DriversDocument::create($insert_dlb_nid);
        }
        if ($driver) {
            // Send SMS to Merchant
            Notification::send($driver, new \App\Notifications\DriverRegistration(
                new Driver(array_merge($insertData, ['password' => $password]))
            ));
            if ($request->add_car) {
                $this->createCar($request, $driver->id);
            }
        }

        $request->session()->flash('message', 'Driver was successful added!');
        return redirect('admin/drivers/create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $show = 1;
        $data = Driver::with('DrivingCity', 'DriversPayoutMethod')->findOrFail($id);
        $data['driver_document'] = $this->driver_document($id);
        return view('admin.drivers.show', compact('show', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $merchants = Merchant::where('status', '1')->get();
        $driver_cities = UpazilaBoundary::orderBy('Upaz_name', 'ASC')->get();
        $driver_payout_method = DriversPayoutMethod::orderBy('id', 'ASC')->where('status', '1')->get();

        $edit = 1;
        $actionLink = 'admin/drivers/' . $id;
        $data = Driver::find($id);
        $data['driver_document'] = $this->driver_document($id);
        return view('admin.drivers.edit', compact('edit', 'actionLink', 'data', 'driver_cities', 'driver_payout_method', 'merchants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'full_name' => 'required',
            'email_address' => 'required|email',
            'gender' => 'required|in:M,F',
            'date_of_birth' => 'required',
            'city_id' => 'required|integer',
            'payment_method_id' => 'required|integer',
        ]);

        $input = $request->all();
        $updateData = [
            'merchants_id' => $input['merchant_id'] != "" ? $input['merchant_id'] : 0,
            'full_name' => $input['full_name'],
            'email_address' => $input['email_address'],
            'PIN' => $input['pin'],
            'gender' => $input['gender'],
            'date_of_birth' => $input['date_of_birth'],
            'city_id' => $input['city_id'],
            'payment_method_id' => $input['payment_method_id'],
            'payment_details' => json_encode($input['payment_details']),
            'updated_at' => date('Y-m-d h:i:s')
        ];

        if ($request->hasFile('driver_photo') != '') {
            $prevImage = Driver::find($id);
            if (!empty($prevImage) && $prevImage->image != "") {
                if (File::exists('storage/drivers/photo/' . $prevImage->image)) {
                    File::delete('storage/drivers/photo/' . $prevImage->image);
                }
            }

            $image = $request->file('driver_photo');
            $fileName = 'driver-photo-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('drivers/photo/' . $fileName, $img);
            $updateData['image'] = $fileName;
        }

        $data = Driver::find($id);
        $data->update($updateData);

        if ($request->hasFile('nid_front') != '') {
            $prevImage = DriversDocument::find($input['nid_front_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/drivers/nid/' . $prevImage->file_name)) {
                    File::delete('storage/drivers/nid/' . $prevImage->file_name);
                }
            }

            $image = $request->file('nid_front');
            $fileName = 'nid-front-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/nid/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/nid/' . $fileName, $img);

            if ($input['nid_front_id'] > 0) :
                DriversDocument::where('id', $input['nid_front_id'])->update(['file_name' => $fileName]);
            else :
                $insert_nf_nid = [
                    'drivers_id' => $id,
                    'document_type' => 'NID_front',
                    'file_name' => $fileName,
                ];
                DriversDocument::create($insert_nf_nid);
            endif;
        }

        if ($request->hasFile('nid_back') != '') {
            $prevImage = DriversDocument::find($input['nid_back_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/drivers/nid/' . $prevImage->file_name)) {
                    File::delete('storage/drivers/nid/' . $prevImage->file_name);
                }
            }

            $image = $request->file('nid_back');
            $fileName = 'nid-back-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/nid/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/nid/' . $fileName, $img);

            if ($input['nid_back_id'] > 0) :
                DriversDocument::where('id', $input['nid_back_id'])->update(['file_name' => $fileName]);
            else :
                $insert_fb_nid = [
                    'drivers_id' => $id,
                    'document_type' => 'NID_back',
                    'file_name' => $fileName,
                ];
                DriversDocument::create($insert_fb_nid);
            endif;
        }

        if ($input['expiry_date'] != "") :
            $expiry_date = $input['expiry_date'];
            $year = substr($expiry_date, 6, 4);
            $month = substr($expiry_date, 3, 2);
            $day = substr($expiry_date, 0, 2);
            $f_expiry_date = $year . '-' . $month . '-' . $day;
        endif;

        if ($request->hasFile('driveing_license_front') != '') {
            $prevImage = DriversDocument::find($input['dv_lic_front_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/drivers/license/' . $prevImage->file_name)) {
                    File::delete('storage/drivers/license/' . $prevImage->file_name);
                }
            }

            $image = $request->file('driveing_license_front');
            $fileName = 'driveing-license-front-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/license/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/license/' . $fileName, $img);

            if ($input['dv_lic_front_id'] > 0) :
                DriversDocument::where('id', $input['dv_lic_front_id'])->update(['file_name' => $fileName, 'expiry_date' => $f_expiry_date]);
            else :
                $insert_dlf_nid = [
                    'drivers_id' => $id,
                    'document_type' => 'driving_license_front',
                    'file_name' => $fileName,
                    'expiry_date' => $f_expiry_date,
                ];
                DriversDocument::create($insert_dlf_nid);
            endif;
        }

        if ($input['dv_lic_front_id'] > 0) :
            DriversDocument::where('id', $input['dv_lic_front_id'])->update(['expiry_date' => $f_expiry_date]);
        endif;

        if ($request->hasFile('driveing_license_back') != '') {
            $prevImage = DriversDocument::find($input['dv_lic_back_id']);
            if (!empty($prevImage) && $prevImage->file_name != "") {
                if (File::exists('storage/drivers/license/' . $prevImage->file_name)) {
                    File::delete('storage/drivers/license/' . $prevImage->file_name);
                }
            }

            $image = $request->file('driveing_license_back');
            $fileName = 'driveing-license-back-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/drivers/license/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('drivers/license/' . $fileName, $img);

            if ($input['dv_lic_back_id'] > 0) :
                DriversDocument::where('id', $input['dv_lic_back_id'])->update(['file_name' => $fileName, 'expiry_date' => $f_expiry_date]);
            else :
                $insert_dlb_nid = [
                    'drivers_id' => $id,
                    'document_type' => 'driving_license_back ',
                    'file_name' => $fileName,
                    'expiry_date' => $f_expiry_date,
                ];
                DriversDocument::create($insert_dlb_nid);
            endif;
        }

        if ($input['dv_lic_back_id'] > 0) :
            DriversDocument::where('id', $input['dv_lic_back_id'])->update(['expiry_date' => $f_expiry_date]);
        endif;

        $request->session()->flash('message', 'Driver was successful updated!');
        return redirect('admin/drivers/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        Driver::find($id)->delete();
        $request->session()->flash('message', 'Driver was successful deleted!');
        return redirect('admin/drivers');

    }

    private function driver_document($driver_id)
    {
        $data = [];
        $data['nid_front_id'] = '';
        $data['nid_front_img'] = '';
        $data['nid_back_id'] = '';
        $data['nid_back_img'] = '';
        $data['dv_lic_front_id'] = '';
        $data['dv_lic_front_img'] = '';
        $data['dv_lic_expiry_date'] = '';
        $data['dv_lic_back_id'] = '';
        $data['dv_lic_back_img'] = '';

        $driver_documents = DriversDocument::where('drivers_id', $driver_id)->get();

        if (!empty($driver_documents)) :
            foreach ($driver_documents as $key => $document) :
                if ($document->document_type == 'NID_front') :
                    $data['nid_front_id'] = $document->id;
                    $data['nid_front_img'] = $document->file_name;
                endif;

                if ($document->document_type == 'NID_back') :
                    $data['nid_back_id'] = $document->id;
                    $data['nid_back_img'] = $document->file_name;
                endif;

                if ($document->document_type == 'driving_license_front') :
                    $data['dv_lic_front_id'] = $document->id;
                    $data['dv_lic_front_img'] = $document->file_name;
                    $data['dv_lic_expiry_date'] = $document->expiry_date;
                endif;

                if ($document->document_type == 'driving_license_back') :
                    $data['dv_lic_back_id'] = $document->id;
                    $data['dv_lic_back_img'] = $document->file_name;
                endif;

            endforeach;
        endif;

        return $data;
    }

    public function getPaymentDetails(Request $request)
    {
        $payemntDetails = DriversPayoutMethod::find($request->id);
        $fields = '';
        if (!empty($payemntDetails)) :
            $fields = explode(',', $payemntDetails->fields_english);
        endif;
        return $fields;
    }
}
