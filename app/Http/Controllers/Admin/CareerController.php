<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Career;
use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Image;

class CareerController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = Career::orderBy('position_name');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('position_name', 'LIKE', $input['q'].'%')
                ->orWhere('position_details', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.career', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $create = 1;
        $actionLink = 'admin/career';
        return view('admin.career', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'position_name' => 'required',
            'position_details' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $fileName = '';
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('careers/' . $fileName, $img);
        }

        $input = $request->all();
        $insert = Career::create([
            'position_name' => $input['position_name'],
            'position_details' => $input['position_details'],
            'image' => $fileName,
            'status' => $input['status'],
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'Career was successful added!');
        return redirect('admin/career/create');
    }

    public function show($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $show = 1;
        $data = Career::find($id);

        return view('admin.career', compact('show', 'data'));
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $edit = 1;
        $actionLink = 'admin/career/'.$id;
        $data = Career::find($id);
        return view('admin.career', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'position_name' => 'required',
            'position_details' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $input = $request->all();
        $updateData = [
            'position_name' => $input['position_name'],
            'position_details' => $input['position_details'],
            'status' => $input['status'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Career::find($id);
        if ($request->hasFile('image')) {
            $prevImage = $data->image;
            if ($prevImage) {
                if (File::exists('storage/careers/' . $prevImage)) {
                    File::delete('storage/careers/' . $prevImage);
                }
            }

            $image = $request->file('image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('careers/' . $fileName, $img);

            $updateData['image'] = $fileName;
        }

        $data->update($updateData);

        $request->session()->flash('message', 'Career was successful updated!');
        return redirect('admin/career/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = Career::find($id);
        $data->delete();

        //Career::destroy($id);
        $request->session()->flash('message', 'Career was successful deleted!');
        return redirect('admin/career');
    }
}
