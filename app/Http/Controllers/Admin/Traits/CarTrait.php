<?php


namespace App\Http\Controllers\Admin\Traits;


use App\Models\CarModel;
use App\Models\Cars;
use App\Models\CarsDocument;
use App\Models\CarsImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;

trait CarTrait
{
    public function createCar(Request $request, $driver_id)
    {
        $input = $request->all();
        $carModel = CarModel::where('car_model', $request->model)->first();
        $insertData = [
            'drivers_id' => $driver_id,
            'merchants_id' => $request->merchant_id,
            'metro' => $request->metro,
            'alphabetical_serial' => $request->alphabetical,
            'serial_number' => $request->serial_number,
            'vehicle_type' => $request->vehicle_type,
            'car_brand' => $request->car_brand,
            'car_model_id' => $carModel ? $carModel->id : null,
            'model' => $request->model,
            'year' => $request->year,
            'owner_name' => $request->owner_name,
            'owner_address' => $request->owner_address,
            'owner_mobile_number' => $request->owner_mobile_number,
            'owner_email' => $request->owner_email,
        ];

        $car = Cars::create($insertData);

        if ($request->hasFile('registration_front') != '') {
            $image = $request->file('registration_front');
            $fileName = 'reg-front-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('cars/registration/' . $fileName, $img);

            $reg_expiry_date = $input['reg_expiry_date'];
            $reg_year = substr($reg_expiry_date, 6, 4);
            $reg_month = substr($reg_expiry_date, 3, 2);
            $reg_day = substr($reg_expiry_date, 0, 2);
            $reg_full_date = $reg_year . '-' . $reg_month . '-' . $reg_day;

            $insert_rg_front = [
                'cars_id' => $car->id,
                'document_type' => 'registration_front',
                'expiry_date' => $reg_full_date,
                'file_name' => $fileName,
            ];
            CarsDocument::create($insert_rg_front);
        }

        if ($request->hasFile('registration_back') != '') {
            $image = $request->file('registration_back');
            $fileName = 'reg-back-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('cars/registration/' . $fileName, $img);

            $reg_expiry_date = $input['reg_expiry_date'];
            $reg_year = substr($reg_expiry_date, 6, 4);
            $reg_month = substr($reg_expiry_date, 3, 2);
            $reg_day = substr($reg_expiry_date, 0, 2);
            $reg_full_date = $reg_year . '-' . $reg_month . '-' . $reg_day;

            $insert_reg_back = [
                'cars_id' => $car->id,
                'document_type' => 'registration_back',
                'expiry_date' => $reg_full_date,
                'file_name' => $fileName,
            ];
            CarsDocument::create($insert_reg_back);
        }

        if ($request->hasFile('fitness_paper') != '') {
            $image = $request->file('fitness_paper');
            $fileName = 'fit-paper-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('cars/fitness/' . $fileName, $img);

            $fit_expiry_date = $input['fit_expiry_date'];
            $fit_year = substr($fit_expiry_date, 6, 4);
            $fit_month = substr($fit_expiry_date, 3, 2);
            $fit_day = substr($fit_expiry_date, 0, 2);
            $fit_full_date = $fit_year . '-' . $fit_month . '-' . $fit_day;

            $insert_fitness = [
                'cars_id' => $car->id,
                'document_type' => 'fitness_paper',
                'expiry_date' => $fit_full_date,
                'file_name' => $fileName,
            ];
            CarsDocument::create($insert_fitness);
        }

        if ($request->hasFile('tax_token') != '') {
            $image = $request->file('tax_token');
            $fileName = 'tax-token-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('cars/tax/' . $fileName, $img);

            $tax_expiry_date = $input['tax_expiry_date'];
            $tax_year = substr($tax_expiry_date, 6, 4);
            $tax_month = substr($tax_expiry_date, 3, 2);
            $tax_day = substr($tax_expiry_date, 0, 2);
            $tax_full_date = $tax_year . '-' . $tax_month . '-' . $tax_day;

            $insert_tax = [
                'cars_id' => $car->id,
                'document_type' => 'tax_token',
                'expiry_date' => $tax_full_date,
                'file_name' => $fileName,
            ];
            CarsDocument::create($insert_tax);
        }

        if ($request->hasFile('insurance_paper') != '') {
            $image = $request->file('insurance_paper');
            $fileName = 'insurance-paper-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(324, 204, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('cars/insurance/' . $fileName, $img);

            $insurance_expiry_date = $input['insurance_expiry_date'];
            $ins_year = substr($insurance_expiry_date, 6, 4);
            $ins_month = substr($insurance_expiry_date, 3, 2);
            $ins_day = substr($insurance_expiry_date, 0, 2);
            $ins_full_date = $ins_year . '-' . $ins_month . '-' . $ins_day;

            $insert_insurance = [
                'cars_id' => $car->id,
                'document_type' => 'insurance_paper',
                'expiry_date' => $ins_full_date,
                'file_name' => $fileName,
            ];
            CarsDocument::create($insert_insurance);
        }

        if ($request->hasFile('car_image_outer')) {
            foreach ($request->file('car_image_outer') as $key => $image) {
                $fileName = 'car-image-' . Auth::user()->id . time() . '-outer-' . $key . '.' . $image->getClientOriginalExtension();

                $img = Image::make($image->getRealPath());
                $img->resize(324, 204, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->stream(); // <-- Key point

                Storage::disk('public')->put('cars/car-img/' . $fileName, $img);
                $insert_car = [
                    'cars_id' => $car->id,
                    'type' => 'outer',
                    'image' => $fileName,
                ];
                CarsImage::create($insert_car);
            }
        }

        if ($request->hasFile('car_image_inner')) {
            foreach ($request->file('car_image_inner') as $key => $image) {
                $fileName = 'car-image-' . Auth::user()->id . time() . '-inner-' . $key . '.' . $image->getClientOriginalExtension();

                $img = Image::make($image->getRealPath());
                $img->resize(324, 204, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->stream(); // <-- Key point

                Storage::disk('public')->put('cars/car-img/' . $fileName, $img);
                $insert_car = [
                    'cars_id' => $car->id,
                    'type' => 'inner',
                    'image' => $fileName,
                ];
                CarsImage::create($insert_car);
            }
        }
        return $car;
    }
}
