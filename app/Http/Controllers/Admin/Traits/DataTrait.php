<?php


namespace App\Http\Controllers\Admin\Traits;


use App\Models\CarsDocument;

trait DataTrait
{
    private function division_district()
    {
        $arr = [
            ['Dhaka Matro', 'ঢাকা মেট্রো'],
            ['Dhaka', 'ঢাকা'],
            ['Barisal Matro', 'বরিশাল মেট্রো'],
            ['Barisal', 'বরিশাল'],
            ['Chittagong Matro', 'চট্টগ্রাম মেট্রো'],
            ['Chittagong', 'চট্টগ্রাম'],
            ['Khulna Matro', 'খুলনা মেট্রো'],
            ['Khulna', 'খুলনা'],
            ['Mymensingh Matro', 'ময়মনসিংহ মেট্রো'],
            ['Mymensingh', 'ময়মনসিংহ'],
            ['Rajshahi Matro', 'রাজশাহী মেট্রো'],
            ['Rajshahi', 'রাজশাহী'],
            ['Sylhet Matro', 'সিলেট মেট্রো'],
            ['Sylhet', 'সিলেট'],
            ['Rangpur Matro', 'রংপুর মেট্রো'],
            ['Barguna', 'বরগুনা'],
            ['Bhola', 'ভোলা'],
            ['Jhalokati', 'ঝালকাঠি'],
            ['Patuakhali', 'পটুয়াখালি'],
            ['Pirojpur', 'পিরোজপুর'],
            ['Bandarban', 'বান্দরবান'],
            ['Brahmanbaria', 'ব্রাহ্মণবাড়িয়া'],
            ['Chandpur', 'চাঁদপুর'],
            ['Comilla', 'কুমিল্লা'],
            ['Cox\'s Bazar', 'কক্সবাজার'],
            ['Feni', 'ফেনী'],
            ['Khagrachhari', 'খাগড়াছড়ি'],
            ['Lakshmipur', 'লক্ষ্মীপুর'],
            ['Noakhali', 'নোয়াখালি'],
            ['Rangamati', 'রাঙ্গামাটি'],
            ['Faridpur', 'ফরিদপুর'],
            ['Gazipur', 'গাজীপুর'],
            ['Gopalganj', 'গোপালগঞ্জ'],
            ['Kishoreganj', 'কিশোরগঞ্জ'],
            ['Madaripur', 'মাদারিপুর'],
            ['Manikganj', 'মানিকগঞ্জ'],
            ['Munshiganj', 'মুন্সিগঞ্জ'],
            ['Narayanganj', 'নারায়ণগঞ্জ'],
            ['Narsingdi', 'নরসিংদি'],
            ['Rajbari', 'রাজবাড়ি'],
            ['Shariatpur', 'শরিয়তপুর	'],
            ['Tangail', 'টাঙ্গাইল'],
            ['Bagerhat', 'বাগেরহাট'],
            ['Chuadanga', 'চুয়াডাঙ্গা'],
            ['Jessore', 'যশোর'],
            ['Jhenaidah', 'ঝিনাইদহ'],
            ['Kushtia', 'কুষ্টিয়া'],
            ['Magura', 'মাগুরা'],
            ['Meherpur', 'মেহেরপুর'],
            ['Narail', 'নড়াইল'],
            ['Satkhira', 'সাতক্ষীরা'],
            ['Jamalpur', 'জামালপুর'],
            ['Netrokona', 'নেত্রকোনা'],
            ['Sherpur', 'শেরপুর'],
            ['Bogra', 'বগুড়া'],
            ['Joypurhat', 'জয়পুরহাট'],
            ['Naogaon', 'নওগাঁ'],
            ['Natore', 'নাটোর'],
            ['Chapainawabganj', 'চাঁপাইনবাবগঞ্জ'],
            ['Pabna', 'পাবনা'],
            ['Sirajganj', 'সিরাজগঞ্জ'],
            ['Dinajpur', 'দিনাজপুর'],
            ['Gaibandha', 'গাইবান্ধা'],
            ['Kurigram', 'কুড়িগ্রাম'],
            ['Lalmonirhat', 'লালমনিরহাট'],
            ['Nilphamari', 'নীলফামারি'],
            ['Panchagarh', 'পঞ্চগড়'],
            ['Thakurgaon', 'ঠাকুরগাঁও'],
            ['Habiganj', 'হবিগঞ্জ'],
            ['Moulvibazar', 'মৌলভীবাজার'],
            ['Sunamganj', 'সুনামগঞ্জ']
        ];

        return $arr;
    }

    private function alphabetical_serial()
    {
        $arr = [
            ['অ', ''],
            ['ই', ''],
            ['উ', ''],
            ['এ', ''],
            ['ক', ''],
            ['খ', ''],
            ['গ', ''],
            ['ঘ', ''],
            ['ঙ', ''],
            ['চ', ''],
            ['ছ', ''],
            ['জ', ''],
            ['ঝ', ''],
            ['ত', ''],
            ['থ', ''],
            ['ঢ', ''],
            ['ড', ''],
            ['ট', ''],
            ['ঠ', ''],
            ['দ', ''],
            ['ধ', ''],
            ['ন', ''],
            ['প', ''],
            ['ফ', ''],
            ['ব', ''],
            ['ভ', ''],
            ['ম', ''],
            ['য', ''],
            ['র', ''],
            ['ল', 'L'],
            ['শ', ''],
            ['স', ''],
            ['হ', 'H']
        ];
        return $arr;
    }

    private function car_brands()
    {
        $arr = [
            0 => "AMC",
            1 => "Acura",
            2 => "Alfa Romeo",
            3 => "Anadol",
            4 => "Aston Martin",
            5 => "Audi",
            6 => "Avanti",
            7 => "BMW",
            8 => "Bentley",
            9 => "Buick",
            10 => "Cadillac",
            11 => "Chevrolet",
            12 => "Chrysler",
            13 => "Dacia",
            14 => "Daewoo",
            15 => "Daihatsu",
            16 => "Datsun",
            17 => "DeLorean",
            18 => "Dodge",
            19 => "Eagle",
            20 => "FIAT",
            21 => "Ferrari",
            22 => "Fisker",
            23 => "Ford",
            24 => "Freightliner",
            25 => "GMC",
            26 => "Geely",
            27 => "Geo",
            28 => "HUMMER",
            29 => "Honda",
            30 => "Hyundai",
            31 => "Infiniti",
            32 => "Isuzu",
            33 => "Jaguar",
            34 => "Jeep",
            35 => "Kia",
            36 => "Lamborghini",
            37 => "Lancia",
            38 => "Land Rover",
            39 => "Lexus",
            40 => "Lincoln",
            41 => "Lotus",
            42 => "MG",
            43 => "MINI",
            44 => "Maserati",
            45 => "Maybach",
            46 => "Mazda",
            47 => "McLaren",
            48 => "Mercedes-Benz",
            49 => "Mercury",
            50 => "Merkur",
            51 => "Mitsubishi",
            52 => "Nissan",
            53 => "Oldsmobile",
            54 => "Opel",
            55 => "Peugeot",
            56 => "Plymouth",
            57 => "Pontiac",
            58 => "Porsche",
            59 => "Proton",
            60 => "RAM",
            61 => "Renault",
            62 => "Rolls-Royce",
            63 => "Rover",
            64 => "SRT",
            65 => "Saab",
            66 => "Saturn",
            67 => "Scion",
            68 => "Skoda",
            69 => "Sterling",
            70 => "Subaru",
            71 => "Suzuki",
            72 => "Tesla",
            73 => "Tofas",
            74 => "Toyota",
            75 => "TATA",
            76 => "Triumph",
            77 => "Volkswagen",
            78 => "Volvo",
            79 => "Yugo",
            80 => "smart",
            81 => "Mahindra",
        ];
        return $arr;
    }

    private function car_document($car_id)
    {
        $data = [];
        $data['reg_front_id'] = '';
        $data['reg_front_img'] = '';

        $data['reg_back_id'] = '';
        $data['reg_back_img'] = '';
        $data['reg_expiry_date'] = '';

        $data['fitness_id'] = '';
        $data['fitness_paper_img'] = '';
        $data['fit_expiry_date'] = '';

        $data['tax_id'] = '';
        $data['tax_token_img'] = '';
        $data['tax_expiry_date'] = '';

        $data['insurance_id'] = '';
        $data['insurance_paper_img'] = '';
        $data['insurance_expiry_date'] = '';

        $car_documents = CarsDocument::where('cars_id', $car_id)->get();

        if (!empty($car_documents)) :
            foreach ($car_documents as $key => $document) :

                if ($document->document_type == 'registration_front') :
                    $data['reg_front_id'] = $document->id;
                    $data['reg_front_img'] = $document->file_name;
                    $data['reg_expiry_date'] = $document->expiry_date;
                endif;

                if ($document->document_type == 'registration_back') :
                    $data['reg_back_id'] = $document->id;
                    $data['reg_back_img'] = $document->file_name;
                endif;

                if ($document->document_type == 'fitness_paper') :
                    $data['fitness_id'] = $document->id;
                    $data['fitness_paper_img'] = $document->file_name;
                    $data['fit_expiry_date'] = $document->expiry_date;
                endif;

                if ($document->document_type == 'tax_token') :
                    $data['tax_id'] = $document->id;
                    $data['tax_token_img'] = $document->file_name;
                    $data['tax_expiry_date'] = $document->expiry_date;
                endif;

                if ($document->document_type == 'insurance_paper') :
                    $data['insurance_id'] = $document->id;
                    $data['insurance_paper_img'] = $document->file_name;
                    $data['insurance_expiry_date'] = $document->expiry_date;
                endif;

            endforeach;
        endif;

        return $data;
    }
}
