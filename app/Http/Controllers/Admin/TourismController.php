<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tourism;
use Auth;
use Illuminate\Support\Facades\Storage;
use Image;

class TourismController extends Controller
{

    public function index(Request $request)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $sql = Tourism::orderBy('tourism_name');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('tourism_name', 'LIKE', $input['q'] . '%')
                ->orWhere('tourism_details', 'LIKE', $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.tourism', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $create = 1;
        $actionLink = 'admin/tourism';
        return view('admin.tourism', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'tourism_type' => 'required|numeric',
            'tourism_name' => 'required',
            'tourism_details' => 'required',
            'status' => 'required|numeric',
            'tourism_thumb' => 'image|mimes:jpeg,png,jpg|max:500',
            'tourism_image' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $thumb = '';
        if ($request->hasFile('tourism_thumb')) {
            $image = $request->file('tourism_thumb');
            $thumb = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('tourisms/' . $thumb, $img);
        }

        $tourism_image = '';
        if ($request->hasFile('tourism_image')) {
            $image = $request->file('tourism_image');
            $tourism_image = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('tourisms/' . $tourism_image, $img);
        }

        $input = $request->all();
        $insert = Tourism::create([
            'tourism_type' => $input['tourism_type'],
            'tourism_name' => $input['tourism_name'],
            'tourism_slug' => urlSlug($input['tourism_name']),
            'tourism_details' => $input['tourism_details'],
            'tourism_thumb' => $thumb,
            'tourism_image' => $tourism_image,
            'sorting' => $input['sorting'],
            'route_title' => $input['route_title'],
            'route_keyword' => $input['route_keyword'],
            'route_description' => $input['route_description'],
            'status' => $input['status'],
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'Tourism was successful added!');
        return redirect('admin/tourism/create');
    }

    public function show($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $show = 1;
        $data = Tourism::find($id);

        return view('admin.tourism', compact('show', 'data'));
    }

    public function edit($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $edit = 1;
        $actionLink = 'admin/tourism/' . $id;
        $data = Tourism::find($id);
        return view('admin.tourism', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tourism_type' => 'required|numeric',
            'tourism_name' => 'required',
            'tourism_details' => 'required',
            'status' => 'required|numeric',
            'tourism_thumb' => 'image|mimes:jpeg,png,jpg|max:500',
            'tourism_image' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $input = $request->all();
        $updateData = [
            'tourism_type' => $input['tourism_type'],
            'tourism_name' => $input['tourism_name'],
            'tourism_slug' => urlSlug($input['tourism_name']),
            'tourism_details' => $input['tourism_details'],
            'sorting' => $input['sorting'],
            'route_title' => $input['route_title'],
            'route_keyword' => $input['route_keyword'],
            'route_description' => $input['route_description'],
            'status' => $input['status'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Tourism::find($id);
        if ($request->hasFile('tourism_thumb')) {
            deleteStorageFile('tourisms', $data->tourism_thumb);

            $image = $request->file('tourism_thumb');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('tourisms/' . $fileName, $img);

            $updateData['tourism_thumb'] = $fileName;
        }

        if ($request->hasFile('tourism_image')) {
            deleteStorageFile('tourisms', $data->tourism_image);

            $image = $request->file('tourism_image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('tourisms/' . $fileName, $img);

            $updateData['tourism_image'] = $fileName;
        }
        $data->update($updateData);

        $request->session()->flash('message', 'Tourism was successful updated!');
        return redirect('admin/tourism/' . $id . '/edit');
    }

    public function destroy(Request $request, $id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $data = Tourism::find($id);
        $data->delete();
        deleteStorageFile('tourisms', $data['tourism_thumb']);
        deleteStorageFile('tourisms', $data['tourism_image']);

        //Tourism::destroy($id);
        $request->session()->flash('message', 'Tourism was successful deleted!');
        return redirect('admin/tourism');
    }
}
