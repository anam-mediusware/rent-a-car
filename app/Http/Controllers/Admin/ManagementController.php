<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Management;
use Auth;
use Illuminate\Support\Facades\Storage;
use Image;

class ManagementController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = Management::orderBy('name');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('name', 'LIKE', $input['q'].'%')
                ->orWhere('details', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.management', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $create = 1;
        $actionLink = 'admin/management';
        return view('admin.management', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'details' => 'required',
            'designation' => 'required',
            'status' => 'required|numeric',
            'image' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $fileName = '';
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('managements/' . $fileName, $img);
        }

        $input = $request->all();
        $insert = Management::create([
            'name' => $input['name'],
            'details' => $input['details'],
            'designation' => $input['designation'],
            'status' => $input['status'],
            'image' => $fileName,
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'Management was successful added!');
        return redirect('admin/management/create');
    }

    public function show($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $show = 1;
        $data = Management::find($id);

        return view('admin.management', compact('show', 'data'));
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $edit = 1;
        $actionLink = 'admin/management/'.$id;
        $data = Management::find($id);
        return view('admin.management', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'details' => 'required',
            'designation' => 'required',
            'status' => 'required|numeric',
            'image' => 'image|mimes:jpeg,png,jpg|max:500',
        ]);

        $input = $request->all();
        $updateData = [
            'name' => $input['name'],
            'details' => $input['details'],
            'designation' => $input['designation'],
            'status' => $input['status'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Management::find($id);
        if ($request->hasFile('image')) {
            deleteStorageFile('managements', $data->image);

            $image = $request->file('image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('managements/' . $fileName, $img);

            $updateData['image'] = $fileName;
        }
        $data->update($updateData);

        $request->session()->flash('message', 'Management was successful updated!');
        return redirect('admin/management/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = Management::find($id);
        $data->delete();
        deleteStorageFile('managements', $data['image']);

        //Management::destroy($id);
        $request->session()->flash('message', 'Management was successful deleted!');
        return redirect('admin/management');
    }
}
