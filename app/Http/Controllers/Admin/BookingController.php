<?php

namespace App\Http\Controllers\Admin;

use App\Events\DriverAssignedEvent;
use App\Http\Controllers\Controller;
use App\Models\Airport;
use App\Models\Booking;
use App\Models\BookingAdditionalPricing;
use App\Models\BookingStatus;
use App\Models\Driver;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\UpazilaBoundary;
use App\Models\User;
use App\Models\VehicleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $customer = User::get();
        $serviceCategories = ServiceCategory::where('status', '1')->get();
        $services = Service::where('status', '1')->get();
        $vehicle_type = VehicleType::where('status', '1')->get();
        $sql = Booking::orderBy('bookings.id', 'DESC')->where('pricing_type', 'fixed')
            ->with(['VehicleType', 'user', 'ServiceCategory', 'service', 'upazilaBoundaryFrom', 'upazilaBoundaryTo']);

        $input = $request->all();
        if (!empty($input['customer_id'])) {
            $sql->Where('bookings.customer_id', $input['customer_id']);
        }

        if (!empty($input['service_category_id'])) {
            $sql->Where('bookings.service_category_id', $input['service_category_id']);
        }

        if (!empty($input['service_id'])) {
            $sql->Where('bookings.service_type_id', $input['service_id']);
        }

        if (!empty($input['vehicle_type_id'])) {
            $sql->Where('bookings.vehicle_type_id', $input['vehicle_type_id']);
        }

        if (!empty($input['q'])) {
            $sql->Where('bookings.pricing_type', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.pick_up_location', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.drop_off_location', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.price', 'LIKE', '%' . $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.booking.booking_list', compact('lists', 'serial', 'records', 'customer', 'serviceCategories', 'services', 'vehicle_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $client = User::all();
        $airports = Airport::all();
        $serviceCategories = ServiceCategory::where('status', '1')->get();
        $vehicleType = VehicleType::where('status', '1')->get();
        $payMethod = config('settings.payment_methods');
        return view('admin.booking.create', compact('client','airports', 'serviceCategories', 'vehicleType', 'payMethod'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if ($request['customer_type'] == 1) :
            $this->validate($request, [
                'name' => 'required',
                'mobile' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required_with:retype_password|same:retype_password',
                'vehicle_type_id' => 'required',
                'service_category_id' => 'required',
                'rent_type' => 'required',
                'pick_up_location' => 'required',
                'pick_up_date_time' => 'required',
                'drop_off_date_time' => 'required',
                'pricing_type' => 'required|in:fixed,bid',
                'price' => 'required',
            ]);
        else :
            $this->validate($request, [
                'existing_client' => 'required',
                'vehicle_type_id' => 'required',
                'service_category_id' => 'required',
                'rent_type' => 'required',
                'pick_up_location' => 'required',
                'pick_up_date_time' => 'required',
                'drop_off_date_time' => 'required',
                'pricing_type' => 'required|in:fixed,bid',
                'price' => 'required',
            ]);
            $customer_id = $request->existing_client;
        endif;

        $input = $request->input();
        if ($request['customer_type'] == 1) :
            $insertUserArr = [
                'name' => $request->name,
                'mobile' => $request->mobile,
                'address' => $request->flat_bit,
                'email' => $request->email,
                'email_verified_at' => date('Y-m-d h:i:s'),
                'password' => bcrypt($request->password),
            ];

            $user = User::create($insertUserArr);
            $customer_id = $user->id;
        endif;

        $insertBookingArr = [
            'customer_type' => $request->customer_type,
            'customer_id' => $customer_id,
            'vehicle_type_id' => $request->vehicle_type_id,
            'service_category_id' => $request->service_category_id,
            'service_type_id' => $request->rent_type,
            'pricing_type' => $request->pricing_type,
            'price' => $request->price,
            'pick_up_date_time' => formatDateTimeDBFull($request->pick_up_date_time),
            'drop_off_date_time' => formatDateTimeDBFull($request->drop_off_date_time),
            'pick_up_city' => $this->getCityId($request->pick_up_point),
            'pick_up_point' => $request->pick_up_point,
            'pick_up_location' => $request->pick_up_location,
            'drop_off_city' => $this->getCityId($request->drop_off_point),
            'drop_off_point' => $request->drop_off_point,
            'drop_off_location' => $request->drop_off_location,
            'charged_amount' => 0,
        ];

        $booking = Booking::create($insertBookingArr);
        if ($booking) {
            if (!empty($request->additional_price) && $request->additional_price[0]['label'] != "" && $request->additional_price[0]['price'] != "") :
                foreach ($request->additional_price as $additional_price) :
                    $insertAdditionalPricing = [
                        'booking_id' => $booking->id,
                        'price_name' => $additional_price['label'],
                        'price' => $additional_price['price'],
                    ];
                    BookingAdditionalPricing::create($insertAdditionalPricing);
                endforeach;
            endif;

            $bookingStatuArr = [
                'booking_id' => $booking->id,
                'booking_status' => 'pending for payment'
            ];
            BookingStatus::create($bookingStatuArr);

            if (isset($input['invoice'])) {
                \App\Models\BookingInvoice::create([
                    'booking_id' => $booking->id,
                    'invoice_number' => $input['invoice_number'],
                    'invoice_date' => $input['invoice_date'],
                    'rental_amount' => $input['rental_amount'],
                    'rental_remark' => $input['rental_remark'],
                    'extra_uses_cost' => $input['extra_uses_cost'],
                    'extra_uses_remark' => $input['extra_uses_remark'],
                    'driver_food_cost' => $input['driver_food_cost'],
                    'driver_food_remark' => $input['driver_food_remark'],
                    'driver_overtime_cost' => $input['driver_overtime_cost'],
                    'driver_overtime_remark' => $input['driver_overtime_remark'],
                    'fuel_cost' => $input['fuel_cost'],
                    'fuel_remark' => $input['fuel_remark'],
                    'toll_cost' => $input['toll_cost'],
                    'toll_remark' => $input['toll_remark'],
                    'other_cost' => $input['other_cost'],
                    'other_remark' => $input['other_remark'],
                    'vat_cost' => $input['vat_cost'],
                    'vat_remark' => $input['vat_remark'],
                    'invoice_amount' => $input['invoice_amount'],
                ]);
            }

            if ($input['bill']==1) {
                \App\Models\BookingBill::create([
                    'booking_id' => $booking->id,
                    'bill_amount' => $input['bill_amount'],
                    'customer_advance' => $input['customer_advance'],
                    'cash_on_delivery' => $input['cash_on_delivery'],
                    'driver_rate' => $input['driver_rate'],
                    'admin_commission' => $input['admin_commission'],
                    'bill_remark' => $input['bill_remark'],
                    'driver_receivable_amount' => $input['driver_receivable_amount'],
                    'driver_receive_amount' => $input['driver_receive_amount'],
                    'driver_receive_due' => ($input['driver_receivable_amount']-$input['driver_receive_amount']),
                    'driver_receive_by' => $input['driver_receive_by'],
                    'driver_receive_remark' => $input['driver_receive_remark'],
                    'driver_payable_amount' => $input['driver_payable_amount'],
                    'driver_paid_amount' => $input['driver_paid_amount'],
                    'driver_paid_due' => ($input['driver_payable_amount']-$input['driver_paid_amount']),
                    'driver_paid_by' => $input['driver_paid_by'],
                    'driver_paid_remark' => $input['driver_paid_remark'],
                    'user_receivable_amount' => $input['user_receivable_amount'],
                    'user_receive_amount' => $input['user_receive_amount'],
                    'user_receive_due' => ($input['user_receivable_amount']-$input['user_receive_amount']),
                    'user_receive_by' => $input['user_receive_by'],
                    'user_receive_remark' => $input['user_receive_remark'],
                    'created_by' => Auth::user()->id,
                ]);
            }

            $request->session()->flash('message', 'Booking was successful added!');
            return redirect('admin/booking/create');
        } else {
            $request->session()->flash('message', 'Booking saving failed. Please try again...');
            return redirect('admin/booking/create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $data = Booking::with(['user', 'vehicleType', 'serviceCategory', 'additionalPricings', 'upazilaBoundaryFrom', 'upazilaBoundaryTo'])->findOrFail($id);
        $drivers = Driver::all();
        return view('admin.booking.show', compact('data', 'drivers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $airports = Airport::all();
        $serviceCategories = ServiceCategory::where('status', '1')->get();
        $vehicleType = VehicleType::where('status', '1')->get();
        $data = Booking::with(['user', 'vehicleType', 'serviceCategory', 'additionalPricings'])->findOrFail($id);
        $services = Service::where(['category_id' => $data->service_category_id])->get();
        $inv = \App\Models\BookingInvoice::where('booking_id', $id)->first();
        $bill = \App\Models\BookingBill::where('booking_id', $id)->first();
        $payMethod = config('settings.payment_methods');
        return view('admin.booking.edit', compact('data', 'inv', 'bill', 'airports', 'serviceCategories', 'vehicleType', 'services', 'payMethod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'vehicle_type_id' => 'required',
            'service_category_id' => 'required',
            'pick_up_point' => 'required',
            'pick_up_location' => 'required',
            'drop_off_point' => 'required',
            'drop_off_location' => 'required',
            'pick_up_date_time' => 'required',
            'drop_off_date_time' => 'required',
            'pricing_type' => 'nullable|in:fixed,bid',
            'price' => 'required',
        ]);
        $validator->validate();

        $input = $request->input();
        $dataToUpdate = [
            'vehicle_type_id' => $request->vehicle_type_id,
            'service_category_id' => $request->service_category_id,
            'service_type_id' => $request->rent_type,
            'pricing_type' => $request->pricing_type,
            'pick_up_date_time' => formatDateTimeDBFull($request->pick_up_date_time),
            'drop_off_date_time' => formatDateTimeDBFull($request->drop_off_date_time),
            'price' => $request->price,
            'decoration_cost' => $request->decoration_cost,
            'trip_started' => $request->trip_started,
            'completed' => $request->completed,
            'pick_up_point' => $request->pick_up_point,
            'pick_up_location' => $request->pick_up_location,
            'drop_off_point' => $request->drop_off_point,
            'drop_off_location' => $request->drop_off_location,
            'charged_amount' => 0,
        ];

        $booking = Booking::findOrFail($id);
        if ($booking->pick_up_point != $request->pick_up_point) {
            $dataToUpdate['pick_up_city'] = $this->getCityId($request->pick_up_point);
        }
        if ($booking->drop_off_point != $request->drop_off_point) {
            $dataToUpdate['drop_off_city'] = $this->getCityId($request->drop_off_point);
        }
        $update = $booking->update($dataToUpdate);

        //Invoice Data...
        if (isset($input['invoice'])) {
            \App\Models\BookingInvoice::updateOrCreate(
                ['booking_id' => $id],
                [
                    'booking_id' => $id,
                    'invoice_number' => $input['invoice_number'],
                    'invoice_date' => $input['invoice_date'],
                    'rental_amount' => $input['rental_amount'],
                    'rental_remark' => $input['rental_remark'],
                    'extra_uses_cost' => $input['extra_uses_cost'],
                    'extra_uses_remark' => $input['extra_uses_remark'],
                    'driver_food_cost' => $input['driver_food_cost'],
                    'driver_food_remark' => $input['driver_food_remark'],
                    'driver_overtime_cost' => $input['driver_overtime_cost'],
                    'driver_overtime_remark' => $input['driver_overtime_remark'],
                    'fuel_cost' => $input['fuel_cost'],
                    'fuel_remark' => $input['fuel_remark'],
                    'toll_cost' => $input['toll_cost'],
                    'toll_remark' => $input['toll_remark'],
                    'other_cost' => $input['other_cost'],
                    'other_remark' => $input['other_remark'],
                    'vat_cost' => $input['vat_cost'],
                    'vat_remark' => $input['vat_remark'],
                    'invoice_amount' => $input['invoice_amount'],
                ]
            );
        }

        //Bill Data...
        if ($input['bill']==1) {
            \App\Models\BookingBill::updateOrCreate(
                ['booking_id' => $id],
                [
                    'booking_id' => $id,
                    'bill_amount' => $input['bill_amount'],
                    'customer_advance' => $input['customer_advance'],
                    'cash_on_delivery' => $input['cash_on_delivery'],
                    'driver_rate' => $input['driver_rate'],
                    'admin_commission' => $input['admin_commission'],
                    'bill_remark' => $input['bill_remark'],
                    'driver_receivable_amount' => $input['driver_receivable_amount'],
                    'driver_receive_amount' => $input['driver_receive_amount'],
                    'driver_receive_due' => ($input['driver_receivable_amount']-$input['driver_receive_amount']),
                    'driver_receive_by' => $input['driver_receive_by'],
                    'driver_receive_remark' => $input['driver_receive_remark'],
                    'driver_payable_amount' => $input['driver_payable_amount'],
                    'driver_paid_amount' => $input['driver_paid_amount'],
                    'driver_paid_due' => ($input['driver_payable_amount']-$input['driver_paid_amount']),
                    'driver_paid_by' => $input['driver_paid_by'],
                    'driver_paid_remark' => $input['driver_paid_remark'],
                    'user_receivable_amount' => $input['user_receivable_amount'],
                    'user_receive_amount' => $input['user_receive_amount'],
                    'user_receive_due' => ($input['user_receivable_amount']-$input['user_receive_amount']),
                    'user_receive_by' => $input['user_receive_by'],
                    'user_receive_remark' => $input['user_receive_remark'],
                    'created_by' => Auth::user()->id,
                ]
            );
        }

        if ($update) {
            $request->session()->flash('message', 'Booking updated successfully!');
        } else {
            $request->session()->flash('message', 'Booking updating failed. Please try again...');
        }
        return redirect()->route('admin.booking.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function assignDriver(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required|numeric'
        ]);
        $validator->validate();

        $booking = Booking::findOrFail($id);
        if ($booking) {
            $booking->update(['driver_id' => $request->driver_id, 'booking_status' => 'Confirmed']);
            if (env('BROADCAST_DRIVER') == 'pusher') {
                DriverAssignedEvent::dispatch('driver-assigned', $booking->driver_id, $booking->with(['upazilaBoundaryFrom', 'upazilaBoundaryTo'])->first());
            }
            return redirect()->back()->with('message', 'Driver assigned successfully!');
        }
        abort(404);
    }

    public function bookingBidList(Request $request)
    {
        $customer = User::get();
        $serviceCategorie = ServiceCategory::where('status', '1')->get();
        $services = Service::where('status', '1')->get();
        $vehicle_type = VehicleType::where('status', '1')->get();
        $sql = Booking::orderBy('bookings.id', 'DESC')->where('pricing_type', 'bid')->with('VehicleType', 'user', 'ServiceCategory', 'service');

        $input = $request->all();
        if (!empty($input['customer_id'])) {
            $sql->Where('bookings.customer_id', $input['customer_id']);
        }

        if (!empty($input['service_category_id'])) {
            $sql->Where('bookings.service_category_id', $input['service_category_id']);
        }

        if (!empty($input['service_id'])) {
            $sql->Where('bookings.service_type_id', $input['service_id']);
        }

        if (!empty($input['vehicle_type_id'])) {
            $sql->Where('bookings.vehicle_type_id', $input['vehicle_type_id']);
        }

        if (!empty($input['q'])) {
            $sql->Where('bookings.pricing_type', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.pick_up_location', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.drop_off_location', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.price', 'LIKE', '%' . $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.booking.booking_bid_list', compact('lists', 'serial', 'records', 'customer', 'serviceCategorie', 'services', 'vehicle_type'));
    }

    public function bookingCancelledList(Request $request)
    {
        $customer = User::get();
        $serviceCategorie = ServiceCategory::where('status', '1')->get();
        $services = Service::where('status', '1')->get();
        $vehicle_type = VehicleType::where('status', '1')->get();
        $sql = Booking::orderBy('bookings.id', 'DESC')
            ->whereIn('booking_status', ['Cancelled by Customer', 'Cancelled by Driver'])
            ->with('VehicleType', 'user', 'ServiceCategory', 'service');

        $input = $request->all();
        if (!empty($input['customer_id'])) {
            $sql->Where('bookings.customer_id', $input['customer_id']);
        }

        if (!empty($input['service_category_id'])) {
            $sql->Where('bookings.service_category_id', $input['service_category_id']);
        }

        if (!empty($input['service_id'])) {
            $sql->Where('bookings.service_type_id', $input['service_id']);
        }

        if (!empty($input['vehicle_type_id'])) {
            $sql->Where('bookings.vehicle_type_id', $input['vehicle_type_id']);
        }

        if (!empty($input['q'])) {
            $sql->Where('bookings.pricing_type', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.pick_up_location', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.drop_off_location', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('bookings.price', 'LIKE', '%' . $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.booking.booking_cancelled_list', compact('lists', 'serial', 'records', 'customer', 'serviceCategorie', 'services', 'vehicle_type'));
    }

    public function AddService(Request $request)
    {
        return Service::where('category_id', $request->id)->get();
    }

    public function getCityId($latLng)
    {
        $latLon = json_decode($latLng);
        $lat = $latLon->lat;
        $lon = $latLon->lon;
        $data = UpazilaBoundary::select(['id'])
            ->whereRaw('ST_CONTAINS(shape, POINT(' . $lon . ', ' . $lat . '))')
            ->first();
        return $data ? $data->id : null;
    }
}
