<?php

namespace App\Http\Controllers\Admin;

use App\Models\Airport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdditionalPricing;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\ServiceSetting;
use App\Models\VehicleType;
use Illuminate\Support\Arr;

class ServiceSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $serviceCategories = ServiceCategory::all();
        $vehicleType = VehicleType::where('status', '1')->get();
        $service = Service::where('status', '1')->get();
        $sql = ServiceSetting::orderBy('service_settings.id', 'DESC')
            ->with('VehicleType', 'Service', 'upazilaBoundaryFrom', 'upazilaBoundaryTo');

        $input = $request->all();
        if (!empty($input['service_category_id'])) {
            $sql->whereHas('Service', function ($service) use ($input) {
                $service->where('category_id', $input['service_category_id']);
            });
        }

        if (!empty($input['service_id'])) {
            $sql->Where('service_settings.service_id', $input['service_id']);
        }

        if (!empty($input['vehicle_type_id'])) {
            $sql->Where('service_settings.vehicle_type_id', $input['vehicle_type_id']);
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;
        return view('admin.service-setting.index', compact('lists', 'records', 'serial', 'serviceCategories', 'vehicleType', 'service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $airports = Airport::all();
        $ServiceCategories = ServiceCategory::all();
        $service = collect([]);
        $vehicleType = VehicleType::all();
        $create = 1;
        $actionLink = 'admin/service-setting';
        return view('admin.service-setting.create', compact('create', 'actionLink', 'vehicleType', 'ServiceCategories', 'service', 'airports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'service_category' => 'required',
            'rent_type' => 'required',
            'vehicle_type_id' => 'required',
            'flat_bit' => 'nullable|in:fixed,bid',
            'price' => 'required',
        ]);

        $insertServiceSetting = [
            'service_id' => $request->rent_type,
            'service_quality_type' => $request->service_quality_type,
            'vehicle_type_id' => $request->vehicle_type_id,
            'pricing_type' => 'fixed',
            'price' => $request->price,
        ];

        if ($request->service_category == 1 || $request->service_category == 2) :
            $insertServiceSetting['starting_point'] = $request->start_point;
            $insertServiceSetting['destination_point'] = $request->destination_point;
        endif;

        if ($request->service_category == 4) :
            $insertServiceSetting['starting_point'] = $request->start_point;
            $insertServiceSetting['destination_point'] = $request->destination_point;
            $insertServiceSetting['decoration_cost'] = $request->decoration_cost;
        endif;

        if ($request->service_category == 3) :
            if ($request->rent_type == 8) :
                $insertServiceSetting['starting_point'] = null;
                $insertServiceSetting['destination_point'] = null;
                $insertServiceSetting['airport_id'] = $request->airport_id;
            endif;

            if ($request->rent_type == 9) :
                $insertServiceSetting['starting_point'] = null;
                $insertServiceSetting['destination_point'] = null;
                $insertServiceSetting['airport_id'] = $request->airport_id;
            endif;
        endif;

        $serviceSetting = ServiceSetting::firstOrCreate(Arr::except($insertServiceSetting, ['price']), $insertServiceSetting);
        if (!empty($request->additional_price) && $request->additional_price[0]['label'] != "" && $request->additional_price[0]['price'] != "") :
            foreach ($request->additional_price as $additional_price) :
                $insertAdditionalPricing = [
                    'service_setting_id' => $serviceSetting->id,
                    'price_name' => $additional_price['label'],
                    'price' => $additional_price['price'],
                ];
                AdditionalPricing::create($insertAdditionalPricing);
            endforeach;
        endif;

        $request->session()->flash('message', 'Service Setting was successful added!');
        return redirect('admin/service-setting/create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $show = 1;
        $data = ServiceSetting::where('id', $id)->with('VehicleType', 'Service', 'upazilaBoundaryFrom', 'upazilaBoundaryTo')->first();
        return view('admin.service-setting.show', compact('show', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $airports = Airport::all();
        $ServiceCategories = ServiceCategory::where('status', '1')->get();
        $vehicleType = VehicleType::where('status', '1')->get();

        $data = ServiceSetting::with('VehicleType', 'Service', 'additionalPricings')->find($id);
        $service = Service::where('category_id', $data->Service->ServiceCategory->id)->get();

        //return $data;
        $edit = 1;
        $actionLink = 'admin/service-setting/' . $id;
        return view('admin.service-setting.edit', compact('edit', 'actionLink', 'vehicleType', 'ServiceCategories', 'data', 'service', 'airports'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'service_category' => 'required',
            'rent_type' => 'required',
            'vehicle_type_id' => 'required',
            'flat_bit' => 'nullable|in:fixed,bid',
            'price' => 'required',
        ]);

        $ServiceSettingArr = [
            'service_id' => $request->rent_type,
            'service_quality_type' => $request->service_quality_type,
            'vehicle_type_id' => $request->vehicle_type_id,
            'pricing_type' => 'fixed',
            'price' => $request->price,
        ];

        if ($request->service_category == 1 || $request->service_category == 2) :
            $ServiceSettingArr['starting_point'] = $request->start_point;
            $ServiceSettingArr['destination_point'] = $request->destination_point;
        endif;

        if ($request->service_category == 4) :
            $ServiceSettingArr['starting_point'] = $request->start_point;
            $ServiceSettingArr['destination_point'] = $request->destination_point;
            $ServiceSettingArr['decoration_cost'] = $request->decoration_cost;
        endif;

        if ($request->service_category == 3) :
            if ($request->rent_type == 8) :
                $ServiceSettingArr['starting_point'] = null;
                $ServiceSettingArr['destination_point'] = null;
                $ServiceSettingArr['airport_id'] = $request->airport_id;
            endif;

            if ($request->rent_type == 9) :
                $ServiceSettingArr['starting_point'] = null;
                $ServiceSettingArr['destination_point'] = null;
                $ServiceSettingArr['airport_id'] = $request->airport_id;
            endif;
        endif;

        $updateSetting = ServiceSetting::where('id', $id);
        $updateSetting->update($ServiceSettingArr);
        $serviceSetting = $updateSetting->first();

        $serviceSetting->additionalPricings()->delete();
        if (!empty($request->additional_price) && $request->additional_price[0]['label'] != "" && $request->additional_price[0]['price'] != "") :
            foreach ($request->additional_price as $additional_price) :
                $updateAdditionalPricing = [
                    'service_setting_id' => $serviceSetting->id,
                    'price_name' => $additional_price['label'],
                    'price' => $additional_price['price'],
                ];
                AdditionalPricing::create($updateAdditionalPricing);
            endforeach;
        endif;

        $request->session()->flash('message', 'Service setting was successful updated!');
        return redirect()->route('admin.service-setting.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        $service_setting = ServiceSetting::where('id', $id)->get();
        ServiceSetting::where('id', $id)->delete();
        AdditionalPricing::where('service_pricing_id', $service_setting[0]->service_id)->delete();
        $request->session()->flash('message', 'Service Setting was successful deleted!');
        return redirect('admin/service-setting');
    }
}
