<?php


namespace App\Http\Controllers\Admin\Api;


use App\Http\Controllers\Controller;
use App\Models\Driver;

class MerchantController extends Controller
{
    public function getDrivers($merchant_id)
    {
        if ($merchant_id == '0') {
            $drivers = Driver::whereNull('merchants_id')->get();
        } else {
            $drivers = Driver::where('merchants_id', $merchant_id)->get();
        }
        if ($drivers) {
            return response()->json(['status' => true, 'data' => $drivers]);
        }
        return response()->json(['status' => false, 'data' => []]);
    }
}
