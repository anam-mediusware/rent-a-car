<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Expense;
use Auth;

class ExpenseController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = Expense::orderBy('expenses.id', 'DESC');
        $sql->select('expenses.*', 'expense_categories.category_name');
        $sql->join('expense_categories', 'expenses.category_id', '=', 'expense_categories.id');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('expenses.expense_remark', 'LIKE', $input['q'].'%')
            ->orWhere('expense_categories.category_name', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.expense', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $categoryData = \App\Models\ExpenseCategories::all();

        $create = 1;
        $actionLink = 'admin/expense';
        return view('admin.expense', compact('create', 'actionLink', 'categoryData'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'expense_amount' => 'required',
            'expense_date' => 'required',
        ]);

        $input = $request->all();
        $insert = Expense::create([
            'category_id' => $input['category_id'],
            'expense_amount' => $input['expense_amount'],
            'expense_date' => $input['expense_date'],
            'expense_remark' => $input['expense_remark'],
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'Expense was successful added!');
        return redirect('admin/expense/create');
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $categoryData = \App\Models\ExpenseCategories::all();

        $edit = 1;
        $actionLink = 'admin/expense/'.$id;
        $data = Expense::find($id);
        return view('admin.expense', compact('edit', 'actionLink', 'categoryData', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'expense_amount' => 'required',
            'expense_date' => 'required',
        ]);

        $input = $request->all();
        $updateData = [
            'category_id' => $input['category_id'],
            'expense_amount' => $input['expense_amount'],
            'expense_date' => $input['expense_date'],
            'expense_remark' => $input['expense_remark'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Expense::find($id);
        $data->update($updateData);

        $request->session()->flash('message', 'Expense was successful updated!');
        return redirect('admin/expense/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = Expense::find($id);
        $data->delete();

        //Expense::destroy($id);
        $request->session()->flash('message', 'Expense was successful deleted!');
        return redirect('admin/expense');
    }
}
