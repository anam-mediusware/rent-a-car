<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ExpenseCategories;
use Auth;

class ExpenseCategoryController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = ExpenseCategories::orderBy('id', 'DESC');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('category_name', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.expense-category', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $create = 1;
        $actionLink = 'admin/expense-category';
        return view('admin.expense-category', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'category_name' => 'required',
        ]);

        $input = $request->all();
        $insert = ExpenseCategories::create([
            'category_name' => $input['category_name'],
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'Expense Category was successful added!');
        return redirect('admin/expense-category/create');
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $edit = 1;
        $actionLink = 'admin/expense-category/'.$id;
        $data = ExpenseCategories::find($id);
        return view('admin.expense-category', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_name' => 'required',
        ]);

        $input = $request->all();
        $updateData = [
            'category_name' => $input['category_name'],
            'updated_by' => Auth::user()->id,
        ];

        $data = ExpenseCategories::find($id);
        $data->update($updateData);

        $request->session()->flash('message', 'Expense Category was successful updated!');
        return redirect('admin/expense-category/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = ExpenseCategories::find($id);
        $data->delete();

        //ExpenseCategories::destroy($id);
        $request->session()->flash('message', 'Expense Category was successful deleted!');
        return redirect('admin/expense-category');
    }
}
