<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Slide;
use Auth;
use Illuminate\Support\Facades\Storage;
use Image;

class SlideController extends Controller
{

    public function index(Request $request)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $sql = Slide::orderBy('title');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('title', 'LIKE', $input['q'] . '%')
                ->orWhere('title_small', 'LIKE', $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.slide', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $create = 1;
        $actionLink = 'admin/slide';
        return view('admin.slide', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'title_small' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:500',
        ]);

        $fileName = '';
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('slides/' . $fileName, $img);
        }

        $input = $request->all();
        $insert = Slide::create([
            'title' => $input['title'],
            'title_small' => $input['title_small'],
            'status' => $input['status'],
            'image' => $fileName,
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'Slide was successful added!');
        return redirect('admin/slide/create');
    }

    public function show($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $show = 1;
        $data = Slide::find($id);

        return view('admin.slide', compact('show', 'data'));
    }

    public function edit($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $edit = 1;
        $actionLink = 'admin/slide/' . $id;
        $data = Slide::find($id);
        return view('admin.slide', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'title_small' => 'required',
            'status' => 'required|numeric',
        ]);

        $input = $request->all();
        $updateData = [
            'title' => $input['title'],
            'title_small' => $input['title_small'],
            'status' => $input['status'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Slide::find($id);
        if ($request->hasFile('image')) {
            deleteStorageFile('slides', $data->image);

            $image = $request->file('image');
            $fileName = time() . '-' . $image->getClientOriginalName();

            $img = Image::make($image->getRealPath());
            $img->resize(600, 600, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point
            Storage::disk('public')->put('slides/' . $fileName, $img);

            $updateData['image'] = $fileName;
        }
        $data->update($updateData);

        $request->session()->flash('message', 'Slide was successful updated!');
        return redirect('admin/slide/' . $id . '/edit');
    }

    public function destroy(Request $request, $id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $data = Slide::find($id);
        $data->delete();
        deleteStorageFile('slides', $data['image']);

        //Slide::destroy($id);
        $request->session()->flash('message', 'Slide was successful deleted!');
        return redirect('admin/slide');
    }
}
