<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bid;
use App\Models\BiddingTrip;
use App\Models\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BiddingTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $sql = BiddingTrip::with(['user', 'vehicleType', 'serviceCategory', 'service', 'upazilaBoundaryFrom', 'upazilaBoundaryTo'])
            ->orderBy('id', 'DESC')->where('status', 1);

        $input = $request->all();
        /*if (!empty($input['q'])) {
            $sql->where('title', 'LIKE', '%' . $input['q'] . '%');
        }*/

        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.bidding-trip.index', compact('serial', 'records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $data = new BiddingTrip();
        return view('admin.bidding-trip.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        $validator->validate();

        $input = $request->all();
        $insert = BiddingTrip::create([
            'title' => $input['title'],
        ]);

        $request->session()->flash("message", "BiddingTrip added successfully!");
        return redirect()->route('route.name.create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $data = BiddingTrip::with(['user', 'vehicleType', 'serviceCategory', 'service', 'upazilaBoundaryFrom', 'upazilaBoundaryTo'])
            ->where('status', 1)
            ->findOrFail($id);
        $drivers = Driver::all();
        return view('admin.bidding-trip.show', compact('data', 'drivers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $data = BiddingTrip::find($id);
        return view('admin.bidding-trip.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        $validator->validate();

        $input = $request->all();
        $updateData = [
            'title' => $input['title'],
        ];

        $data = BiddingTrip::find($id);
        $data->update($updateData);

        $request->session()->flash("message", "BiddingTrip updated successfully!");
        return redirect()->route('route.name.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        BiddingTrip::destroy($id);
        session()->flash("message", "BiddingTrip deleted successfully!");
        return redirect()->route('route.name.index');
    }

    public function submitBid(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'price' => 'required:numeric',
        ], [
            'driver_id.required' => 'Driver selection is required',
        ]);
        $validator->validate();

        Bid::create([
            'bidding_trip_id' => $id,
            'driver_id' => $request->driver_id,
            'price' => $request->price,
            'note' => $request->note,
        ]);

        $request->session()->flash("message", "Bid submitted successfully!");
        return redirect()->route('admin.bidding-trips.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function bids(Request $request, $id)
    {
        $sql = Bid::with(['biddingTrip', 'driver'])->where('bidding_trip_id', $id);
        if ($request->q) {
            $sql->whereHas('driver', function ($q) use ($request) {
                $q->where('full_name', 'LIKE', "%$request->q%");
            });
        }
        $records = $sql->paginate(25);
        if ($records->total()) {
            return view('admin.bidding-trip.bids', compact('records'));
        }
        abort(404);
    }
}
