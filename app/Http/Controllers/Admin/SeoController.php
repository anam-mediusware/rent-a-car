<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seo;
use Auth;

class SeoController extends Controller
{
    private $routeArray;

    public function __construct()
    {
        $this->routeArray = [
            'home' => 'Home',
            'about-us' => 'About Us',
            'fleet' => 'Fleet',
            'management-team' => 'Management Team',
            'careers' => 'Careers',
            'news' => 'News',
            'contact-us' => 'Contact Us',
            'cancellation-policy' => 'Cancellation Policy',
            'wait-time-policy' => 'Wait Time Policy',
            'privacy-policy' => 'Privacy Policy',
            'refund-and-return-policy' => 'Refund And Return Policy',
            'terms-and-conditions' => 'Terms And Conditions',
            'faq' => 'FAQ',
            'sitemap' => 'Sitemap',
            'partner-with-us' => 'Partner With Us',
            'one-day-tour' => 'One Day Tour',
            'tourist-place' => 'Tourist Place',
        ];
    }

    public function index(Request $request)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $sql = Seo::orderBy('route_name');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('route_name', 'LIKE', $input['q'] . '%')
                ->orWhere('route_title', 'LIKE', $input['q'] . '%')
                ->orWhere('route_keyword', 'LIKE', $input['q'] . '%')
                ->orWhere('route_description', 'LIKE', $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.seo', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $routeArray = $this->routeArray;

        $create = 1;
        $actionLink = 'admin/seo';
        return view('admin.seo', compact('create', 'actionLink', 'routeArray'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'route_name' => 'required',
            'route_title' => 'required',
        ]);

        $input = $request->all();
        $insert = Seo::create([
            'route_name' => $input['route_name'],
            'route_title' => $input['route_title'],
            'route_keyword' => $input['route_keyword'],
            'route_description' => $input['route_description'],
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'SEO was successful added!');
        return redirect('admin/seo/create');
    }

    public function show($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $show = 1;
        $data = Seo::find($id);

        return view('admin.seo', compact('show', 'data'));
    }

    public function edit($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $routeArray = $this->routeArray;

        $edit = 1;
        $actionLink = 'admin/seo/' . $id;
        $data = Seo::find($id);
        return view('admin.seo', compact('edit', 'actionLink', 'routeArray', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'route_name' => 'required',
            'route_title' => 'required',
        ]);

        $input = $request->all();
        $updateData = [
            'route_name' => $input['route_name'],
            'route_title' => $input['route_title'],
            'route_keyword' => $input['route_keyword'],
            'route_description' => $input['route_description'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Seo::find($id);
        $data->update($updateData);

        $request->session()->flash('message', 'SEO was successful updated!');
        return redirect('admin/seo/' . $id . '/edit');
    }

    public function destroy(Request $request, $id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $data = Seo::find($id);
        $data->delete();

        //Seo::destroy($id);
        $request->session()->flash('message', 'SEO was successful deleted!');
        return redirect('admin/seo');
    }
}
