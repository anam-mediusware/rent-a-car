<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\WebSetting;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WebSettingController extends Controller
{
    private $controllerName;
    private $routeNamePrefix;
    private $actionLinkPrefix;

    public function __construct()
    {
        $this->controllerName = 'Website Settings';
        $this->routeNamePrefix = 'admin.web-settings';
        $this->actionLinkPrefix = 'admin/web-settings';
    }

    public function index(Request $request)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }
        $data = WebSetting::first();

        $actionLink = $this->actionLinkPrefix;
        return view('admin.web-setting', compact('data', 'actionLink'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'phone' => 'required',
            'email' => 'required',
            'facebook' => 'nullable',
            'twitter' => 'nullable',
            'instagram' => 'nullable',
            'linkedin' => 'nullable',
        ]);
        $validator->validate();

        $insert = WebSetting::updateOrCreate(['id' => 1], array_merge($validator->getData(), [
            'updated_by' => Auth::id(),
        ]));
        if ($insert) {
            $request->session()->flash('message', $this->controllerName . ' saved successfully!');
        } else {
            $request->session()->flash('error', $this->controllerName . ' failed to save!');
        }
        return redirect()->route($this->routeNamePrefix . '.index');
    }
}
