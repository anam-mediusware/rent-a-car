<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Sms;
use Auth;

class SmsController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = Sms::orderBy('id', 'DESC');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('mobile_number', 'LIKE', $input['q'].'%')
                ->orWhere('message', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.sms', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $create = 1;
        $actionLink = 'admin/sms';
        return view('admin.sms', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'mobile_number' => 'required',
            'message' => 'required',
        ]);

        $input = $request->all();
        $insert = Sms::create([
            'mobile_number' => $input['mobile_number'],
            'message' => $input['message'],
            'created_by' => Auth::user()->id,
        ]);

        //Send SMS...
        sendSMS([$input['mobile_number']], $input['message']);

        $request->session()->flash('message', 'Sms was successful added!');
        return redirect('admin/sms/create');
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $edit = 1;
        $actionLink = 'admin/sms/'.$id;
        $data = Sms::find($id);
        return view('admin.sms', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'mobile_number' => 'required',
            'message' => 'required',
        ]);

        $input = $request->all();
        $updateData = [
            'mobile_number' => $input['mobile_number'],
            'message' => $input['message'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Sms::find($id);
        $data->update($updateData);

        //Send SMS...
        sendSMS([$input['mobile_number']], $input['message']);

        $request->session()->flash('message', 'Sms was successful updated!');
        return redirect('admin/sms/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = Sms::find($id);
        $data->delete();

        //Sms::destroy($id);
        $request->session()->flash('message', 'Sms was successful deleted!');
        return redirect('admin/sms');
    }
}
