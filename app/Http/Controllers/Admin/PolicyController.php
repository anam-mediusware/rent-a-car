<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Policy;
use Auth;

class PolicyController extends Controller {

    public function index()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $lists = 1;
        $perPage = 25;
        $records = Policy::paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.policy', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $create = 1;
        $actionLink = 'admin/policy';
        return view('admin.policy', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'policy_type' => 'required',
            'policy_details' => 'required',
        ]);

        $input = $request->all();
        $insert = Policy::create([
            'policy_type' => $input['policy_type'],
            'policy_details' => $input['policy_details'],
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'Policy was successful added!');
        return redirect('admin/policy/create');
    }

    public function show($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $show = 1;
        $data = Policy::find($id);

        return view('admin.policy', compact('show', 'data'));
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $edit = 1;
        $actionLink = 'admin/policy/'.$id;
        $data = Policy::find($id);
        return view('admin.policy', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'policy_type' => 'required',
            'policy_details' => 'required',
        ]);

        $input = $request->all();
        $updateData = [
            'policy_type' => $input['policy_type'],
            'policy_details' => $input['policy_details'],
            'updated_by' => Auth::user()->id,
        ];

        $data = Policy::find($id);
        $data->update($updateData);

        $request->session()->flash('message', 'Policy was successful updated!');
        return redirect('admin/policy/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = Policy::find($id);
        $data->delete();

        //Policy::destroy($id);
        $request->session()->flash('message', 'Policy was successful deleted!');
        return redirect('admin/policy');
    }
}
