<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdditionalPricing;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Package;
use App\Models\PackageImageVideo;
use App\Models\ServiceSetting;
use App\Models\VehicleType;
use Illuminate\Support\Facades\Auth;
use Image;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $vehicle_type = VehicleType::where('status', '1')->get();
        $sql = Package::orderBy('packages.id', 'DESC')->where('status', '1')->with('VehicleType', 'ServieSetting');

        $input = $request->all();
        if (!empty($input['vehicle_type'])) {
            $sql->Where('packages.vehicle_type_id', $input['vehicle_type']);
        }

        if (!empty($input['q'])) {
            $sql->Where('packages.from_location', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('packages.to_location', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('packages.package_description', 'LIKE', '%' . $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;
        return view('admin.package', compact('lists', 'serial', 'records', 'vehicle_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $vehicleType = VehicleType::where('status', '1')->get();
        $create = 1;
        $actionLink = 'admin/package';
        return view('admin.package', compact('create', 'actionLink', 'vehicleType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'vehicle_type_id' => 'required',
            'from_location' => 'required',
            'to_location' => 'required',
            'day_trip_status' => 'required|in:yes,no',
            'package_start_date' => 'required',
            'package_end_date' => 'required',
            'flat_bit' => 'required|in:fixed,bid',
            'package_price' => 'required',
        ]);

        $input = $request->all();
        $insertpackage = [
            'vehicle_type_id' => $input['vehicle_type_id'],
            'from_location' => $input['from_location'],
            'to_location' => $input['to_location'],
            'day_trip_status' => $input['day_trip_status'],
            'day_count' => 0,
            'night_count' => 0,
            'package_description' => htmlspecialchars($input['package_description']),
            'package_title' => $input['package_title'],
            'package_seo_title' => $input['package_seo_title'],
            'package_seo_keywords' => $input['package_seo_keywords'],
            'package_seo_description' => $input['package_seo_description'],
        ];

        if ($input['day_trip_status'] == 'no') :
            $trip_start_date = $input['trip_start_date'];
            $trip_start_year = substr($trip_start_date, 6, 4);
            $trip_start_month = substr($trip_start_date, 3, 2);
            $trip_start_day = substr($trip_start_date, 0, 2);
            $trip_start_full_date = $trip_start_year . '-' . $trip_start_month . '-' . $trip_start_day;
            $insertpackage['trip_start_date'] = $trip_start_full_date;
        endif;

        if ($input['day_trip_status'] == 'no') :
            $trip_end_date = $input['trip_end_date'];
            $trip_end_year = substr($trip_end_date, 6, 4);
            $trip_end_month = substr($trip_end_date, 3, 2);
            $trip_end_day = substr($trip_end_date, 0, 2);
            $trip_end_full_date = $trip_end_year . '-' . $trip_end_month . '-' . $trip_end_day;
            $insertpackage['trip_end_date'] = $trip_end_full_date;
        endif;

        if ($input['package_start_date'] != '') :
            $package_start_date = $input['package_start_date'];
            $package_start_year = substr($package_start_date, 6, 4);
            $package_start_month = substr($package_start_date, 3, 2);
            $package_start_day = substr($package_start_date, 0, 2);
            $package_start_full_date = $package_start_year . '-' . $package_start_month . '-' . $package_start_day;
            $insertpackage['package_start_date'] = $package_start_full_date;
        endif;

        if ($input['package_end_date'] != '') :
            $package_end_date = $input['package_end_date'];
            $package_end_year = substr($package_end_date, 6, 4);
            $package_end_month = substr($package_end_date, 3, 2);
            $package_end_day = substr($package_end_date, 0, 2);
            $package_end_full_date = $package_end_year . '-' . $package_end_month . '-' . $package_end_day;
            $insertpackage['package_end_date'] = $package_end_full_date;
        endif;

        if ($request->hasFile('banner_image') != '') {
            $image = $request->file('banner_image');
            $fileName = 'banner-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath())->resize(600, 300);
            $img->stream(); // <-- Key point
            $dir = public_path('storage/app/public/package/banner_image/');
            if (!file_exists($dir) && !is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            Storage::disk('public')->put('package/banner_image/' . $fileName, $img);
            $insertpackage['banner_image'] = $fileName;
        }

        $package = Package::create($insertpackage);

        $insertServiceSetting = [
            'service_id' => $package->id,
            'vehicle_type_id' => $package->vehicle_type_id,
            'pricing_type' => $input['flat_bit'],
            'price' => $input['package_price'],
        ];
        $serviceSetting = ServiceSetting::create($insertServiceSetting);

        if (!empty($input['additional_price']) && $request['additional_price'][0]['label'] != "" && $request['additional_price'][0]['price'] != "") :
            foreach ($input['additional_price'] as $additional_price) :
                $insertAdditionalPricing = [
                    'service_pricing_id' => $package->id,
                    'price_name' => $additional_price['label'],
                    'price' => $additional_price['price'],
                ];
                AdditionalPricing::create($insertAdditionalPricing);
            endforeach;
        endif;

        $request->session()->flash('message', 'Package was successful added!');
        return redirect('admin/package/create');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $show = 1;
        $data = Package::orderBy('packages.id', 'DESC')->where('id', $id)->with('VehicleType', 'ServieSetting', 'PackageImageVideo')->get();
        $data['additional_pricing'] = AdditionalPricing::where('service_pricing_id', $data[0]->ServieSetting->service_id)->get();
        return view('admin.package', compact('show', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $edit = 1;
        $actionLink = 'admin/package/' . $id;
        $vehicleType = VehicleType::where('status', '1')->get();
        $data = Package::orderBy('packages.id', 'DESC')->where('id', $id)->with('VehicleType', 'ServieSetting')->get();
        $data['additional_pricing'] = AdditionalPricing::where('service_pricing_id', $data[0]->ServieSetting->service_id)->get();
        return view('admin.package', compact('edit', 'actionLink', 'data', 'vehicleType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'vehicle_type_id' => 'required',
            'from_location' => 'required',
            'to_location' => 'required',
            'day_trip_status' => 'required|in:yes,no',
            'package_start_date' => 'required',
            'package_end_date' => 'required',
            'flat_bit' => 'required|in:fixed,bid',
            'package_price' => 'required',
        ]);

        $input = $request->all();
        $PackageArr = [
            'vehicle_type_id' => $input['vehicle_type_id'],
            'from_location' => $input['from_location'],
            'to_location' => $input['to_location'],
            'day_trip_status' => $input['day_trip_status'],
            'day_count' => 0,
            'night_count' => 0,
            'package_description' => htmlspecialchars($input['package_description']),
            'package_seo_title' => $input['package_seo_title'],
            'package_title' => $input['package_title'],
            'package_seo_keywords' => $input['package_seo_keywords'],
            'package_seo_description' => $input['package_seo_description'],
            'updated_at' => date('Y-m-d h:i:s'),
        ];

        if ($input['day_trip_status'] == 'no') :
            $trip_start_date = $input['trip_start_date'];
            $trip_start_year = substr($trip_start_date, 6, 4);
            $trip_start_month = substr($trip_start_date, 3, 2);
            $trip_start_day = substr($trip_start_date, 0, 2);
            $trip_start_full_date = $trip_start_year . '-' . $trip_start_month . '-' . $trip_start_day;
            $PackageArr['trip_start_date'] = $trip_start_full_date;
        endif;

        if ($input['day_trip_status'] == 'no') :
            $trip_end_date = $input['trip_end_date'];
            $trip_end_year = substr($trip_end_date, 6, 4);
            $trip_end_month = substr($trip_end_date, 3, 2);
            $trip_end_day = substr($trip_end_date, 0, 2);
            $trip_end_full_date = $trip_end_year . '-' . $trip_end_month . '-' . $trip_end_day;
            $PackageArr['trip_end_date'] = $trip_end_full_date;
        endif;

        if ($input['package_start_date'] != '') :
            $package_start_date = $input['package_start_date'];
            $package_start_year = substr($package_start_date, 6, 4);
            $package_start_month = substr($package_start_date, 3, 2);
            $package_start_day = substr($package_start_date, 0, 2);
            $package_start_full_date = $package_start_year . '-' . $package_start_month . '-' . $package_start_day;
            $PackageArr['package_start_date'] = $package_start_full_date;
        endif;

        if ($input['package_end_date'] != '') :
            $package_end_date = $input['package_end_date'];
            $package_end_year = substr($package_end_date, 6, 4);
            $package_end_month = substr($package_end_date, 3, 2);
            $package_end_day = substr($package_end_date, 0, 2);
            $package_end_full_date = $package_end_year . '-' . $package_end_month . '-' . $package_end_day;
            $PackageArr['package_end_date'] = $package_end_full_date;
        endif;

        if ($request->hasFile('banner_image') != '') {
            $prevImage = Package::find($id);
            if (!empty($prevImage) && $prevImage->banner_image != "") {
                if (File::exists('storage/package/banner_image/' . $prevImage->image)) {
                    File::delete('storage/package/banner_image/' . $prevImage->image);
                }
            }

            $image = $request->file('banner_image');
            $fileName = 'banner-' . Auth::user()->id . time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath())->resize(600, 300);
            $img->stream(); // <-- Key point
            Storage::disk('public')->put('package/banner_image/' . $fileName, $img);
            $PackageArr['banner_image'] = $fileName;
        }

        $updatePacakge = Package::find($id);
        $updatePacakge->update($PackageArr);

        $ServiceSettingArr = [
            'vehicle_type_id' => $input['vehicle_type_id'],
            'pricing_type' => $input['flat_bit'],
            'price' => $input['package_price'],
            'updated_at' => date('Y-m-d h:i:s'),
        ];
        $updateSetting = ServiceSetting::where('service_id', $id);
        $updateSetting->update($ServiceSettingArr);

        AdditionalPricing::where('service_pricing_id', $id)->delete();
        if (!empty($input['additional_price']) && $input['additional_price'][0]['label'] != null) :
            foreach ($input['additional_price'] as $additional_price) :
                $updateAdditionalPricing = [
                    'service_pricing_id' => $id,
                    'price_name' => $additional_price['label'] != "" ? $additional_price['label'] : '---',
                    'price' => $additional_price['price'] != "" ? $additional_price['price'] : 0,
                ];
                AdditionalPricing::create($updateAdditionalPricing);
            endforeach;
        endif;

        $request->session()->flash('message', 'Package was successful updated!');
        return redirect('admin/package/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy(Request $request, $id)
    {
        Package::find($id)->delete();
        ServiceSetting::where('service_id', $id)->delete();
        AdditionalPricing::where('service_pricing_id', $id)->delete();
        $request->session()->flash('message', 'Package was successful deleted!');
        return redirect('admin/package');
    }

    public function imageVideoUpload(Request $request, $id)
    {
        $sql = PackageImageVideo::orderBy('package_image_videos.id', 'DESC')->where('status', '1')->where('package_id', $id);

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('package_image_videos.image', 'LIKE', '%' . $input['q'] . '%')
                ->orWhere('package_image_videos.video_link', 'LIKE', '%' . $input['q'] . '%');
        }

        $actionLink = 'admin/package/package-image-upload';
        $images = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;
        return view('admin.package', compact('images', 'actionLink', 'id', 'serial', 'records'));
    }

    public function packageImageUpload(Request $request)
    {

        $dir = public_path('storage/app/public/package/gallery/');
        if (!file_exists($dir) && !is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        if ($request->hasfile('image_upload')) :
            $count = 0;
            foreach ($request->file('image_upload') as $file) :
                $fileName = 'gallery-' . Auth::user()->id . $count . time() . '.' . $file->getClientOriginalExtension();
                $img = Image::make($file->getRealPath())->resize(600, 300);
                $img->stream(); // <-- Key point
                Storage::disk('public')->put('package/gallery/' . $fileName, $img);

                if ($fileName != "") :
                    $insertImage = [
                        'package_id' => $request->id,
                        'data_type' => 1,
                        'image' => $fileName,
                    ];
                    PackageImageVideo::create($insertImage);
                    $count++;
                endif;
            endforeach;
        endif;

        if (!empty($request->video_upload[0])) :
            foreach ($request->video_upload as $videoLink) :
                $insertVideo = [
                    'package_id' => $request->id,
                    'data_type' => 2,
                    'image' => $videoLink,
                ];
                PackageImageVideo::create($insertVideo);
            endforeach;
        endif;

        $url = 'admin/package/' . $request->id . '/image-video-upload';
        $request->session()->flash('message', 'Image or video upload was successful deleted!');
        return redirect($url);
    }
}
