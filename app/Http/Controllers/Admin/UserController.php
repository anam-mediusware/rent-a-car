<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use Image;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $sql = Admin::orderBy('name');
        $sql->where('account_type', '>', 1);

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('name', 'LIKE', $input['q'] . '%')
                ->orWhere('email', 'LIKE', $input['q'] . '%');
        }
        if (!empty($input['type'])) {
            $sql->Where('account_type', '=', $input['type']);
        }
        if (!empty($input['status'])) {
            $sql->Where('status', '=', $input['status']);
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.user', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $create = 1;
        $actionLink = 'admin/user';
        return view('admin.user', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'account_type' => 'required|numeric',
            'email' => 'required|email|max:100|unique:admins,email',
            'password' => 'required|max:20|min:6',
            'retype_password' => 'required|same:password',
            'image' => 'image|mimes:jpeg,png,jpg|max:300',
        ]);

        $image = '';
        if ($request->file('image') != '') {
            $file = $request->file('image');
            $image = time() . '-' . $file->getClientOriginalName();
            $file->move('uploads/users', $image);

            //Thumbnail create
            Image::make('uploads/users/' . $image)->resize(100, 100)->save('uploads/users/thumb_' . $image);
        }

        $input = $request->all();
        $insert = Admin::create([
            'name' => $input['name'],
            'account_type' => $input['account_type'],
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
            'status' => $input['status'],
            'image' => $image,
        ]);

        $request->session()->flash('message', 'User was successful added!');
        return redirect('admin/user/create');
    }

    public function show($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $show = 1;
        $data = Admin::find($id);

        return view('admin.user', compact('show', 'data'));
    }

    public function edit($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $edit = 1;
        $actionLink = 'admin/user/' . $id;
        $data = Admin::find($id);
        return view('admin.user', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'account_type' => 'required|numeric',
            'email' => 'required|email|max:100|unique:admins,email,' . $id . ',id',
            'image' => 'image|mimes:jpeg,png,jpg|max:300',
        ]);

        $input = $request->all();
        $updateData = [
            'name' => $input['name'],
            'account_type' => $input['account_type'],
            'email' => $input['email'],
            'status' => $input['status'],
        ];

        if ($input['password'] != '') {
            $this->validate($request, [
                'password' => 'required|max:20|min:6',
                'retype_password' => 'required|same:password',
            ]);
            $updateData['password'] = bcrypt($input['password']);
        }

        if ($request->file('image') != '') {
            $file = $request->file('image');
            $image = time() . '-' . $file->getClientOriginalName();
            $file->move('uploads/users', $image);

            //Thumbnail create
            Image::make('uploads/users/' . $image)->resize(100, 100)->save('uploads/users/thumb_' . $image);

            $updateData['image'] = $image;
        }

        $data = Admin::find($id);
        $data->update($updateData);

        $request->session()->flash('message', 'User was successful updated!');
        return redirect('admin/user/' . $id . '/edit');
    }

    public function destroy(Request $request, $id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $data = Admin::find($id);
        $data->delete();
        deleteFile('uploads/users', $data['image']);

        //Admin::destroy($id);
        $request->session()->flash('message', 'User was successful deleted!');
        return redirect('admin/user');
    }

    public function activity(Request $request, $id, $status)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $input = ['status' => $status];
        $data = Admin::find($id);
        $data->update($input);

        $msg = ($status == 1) ? 'Activated' : 'Deactivated';
        $request->session()->flash('message', "User was successful $msg!");
        return redirect('admin/user');
    }
}
