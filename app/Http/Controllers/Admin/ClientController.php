<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $sql = User::orderBy('name');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('name', 'LIKE', $input['q'] . '%')
                ->orWhere('mobile', 'LIKE', $input['q'] . '%')
                ->orWhere('address', 'LIKE', $input['q'] . '%')
                ->orWhere('email', 'LIKE', $input['q'] . '%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        return view('admin.client', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $create = 1;
        $actionLink = 'admin/client';
        return view('admin.client', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'mobile' => 'required',
            'email' => 'required|email|max:100|unique:users,email',
            'password' => 'required|max:20|min:6',
            'retype_password' => 'required|same:password',
        ]);

        $input = $request->all();
        $insert = User::create([
            'name' => $input['name'],
            'mobile' => $input['mobile'],
            'address' => $input['address'],
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
        ]);

        $request->session()->flash('message', 'Client was successful added!');
        return redirect('admin/client/create');
    }

    public function show($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $show = 1;
        $data = User::find($id);

        return view('admin.client', compact('show', 'data'));
    }

    public function edit($id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $edit = 1;
        $actionLink = 'admin/client/' . $id;
        $data = User::find($id);
        return view('admin.client', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'mobile' => 'required',
            'email' => 'required|email|max:100|unique:users,email,' . $id . ',id',
        ]);

        $input = $request->all();
        $updateData = [
            'name' => $input['name'],
            'mobile' => $input['mobile'],
            'address' => $input['address'],
            'email' => $input['email'],
        ];

        if ($input['password'] != '') {
            $this->validate($request, [
                'password' => 'required|max:20|min:6',
                'retype_password' => 'required|same:password',
            ]);
            $updateData['password'] = bcrypt($input['password']);
        }

        $data = User::find($id);
        $data->update($updateData);

        $request->session()->flash('message', 'Client was successful updated!');
        return redirect('admin/client/' . $id . '/edit');
    }

    public function destroy(Request $request, $id)
    {
        if (!roleAccess('')) {
            return redirect('not-found');
        }

        $data = User::find($id);
        $data->delete();

        //User::destroy($id);
        $request->session()->flash('message', 'Client was successful deleted!');
        return redirect('admin/client');
    }
}
