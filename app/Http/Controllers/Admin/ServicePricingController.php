<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdditionalPricing;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\ServicePricing;
use App\Models\ServiceSetting;
use App\Models\VehicleType;

class ServicePricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $vehicle_types = VehicleType::where('status', '1')->get();
        $service_pricing = ServiceCategory::with('service')->where('status', '1')->get();
        $lists = 1;
        return view('admin.serviePricing', compact('lists', 'service_pricing', 'vehicle_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'price' => 'required',
        ]);

        $input = $request->all();
        $insert = ServicePricing::create([
            'service_id' => $input['id'],
            'price' => $input['price']
        ]);

        $arr = ['status' => false, 'msg' => 'Something went wrong. Please try again!'];
        if ($insert) {
            $arr = ['status' => true, 'msg' => 'Price add successfully.'];
        }
        return Response()->json($arr);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $edit = 1;
        $actionLink = 'admin/servie-pricing/' . $id;
        $data = Service::where('id', $id)->with('ServiceCategory')->get();
        $servicePricing = ServicePricing::where('service_id', $id)->with('additionalPricings')->get();
        //return $servicePricing;
        return view('admin.serviePricing', compact('edit', 'actionLink', 'data', 'servicePricing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy($id)
    {
        $delete = AdditionalPricing::find($id)->delete();
        $arr = ['status' => false, 'msg' => 'Something went wrong. Please try again!'];
        if ($delete) {
            $arr = ['status' => true, 'msg' => 'Additional Price Deleted successfully.'];
        }
        return Response()->json($arr);
    }

    public function updateGlobalSetting(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'type' => 'required',
            'vehicle_id' => 'required',
        ]);

        $input = $request->all();
        $data = ServiceSetting::where('service_id', $input['id']);
        $data->update(['status' => '0', 'updated_at' => date('Y-m-d h:i:s')]);

        $insert = ServiceSetting::create([
            'service_id' => $input['id'],
            'vehicle_type_id' => $input['vehicle_id'],
            'pricing_type' => $input['type'],
        ]);

        $arr = ['status' => false, 'msg' => 'Something went wrong. Please try again!'];
        if ($insert) {
            $arr = ['status' => true, 'msg' => 'Setting update successfully.'];
        }
        return Response()->json($arr);
    }

    public function updatePrice(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'price' => 'required',
        ]);

        $input = $request->all();
        $data = ServicePricing::where('id', $input['id']);
        $update = $data->update(['price' => $input['price'], 'updated_at' => date('Y-m-d h:i:s')]);

        $arr = ['status' => false, 'msg' => 'Something went wrong. Please try again!'];
        if ($update) {
            $arr = ['status' => true, 'msg' => 'Service Pricing update successfully.'];
        }
        return Response()->json($arr);
    }

    public function updateAdditionalPrice(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'ad_price_name' => 'required',
            'price' => 'required',
            'price_type' => 'required',
        ]);

        $input = $request->all();
        $data = AdditionalPricing::find($input['id']);
        $update = $data->update(['price' => $input['price'], 'price_name' => $input['ad_price_name'], 'price_type' => $input['price_type'], 'updated_at' => date('Y-m-d h:i:s')]);

        $arr = ['status' => false, 'msg' => 'Something went wrong. Please try again!'];
        if ($update) {
            $arr = ['status' => true, 'msg' => 'Additional Price update successfully.'];
        }
        return Response()->json($arr);
    }

    public function addAdditionalPrice(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'price_name' => 'required',
            'price_type' => 'required',
        ]);

        $input = $request->all();
        $insert = AdditionalPricing::create([
            'service_pricing_id' => $input['id'],
            'price_name' => $input['price_name'],
            'price' => $input['price'],
            'price_type' => $input['price_type'],
        ]);

        $arr = ['status' => false, 'msg' => 'Something went wrong. Please try again!'];
        if ($insert) {
            $arr = ['status' => true, 'msg' => 'Setting update successfully.'];
        }
        return Response()->json($arr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function additionalPriceDestroy($id)
    {
        $delete = AdditionalPricing::where('service_pricing_id', $id)->delete();
        $arr = ['status' => false, 'msg' => 'Something went wrong. Please try again!'];
        if ($delete) {
            $arr = ['status' => true, 'msg' => 'All Additional Price Deleted successfully.'];
        }
        return Response()->json($arr);
    }
}
