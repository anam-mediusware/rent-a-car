<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\DataTrait;
use App\Http\Controllers\Controller;
use App\Models\CarModel;
use App\Models\VehicleType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CarModelController extends Controller
{
    use DataTrait;
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $sql = CarModel::orderBy('car_model');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->where('car_model', 'LIKE', '%' . $input['q'] . '%');
        }
        if (!empty($input['car_brand'])) {
            $sql->where('car_brand', 'LIKE', '%' . $input['car_brand'] . '%');
        }

        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page'])) ? (($perPage * ($input['page'] - 1)) + 1) : 1;

        $car_brands = $this->car_brands();
        return view('admin.car-models.index', compact('serial', 'records', 'car_brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $data = new CarModel();
        $vehicle_types = VehicleType::all();
        $car_brands = $this->car_brands();
        return view('admin.car-models.create', compact('data', 'vehicle_types', 'car_brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vehicle_type_id' => 'required',
            'car_brand' => 'required',
            'car_model' => 'required',
        ]);
        $validator->validate();

        $input = $request->all();
        $insert = CarModel::create([
            'vehicle_type_id' => $input['vehicle_type_id'],
            'car_brand' => $input['car_brand'],
            'car_model' => $input['car_model'],
        ]);

        $request->session()->flash("message", "Car Model added successfully!");
        return redirect()->route('admin.car-models.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {
        $data = CarModel::with(['vehicleType'])->findOrFail($id);
        return view('admin.car-models.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $data = CarModel::findOrFail($id);
        $vehicle_types = VehicleType::all();
        $car_brands = $this->car_brands();
        return view('admin.car-models.edit', compact('data', 'vehicle_types', 'car_brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'vehicle_type_id' => 'required',
            'car_brand' => 'required',
            'car_model' => 'required',
        ]);
        $validator->validate();

        $input = $request->all();
        $updateData = [
            'vehicle_type_id' => $input['vehicle_type_id'],
            'car_brand' => $input['car_brand'],
            'car_model' => $input['car_model'],
        ];

        $data = CarModel::find($id);
        $data->update($updateData);

        $request->session()->flash("message", "Car Model updated successfully!");
        return redirect()->route('admin.car-models.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        CarModel::destroy($id);
        session()->flash("message", "Car Model deleted successfully!");
        return redirect()->route('admin.car-models.index');
    }
}
