<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ContactType;
use Auth;

class ContactTypeController extends Controller {

    public function index(Request $request)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $sql = ContactType::orderBy('contact_type');

        $input = $request->all();
        if (!empty($input['q'])) {
            $sql->Where('contact_type', 'LIKE', $input['q'].'%');
        }

        $lists = 1;
        $perPage = 25;
        $records = $sql->paginate($perPage);
        $serial = (!empty($input['page']))?(($perPage*($input['page']-1))+1):1;

        return view('admin.contact-type', compact('lists', 'serial', 'records'));
    }

    public function create()
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $create = 1;
        $actionLink = 'admin/contact-type';
        return view('admin.contact-type', compact('create', 'actionLink'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'contact_type' => 'required',
        ]);

        $input = $request->all();
        $insert = ContactType::create([
            'contact_type' => $input['contact_type'],
            'status' => $input['status'],
            'created_by' => Auth::user()->id,
        ]);

        $request->session()->flash('message', 'Contact Type was successful added!');
        return redirect('admin/contact-type/create');
    }

    public function show($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $show = 1;
        $data = ContactType::find($id);

        return view('admin.contact-type', compact('show', 'data'));
    }

    public function edit($id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $edit = 1;
        $actionLink = 'admin/contact-type/'.$id;
        $data = ContactType::find($id);
        return view('admin.contact-type', compact('edit', 'actionLink', 'data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'contact_type' => 'required',
        ]);

        $input = $request->all();
        $updateData = [
            'contact_type' => $input['contact_type'],
            'status' => $input['status'],
            'updated_by' => Auth::user()->id,
        ];

        $data = ContactType::find($id);
        $data->update($updateData);

        $request->session()->flash('message', 'Contact Type was successful updated!');
        return redirect('admin/contact-type/'.$id.'/edit');
    }

    public function destroy(Request $request, $id)
    {
        if(!roleAccess('')){ return redirect('not-found'); }

        $data = ContactType::find($id);
        $data->delete();

        //ContactType::destroy($id);
        $request->session()->flash('message', 'Contact Type was successful deleted!');
        return redirect('admin/contact-type');
    }
}
