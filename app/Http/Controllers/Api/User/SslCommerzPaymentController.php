<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\User\ServiceController;
use App\Models\Bid;
use App\Models\Booking;
use App\Services\BookingService;
use Illuminate\Http\Request;
use App\Library\SslCommerz\SslCommerzNotification;
use Illuminate\Support\Facades\Validator;

class SslCommerzPaymentController extends Controller
{
    public function payNow(Request $request)
    {
        $user = json_decode($request->booking_user, true);
        $data = json_decode($request->data, true);
        $bid_id = $request->bid_id;
        $pricing_type = $bid_id ? 'bid' : 'fixed';
        $search_result = json_decode($request->search_result, true);
        return view('web-view.pay-now', compact('user', 'data', 'pricing_type', 'search_result', 'bid_id'));
    }

    public function index(Request $request)
    {
        $searchParams = json_decode($request->search_params);
        $searchParams->class = $searchParams->service_quality;
        if ($searchParams->starting_point) {
            $starting_point_geo = $searchParams->starting_point_geo;
            $pick_up_location = app(ServiceController::class)->getUpazilaId($starting_point_geo->lat, $starting_point_geo->lon);
        }

        if ($searchParams->ending_point) {
            $ending_point_geo = $searchParams->ending_point_geo;
            $destination_point = app(ServiceController::class)->getUpazilaId($ending_point_geo->lat, $ending_point_geo->lon);
        }
        $data = app(ServiceController::class)->makeSessionArray(
            $searchParams,
            json_decode($request->search_result, true),
            $pick_up_location,
            $destination_point
        );
        $user_data = json_decode($request->user, true);
        $validator = Validator::make($user_data, [
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
        ]);
        $validator->validate();

        if ($request->pricing_type == 'bid') {
            $biddingData = Bid::with(['biddingTrip.vehicleType', 'biddingTrip.service', 'driver'])->findOrFail($request->bid_id);
            $validatedData = array_merge($validator->getData(), $biddingData->biddingTrip->toArray());
            $validatedData['price'] = $biddingData->price;
            $validatedData['bid_id'] = $request->bid_id;
            $validatedData['pricing_type'] = 'bid';
        } else {
            $serviceBookingData = $data;
            $serviceBookingData['pick_up_date_time'] = $serviceBookingData['pick_up_date'] . ' ' . $serviceBookingData['pick_up_time'];
            $serviceBookingData['drop_off_date_time'] = $serviceBookingData['drop_off_date'] . ' ' . $serviceBookingData['drop_off_time'];
            $serviceBookingData['pricing_type'] = 'fixed';
            $serviceBookingData['customer_id'] = $user_data['id'];
            $serviceBookingData['price'] = $serviceBookingData['service_setting']['price'];
            foreach ($serviceBookingData['service_setting']['additional_pricings'] as $pricing) {
                $serviceBookingData['price'] += $pricing['price'];
            }
            $validatedData = array_merge($validator->getData(), $serviceBookingData);
        }

        # Here you have to receive all the booking data to initiate the payment.
        # Let's say, your oder transaction information are saving in a table called "bookings"
        # In "bookings" table, booking unique identity is "transaction_id". "status" field contain status of the transaction, "amount" is the booking amount to be paid and "currency" is for storing Site Currency which will be checked with paid currency.

        $post_data = array();
        $post_data['total_amount'] = nearest($validatedData['price'] * 0.30, 50); # You cant not pay less than 10
        $post_data['currency'] = "BDT";
        $post_data['tran_id'] = uniqid(); // tran_id must be unique

        # CUSTOMER INFORMATION
        $post_data['cus_name'] = $validatedData['name'];
        $post_data['cus_email'] = $validatedData['email'];
        $post_data['cus_add1'] = 'Customer Address';
        $post_data['cus_add2'] = "";
        $post_data['cus_city'] = "";
        $post_data['cus_state'] = "";
        $post_data['cus_postcode'] = "";
        $post_data['cus_country'] = "Bangladesh";
        $post_data['cus_phone'] = $validatedData['mobile'];
        $post_data['cus_fax'] = "";

        # SHIPMENT INFORMATION
        $post_data['ship_name'] = "Store Test";
        $post_data['ship_add1'] = "Dhaka";
        $post_data['ship_add2'] = "Dhaka";
        $post_data['ship_city'] = "Dhaka";
        $post_data['ship_state'] = "Dhaka";
        $post_data['ship_postcode'] = "1000";
        $post_data['ship_phone'] = "";
        $post_data['ship_country'] = "Bangladesh";

        $post_data['shipping_method'] = "NO";
        $post_data['product_name'] = "Computer";
        $post_data['product_category'] = "Goods";
        $post_data['product_profile'] = "physical-goods";

        # OPTIONAL PARAMETERS
        $post_data['value_a'] = "ref001";
        $post_data['value_b'] = "ref002";
        $post_data['value_c'] = "ref003";
        $post_data['value_d'] = "ref004";

        #Before  going to initiate the payment booking status need to insert or update as Pending.
        (new BookingService())->makeBooking(array_merge($validatedData, $post_data));

        $sslc = new SslCommerzNotification('sslcommerz.user_app');
        # initiate(Transaction Data , false: Redirect to SSLCOMMERZ gateway/ true: Show all the Payment gateway here )
        $payment_options = $sslc->makePayment($post_data, 'hosted');

        if (!is_array($payment_options)) {
            print_r($payment_options);
            $payment_options = array();
        }
    }

    public function success(Request $request)
    {
//        echo "Transaction is Successful";

        $tran_id = $request->input('tran_id');
        $amount = $request->input('amount');
        $currency = $request->input('currency');

        $sslc = new SslCommerzNotification('sslcommerz.user_app');

        #Check booking status in booking table against the transaction id or booking id.
        $booking_details = Booking::where('transaction_id', $tran_id)
            ->select(['transaction_id', 'status', 'price'])->first();

        if ($booking_details->status == 'Pending') {
            $validation = $sslc->orderValidate($tran_id, $amount, $currency, $request->all());

            if ($validation == TRUE) {
                /*
                That means IPN did not work or IPN URL was not set in your merchant panel. Here you need to update booking status
                in booking table as Processing or Complete.
                Here you can also sent sms or email for successful transaction to customer
                */
                $update_product = Booking::where('transaction_id', $tran_id)
                    ->update(['status' => 'Processing']);

                return redirect()->route('api.user.transaction.success', ['transaction_message' => 'Transaction is successfully Completed']);
            } else {
                /*
                That means IPN did not work or IPN URL was not set in your merchant panel and Transaction validation failed.
                Here you need to update booking status as Failed in booking table.
                */
                $update_product = Booking::where('transaction_id', $tran_id)
                    ->update(['status' => 'Failed']);
                return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'validation Fail']);
            }
        } else if ($booking_details->status == 'Processing' || $booking_details->status == 'Complete') {
            /*
             That means through IPN Order status already updated. Now you can just show the customer that transaction is completed. No need to update database.
             */
            return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'Transaction is successfully Completed']);
        } else {
            #That means something wrong happened. You can redirect customer to your product page.
            return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'Invalid Transaction']);
        }
    }

    public function fail(Request $request)
    {
        $tran_id = $request->input('tran_id');

        $booking_details = Booking::where('transaction_id', $tran_id)
            ->select(['transaction_id', 'status', 'price'])->first();

        if ($booking_details->status == 'Pending') {
            $update_product = Booking::where('transaction_id', $tran_id)
                ->update(['status' => 'Failed']);
            return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'Transaction is Failed']);
        } else if ($booking_details->status == 'Processing' || $booking_details->status == 'Complete') {
            return redirect()->route('api.user.transaction.success', ['transaction_message' => 'Transaction is already Successful']);
        } else {
            return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'Transaction is Invalid']);
        }
    }

    public function cancel(Request $request)
    {
        $tran_id = $request->input('tran_id');

        $booking_details = Booking::where('transaction_id', $tran_id)
            ->select(['transaction_id', 'status', 'price'])->first();

        if ($booking_details->status == 'Pending') {
            $update_product = Booking::where('transaction_id', $tran_id)
                ->update(['status' => 'Canceled']);
            return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'Transaction is Cancel']);
        } else if ($booking_details->status == 'Processing' || $booking_details->status == 'Complete') {
            return redirect()->route('api.user.transaction.success', ['transaction_message' => 'Transaction is already Successful']);
        } else {
            return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'Transaction is Invalid']);
        }
    }

    public function ipn(Request $request)
    {
        #Received all the payment information from the gateway
        if ($request->input('tran_id')) #Check transaction id is posted or not.
        {
            $tran_id = $request->input('tran_id');
            \DB::table('ssl_commerz_ipn_logs')->insert([
                'transaction_id' => $tran_id,
                'response' => json_encode($request->all())
            ]);

            #Check booking status in booking table against the transaction id or booking id.
            $booking_details = Booking::where('transaction_id', $tran_id)
                ->select(['transaction_id', 'status', 'price'])->first();

            if ($booking_details->status == 'Pending') {
                $sslc = new SslCommerzNotification('sslcommerz.user_app');
                $validation = $sslc->orderValidate($tran_id, $booking_details->amount, $booking_details->currency, $request->all());
                if ($validation == TRUE) {
                    /*
                    That means IPN worked. Here you need to update booking status
                    in booking table as Processing or Complete.
                    Here you can also sent sms or email for successful transaction to customer
                    */
                    $update_product = Booking::where('transaction_id', $tran_id)
                        ->update(['status' => 'Processing']);

                    return redirect()->route('api.user.transaction.success', ['transaction_message' => 'Transaction is successfully Completed']);
                } else {
                    /*
                    That means IPN worked, but Transaction validation failed.
                    Here you need to update booking status as Failed in booking table.
                    */
                    $update_product = Booking::where('transaction_id', $tran_id)
                        ->update(['status' => 'Failed']);

                    return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'validation Fail']);
                }
            } else if ($booking_details->status == 'Processing' || $booking_details->status == 'Complete') {

                #That means Order status already updated. No need to update database.

                return redirect()->route('api.user.transaction.success', ['transaction_message' => 'Transaction is already successfully Completed']);
            } else {
                #That means something wrong happened. You can redirect customer to your product page.

                return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'Invalid Transaction']);
            }
        } else {
            return redirect()->route('api.user.transaction.failed', ['transaction_error' => 'Invalid Data']);
        }
    }
}
