<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Driver;
use Illuminate\Http\Request;

class CustomerTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $booking_status = $request->booking_status;
        $sql = Booking::with(['driver', 'rating', 'vehicleType'])
            ->where('customer_id', auth()->id())
            ->when($booking_status, function ($q) use ($booking_status) {
                if ($booking_status == 'Pending') {
                    $q->whereNull('driver_id');
                } else if ($booking_status == 'Cancelled by Customer') {
                    $q->where('booking_status', 'Cancelled by Customer');
                } else {
                    $q->where('booking_status', $booking_status);
                }
            });
        if ($booking_status == 'Cancelled by Driver') {
            $sql->orWhereIn('id', function ($q) {
                $q->select('booking_id')
                    ->from('booking_status_change_logs')
                    ->where('loggable_type', Driver::class)
                    ->where('loggable_id', auth()->id())
                    ->whereIn('new_status', ['Cancelled by Driver']);
            });
        }
        $trips = $sql->latest()->paginate(20);
        if ($trips) {
            return response()->json(['success' => true, 'data' => $trips]);
        }
        return response()->json(['success' => false, 'message' => 'Failed To Load Trip Data.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $trip = Booking::with(['user'])->where('driver_id', auth()->id())
            ->where('pricing_type', 'bid')->find($id);
        if($trip){
            return response()->json(['success'=> true, 'data'=> $trip]);
        }
        return response()->json(['success'=> false, 'message'=> 'Failed To Load Trip Data.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get Running Trip
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRunningTrip()
    {
        $trip = Booking::with(['user'])->where('driver_id', auth()->id())
            ->where('booking_status', 'Trip Started')
            ->first();
        if($trip){
            return response()->json(['success'=> true, 'data'=> $trip]);
        }
        return response()->json(['success'=> false, 'message'=> 'Trip Status Changing Failed.']);
    }

    /**
     * Update Booking Status
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeBookingStatus(Request $request, $id)
    {
        $trip = Booking::where('customer_id', auth()->id())->find($id);
        if($trip){
            $trip->update(['booking_status' => $request->booking_status]);
            return response()->json(['success'=> true, 'data'=> $trip]);
        }
        return response()->json(['success'=> false, 'message'=> 'Failed To Load Trip Data.']);
    }
}
