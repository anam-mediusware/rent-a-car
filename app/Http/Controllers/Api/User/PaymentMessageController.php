<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentMessageController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function success(Request $request)
    {
        $transaction_message = $request->transaction_message;
        return view('web-view.success', compact('transaction_message'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function failed(Request $request)
    {
        $transaction_error = $request->transaction_error;
        return view('web-view.failed', compact('transaction_error'));
    }
}
