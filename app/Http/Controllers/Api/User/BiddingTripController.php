<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\User\ServiceController;
use App\Models\Bid;
use App\Models\BiddingTrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BiddingTripController extends Controller
{
    public function index()
    {
        $records = BiddingTrip::with(['vehicleType', 'service', 'upazilaBoundaryFrom', 'upazilaBoundaryTo'])
            ->withCount('bids')
            ->whereCustomerId(Auth::id())
            ->where('status', 1)
            ->latest()
            ->paginate(20);
        if ($records) {
            return response()->json(['success' => true, 'data' => $records]);
        }
        return response()->json(['success' => false, 'message' => 'Failed To Load Bidding Trips Data.']);
    }

    public function show($id)
    {
        $bid = Bid::with(['biddingTrip.vehicleType'])->find($id);
        if ($bid) {
            return response()->json(['success' => true, 'data' => $bid]);
        }
        return response()->json(['success' => false, 'message' => 'Failed To Load Bidding Trip Data.']);
    }

    public function showBids($id)
    {
        $bids = Bid::where('bidding_trip_id', $id)
            ->with(['driverWithRatings.ratings'])
            ->latest()
            ->paginate(20);
        if ($bids) {
            return response()->json(['success' => true, 'data' => $bids]);
        }
        return response()->json(['success' => false, 'message' => 'Failed To Load Bidding Trip Data.']);
    }

    public function store(Request $request)
    {
        $validatedData = $request->all();

        if ($request->starting_point) {
            $starting_point_geo = (object)$request->starting_point_geo;
            $pick_up_city = app(ServiceController::class)->getUpazilaId($starting_point_geo->lat, $starting_point_geo->lon);
        }

        if ($request->ending_point) {
            $ending_point_geo = (object)$request->ending_point_geo;
            $drop_off_city = app(ServiceController::class)->getUpazilaId($ending_point_geo->lat, $ending_point_geo->lon);
        }
        $dataToInsert = [
            'customer_id' => Auth::id(),
            'vehicle_type_id' => $validatedData['vehicle_type'],
            'service_quality_type' => $validatedData['service_quality'],
            'car_model_id' => $validatedData['car_model_id'],
            'service_category_id' => $validatedData['service_category_id'],
            'service_type_id' => $validatedData['service_type_id'],
            'pick_up_city' => $pick_up_city,
            'pick_up_location' => $validatedData['starting_point'],
            'pick_up_point' => jsonString($validatedData['starting_point_geo']),
            'drop_off_city' => $drop_off_city,
            'drop_off_location' => $validatedData['ending_point'],
            'drop_off_point' => jsonString($validatedData['ending_point_geo']),
            'pick_up_date_time' => formatDateTimeDBFull($validatedData['pick_up_date'] . ' ' .$validatedData['pick_up_time']),
            'drop_off_date_time' => formatDateTimeDBFull($validatedData['drop_off_date'] . ' ' .$validatedData['drop_off_time']),
        ];
        $biddingTrip = BiddingTrip::create($dataToInsert);
        if ($biddingTrip) {
            return response()->json(['success' => true, 'data' => $biddingTrip, 'message' => 'Bidding trip submitted successfully!']);
        }
        return response()->json(['success' => true, 'error' => 'Bidding trip submission failed!']);
    }
}
