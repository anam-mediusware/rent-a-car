<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\UpazilaBoundary;
use App\Models\VehicleType;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getUpazilaBoundary($lat, $lon)
    {
        $data = UpazilaBoundary::select(['id'])
            ->whereRaw('ST_CONTAINS(shape, POINT(' . $lon . ', ' . $lat . '))')
            ->first();
        return $data;
    }

    public function bookingConfirmation(Request $request)
    {
        $request_params = $request->all();
        $vehicle_type = VehicleType::find($request_params['vehicle_type']);
        $starting_point_geo = json_decode($request_params['starting_point_geo']);
        $ending_point_geo = json_decode($request_params['ending_point_geo']);
        $pick_up_location = $this->getUpazilaBoundary($starting_point_geo->lat, $starting_point_geo->lon);
        $drop_off_location = $this->getUpazilaBoundary($ending_point_geo->lat, $ending_point_geo->lon);
        return response()->json(['success' => true, 'data' => compact('request_params', 'vehicle_type', 'pick_up_location', 'drop_off_location')]);
    }
}
