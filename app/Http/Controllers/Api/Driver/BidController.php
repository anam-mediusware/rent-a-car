<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\Bid;
use App\Models\BiddingTrip;
use Illuminate\Http\Request;

class BidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $bids = BiddingTrip::with(['user'])->latest()->paginate(20);
        if($bids){
            return response()->json(['success'=> true, 'data'=> $bids]);
        }
        return response()->json(['success'=> false, 'message'=> 'Failed To Load Trip Data.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $driver = request()->user();
        $dataToUpdate = $request->only('price', 'note');
        $bid = Bid::updateOrCreate([
            'driver_id' => $driver->id,
            'bidding_trip_id' => $request->bidding_trip_id,
        ], $dataToUpdate);
        if ($bid) {
            return response()->json(['success' => true, 'data' => $bid]);
        }
        return response()->json(['success' => false, 'message' => 'Failed Submit Bid.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $bid = Bid::with(['driver'])->find($id);
        if($bid){
            return response()->json(['success'=> true, 'data'=> $bid]);
        }
        return response()->json(['success'=> false, 'message'=> 'Failed To Load Bid Data.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
