<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\BiddingTrip;
use App\Models\Driver;
use Illuminate\Http\Request;

class BiddingTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $driver = Driver::with(['DrivingCity', 'cars'])->find(auth()->id());
        $trips = null;
        if ($driver->cars) {
            $carClass = $driver->cars->year < 2015 ? 1 : 2;
            $trips = BiddingTrip::with(['user', 'upazilaBoundaryFrom'])
                ->whereHas('upazilaBoundaryFrom', function($city) use ($driver) {
                    $city->where('Dist_ID', $driver->DrivingCity->Dist_ID);
                })
                ->where([
                    'service_quality_type' => $carClass,
                    'vehicle_type_id' => $driver->cars->vehicle_type,
                    'car_model_id' => $driver->cars->car_model_id,
                ])
                ->latest()->paginate(20);
        }
        if($trips){
            return response()->json(['success'=> true, 'data'=> $trips]);
        }
        return response()->json(['success'=> false, 'message'=> 'Failed To Load Trip Data.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $driver = request()->user();
        $trip = BiddingTrip::with(['user', 'bids' => function($q) use ($driver) {
            $q->where('driver_id', $driver->id);
        }])->find($id);
        if($trip){
            return response()->json(['success'=> true, 'data'=> $trip]);
        }
        return response()->json(['success'=> false, 'message'=> 'Failed To Load Trip Data.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
