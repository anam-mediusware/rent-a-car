<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;

class FixedPriceTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $trips = Booking::with(['user'])
            ->where('driver_id', auth()->id())
            ->where('pricing_type', 'fixed')
            ->where('booking_status', 'Confirmed')
            ->latest()
            ->paginate(20);
        if($trips){
            return response()->json(['success'=> true, 'data'=> $trips]);
        }
        return response()->json(['success'=> false, 'message'=> 'Failed To Load Trip Data.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $trip = Booking::where('driver_id', auth()->id())->find($id);
        if($trip){
            return response()->json(['success'=> true, 'data'=> $trip]);
        }
        return response()->json(['success'=> false, 'message'=> 'Failed To Load Trip Data.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
