<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\BookingStatus;
use App\Models\DriverRating;
use App\Repository\BookingStatusRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{
    private $bookingStatus;

    public function __construct(BookingStatusRepository $bookingStatus)
    {
        $this->bookingStatus = $bookingStatus;
    }

    public function index()
    {
        $bookingStatus = $this->bookingStatus;
        $records = Booking::with(['upazilaBoundaryFrom', 'upazilaBoundaryTo'])
            ->where('customer_id', Auth::id())->latest()->paginate();
        return view('user.booking.index', compact('records', 'bookingStatus'));
    }

    public function updateStatus(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'action' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()]);
        }
        $dataToUpdate = $validator->getData();

        $booking = Booking::find($id);

        if ($request->ratingData) {
            $this->addReview($request->ratingData, $id);
        }
        $updated = $booking->update(['booking_status' => $dataToUpdate['action']]);
        if ($updated) {
            Session::flash('message', 'Booking Status Updated Successfully!');
            return response()->json(['success' => true, 'data' => $booking]);
        } else {
            return response()->json(['success' => false, 'error' => 'Whoops! Failed to update Booking Status']);
        }
    }

    private function addReview(array $values, $id)
    {
        $validator = Validator::make($values, [
            'overall_rating' => 'required',
            'car_condition' => 'required',
            'driver_behavior' => 'required',
            'driver_punctuality' => 'required',
            'comment' => 'nullable',
        ]);
        if ($validator->fails()) {
            return ['success' => false, 'errors' => $validator->errors()];
        }
        $dataToAdd = $validator->getData();
        $booking = Booking::find($id);
        if ($booking) {
            $dataToAdd = array_merge($dataToAdd, ['booking_id' => $id, 'driver_id' => $booking->driver_id]);
            $rating = DriverRating::create($dataToAdd);
            return $rating;
        }
        return null;
    }

    public function BookingConfirmationDetails(Request $request)
    {
        $input = $request->all();
        $input['customer_id'] = Auth::user()->id;
        if ($input['pricing_type'] == 'bid') {
            $input['price'] = 0;
        }
        $input['booking_status'] = 'Pending for Payment';
        $result = Booking::create($input);

        $BookingInsertId = $result->id;
        $BookingStatus = BookingStatus::create([
            'booking_id' => $BookingInsertId,
            'booking_status' => 'Pending for Payment'
        ]);
        return response()->json(['success' => $BookingStatus], 200);
    }
}
