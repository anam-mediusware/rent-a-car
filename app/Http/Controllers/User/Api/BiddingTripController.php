<?php

namespace App\Http\Controllers\User\Api;

use App\Http\Controllers\Controller;
use App\Models\BiddingTrip;

class BiddingTripController extends Controller
{
    public function show($id)
    {
        $biddingTrip = BiddingTrip::with(['vehicleType', 'service', 'bids.driver'])->find($id);
        return response()->json(['success' => true, 'data' => $biddingTrip]);
    }
}
