<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Bid;

class BidController extends Controller
{
    public function show($id)
    {
        $bidInfo = Bid::with([
            'biddingTrip.vehicleType',
            'biddingTrip.service',
            'biddingTrip.upazilaBoundaryFrom',
            'biddingTrip.upazilaBoundaryTo',
            'driver'
        ])->findOrFail($id);
        return view('user.bid.show', compact('bidInfo'));
    }
}
