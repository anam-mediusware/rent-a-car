<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PaymentMessageController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function success()
    {
        $transaction_message = Session::get('transaction_message');
        if ($transaction_message) {
            Session::put('payment_message', $transaction_message);
        } else {
            $transaction_message = Session::get('payment_message');
        }
        return view('user.payment.success', compact('transaction_message'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function failed()
    {
        $transaction_error = Session::get('transaction_error');
        if ($transaction_error) {
            Session::put('payment_error', $transaction_error);
        } else {
            $transaction_error = Session::get('payment_error');
        }
        return view('user.payment.failed', compact('transaction_error'));
    }
}
