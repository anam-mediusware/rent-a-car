<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\BiddingTrip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BiddingTripController extends Controller
{
    public function index()
    {
        $records = BiddingTrip::with(['vehicleType', 'service', 'upazilaBoundaryFrom', 'upazilaBoundaryTo'])
            ->withCount('bids')
            ->whereCustomerId(Auth::id())
            ->where('status', 1)
            ->latest()
            ->paginate();
        return view('user.bidding-trip.index', compact('records'));
    }

    public function show($id)
    {
        $biddingTrip = BiddingTrip::with(['vehicleType', 'service', 'bids.driver.ratings'])
            ->whereCustomerId(Auth::id())
            ->where('status', 1)
            ->findOrFail($id);
        return view('user.bidding-trip.show', compact('biddingTrip'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'mobile' => 'required',
        ]);
        $validator->validate();

        $serviceBookingData = Session::get('serviceBookingData');
        $validatedData = array_merge($validator->getData(), $serviceBookingData);

        $dataToInsert = [
            'customer_id' => Auth::id(),
            'vehicle_type_id' => $validatedData['vehicle_type_id'],
            'service_quality_type' => $validatedData['service_quality_type'],
            'car_model_id' => $validatedData['car_model_id'],
            'service_category_id' => $validatedData['service_category_id'],
            'service_type_id' => $validatedData['service_type_id'],
            'pick_up_city' => $validatedData['pick_up_city'],
            'pick_up_location' => $validatedData['pick_up_location'],
            'pick_up_point' => $validatedData['pick_up_point'],
            'drop_off_city' => $validatedData['drop_off_city'],
            'drop_off_location' => $validatedData['drop_off_location'],
            'drop_off_point' => $validatedData['drop_off_point'],
            'pick_up_date_time' => formatDateTimeDBFull($validatedData['pick_up_date'] . ' ' .$validatedData['pick_up_time']),
            'drop_off_date_time' => formatDateTimeDBFull($validatedData['drop_off_date'] . ' ' .$validatedData['drop_off_time']),
        ];
        BiddingTrip::create($dataToInsert);

        $request->session()->flash("message", "Bidding trip submitted successfully!");
        return redirect()->route('user.bidding-trips.index');
    }
}
