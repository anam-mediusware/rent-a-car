<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\ServiceSetting;
use App\Models\UpazilaBoundary;
use App\Models\VehicleType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $validator = $this->serviceFilterValidator($request);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()], 200);
        }

        $starting_point = $request->starting_point;
        if ($starting_point) {
            $starting_point_geo = json_decode($request->starting_point_geo);
            $pick_up_location = $this->getUpazilaId($starting_point_geo->lat, $starting_point_geo->lon);
        }

        if ($request->ending_point) {
            $ending_point_geo = json_decode($request->ending_point_geo);
            $destination_point = $this->getUpazilaId($ending_point_geo->lat, $ending_point_geo->lon);
        }

        $sql = ServiceSetting::select([
            'service_settings.id',
            'service_settings.price',
            'service_settings.service_quality_type',
            'vehicle_types.vehicle_type_english',
            'services.service_name',
        ])
            ->with(['additionalPricings'])
            ->join('vehicle_types', 'vehicle_types.id', '=', 'service_settings.vehicle_type_id')
            ->join('services', 'services.id', '=', 'service_settings.service_id')
            ->where('service_settings.pricing_type', '=', 'fixed');

        if ($request->starting_point) {
            $sql->where('service_settings.starting_point', $pick_up_location);
        }
        if ($request->vehicle_type) {
            $sql->where('service_settings.vehicle_type_id', $request->vehicle_type);
        }
        if ($request->class) {
            $sql->where('service_settings.service_quality_type', $request->class);
        }
        if ($request->service_type_id) {
            $sql->where('service_settings.service_id', $request->service_type_id);
        }
        if (!$request->service_category_id && $request->ending_point) {
            $sql->where('service_settings.destination_point', $destination_point);
        }
//        if ($request->airport_city_id) {
//            $sql->where('service_settings.airport_id', $request->airport_city_id);
//        }
        $result = $sql->first();

        $SessionArray = $this->makeSessionArray($request, $result, $pick_up_location, $destination_point);

        if ($result) {
            $totalPrice = $result->price;
            foreach ($result->additionalPricings as $additional) {
                $totalPrice += $additional->price;
            }
            $SessionArray['service_name'] = $result->service_name;
            $SessionArray['vehicle_name'] = $result->vehicle_type_english;
            $SessionArray['price'] = $totalPrice;
            Session::put('serviceBookingData', $SessionArray);
            return response()->json(['success' => true, 'data' => $result], 200);
        } else {
            $SessionArray['vehicle_name'] = VehicleType::find($request->vehicle_type)->vehicle_type_english;
            Session::put('serviceBookingData', $SessionArray);
            return response(['success' => false, 'data' => $result], 200);
        }
    }

    public function makeSessionArray($request, $result, $pick_up_location, $destination_point)
    {
        return [
            'service_setting' => $result,
            'service_type_id' => $request->service_type_id,
            'service_name' => '',
            'service_category_id' => $request->service_category_id,
            'vehicle_type_id' => $request->vehicle_type,
            'service_quality_type' => $request->class,
            'car_model_id' => $request->car_model_id,
            'vehicle_name' => '',
            'pick_up_city' => $pick_up_location ?? null,
            'pick_up_location' => $request->starting_point,
            'pick_up_point' => $request->starting_point_geo ?? null,
            'drop_off_city' => $destination_point ?? null,
            'drop_off_location' => $request->ending_point,
            'drop_off_point' => $request->ending_point_geo ?? null,
            'airport_id' => $request->airport_city_id ?? null,
            'pick_up_date' => Carbon::parse($request->pick_up_date)->format('Y-m-d'),
            'drop_off_date' => Carbon::parse($request->drop_off_date)->format('Y-m-d'),
            'pick_up_time' => Carbon::parse($request->pick_up_time)->format('h:i A'),
            'drop_off_time' => Carbon::parse($request->drop_off_time)->format('h:i A'),
            'price' => 0,
        ];
    }

    /**
     * Validate Service Filter Request
     *
     * @param Request $request
     * @return \Illuminate\Validation\Validator
     */
    private function serviceFilterValidator(Request $request)
    {
        $input = $request->all();
        $input['starting_point_geo'] = isJsonEmpty($input['starting_point_geo']) ? null : $input['starting_point_geo'];
        $input['ending_point_geo'] = isJsonEmpty($input['ending_point_geo']) ? null : $input['ending_point_geo'];
        $rules = [
            'vehicle_type' => 'required',
            'car_model_id' => 'required',
            'service_type_id' => 'required',
            'starting_point' => 'required',
            'starting_point_geo' => 'required',
            'ending_point' => 'required',
            'ending_point_geo' => 'required',
        ];
        $messages = [
            'vehicle_type.required' => 'The vehicle type field is required.',
            'car_model_id.required' => 'The car model field is required.',
            'service_type_id.required' => 'The service type field is required.',
            'starting_point.required' => 'The PICK-UP LOCATION field is required.',
            'starting_point_geo.required' => 'The PICK-UP LOCATION is invalid.',
            'ending_point.required' => 'The DROP-OFF LOCATION field is required.',
            'ending_point_geo.required' => 'The DROP-OFF LOCATION is invalid.',
        ];
        return Validator::make($input, $rules, $messages);
    }

    public function getServiceCategoryList()
    {
        $ServicesCategories = ServiceCategory::all();
        return response([
            'serviceCategories' => $ServicesCategories
        ], 200);
    }

    public function getServicesList($id)
    {
        $Services = Service::where('category_id', $id)->get();
        return response([
            'Services' => $Services
        ], 200);
    }

    public function checkAuthUserForBooking($value)
    {
        if (!Auth::guest()) {
            return Redirect::route('booking.details', ['booking_type' => $value]);
        } else {
            return Redirect::guest('login');
        }
    }

    public function bookingConfirmation($booking_type)
    {
        $data = Session::get('serviceBookingData');
        $vehicle_type = VehicleType::where(['vehicle_type_english' => $data['vehicle_name']])->first();
        $pick_up_location = UpazilaBoundary::find($data['pick_up_location']);
        $drop_off_location = UpazilaBoundary::find($data['drop_off_location']);
        return view('user.booking', compact('data', 'booking_type', 'vehicle_type', 'pick_up_location', 'drop_off_location'));
    }

    public function getUpazilaId($lat, $lon)
    {
        $data = UpazilaBoundary::select(['id'])
            ->whereRaw('ST_CONTAINS(shape, POINT(' . $lon . ', ' . $lat . '))')
            ->first();
        return $data ? $data->id : null;
    }
}
