<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountsController extends Controller
{
    public function index()
    {
        return redirect('/account/profile');
    }

    public function myProfile()
    {
        $id = Auth::user()->id;
        $data = User::findOrFail($id);
        return response()->json(['profile' => $data]);
    }

    public function profile()
    {
        return view('user.profile.index');
    }

    public function update_profile(Request $request)
    {
        $input = $request->all();
        $data = array(
            'name' => $input['name'],
            'address' => $input['address'],
            'mobile' => $input['mobile'],
        );
        $res = User::where('id', Auth::user()->id)->update($data);
        return response()->json(['status' => $res]);
    }
}
