<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Career;
use App\Models\Cars;
use App\Models\ContactMail;
use App\Models\ContactType;
use App\Models\Management;
use App\Models\News;
use App\Models\Policy;
use App\Models\Seo;
use App\Models\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;

class PagesController extends Controller
{
    public function __construct(Request $request)
    {
        $routeName = $request->route()->getName();
        $commonData = Seo::where('route_name', $routeName)->first();
        if (empty($commonData)) {
            $commonData = new Seo([
                'route_title' => config('app.name'),
                'route_keyword' => '',
                'route_description' => '',
            ]);
        }

        view()->share('commonData', $commonData);
    }

    //Custom 404 error page
    public function error()
    {
        return view('web.pages.error', compact('slides', 'bookServices', 'timeOption'));
    }

    public function index()
    {
        $slides = Slide::all();
        $data = Policy::where('policy_type', 'Home')->first();

        return view('web.home.index', compact('slides', 'data'));
    }

    public function aboutUs()
    {
        $data = Policy::where('policy_type', 'About Us')->first();

        return view('web.pages.about', compact('data'));
    }

    public function services()
    {
        return view('web.pages.services');
    }

    public function fleet(Request $request)
    {
        $records = Cars::where('status', 1)->get();

        $data = Policy::where('policy_type', 'Fleet')->first();

        return view('web.pages.fleet', compact('records', 'data'));
    }

    public function management()
    {
        $records = Management::where('status', 1)->get();
        return view('web.pages.management', compact('records'));
    }

    public function careers()
    {
        $records = Career::where('status', 1)->get();

        $data = Policy::where('policy_type', 'Careers')->first();

        return view('web.pages.careers', compact('records', 'data'));
    }

    public function news($slug = null)
    {

        if ($slug) {
            $news = 1;
            $data = News::where('news_slug', $slug)->first();
            return view('web.pages.news', compact('news', 'data'));
        } else {
            $records = News::where('status', 1)->get();

            $data = Policy::where('policy_type', 'News')->first();

            return view('web.pages.news', compact('records', 'data'));
        }
    }

    public function contactUs()
    {
        $typeData = ContactType::where('status', 1)->get();
        return view('web.pages.contact', compact('typeData'));
    }

    public function contactSendMail(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'support_type' => 'required',
            'message' => 'required',
        ]);

        $input = $request->all();
        $insert = ContactMail::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'phone_no' => $input['phone_no'],
            'support_type' => $input['support_type'],
            'message' => $input['message'],
        ]);

        Mail::send('mail.contact', ['data' => (object)$input], function ($message) use ($input) {
            $message->subject('Email Support');
            $message->from('control@groundlink.com.bd', $input['name']);
            $message->to('contact@groundlink.com.bd', 'GroundLink Car Rentals');
            //$message->attach('C:\project\public\uploads\image.png');
        });

        if ($input['email'] != '') {
            // Visitor Mail...
            Mail::send('mail.contact-visitor', ['data' => (object)$input], function ($message) use ($input) {
                $message->subject('Email Support');
                $message->from('contact@groundlink.com.bd', 'GroundLink Car Rentals');
                $message->to($input['email'], $input['name']);
            });
        }

        $request->session()->flash('message', 'Your query has been received.
        Our customer support team will get back to you.
        Thank you!');
        return redirect('contact-us');
    }

    public function cancellationPolicy()
    {
        $data = Policy::where('policy_type', 'Cancellation Policy')->first();
        return view('web.pages.cancellation-policy', compact('data'));
    }

    public function waitTimePolicy()
    {
        $data = Policy::where('policy_type', 'Wait Time Policy')->first();
        return view('web.pages.wait-time-policy', compact('data'));
    }

    public function privacyPolicy()
    {
        $data = Policy::where('policy_type', 'Privacy Policy')->first();
        return view('web.pages.privacy-policy', compact('data'));
    }

    public function refundPolicy()
    {
        $data = Policy::where('policy_type', 'Refund And Return Policy')->first();
        return view('web.pages.refund-policy', compact('data'));
    }

    public function termsConditions()
    {
        $data = Policy::where('policy_type', 'Terms And Conditions')->first();
        return view('web.pages.terms-conditions', compact('data'));
    }

    public function faq()
    {
        $data = Policy::where('policy_type', 'FAQ')->first();
        return view('web.pages.faq', compact('data'));
    }

    public function sitemap()
    {
        return view('web.pages.sitemap');
    }

    public function partnerWithUs()
    {
        $disArray = district();

        return view('web.pages.partner-with-us', compact('disArray'));
    }

    public function partnerMade(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'contact' => 'required',
            'district' => 'required',
            'email' => 'required|email|max:100|unique:partners,email',
            'password' => 'required|max:20|min:6',
            'confirm_password' => 'required|same:password',
        ]);

        $input = $request->all();
        $insert = Partners::create([
            'name' => $input['name'],
            'company_name' => $input['company_name'],
            'contact' => $input['contact'],
            'address' => $input['address'],
            'district' => $input['district'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        if ($input['email'] != '') {
            // Partner Mail...
            Mail::send('mail.partner', ['data' => (object)$input], function ($message) use ($input) {
                $message->subject('Service Partner Registration Complete');
                $message->from('contact@groundlink.com.bd', 'GroundLink Car Rentals');
                $message->to($input['email'], $input['name']);
            });
        }

        // Admin Mail...
        Mail::send('mail.partner-admin', ['data' => (object)$input], function ($message) use ($input) {
            $message->subject('New Partner Registered!');
            $message->from('control@groundlink.com.bd', $input['name']);
            $message->to('contact@groundlink.com.bd', 'GroundLink Car Rentals');
        });

        $request->session()->flash('message', 'Your data has been saved. Thank you for becoming our service partner. Our team will contact with you soon.');
        return redirect('partner-with-us');
    }
}
