<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\UpazilaBoundary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UpazilaBoundaryController extends Controller
{
    public function index(Request $request)
    {
        $lat = null;
        $lon = null;
        if ($request->lat) {
            $lat = $request->lat;
        }
        if ($request->lon) {
            $lon = $request->lon;
        }
        $data = null;
        if ($lat && $lon) {
            $data = UpazilaBoundary::select(['Upaz_name as upazila_name', 'Divi_name as division_name', 'Dist_name as district_name'])
                ->whereRaw('ST_CONTAINS(shape, POINT(' . $lon . ', ' . $lat . '))')
                ->first();
        }
        if ($data) {
            return response()->json(['success' => true, 'data' => $data]);
        }
        return response()->json(['success' => false, 'error' => 'Data Not Found']);
    }

    public function show($id)
    {
        $data = UpazilaBoundary::select(['id', 'Upaz_name as upazila_name', 'Divi_name as division_name', 'Dist_name as district_name'])
            ->find($id);
        if ($data) {
            return response()->json(['success' => true, 'data' => $data]);
        }
        return response()->json(['success' => false, 'error' => 'Data Not Found']);
    }

    public function getUpazilaNameSelect2(Request $request)
    {
        $queryString = strtolower($request->input('q'));
        return UpazilaBoundary::select(['id', DB::raw("CONCAT(Upaz_name, ', ', Dist_name) as upazila_name")])
            ->when($queryString, function ($query, $queryString) {
                return $query->where(DB::raw('LOWER(Upaz_name)'), 'like', '%' . $queryString . '%')
                    ->orWhere(DB::raw('LOWER(Dist_name)'), 'like', '%' . $queryString . '%');
            })
            ->orderBy(DB::raw("LOCATE('$queryString', LOWER('Upaz_name'))"))
            ->paginate(10);
    }

    public function getUpazilasByUpazilaBoundaryIds(Request $request)
    {
        $upazilaIds = $request->input('upazila_ids');
        return UpazilaBoundary::select(['id', DB::raw("CONCAT(Upaz_name, ', ', Dist_name) as upazila_name")])
            ->when($upazilaIds, function ($query, $upazilaIds) {
                return $query->whereIn('id', $upazilaIds);
            })->get();
    }

    public function isInsideDistrict(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'district' => 'required',
            'lat' => 'required',
            'lon' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()], 200);
        }
        $validatedData = $validator->getData();
        $data = UpazilaBoundary::whereRaw('ST_CONTAINS(shape, POINT(' . $validatedData['lon'] . ', ' . $validatedData['lat'] . '))')
            ->where('Dist_ID', $validatedData['district'])
            ->first();
        if ($data) {
            return response()->json(['success' => true, 'data' => $data]);
        }
        return response()->json(['success' => false, 'error' => 'Out of District']);
    }
}
