<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CarModel;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function getCarByVehicleType(Request $request)
    {
        $data = CarModel::where('vehicle_type_id', $request->vehicle_type_id)
            ->whereIn('id', function ($q) use ($request) {
                $q->select('car_model_id')
                    ->from('cars');
                if ($request->service_quality_type == '1') {
                    $q->where('year', '<', 2015);
                } else {
                    $q->where('year', '>=', 2015);
                }
            })
            ->get();
        if ($data) {
            return response()->json(['success' => true, 'data' => $data]);
        }
        return response()->json(['success' => false, 'error' => 'Data Not Found']);
    }
}
