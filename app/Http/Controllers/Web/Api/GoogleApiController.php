<?php

namespace App\Http\Controllers\Web\Api;

use App\Http\Controllers\Controller;
use App\Services\GoogleAutocompleteService;
use Biscolab\GoogleMaps\Api\Places;
use Biscolab\GoogleMaps\Enum\GoogleMapsApiConfigFields;
use Illuminate\Http\Request;

class GoogleApiController extends Controller
{
    private $places;
    private $autocomplete;

    public function __construct()
    {
        $this->autocomplete = new GoogleAutocompleteService();
        $this->places = new Places([
            GoogleMapsApiConfigFields::KEY => env('GOOGLE_API_KEY')
        ]);
    }

    public function autocomplete(Request $request)
    {
        $data = $this->autocomplete->search($request->q)->toArray();
        return response()->json([
            'success' => true,
            'data' => $data['predictions'],
            'total' => count($data['predictions'])
        ]);
    }

    public function getPlaceDetails(Request $request)
    {
        $place_id = $request->place_id;
        $result = $this->autocomplete->details($place_id)->toArray();
        if ($result['status'] == 'OK') {
            $data = [
                'lat' => $result['result']['geometry']['location']['lat'],
                'lon' => $result['result']['geometry']['location']['lng']
            ];
            return response()->json([
                'success' => true,
                'data' => $data,
                'status' => $result['status'],
            ]);
        } else {
            return response()->json([
                'success' => false,
                'data' => [],
                'status' => $result['status'],
            ]);
        }
    }

    public function getReversePlaceDetails(Request $request)
    {
        $lat = $request->lat;
        $lng = $request->lng;
        $result = $this->autocomplete->reverse($lat, $lng)->toArray();
        if ($result['status'] == 'OK') {
            $data=$result['results'][0];
            return response()->json([
                'success' => true,
                'data' => $data,
                'status' => $result['status'],
            ]);
        } else {
            return response()->json([
                'success' => false,
                'data' => [],
                'status' => $result['status'],
            ]);
        }
    }
}
