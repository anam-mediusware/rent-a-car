<?php

namespace App\Http\Controllers\CommonApi;

use App\Http\Controllers\Controller;
use App\Models\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Service::all();
        if ($data) {
            return response(['success' => true, 'data' => $data], 200);
        }
        return response(['success' => false, 'error' => 'Service Not Found'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Service::find($id);
        if ($data) {
            return response(['success' => true, 'data' => $data], 200);
        }
        return response(['success' => false, 'error' => 'Service Not Found'], 200);
    }

    /**
     * Display by specified Service Category.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getByServiceCategory($id)
    {
        $data = Service::where('category_id', $id)->get();
        if ($data) {
            return response(['success' => true, 'data' => $data], 200);
        }
        return response(['success' => false, 'error' => 'Service Not Found'], 200);
    }
}
