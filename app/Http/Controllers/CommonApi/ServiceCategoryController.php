<?php

namespace App\Http\Controllers\CommonApi;

use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;

class ServiceCategoryController extends Controller
{
    private $controllerName;

    public function __construct() {
        $this->controllerName = 'Service Category';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ServiceCategory::all();
        if ($data) {
            return response(['success' => true, 'data' => $data], 200);
        }
        return response(['success' => false, 'error' => $this->controllerName . ' Not Found'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ServiceCategory::find($id);
        if ($data) {
            return response(['success' => true, 'data' => $data], 200);
        }
        return response(['success' => false, 'error' => $this->controllerName . ' Not Found'], 200);
    }
}
