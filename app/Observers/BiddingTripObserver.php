<?php

namespace App\Observers;

use App\Models\BiddingTrip;
use App\Models\Driver;

class BiddingTripObserver
{
    /**
     * Handle the bidding trip "created" event.
     *
     * @param  \App\Models\BiddingTrip  $biddingTrip
     * @return void
     */
    public function created(BiddingTrip $biddingTrip)
    {
        $trip = $biddingTrip->with(['upazilaBoundaryFrom', 'upazilaBoundaryTo'])->first();
        $drivers = Driver::select(['id', 'city_id'])
            ->whereHas('DrivingCity', function ($city) use ($trip) {
                $city->where('Dist_ID', $trip->upazilaBoundaryFrom->Dist_ID);
            })
            ->whereHas('cars', function ($car) use ($trip) {
                $cmp = $trip->service_quality_type == 1 ? '<' : '>=';
                $car->where([
                    'car_model_id' => $trip->car_model_id,
                    'vehicle_type' => $trip->vehicle_type_id,
                ])->where('year', $cmp, 2015);
            })
            ->get();
        \App\Events\NewBidEvent::broadcast($trip, $drivers->pluck('id'));
    }

    /**
     * Handle the bidding trip "updated" event.
     *
     * @param  \App\Models\BiddingTrip  $biddingTrip
     * @return void
     */
    public function updated(BiddingTrip $biddingTrip)
    {
        //
    }

    /**
     * Handle the bidding trip "deleted" event.
     *
     * @param  \App\Models\BiddingTrip  $biddingTrip
     * @return void
     */
    public function deleted(BiddingTrip $biddingTrip)
    {
        //
    }

    /**
     * Handle the bidding trip "restored" event.
     *
     * @param  \App\Models\BiddingTrip  $biddingTrip
     * @return void
     */
    public function restored(BiddingTrip $biddingTrip)
    {
        //
    }

    /**
     * Handle the bidding trip "force deleted" event.
     *
     * @param  \App\Models\BiddingTrip  $biddingTrip
     * @return void
     */
    public function forceDeleted(BiddingTrip $biddingTrip)
    {
        //
    }
}
