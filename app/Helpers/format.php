<?php

if ( ! function_exists('carbonParse')) {
    function carbonParse($stringToParse)
    {
        $sanitizedString = str_replace('/', '-', $stringToParse);
        return \Carbon\Carbon::parse($sanitizedString);
    }
}

if ( ! function_exists('formatDate')) {
    function formatDate($stringToParse)
    {
        return \Carbon\Carbon::parse($stringToParse)->format('d-m-Y');
    }
}

if ( ! function_exists('formatDateTime')) {
    function formatDateTime($stringToParse)
    {
        return \Carbon\Carbon::parse($stringToParse)->format('d-m-Y h:i A');
    }
}

if ( ! function_exists('formatDatePickerFull')) {
    function formatDatePickerFull($stringToParse)
    {
        $cleanString = str_replace('/', '-', $stringToParse);
        return \Carbon\Carbon::parse($cleanString)->format('d/m/Y H:i');
    }
}

if ( ! function_exists('formatDateTimeFull')) {
    function formatDateTimeFull($stringToParse)
    {
        $cleanString = str_replace('/', '-', $stringToParse);
        return \Carbon\Carbon::parse($cleanString)->format('d-m-Y H:i:s');
    }
}

if ( ! function_exists('formatDateTimeDBFull')) {
    function formatDateTimeDBFull($stringToParse)
    {
        $cleanString = str_replace('/', '-', $stringToParse);
        return \Carbon\Carbon::parse($cleanString)->format('Y-m-d H:i:s');
    }
}

if ( ! function_exists('makeLabel')) {
    function makeLabel($stringToParse)
    {
        return Str::title(str_replace('_', ' ', $stringToParse));
    }
}

if ( ! function_exists('nearest')) {
    function nearest($number, $round = 50)
    {
        return ceil($number / $round) * $round;
    }
}

if ( ! function_exists('currency')) {
    function currency(float $number , int $decimals = 0 , string $dec_point = "." , string $thousands_sep = "")
    {
        $symbol = '&#2547;';
        return number_format($number, $decimals, $dec_point, $thousands_sep) .  " $symbol";
    }
}

if ( ! function_exists('isJson')) {
    function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if ( ! function_exists('isJsonEmpty')) {
    function isJsonEmpty($string)
    {
        $data = json_decode($string, true);
        return !((json_last_error() == JSON_ERROR_NONE) && !empty($data) && is_array($data));
    }
}

if ( ! function_exists('jsonString')) {
    function jsonString($input)
    {
        if (is_object($input)) {
            return json_encode($input);
        } elseif (is_array($input)) {
            return json_encode($input);
        }
        return $input;
    }
}
