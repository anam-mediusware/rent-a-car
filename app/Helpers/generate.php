<?php

if (!function_exists('bladeCompile')) {
    function bladeCompile($value, array $args = array())
    {
        $args['__env'] = app(\Illuminate\View\Factory::class);
        $generated = \Blade::compileString($value);

        ob_start() and extract($args, EXTR_SKIP);
        try {
            // We'll include the view contents for parsing within a catcher
            // so we can avoid any WSOD errors. If an exception occurs we
            // will throw it out to the exception handler.
            eval('?>' . $generated);
        } catch (\Exception $e) {
            // If we caught an exception, we'll silently flush the output
            // buffer so that no partially rendered views get thrown out
            // to the client and confuse the user with junk.
            ob_get_clean();
            throw $e;
        }

        $content = ob_get_clean();

        return $content;
    }
}

if (!function_exists('getColumnListing')) {
    function getColumnListing($model)
    {
        $model = new $model;
        $tableName = $model->getTable();
        return $model->getConnection()->getSchemaBuilder()->getColumnListing($tableName);
    }
}

if (!function_exists('getFillableArray')) {
    function getFillableArray($model)
    {
        $model = new $model;
        $tableName = $model->getTable();
        $columns = $model->getConnection()->getSchemaBuilder()->getColumnListing($tableName);
        $fillableColumns = array_diff($columns, ['id', 'created_at', 'updated_at', 'deleted_at']);
        $str = "";
        foreach ($fillableColumns as $column) {
            $str .= "'" . $column . "', ";
        }
        return $str;
    }
}

if (!function_exists('arrayToString')) {
    function arrayToString(array $array)
    {
        return str_replace('"', '\'', json_encode($array));
    }
}

if (!function_exists('genDetailsTr')) {
    function genDetailsTr($data)
    {
        $str = "<table>
                ";
        foreach ($data->toArray() as $key => $value) {
            $str .= "<tr>
            <th>" . makeLabel($key) . "</th>
            <th>:</th>
            <td>{{ \$data->$key }}</td>
            </tr>
            ";
        }

        return $str . "</table>";
    }
}
