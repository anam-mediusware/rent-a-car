<?php

namespace App\Providers;

use App\Models\BiddingTrip;
use App\Observers\BiddingTripObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        if (env('BROADCAST_DRIVER') == 'pusher') {
            BiddingTrip::observe(BiddingTripObserver::class);
        }
        //
    }
}
