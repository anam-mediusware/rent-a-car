<?php

namespace App\Providers;

use App\Models\WebSetting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $web_settings = new WebSetting();
        if (Schema::hasTable($web_settings->getTable())) {
            $data = WebSetting::first();
            if ($data) {
                $web_settings = $data;
            }
        }
        View::share('web_settings', $web_settings);
    }
}
