<?php


namespace App\Services;


class SendSMSService
{
    private $number;
    private $message;
    private $smsService;

    public function __construct($number, $message)
    {
        $this->number = $number;
        $this->message = $message;
        $this->smsService = new SslSmsService();
    }

    /**
     * Fire API using Message
     * @param string $message
     */
    private function fireApi($message)
    {
        \Log::info("Send Message: $message");
        return $this->smsService->send([$this->number], $message);
    }

    /**
     * Send SMS using Plain Text Message
     */
    public function send()
    {
        return $this->fireApi($this->message);
    }

    /**
     * Send SMS using SimpleMessage Instance
     */
    public function sendSimpleMessage()
    {
        $txt = '';
        foreach ($this->message->introLines as $line) {
            $txt .= $line . "\n";
        }

        return $this->fireApi($txt);
    }
}
