<?php


namespace App\Services;

use App\Events\BidAcceptedEvent;
use App\Models\AdditionalPricing;
use App\Models\Bid;
use App\Models\BiddingTrip;
use App\Models\Booking;
use App\Models\BookingAdditionalPricing;

class BookingService
{
    public function makeBooking(array $data)
    {
        $dataToInsert = [
            "pricing_type" => $data["pricing_type"],
            "customer_id" => $data["customer_id"],
            "service_setting_id" => $data["pricing_type"] == 'fixed' ? $data["service_setting"]["id"] : null,
            "bid_id" => $data["pricing_type"] == 'bid' ? $data["bid_id"] : null,
            "driver_id" => $this->getDriverId($data),
            "service_type_id" => $data["service_type_id"],
            "vehicle_type_id" => $data["vehicle_type_id"],
            'service_quality_type' => $data['service_quality_type'],
            'car_model_id' => $data['car_model_id'],
            "pick_up_city" => $data["pick_up_city"],
            "pick_up_location" => $data["pick_up_location"],
            "pick_up_point" => is_object($data["pick_up_point"]) ? json_encode($data["pick_up_point"]) : $data["pick_up_point"],
            "drop_off_city" => $data["drop_off_city"],
            "drop_off_location" => $data["drop_off_location"],
            "drop_off_point" => is_object($data["drop_off_point"]) ? json_encode($data["drop_off_point"]) : $data["drop_off_point"],
            "airport_id" => $data["airport_id"],
            "price" => $data["price"],
            "service_category_id" => $data["service_category_id"],
            "pick_up_date_time" => formatDateTimeDBFull($data["pick_up_date_time"]),
            "drop_off_date_time" => formatDateTimeDBFull($data["drop_off_date_time"]),
            "transaction_id" => $data["tran_id"],
            "charged_amount" => $data["total_amount"],
        ];
        $dataToInsert["booking_status"] = ($data["pricing_type"] == 'fixed') ? 'Pending' : 'Confirmed';
        $booking = Booking::updateOrCreate([
            'transaction_id' => $data['tran_id']
        ], $dataToInsert);

        if ($data["pricing_type"] == 'fixed') {
            $addtionalPricings = AdditionalPricing::where('service_setting_id', $data["service_setting"]["id"])->get();
            $pr = [];
            foreach ($addtionalPricings as $pricing) {
                $pr[] = new BookingAdditionalPricing([
                    'service_pricing_id' => $pricing['service_pricing_id'],
                    'service_setting_id' => $pricing['service_setting_id'],
                    'price_name' => $pricing['price_name'],
                    'price' => $pricing['price'],
                    'price_type' => $pricing['price_type'],
                ]);
            }
            $booking->additionalPricings()->saveMany($pr);
        } elseif ($booking && $data["pricing_type"] == 'bid') {
            BiddingTrip::find($data["id"])->update(['status' => 0]);
            if (env('BROADCAST_DRIVER') == 'pusher') {
                BidAcceptedEvent::dispatch('bid-accepted', $booking->driver_id, $booking->with(['upazilaBoundaryFrom', 'upazilaBoundaryTo'])->first());
            }
        }
        return $booking;
    }

    private function getDriverId($data)
    {
        if ($data["pricing_type"] == 'bid') {
            $bid = Bid::find($data['bid_id']);
            return $bid ? $bid->driver_id : null;
        }
        return null;
    }
}
