<?php


namespace App\Services;


use GuzzleHttp\Client;

class GoogleAutocompleteService
{
    private $baseurl = 'https://maps.googleapis.com/maps/api/';
    private $client;
    private $result;
    private $key;

    public function __construct()
    {
        $this->key = env('GOOGLE_API_KEY');
        $this->client = new Client([
            'base_uri' => $this->baseurl
        ]);
    }

    public function search($query)
    {
        $response = $this->client->get('place/autocomplete/json', [
            'query' => [
                'key' => $this->key,
                'types' => '(cities)',
                'language' => 'en_US',
                'components' => 'country:bd',
                'input' => $query,
            ],
        ]);
        $this->result = $response->getBody()->getContents();
        return $this;
    }

    public function details($place_id)
    {
        $response = $this->client->get('place/details/json', [
            'query' => [
                'key' => $this->key,
                'place_id' => $place_id,
            ],
        ]);
        $this->result = $response->getBody()->getContents();
        return $this;
    }

    public function reverse($lat, $lng)
    {
        $response = $this->client->get('geocode/json', [
            'query' => [
                'key' => $this->key,
                'language' => 'en_BD',
                'latlng' => $lat . ',' . $lng,
            ],
        ]);
        $this->result = $response->getBody()->getContents();
        return $this;
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return json_decode($this->result, true);
    }
}
