<?php

// SSL Wireless SMS Gateway Configuration

return [
    'user' => env('SSL_SMS_USER'),
    'pass' => env('SSL_SMS_PASS'),
    'sid' => env('SSL_SMS_SID'),
];
